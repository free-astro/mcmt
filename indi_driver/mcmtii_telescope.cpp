/* This is the INDI Telescope driver for the MCMTII motor unit for telescopes.
 * Developped by Vincent from free-astro.vinvin.tf
 * Documentation on the MCMTII, libmcmtii and this driver is here:
 * http://free-astro.vinvin.tf/index.php/MCMTII
 */

#include <sys/time.h>
#include <math.h>
#include <memory>

#define POLLMS 1000
#ifdef TESTING
#define DEFAULT_NAME "MCMTII Telescope TEST"
#else
#define DEFAULT_NAME "MCMTII Telescope"
#endif
#define UNINIT_RA_VALUE -1.0
#define UNINIT_DEC_VALUE -91.0

#include "mcmtii_telescope.h"
#include "indicom.h"
#include "mcmtii.h"

//const float SIDRATE	= 0.004178;	/* sidereal rate, degrees/s */
//const int   SLEW_RATE	= 1;  		/* slew rate, degrees/s */

std::auto_ptr<MCMTII_Telescope> mcmtii_Telescope(0);

/**************************************************************************************
 ** Initilize MCMTII_Telescope object
 ***************************************************************************************/
void ISInit()
{
	static int isInit=0;
	if (isInit)
		return;
	if (mcmtii_Telescope.get() == 0)
	{
		isInit = 1;
		mcmtii_Telescope.reset(new MCMTII_Telescope());
	}
}

/**************************************************************************************
 ** Return properties of device.
 ***************************************************************************************/
void ISGetProperties (const char *dev)
{
	ISInit();
	mcmtii_Telescope->ISGetProperties(dev);
}

/**************************************************************************************
 ** Process new switch from client
 ***************************************************************************************/
void ISNewSwitch (const char *dev, const char *name, ISState *states, char *names[], int n)
{
	ISInit();
	mcmtii_Telescope->ISNewSwitch(dev, name, states, names, n);
}

/**************************************************************************************
 ** Process new text from client
 ***************************************************************************************/
void ISNewText (const char *dev, const char *name, char *texts[], char *names[], int n)
{
	ISInit();
	mcmtii_Telescope->ISNewText(dev, name, texts, names, n);
}

/**************************************************************************************
 ** Process new number from client
 ***************************************************************************************/
void ISNewNumber (const char *dev, const char *name, double values[], char *names[], int n)
{
	ISInit();
	mcmtii_Telescope->ISNewNumber(dev, name, values, names, n);
}

/**************************************************************************************
 ** Process new blob from client
 ***************************************************************************************/
void ISNewBLOB (const char *dev, const char *name, int sizes[], int blobsizes[],
		char *blobs[], char *formats[], char *names[], int n)
{
	ISInit();
	mcmtii_Telescope->ISNewBLOB(dev, name, sizes, blobsizes, blobs, formats, names, n);
}

/**************************************************************************************
 ** Process snooped property from another driver
 ***************************************************************************************/
void ISSnoopDevice (XMLEle *root)
{
	INDI_UNUSED(root);
}

MCMTII_Telescope::MCMTII_Telescope()
{
	fd = -1;
	target_RA = UNINIT_RA_VALUE;
	target_DEC = UNINIT_DEC_VALUE;
}

bool MCMTII_Telescope::initProperties()
{
	double lat, lon;
	INDI::Telescope::initProperties();

	mcmtii_get_latitude_longitude(&lat, &lon);
	LocationN[0].value = lat;
	LocationN[1].value = lon;
	LocationNV.p = IP_RO;

	IUFillSwitch(SwAllowFlip, "ALLOW", "Allow flip", ISS_OFF);
	IUFillSwitch(SwIsFlipped, "STATUS", "Flip status", ISS_OFF);
	IUFillSwitchVector(&SwVectorAllow, SwAllowFlip, 1, getDeviceName(),
			"ALLOW_FLIP", "Allow flip", MOTION_TAB, IP_RW,
			ISR_NOFMANY, 2, IPS_OK);
	IUFillSwitchVector(&SwVectorStatus, SwIsFlipped, 1, getDeviceName(),
			"FLIP_STATUS", "Flip status", MOTION_TAB, IP_RO,
			ISR_NOFMANY, 2, IPS_IDLE);
	return true;
}

void MCMTII_Telescope::ISGetProperties (const char *dev)
{
	//IDLog("ISGetProperties()\n");
	INDI::Telescope::ISGetProperties(dev);

	if(isConnected()) {
		defineSwitch(&SwVectorAllow);
		defineSwitch(&SwVectorStatus);
	}
}

bool MCMTII_Telescope::updateProperties()
{
	//IDLog("updateProperties()\n");
	INDI::Telescope::updateProperties();

	if (isConnected()) {
		defineSwitch(&SwVectorAllow);
		defineSwitch(&SwVectorStatus);
	} else {
		deleteProperty(SwVectorAllow.name);
		deleteProperty(SwVectorStatus.name);
	}
	return true;
}

bool MCMTII_Telescope::ISNewSwitch (const char *dev, const char *name, ISState *states, char *names[], int n)
{
	//IDLog("Enter IsNewSwitch for %s\n", name);
	//for (int x=0; x<n; x++)
	//	IDLog("Switch %s %d\n", names[x], states[x]);

	if (!strcmp(dev, getDeviceName())) {
		if (!strcmp(name,"ALLOW_FLIP")) {
			IUUpdateSwitch(&SwVectorAllow, states, names, n);
			SwVectorAllow.s = IPS_OK;
			//IUResetSwitch(&SwVectorAllow);
			IDSetSwitch(&SwVectorAllow, NULL);
			return true;
		}
		//else if (!strcmp(name,"FLIP_STATUS")) { }
	}

	return INDI::Telescope::ISNewSwitch(dev,name,states,names,n);
}
/**************************************************************************************
 ** Client is asking us to establish connection to the device
 ***************************************************************************************/
bool MCMTII_Telescope::Connect()
{
	IDLog("Connect() to %s\n", PortT[0].text);
#ifdef TESTING
	TrackState = SCOPE_IDLE;
	fd = 1;
	SetTimer(POLLMS);
	return true;
#endif
	fd = mcmtii_serial_link_open(PortT[0].text);
	if (fd == -1) {
		IDMessage(getDeviceName(), "MCMTII Telescope failed to connect.");
		return false;
	}
	if (this->ReadScopeStatus() == false) {
		IDMessage(getDeviceName(), "MCMTII Telescope failed to communicate, disconnecting.");
		mcmtii_serial_link_close(fd);
		fd = -1;
		return false;
	}
	int retval = mcmtii_is_parked(fd);
	if (retval == MCMTII_RET_ERR) {
		IDMessage(getDeviceName(), "MCMTII Telescope failed to communicate, disconnecting.");
		mcmtii_serial_link_close(fd);
		fd = -1;
		return false;
	}
	if (retval)
		TrackState = SCOPE_PARKED;
	IDMessage(getDeviceName(), "MCMTII Telescope connected successfully!");

	// This timer calls Telescope::TimerHit(), which calls ReadScopeStatus and
	// rearms the timer to 1000 ms, so if TimerHit is not redefined here, our
	// POLLMS will be used only once (inditelescope.cpp)
	SetTimer(POLLMS);
	return true;
}

/**************************************************************************************
 ** Client is asking us to terminate connection to the device
 ***************************************************************************************/
bool MCMTII_Telescope::Disconnect()
{
	IDLog("Disconnect()\n");
	if (fd != -1) {
#ifndef TESTING
		mcmtii_stop_slewing(fd, MCMTII_AXIS_RA);
		mcmtii_stop_slewing(fd, MCMTII_AXIS_DEC);
		mcmtii_serial_link_close(fd);
#endif
		fd = -1;
		IDMessage(getDeviceName(), "MCMTII Telescope disconnected successfully.");
		return true;
	} else {
		IDMessage(getDeviceName(), "MCMTII Telescope was not connected.");
		return false;
	}
}

/**************************************************************************************
 ** INDI is asking us for our default device name
 ***************************************************************************************/
const char * MCMTII_Telescope::getDefaultName()
{
	return DEFAULT_NAME;
}

/**************************************************************************************
 ** Client is asking us to slew to a new position
 ***************************************************************************************/
bool MCMTII_Telescope::Goto(double ra, double dec)
{
	int retval;
	IDLog("Goto() RA=%g, DEC=%g\n", ra, dec);
	if (fd == -1) {
		IDMessage(getDeviceName(), "MCMTII Telescope is not connected.");
		return false;
	}
	// TrackState is of type enum TelescopeStatus (from inditelescope.h)
	// { SCOPE_IDLE, SCOPE_SLEWING, SCOPE_TRACKING, SCOPE_PARKING, SCOPE_PARKED };
	if (TrackState == SCOPE_SLEWING) {
		// I have no idea yet if it supports new values while slewing
		// so we'll say it does not for now.
		IDMessage(getDeviceName(), "Slewing already in progress, please wait");
		return false;
	}
	if (TrackState != SCOPE_IDLE && TrackState != SCOPE_TRACKING) {
		// is parking or parked
		IDMessage(getDeviceName(), "Slewing is not a possible operation right know");
		return false;
	}

	retval = mcmtii_slew_absolute(fd, ra, dec, (SwAllowFlip->s == ISS_ON));
	if (retval == MCMTII_RET_OK) {
		IDMessage(getDeviceName(), "Slewing to RA: %g - DEC: %g", ra, dec);
		target_RA = ra;
		target_DEC = dec;
#ifdef TESTING
		TrackState = SCOPE_IDLE;	// instantaneous slew
#else
		TrackState = SCOPE_SLEWING;
#endif
	} else if (retval == MCMTII_RET_UNINIT) {
		// if sync is not supported by the client, sync should be done here
		// but if it is, then it should have been done before the Goto and
		// this UNINIT case won't happen
		return this->Sync(ra, dec);
	} else if (retval == MCMTII_RET_ERR) {
		IDMessage(getDeviceName(), "ERROR: an unknown error occurred while "
				"trying to slew");
		return false;
	}
	return true;
}

/**************************************************************************************
 ** Client is asking us to abort our motion
 ***************************************************************************************/
bool MCMTII_Telescope::Abort()
{
	IDLog("Abort()\n");
	mcmtii_stop_slewing(fd, MCMTII_AXIS_RA);
	mcmtii_stop_slewing(fd, MCMTII_AXIS_DEC);
	target_RA = UNINIT_RA_VALUE; target_DEC = UNINIT_DEC_VALUE;
	TrackState = SCOPE_IDLE;
	IDMessage(getDeviceName(), "MCMTII Telescope slewing stopped.");
	// reseting properties and switches
	IUResetSwitch(&MovementWESP);
	MovementWESP.s = IPS_IDLE;
	IDSetSwitch(&MovementWESP, NULL);
	IUResetSwitch(&MovementNSSP);
	MovementNSSP.s = IPS_IDLE;
	IDSetSwitch(&MovementNSSP, NULL);
	return true;
}

/**************************************************************************************
 ** Client is asking us to report telescope status, and called by the timer set in Connect()
 ***************************************************************************************/
bool MCMTII_Telescope::ReadScopeStatus()
{
	int retval1, retval2;
	mcmtii_flip_status flip_status;
	if (fd == -1) {
		IDMessage(getDeviceName(), "MCMTII Telescope is not connected.");
		return false;
	}
	//IDLog("ReadScopeStatus()\n");

	// check status of the telescope first
	retval1 = mcmtii_device_ready(fd, MCMTII_AXIS_RA);
	retval2 = mcmtii_device_ready(fd, MCMTII_AXIS_DEC);
	if (retval1 == -1 || retval2 == -1) {
		IDMessage(getDeviceName(), "MCMTII Telescope has some communication problems.");
		return false;
	}
	if (retval1 && retval2)	{	// telescope is ready
		if (target_RA != UNINIT_RA_VALUE || target_DEC != UNINIT_DEC_VALUE) {
			IDMessage(getDeviceName(), "Telescope slew is complete.");
			target_RA = UNINIT_RA_VALUE; target_DEC = UNINIT_DEC_VALUE;
			// end of slew is also end of flip
			flip_status = mcmtii_get_flip_status();
			if (flip_status == MCMTII_FLIP_FLIPPED)
				SwIsFlipped[0].s = ISS_ON;
			else if (flip_status == MCMTII_FLIP_NONE)
				SwIsFlipped[0].s = ISS_OFF;
		}
		TrackState = SCOPE_IDLE;
	} else {
		TrackState = SCOPE_SLEWING;
		// if (target_RA == UNINIT_RA_VALUE && target_DEC == UNINIT_DEC_VALUE)
		// this is external slewing (hand controller)
	}

	// then get its current position
	if (mcmtii_conversion_is_init()) {
		double ra, dec;
		if (mcmtii_get_current_position(fd, &ra, &dec) == MCMTII_RET_OK) {
			IDLog("Current RA: %g - DEC: %g\n", ra, dec);
			NewRaDec(ra, dec);
		} else {
			IDMessage(getDeviceName(), "MCMTII Telescope has some problems, "
					"unable to get the current position.");
			return false;
		}
	}
	return true;
}

bool MCMTII_Telescope::MoveNS(TelescopeMotionNS dir) {
	int retval;
	IDLog("MoveNS() dir=%s\n", dir == MOTION_NORTH ? "north" : "south");
	if (fd == -1) {
		IDMessage(getDeviceName(), "MCMTII Telescope is not connected.");
		return false;
	}
	if (dir == MOTION_NORTH) {
		retval = mcmtii_move(fd, MCMTII_NORTH, MCMTII_SPEED_MED);
	} else {	// MOTION_SOUTH
		retval = mcmtii_move(fd, MCMTII_SOUTH, MCMTII_SPEED_MED);
	}
	if (retval == MCMTII_RET_OK) {
		TrackState = SCOPE_SLEWING;
		IDMessage(getDeviceName(), "Moving %s at medium speed",
				dir == MOTION_NORTH ? "north" : "south");
		return true;
	}
	/* code from inditelescope.cpp:MoveNS() */
	IUResetSwitch(&MovementNSSP);
	MovementNSSP.s = IPS_IDLE;
	IDSetSwitch(&MovementNSSP, NULL);
	return false;
}

bool MCMTII_Telescope::MoveWE(TelescopeMotionWE dir) {
	int retval;
	IDLog("MoveWE() dir=%s\n", dir == MOTION_WEST ? "west" : "east");
	if (fd == -1) {
		IDMessage(getDeviceName(), "MCMTII Telescope is not connected.");
		return false;
	}
	if (dir == MOTION_WEST) {
		retval = mcmtii_move(fd, MCMTII_WEST, MCMTII_SPEED_MED);
	} else {	// MOTION_EAST
		retval = mcmtii_move(fd, MCMTII_EAST, MCMTII_SPEED_MED);
	}
	if (retval == MCMTII_RET_OK) {
		TrackState = SCOPE_SLEWING;
		IDMessage(getDeviceName(), "Moving %s at medium speed",
				dir == MOTION_WEST ? "west" : "east");
		return true;
	}
	/* code from inditelescope.cpp:MoveWE() */
	IUResetSwitch(&MovementWESP);
	MovementWESP.s = IPS_IDLE;
	IDSetSwitch(&MovementWESP, NULL);
	return false;
}

bool MCMTII_Telescope::canPark() {
	// parking is not implemented yet	(this is default behaviour too)
	// we could still set the ParkMode flag to true in the MCMTII to make it not move.
	return false;
}

/* Sync is the method used to inform the telescope about its position (if I
 * understand right the following documentation of the Sync method:
 * "Set the telescope current RA and DEC coordinates to the supplied RA and DEC
 * coordinates."
 * http://www.indilib.org/api/classINDI_1_1Telescope.html
 * By default in inditelescope.cpp canSync returns false, we need to redefine
 * canSync and Sync below in order to use the Sync functionality */

bool MCMTII_Telescope::canSync() {
	return true;
}

bool MCMTII_Telescope::Sync(double ra, double dec) {
	IDLog("Sync() RA=%g, DEC=%g\n", ra, dec);
	if (mcmtii_set_current_position(fd, ra, dec) == MCMTII_RET_OK) {
		IDMessage(getDeviceName(), "Sync complete. This destination has been "
				"taken as current absolute position for future "
				"relative GoTo requests");
		NewRaDec(ra, dec);	// inform about position
		return true;
	} else {
		IDMessage(getDeviceName(), "ERROR while syncing. "
				"Relative GoTo is not initialized");
		return false;
	}
}

