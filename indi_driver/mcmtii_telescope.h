/* This is the INDI Telescope driver for the MCMTII motor unit for telescopes.
 * Developped by Vincent from free-astro.vinvin.tf
 * Documentation on the MCMTII, libmcmtii and this driver is here:
 * http://free-astro.vinvin.tf/index.php/MCMTII
 */

#ifndef _MCMTII_SCOPE_H 
#define _MCMTII_SCOPE_H 

#include "inditelescope.h"
//#include "indiguiderinterface.h"

class MCMTII_Telescope: public INDI::Telescope//, public INDI::GuiderInterface
{
	public:
		MCMTII_Telescope();

		// General device functions
		bool Connect();
		bool Disconnect();
		const char *getDefaultName();
		bool initProperties();
		void ISGetProperties (const char *dev);
		bool updateProperties();
		bool ISNewSwitch (const char *dev, const char *name, ISState *states, char *names[], int n);

	protected:
		// Telescope specific functions
		bool ReadScopeStatus();
		bool Goto(double, double);
		bool Abort();
		bool canPark();
		bool MoveNS(TelescopeMotionNS dir);
		bool MoveWE(TelescopeMotionWE dir);
		bool canSync();
		bool Sync(double, double);

	private:
		int fd;
		double target_RA, target_DEC;

		ISwitch SwAllowFlip[1];
		ISwitchVectorProperty SwVectorAllow;
		ISwitch SwIsFlipped[1];
		ISwitchVectorProperty SwVectorStatus;
};

#endif
