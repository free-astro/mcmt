unit FeuRouge;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TForm_FeuRouge = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Button_Renoncer: TButton;
    Button_Perseverer: TButton;
    Label5: TLabel;
    Image1: TImage;
    procedure Button_RenoncerClick(Sender: TObject);
    procedure Button_PersevererClick(Sender: TObject);
    procedure validationCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form_FeuRouge: TForm_FeuRouge;
  Daccord_Renoncer, Daccord_Perseverer : Boolean;
  
implementation

{$R *.dfm}


procedure TForm_FeuRouge.Button_RenoncerClick(Sender: TObject);
begin
  Daccord_Renoncer:=true
end;

procedure TForm_FeuRouge.Button_PersevererClick(Sender: TObject);
begin
  Daccord_Perseverer:=true
end;

procedure TForm_FeuRouge.validationCreate(Sender: TObject);
begin
Daccord_Renoncer:=false;
Daccord_Perseverer:=false;
end;


end.
