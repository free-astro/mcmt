unit Utils;

interface
{$R+}
uses
      SysUtils,
      Math,
      DateUtils,
      Forms,
      SyncObjs,
      ShlObj;

{Remet un angle dans l'intervalle 0 2Pi}
procedure Remet2Pi(Var Angle:Double);

{Calcul heure sid�rale locale}
Function CalculeTSL(DateTime:TdateTime;Longitude:Double):Double;

{Calcul l'angle horaire}
function AH(long,alpha :extended;GMT : integer):extended;

{Calcul vitesse relative de King}
Function getKingSpeedCorrection(AH,Delta,Latitude:Double):Double;

{Renvoie le string 'o' si la valeur en entr�e est vide; }
function TestString(Entree:string):string;

Function WriteBoolToReg(S:String;B:Boolean):Boolean;

Function  LocalAppDataPath : string;
Procedure WaitMainthread(procCaller:String);


Function Return_MCMT_StoreIniFilePath:String;


Procedure LogEnterProc    (ProcName:String);
Procedure LogEndProc      (Procname:String);
Procedure LogEnterProc_DLL(ProcName:String);
Procedure LogEndProc_DLL  (Procname:String);
Procedure LogProc         (Procname,message:String);


Var
 LogEnabled           : Boolean;
 FactorTime           : Double ;


IMPLEMENTATION

Uses registry,Windows;

Var

  SectionLog           : TCriticalSection;
  PreviousLogTimeStamp : Int64;
  LogFileName          : String;
  PreviousThreadID     : Cardinal;



procedure Remet2Pi(Var Angle:Double); // 0 to 2Pi
begin
Try
 While (Angle <  0   ) do Angle:=Angle+(2*Pi);
 While (Angle >= 2*Pi) do Angle:=Angle-(2*Pi);
Except
end;
end;


Function CalculeTSL(DateTime:TdateTime;Longitude:Double):Double;
 {La reference est 31 Dec 1899 � 12 h}
Var

  DateOr: Tdatetime;
  Teta  : Double   ;

Begin
  DateOr:=EncodeDate(1899,12,31)+0.5;
  Teta  :=(DateTime-DateOr)/36525.0 ;   {Teta en frac de siecles !}
  Teta  := 67125.836 + ((36625.0*86400.0)+184.542)*Teta + 0.0929*Sqr(Teta); {en sec}
  Teta  :=(Frac(Teta/86400.0)*86400.0)/3600.0; {en heures}
  Teta  := (Teta/12.0)*Pi - Longitude;
  While Teta <  0    do Teta:=Teta+(2*Pi);
  While Teta >= 2*Pi do Teta:=Teta-(2*Pi);
  CalculeTSL:=Teta;
end;

function AH(long,alpha :extended;GMT : integer):extended;    {Angle Horaire}
var
   temp:extended;
begin
   Temp:=CalculeTSL(Now+GMT/24,long)-alpha;
   if (temp>pi) then temp:=temp-2*pi;

   //Avec ce contr�le suppl�mentaire, on obtient  -pi < AH <= +pi
   if (temp<=-pi) then temp:=temp+2*pi;
   result:=temp;

end;


Function getKingSpeedCorrection(AH,Delta,Latitude:Double):Double;
Const Delta_n=0.000277; {n-1 pour T = 288 K et p = 1 bar ; source : Danjon }
var   sinus_h,Denominateur,Numerateur : double;

begin
  Try
  sinus_h           :=sin(Latitude)  * sin(Delta)+cos(Delta)*cos(Latitude)*cos(AH);
  Denominateur      := cos(Delta)    * sqr(sinus_h);
  Numerateur        := cos(Latitude) * ( cos(Latitude)*cos(Delta) +
                                         sin(Delta)*sin(Latitude)*cos(AH));
  Result:=1 - Delta_n*Numerateur/Denominateur;
  Except
  Result:=1;
  End;
end;


function TestString(Entree:string):string;
begin
 if Entree='' then TestString:='0' else TestString:=Entree;
end;

Function WriteBoolToReg(S:String;B:Boolean):Boolean;
Var  Reg:TRegistry;

Begin
  Result:=True;
  Reg   := TRegistry.Create;
  Try
  Reg.RootKey := HKEY_CURRENT_USER;
  if not(Reg.OpenKey('SOFTWARE\MCMTII',False))
    then Reg.OpenKey('SOFTWARE\MCMTII',true);
  Reg.WriteBool(S,B);
  Reg.CloseKey;
  Finally
  Reg.Free;
  end;
End;

Function CreateStringLog:String;
var
  I: Integer;
Begin
 Result:='';
 randomize;
 For I := 1 to 10 do
   Result:=Result+inttostr(random(10));
End;


Const
  NameLog     :String= 'MCMTII_DLL_';
  DirNameMCMT :String= 'MCMTII\';
  DirLogMCMT  :String= 'MCMTII_logs\';
  MCMT_INIFILE:String= 'mcmtII.ini';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function LocalAppDataPath : string;
var  pidl       : PItemIDList;
     InFolder   : array[0..MAX_PATH] of Char;

begin
   SHGetSpecialFolderLocation(Application.Handle, CSIDL_PERSONAL, pidl);
   SHGetPathFromIDList(PIDL, InFolder);
   result := InFolder;         // -> C:/users/cavadore/documents
end;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Function return_MCMT_StoreIniFilePath:String;
Var UserDocPath:String;
Begin
   UserDocPath:=LocalAppDataPath;
   if UserDocPath[Length(UserDocPath)]<>'\' then UserDocPath:=UserDocPath+'\';
   UserDocPath:=UserDocPath+DirNameMCMT;
   ForceDirectories(UserDocPath);
   Result:=UserDocPath+MCMT_INIFILE;
End;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


procedure WriteToLogFile(Data: string);
var
  Hour, Min,
  Sec, MSec    : Word;
  File_Debug   : TextFile;
  Value        : Int64;
  S2,S,St      : String;
  Diff         : Int64 ;
  Dts          : Int64 ;
  N            : TdateTime;
  ThreadID     : Cardinal;


begin
  if Not LogEnabled then Exit;

  Try
  SectionLog.Enter;

  Try

  QueryPerformanceCounter(Value);
  N:=Now;

  AssignFile(File_Debug,LogFileName);

  If FileExists(LogFileName) then Append (File_Debug)
                             else Rewrite(File_Debug);

  Try

  DecodeTime(N,Hour,Min,Sec,MSec);
  S:=InttoStr(MSec);

  If (Msec<10 ) then S:='00'+S else
  If (Msec<100) then S:='0'+S;

  Diff:=Value-PreviousLogTimeStamp;
  Dts:=Round(Diff*FactorTime); // �sec

  if (Dts<1000) then   // 1ms
  S2:=Format(' {%d �s}',[Dts])
  else
  if (Dts<1000000) then   // 1s
  S2:=Format(' {%1.1f ms}',[Dts/1000])
  else
  S2:=Format(' {%1.1f s}',[Dts/1000000]);

  ThreadID        := GetCurrentThreadID;

  if (ThreadID<>PreviousThreadID)  then
    St:='@'
  else
    St:=' ';


  Writeln(File_Debug,St+'[0x'+IntToHex(GetCurrentThreadID,4)+']: '+DateTimeToStr(now)+'.'+S+S2+' : '+Data);

  PreviousLogTimeStamp:=Value;
  PreviousThreadID    :=ThreadID;

  Finally
  Closefile(File_Debug);
  end;

  Finally
  SectionLog.Leave;
  end;

  Except
  end;

end;


Function ReturnFactorTime:Double;
Const timems=200;
Var   Value1,Value2:int64;

Begin
  // �sec
  QueryPerformanceCounter(Value1);
  Sleep(timems);
  QueryPerformanceCounter(Value2);

  Result:=(timems)/(Value2-Value1)*1000;
End;


Procedure LogEnterProc(ProcName:String);
begin
  WriteToLOGFile('->>>  '+Procname+' enter');
end;

Procedure LogEndProc(Procname:String);
begin
 WriteToLOGFile(Procname+' exit  ->>>');
end;


Procedure LogEnterProc_DLL(ProcName:String);
begin
  WriteToLOGFile('^');
  WriteToLOGFile(Procname+' enter');
end;

Procedure LogEndProc_DLL(Procname:String);
begin
 WriteToLOGFile(Procname+' exit');
 WriteToLOGFile('�');
end;


Procedure LogProc(Procname,message:String);
begin
  WriteToLOGFile('('+Procname+') '+message);
end;

Procedure WaitMainthread(procCaller:String);
begin
 If (Windows.GetCurrentThreadId()=System.MainThreadID) then
  Application.ProcessMessages;

 Sleep(20);
 WriteToLOGFile('Waiting >>> ['+procCaller+']');
end;



Initialization

 LogEnabled          := False;
 SectionLog          := TCriticalSection.Create;
 NameLog             := NameLog+CreateStringLog+'.log';
 QueryPerformanceCounter(PreviousLogTimeStamp);
 FactorTime          := ReturnFactorTime;

 LogFileName:=LocalAppDataPath;

 if (LogFileName[Length(LogFileName)]<>'\') then
   LogFileName:=LogFileName+'\';

 LogFileName:=LogFileName+DirNameMCMT+DirLogMCMT;

 ForceDirectories(LogFileName);

 LogFileName         := LogFileName+NameLog;
 PreviousThreadID    := GetCurrentThreadID;

Finalization
 SectionLog.Free;
end.
