unit VarGlobal;
{R+}
interface

Uses
      Messages,
      ExtCtrls,
      SyncObjs,
      HiresTim;


const
  CCDCameraClassDll     = 200;
  TelescopeClassDll     = 201;
  FocuserClassDll       = 202;
  FilterWClassDll       = 203;
  DomeClassDll          = 204;

  MaxAllocTSRZ          = 1024;

  MaxPECHarmos:Integer  = 2;
  MaxPECTABLE           = 20;

  Pi_d_2                = Pi/2;
  T_pi                  = 2*Pi;

Type

  TInfoDLL = record
    ClassName       : Pchar;
    ClassType       : Byte ;
    Author          : Pchar;
    What_it_does    : Pchar;
    Version         : Pchar;
    lastDateModified: Pchar;
  end;

  TEngineStruct = record
    AbleTo_WidthStand_SpeedChanges: Boolean;  // Is drives able to change speed upon demand ?
    AbleTo_ApplyOffsets           : Boolean;  // Engine able to achieve accurate offsets
    AbleTo_Stop_All               : Boolean;  // Can all drives be ALL stopped
    Able_ToSupportEmbeddedSpeeds  : Boolean;  // Can the drives has solar,Mpec and moon speeds embedded
    RateEvent                     : Integer;  // Check every RateEvent Ms if telescope has been moved
    HasAConsole                   : Boolean;  // Has a console....
    HasASpeedpanel                : Boolean;  // Has a speed panel....
    AbleToPulseGuide              : Boolean;  // Able to perform guiding
    AbleToPark                    : Boolean;  // Able to park
  end;


  TDirection                     = (DNord, DSud, DEst, DOuest, DNone);
  TSpeed                         = (SMax, SMedium, SSideral, SNone);
  TsetupEngine_encoderEnumerator = (SetupEngine, SetupEncoders);


  TSpeedInternalMCMT=record    // ALMOST ALL READ-ONLY
    V_Guidage_AD,              // Vitesses  ALPHA   (ecrit dans ReturnToSideralSpeed)
    V_Guidage_AD_SID,
    V_Corr_P_AD,
    V_Corr_M_AD,
    V_Point_L_AD,
    V_Point_R_AD              : Double;

    Accel_AD                  : Integer;

    V_Guidage_DEC,             // Vitesses  DELTA
    V_Corr_P_DEC,
    V_Corr_M_DEC,
    V_Point_L_DEC,
    V_Point_R_DEC             : Double;

    Accel_DEC                 : Integer;

    Resol_AD,
    Resol_DEC                 : LongInt; // R�solution des codeurs

    Inv_Codeur_AD,
    Inv_Codeur_DEC            : Boolean;
    GMT                       : Integer;
  end;


  TBackLashDatas=record
   Delta_Mode       : Integer;
   Activated,
   North,
   Saved_Park       : Boolean;
   StepAD,
   StepDEC,
   Pause            : Integer;
   Value            : Double ;
  end;


  TInternalMCMT_PEC=record
    ActiverPEC,
    DesactiverPEC,
    EffacerPEC,
    EnregistrerPEC :Boolean;
  end;


  TGermanMountSetup=record
    Monture_Allemande             : Boolean;
    Passage_Interdit              : Boolean;
    Degagement_Est                : Double;
    Degagement_Ouest              : Double;
    Declinaison_critique_Est      : Double;
    Declinaison_critique_Ouest    : Double;
    TelescopeZenithOuest          : Boolean;
    Retournement_Autorise         : Boolean;
    LongitudeHorairePointage      : double;
    AnglePolairePointage          : double;
    AngleHoraire                  : extended;
    AngleHorairePointage          : extended;
    AngleHorairePointagePrecedent : extended;
    DatePointagePrecedent         : Tdatetime;
  end;


  TLongLat=record
   Long : Double;
   Lat  : Double;
  end;


  TStateTelescope=record
      Delta,
      Alpha                   : Double;   // Retranscrit les positions en AD et DEC des codeurs
      AlphaPointage,
      DeltaPointage           : Double;   // identique aux 2 precedents, mais utilises

      AlphaRaw,
      DeltaRaw,
      AlphaRawPrevious,
      DeltaRawPrevious,
      AlphaRawInit            : LongInt; // Lecture codeurs en upas

      AlphaInit,
      DeltaInit,
      AlphaOldPoint,
      DeltaOldPoint             : Double; // Valeurs initiales pour calibration

      AlphaCodeurInit,
      DeltaCodeurInit           : Double; // Valeurs initiales Codeurs pour calibration

      FirstTime,
      Atime                     : Double;
  end;

  Tspeeds=record
    SpeedAlpha: TSpeed; // Pour conna�tre la vitesse impos�e en Alpha suivant l'objet TSpeed
    SpeedDelta: TSpeed; // Pour conna�tre la vitesse impos�e en Delta suivant l'objet TSpeed
  end;


  TPEC=record
    PEC_Step                  : Integer;
    PEC_Step_Codeur_Arcsec    : Double;

    Amplitude_PEC,
    Phase_PEC,
    Periode_PEC               : Array[1..MaxPECTABLE] of double;

    PEC_Step_old,
    FCorrectionPeriodeMs      : Integer;
  end;



Const
  ERROR                   = False;
  NOERROR                 = True;
  C_Alpha                 = 0;
  C_Delta                 = 1;
  SautMaxCodeur           = (2.0*pi)-0.35;


  WM_HandleMessageUpdate  = WM_USER+100;
  OmegaSid                = 2.0*Pi*(365.2564 + 1)/365.2564;


 Var
  (*
     These are GLOBAL VARIABLES
     If Written and read in different threads, access must be protected by critical sections
     to avoid race conditions.
     Race condition bugs are difficult to trace and can lead to access-violations, or DLL crash
  *)


  InfoDLL           : TInfoDLL    ;
  szVersionDLL      : Pchar       ;

  SpeedIntMCMT      : TSpeedInternalMCMT;
  InternalMCMT_PEC  : TInternalMCMT_PEC ;

  GerMountSetup     : TGermanMountSetup ;
  GerMountSetupSem  : TcriticalSection  ;

  BackLash          : TBackLashDatas    ;
  LongLat           : TLongLat          ;

  STSTel            : TStateTelescope   ;
  SectionSTSTel     : TcriticalSection  ;

  Speeds            : Tspeeds           ;
  SpeedsSection     : TcriticalSection  ;

  Hcom              : Thandle           ;

  PEC               : TPEC              ;
  PECSection        : TcriticalSection  ;

  //////////////////////////////////////////////////////////
  (* Atomic *)

  KING_Activated,
  PEC_Activated             : Boolean;

  Park,
  is_Parking                : Boolean;

  /////////////////////////////////////////////////////////
  // ASCOM features

  King_Ascom_Activated,
  PEC_Ascom_Activated       : Boolean;

  Amplitude_PEC_Ascom,
  Phase_PEC_Ascom,
  Periode_PEC_Ascom         : Array[0..3] of double;


IMPLEMENTATION

Uses SysUtils;


initialization
 InfoDLL.ClassName        :=StrAlloc(MaxAllocTSRZ);
 InfoDLL.Author           :=StrAlloc(MaxAllocTSRZ);
 InfoDLL.What_it_does     :=StrAlloc(MaxAllocTSRZ);
 InfoDLL.Version          :=StrAlloc(MaxAllocTSRZ);
 InfoDLL.lastDateModified :=StrAlloc(MaxAllocTSRZ);
 szVersionDLL             :=StrAlloc(MaxAllocTSRZ);

 GerMountSetupSem         :=TcriticalSection.create;
 SpeedsSection            :=TcriticalSection.create;
 PECSection               :=TcriticalSection.create;
 SectionSTSTel            :=TcriticalSection.create;

finalization
 StrDispose(InfoDLL.ClassName       );
 StrDispose(InfoDLL.Author          );
 StrDispose(InfoDLL.What_it_does    );
 StrDispose(InfoDLL.Version         );
 StrDispose(InfoDLL.lastDateModified);
 StrDispose(szVersionDLL            );

 GerMountSetupSem.Free;
 SpeedsSection   .Free;
 PECSection      .Free;
 SectionSTSTel   .Free;

end.
