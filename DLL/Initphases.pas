unit Initphases;

Interface

Uses Forms,Controls,Windows,Classes,VarGlobal;


Function InitCodeurs(var TimeCheck: Integer; Aowner: Tcomponent; Latitude, Longitude: Double): boolean; stdcall;
function ReturnDLLinfos: TInfoDLL; stdcall;
function GetDLLVer: Pchar; stdcall;
procedure Init_telescope_Panel(Aowner: Tcomponent; TwinHandle: Thandle; TypeSetup: TsetupEngine_encoderEnumerator); stdcall;


implementation

uses
  IniFiles,
  Dialogs,
  Registry,
  ComObj,
  Variants,
  SysUtils,

  Fastmm4,
  Setup,
  Utils,
  Console,
  scan_mcmt,
  ProcessBox_mcmtII,
  Comm,
  main_mcmt_unit;



const
  LastDateModified = '24 Juin 2012';


function ReturnVersionDLL_str(Index:Integer): string;
Const
  VersionInfo: array [1..8] of string
      = ('FileDescription', 'CompanyName'   , 'FileVersion',
         'InternalName'   , 'LegalCopyRight', 'OriginalFileName',
         'ProductName'    , 'ProductVersion');

  Version_Inconnue='Version inconnue !';


var

  Handle   : DWord;
  Info     : Pointer;
  InfoData : Pointer;
  InfoSize : LongInt;
  DataLen  : UInt;
  LangPtr  : Pointer;
  InfoType : string;

begin
    Result   := Version_Inconnue;

    Try

    InfoSize := GetFileVersionInfoSize(PChar('McmtII_Telescope.dll'),Handle);

    If (InfoSize>0) then
    begin
      GetMem(Info, InfoSize);
      Try
      If GetFileVersionInfo(PChar('McmtII_Telescope.dll'),Handle, InfoSize, Info) then
      begin

          InfoType := VersionInfo[index];

          if VerQueryValue(Info,'\VarFileInfo\Translation',LangPtr, DataLen) then
            InfoType:=Format('\StringFileInfo\%0.4x%0.4x\%s'#0,[LoWord(LongInt(LangPtr^)),
                                                                HiWord(LongInt(LangPtr^)), InfoType]);

          if VerQueryValue(Info,@InfoType[1],InfoData,Datalen) then  // Based on FileVersion
           result:=strPas(Pchar(InfoData));

      end;

      Finally
      FreeMem(Info,InfoSize);
      End;

    end;


    except
    Result := Version_Inconnue;
    end;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Const
  FileVersionProduct=3;

function ReturnDLLinfos: TInfoDLL; stdcall;

begin
  InfoDLL.ClassType        := TelescopeClassDll;

  StrCopy(InfoDLL.ClassName        ,Pchar('Plugin telescope MCMTII'));
  StrCopy(InfoDLL.Author           ,Pchar('Cavadore/Dufour/Lehir/Meunier'));
  StrCopy(InfoDLL.What_it_does     ,Pchar('Contr�le le telescope avec MCMTII'));
  StrCopy(InfoDLL.Version          ,Pchar(ReturnVersionDLL_str(FileVersionProduct)));
  StrCopy(InfoDLL.lastDateModified ,Pchar(LastDateModified));

  Result := InfoDLL;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GetDLLVer: Pchar; stdcall;
begin
  StrCopy(szVersionDLL,PChar('MCMTII DLL version ' + ReturnVersionDLL_str(FileVersionProduct)));
  GetDLLVer :=szVersionDLL;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function InitCodeurs(var TimeCheck: Integer; Aowner: Tcomponent; Latitude, Longitude: Double): boolean; stdcall;

var
  FileIni            : TIniFile;
  Reg                : TRegistry;
  TimeZone           : TTimeZoneInformation;
  TmpPorts           : TStringList;

  i                  : Integer;
  retour,
  ErrCode            : Integer;
  found              : Boolean;
  Park_AH,
  Park_Dec           : Double;
  astring,
  receivestring      : ShortString;

  Mj_ra, Mn_ra, Mi_ra: Byte;
  Mj_dc, Mn_dc, Mi_dc: Byte;
  Chsr               : Variant;
  KeyHandle          : HKEY;

  valueDouble        : Double;
  ValueInt           : Integer;
  ValueBool          : Boolean;
  ValueLong          : longint;
  AutoComPort        : Boolean;
  ok                 : Boolean;
  Form_scan_mcmt     : TForm_scan_mcmt;
  ProcessingBox      : TProcessingBox;
  NumCOMp            : Byte;

Const
  procName='InitCodeurs';


begin

  Try
  LogEnterProc_DLL(ProcName);

  Park_AH                  :=0;
  Park_Dec                 :=0;
  Decimalseparator         := '.';
  AutoComPort              :=True;
  FormConsole              := Nil;

  Retour := GetTimeZoneInformation(TimeZone);
  case retour of
    TIME_ZONE_ID_UNKNOWN : SpeedIntMCMT.GMT := TimeZone.Bias div 60;
    TIME_ZONE_ID_STANDARD: SpeedIntMCMT.GMT := (TimeZone.Bias + TimeZone.StandardBias) div 60;
    TIME_ZONE_ID_DAYLIGHT: SpeedIntMCMT.GMT := (TimeZone.Bias + TimeZone.DaylightBias) div 60;
    else                   Raise Exception.create('Erreur Fatale : Librairie MCMT II "GetTimeZoneInformation" a �chou� !');
  end;


  If FastMM_infos.ISfullDebugMode then
  begin
   ShowMessage('!! WARNING -> MCMT II DLL / FULL DEBUG MODE <- WARNING !!');
  end;


  FileIni     := TIniFile.Create(return_MCMT_StoreIniFilePath);
  Reg         := TRegistry.Create;

  Try

  Reg.RootKey := HKEY_CURRENT_USER;
  if not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then Reg.OpenKey('SOFTWARE\MCMTII', true);

  // Principe :
  // Si le fichier INI n'existe pas, charge les parametres depuis la base de registres

  LogEnabled:=False;
  if Reg.ValueExists('LogEnabled') then
    LogEnabled:= Reg.ReadBool('LogEnabled');

  Logproc(procName,'Init started');

  If Reg.ValueExists('Port') then
  begin
    NumCOMp     := Reg.ReadInteger('Port');
    Try
    AutoComPort := Reg.ReadBool   ('FindAutoPort');
    If AutoComPort then Logproc(procName,'AutoComPort enabled')
                   else Logproc(procName,'AutoComPort disabled');
    Except
    AutoComPort := True;
    End;
  end
  else
  begin
    NumCOMp:= FileIni.ReadInteger('Configuration', 'Port', 1);
    Reg.WriteInteger('Port', NumCOMp);
  end;

  If Reg.ValueExists('TimeOutMs_ConnectionsWrite') then
  begin
   TimeOutMs_ConnectionsWrite := Reg.ReadInteger('TimeOutMs_ConnectionsWrite');
   If TimeOutMs_ConnectionsWrite<100  then TimeOutMs_ConnectionsWrite:=100 ;
   If TimeOutMs_ConnectionsWrite>5000 then TimeOutMs_ConnectionsWrite:=5000;
  end;
  Logproc(procName,'TimeOUTWrite_PortMs : '+Inttostr(TimeOutMs_ConnectionsWrite));


  If Reg.ValueExists('TimeOutMs_ConnectionsRead') then
  begin
   TimeOutMs_ConnectionsRead := Reg.ReadInteger('TimeOutMs_ConnectionsRead');
   If TimeOutMs_ConnectionsRead<100  then TimeOutMs_ConnectionsRead:=100 ;
   If TimeOutMs_ConnectionsRead>5000 then TimeOutMs_ConnectionsRead:=5000;
  end;
  Logproc(procName,'TimeOUTRead_PortMs : '+Inttostr(TimeOutMs_ConnectionsRead));


  If Reg.ValueExists('PurgeCommMethod') then
    PurgeCommMethod:= Reg.ReadBool('PurgeCommMethod');

  If PurgeCommMethod then
   Logproc(procName,'PurgeComm enabled, fileflushbuffer disabled !') else
   Logproc(procName,'PurgeComm disabled, fileflushbuffer enabled !');



  if Reg.ValueExists('LastDelta') then
    STSTel.Delta := Reg.ReadFloat('LastDelta')
  else
  begin
    STSTel.Delta := strtofloat(FileIni.ReadString('Configuration', 'LastDelta', '0'));
    Reg.WriteFloat('LastDelta', STSTel.Delta);
  end;

  if Reg.ValueExists('LastAlpha') then
    STSTel.Alpha := Reg.ReadFloat('LastAlpha')
  else
  begin
    STSTel.Alpha := strtofloat(FileIni.ReadString('Configuration', 'LastAlpha', '0'));
    Reg.WriteFloat('LastAlpha', STSTel.Alpha);
  end;
  Logproc(procName,'LastAlphaDelta lu '+FloatToStr(STSTel.Alpha)+' '+FloatToStr(STSTel.Delta));

  if Reg.ValueExists('DIR_AD') then
    SpeedIntMCMT.Inv_Codeur_AD := Reg.ReadBool('DIR_AD')
  else
  begin
    SpeedIntMCMT.Inv_Codeur_AD := FileIni.ReadBool('Parameters', 'DIR_AD', False);
    Reg.WriteBool('DIR_AD', SpeedIntMCMT.Inv_Codeur_AD);
  end;

  if Reg.ValueExists('DIR_DEC') then
    SpeedIntMCMT.Inv_Codeur_DEC := Reg.ReadBool('DIR_DEC')
  else
  begin
    SpeedIntMCMT.Inv_Codeur_DEC := FileIni.ReadBool('Parameters', 'DIR_DEC', False);
    Reg.WriteBool('DIR_DEC', SpeedIntMCMT.Inv_Codeur_DEC);
  end;

  if Reg.ValueExists('Resol_AD') then
    SpeedIntMCMT.Resol_AD := Reg.ReadInteger('Resol_AD')
  else
  begin
    SpeedIntMCMT.Resol_AD := FileIni.ReadInteger('Parameters', 'Resol_AD', 4608000);
    Reg.WriteInteger('Resol_AD',SpeedIntMCMT.Resol_AD);
  end;
  if (SpeedIntMCMT.Resol_AD=0) then raise Exception.create('Fatal : La r�solution en Alpha est nulle ! Verifier dans le panneau de configuration...');


  if Reg.ValueExists('Resol_DEC') then
    SpeedIntMCMT.Resol_DEC := Reg.ReadInteger('Resol_DEC')
  else
  begin
    SpeedIntMCMT.Resol_DEC := FileIni.ReadInteger('Parameters', 'Resol_DEC', 4608000);
    Reg.WriteInteger('Resol_DEC', SpeedIntMCMT.Resol_DEC);
  end;
  if (SpeedIntMCMT.Resol_DEC=0) then raise Exception.create('Fatal : La r�solution en Delta est nulle ! Verifier dans le panneau de configuration...');

  if Reg.ValueExists('V_Guidage_AD') then
    SpeedIntMCMT.V_Guidage_AD := Reg.ReadFloat('V_Guidage_AD')
  else
  begin
    SpeedIntMCMT.V_Guidage_AD := StrToFloat(FileIni.ReadString('Parameters', 'V_Guidage_AD', '1'));
    Reg.WriteFloat('V_Guidage_AD', SpeedIntMCMT.V_Guidage_AD);
  end;
  SpeedIntMCMT.V_Guidage_AD_SID := SpeedIntMCMT.V_Guidage_AD;

  if Reg.ValueExists('V_Guidage_DEC') then
    SpeedIntMCMT.V_Guidage_DEC := Reg.ReadFloat('V_Guidage_DEC')
  else
  begin
    SpeedIntMCMT.V_Guidage_DEC := StrToFloat(FileIni.ReadString('Parameters', 'V_Guidage_DEC', '1'));
    Reg.WriteFloat('V_Guidage_DEC', SpeedIntMCMT.V_Guidage_DEC);
  end;

  if Reg.ValueExists('V_Corr_P_AD') then
    SpeedIntMCMT.V_Corr_P_AD := Reg.ReadFloat('V_Corr_P_AD')
  else
  begin
    SpeedIntMCMT.V_Corr_P_AD := StrToFloat(FileIni.ReadString('Parameters', 'V_Corr_P_AD', '1'));
    Reg.WriteFloat('V_Corr_P_AD', SpeedIntMCMT.V_Corr_P_AD);
  end;

  if Reg.ValueExists('V_Corr_P_DEC') then
    SpeedIntMCMT.V_Corr_P_DEC := Reg.ReadFloat('V_Corr_P_DEC')
  else
  begin
    SpeedIntMCMT.V_Corr_P_DEC := strtofloat(FileIni.ReadString('Parameters', 'V_Corr_P_DEC', '1'));
    Reg.WriteFloat('V_Corr_P_DEC', SpeedIntMCMT.V_Corr_P_DEC);
  end;

  if Reg.ValueExists('V_Corr_M_AD') then
    SpeedIntMCMT.V_Corr_M_AD := Reg.ReadFloat('V_Corr_M_AD')
  else
  begin
    SpeedIntMCMT.V_Corr_M_AD := strtofloat(FileIni.ReadString('Parameters', 'V_Corr_M_AD', '1'));
    Reg.WriteFloat('V_Corr_M_AD', SpeedIntMCMT.V_Corr_M_AD);
  end;

  if Reg.ValueExists('V_Corr_M_DEC') then
    SpeedIntMCMT.V_Corr_M_DEC := Reg.ReadFloat('V_Corr_M_DEC')
  else
  begin
    SpeedIntMCMT.V_Corr_M_DEC := strtofloat(FileIni.ReadString('Parameters', 'V_Corr_M_DEC', '1'));
    Reg.WriteFloat('V_Corr_M_DEC', SpeedIntMCMT.V_Corr_M_DEC);
  end;

  if Reg.ValueExists('V_Point_L_AD') then
    SpeedIntMCMT.V_Point_L_AD := Reg.ReadFloat('V_Point_L_AD')
  else
  begin
    SpeedIntMCMT.V_Point_L_AD := strtofloat(FileIni.ReadString('Parameters', 'V_Point_L_AD', '1'));
    Reg.WriteFloat('V_Point_L_AD', SpeedIntMCMT.V_Point_L_AD);
  end;

  if Reg.ValueExists('V_Point_L_DEC') then
    SpeedIntMCMT.V_Point_L_DEC := Reg.ReadFloat('V_Point_L_DEC')
  else
  begin
    SpeedIntMCMT.V_Point_L_DEC := strtofloat(FileIni.ReadString('Parameters', 'V_Point_L_DEC', '1'));
    Reg.WriteFloat('V_Point_L_DEC', SpeedIntMCMT.V_Point_L_DEC);
  end;

  if Reg.ValueExists('V_Point_R_AD') then
    SpeedIntMCMT.V_Point_R_AD := Reg.ReadFloat('V_Point_R_AD')
  else
  begin
    SpeedIntMCMT.V_Point_R_AD := strtofloat(FileIni.ReadString('Parameters', 'V_Point_R_AD', '1'));
    Reg.WriteFloat('V_Point_R_AD', SpeedIntMCMT.V_Point_R_AD);
  end;

  if Reg.ValueExists('V_Point_R_DEC') then
    SpeedIntMCMT.V_Point_R_DEC := Reg.ReadFloat('V_Point_R_DEC')
  else
  begin
    SpeedIntMCMT.V_Point_R_DEC := strtofloat(FileIni.ReadString('Parameters', 'V_Point_R_DEC', '1'));
    Reg.WriteFloat('V_Point_R_DEC', SpeedIntMCMT.V_Point_R_DEC);
  end;

  if Reg.ValueExists('Accel_AD') then
    SpeedIntMCMT.Accel_AD := Reg.ReadInteger('Accel_AD')
  else
  begin
    SpeedIntMCMT.Accel_AD := strtoint(FileIni.ReadString('Parameters', 'Accel_AD', '0'));
    Reg.WriteInteger('Accel_AD', SpeedIntMCMT.Accel_AD);
  end;

  if Reg.ValueExists('Accel_DEC') then
    SpeedIntMCMT.Accel_DEC := Reg.ReadInteger('Accel_DEC')
  else
  begin
    SpeedIntMCMT.Accel_DEC := strtoint(FileIni.ReadString('Parameters', 'Accel_DEC', '0'));
    Reg.WriteInteger('Accel_DEC', SpeedIntMCMT.Accel_DEC);
  end;

  if Reg.ValueExists('Backlash_Activated') then
    Backlash.Activated := Reg.ReadBool('Backlash_Activated')
  else
  begin
    Backlash.Activated := FileIni.ReadBool('Parameters', 'Backlash_Activated', false);
    Reg.WriteBool('Backlash_Activated', Backlash.Activated);
  end;

  if Reg.ValueExists('Backlash') then
    Backlash.Value := Reg.ReadFloat('Backlash')
  else
  begin
    Backlash.Value := strtofloat(FileIni.ReadString('Parameters', 'Backlash', '20'));
    Reg.WriteFloat('Backlash', Backlash.Value);
  end;

  if Reg.ValueExists('Backlash_Delta_Mode')
   then Backlash.Delta_Mode := Reg.ReadInteger('Backlash_Delta_Mode')
  else
  begin
   Backlash.Delta_Mode := FileIni.ReadInteger('Parameters', 'Backlash_Delta_Mode', 0);
   Reg.WriteInteger('Backlash_Delta_Mode',Backlash.Delta_Mode);
  end;

  if Reg.ValueExists('Backlash_Pause') then
    Backlash.Pause := Reg.ReadInteger('Backlash_Pause')
  else
  begin
    Backlash.Pause := FileIni.ReadInteger('Parameters', 'Backlash_Pause', 0) * 1000;
    Reg.WriteInteger('Backlash_Pause', Backlash.Pause);
  end;

  if Reg.ValueExists('Park_AH') then
    Park_AH := Reg.ReadFloat('Park_AH')
  else
  begin
    Park_AH := FileIni.Readfloat('ParkMode', 'AH', 0);
    Reg.WriteFloat('Park_AH', Park_AH);
  end;

  if Reg.ValueExists('Park_Dec') then
    Park_Dec := Reg.ReadFloat('Park_Dec')
  else
  begin
    Park_Dec := FileIni.Readfloat('ParkMode', 'DEC', 0);
    Reg.WriteFloat('Park_Dec', Park_Dec);
  end;

  if Reg.ValueExists('Passage_Interdit')
    then GerMountSetup.Passage_Interdit := Reg.ReadBool('Passage_Interdit')
    else
      begin
        GerMountSetup.Passage_Interdit := FileIni.ReadBool('Pointage','Passage_Interdit',false);
        Reg.WriteBool('Passage_Interdit',GerMountSetup.Passage_Interdit);
      end;

  if Reg.ValueExists('Monture_Allemande')
    then GerMountSetup.Monture_Allemande := Reg.ReadBool('Monture_Allemande')
    else
      begin
        GerMountSetup.Monture_Allemande := FileIni.ReadBool('Pointage','Monture_Allemande',false);
        Reg.WriteBool('Monture_Allemande',GerMountSetup.Monture_Allemande);
      end;

  if Reg.ValueExists('Degagement_Est')
    then GerMountSetup.Degagement_Est := Reg.ReadFloat('Degagement_Est')
    else
      begin
        GerMountSetup.Degagement_Est := FileIni.Readfloat('Pointage', 'Degagement_Est', 0);
        Reg.WriteFloat('Degagement_Est',GerMountSetup.Degagement_Est);
      end;

  if Reg.ValueExists('Degagement_Ouest')
    then GerMountSetup.Degagement_Ouest := Reg.ReadFloat('Degagement_Ouest')
    else
      begin
        GerMountSetup.Degagement_Ouest := FileIni.Readfloat('Pointage', 'Degagement_Ouest', 0);
        Reg.WriteFloat('Degagement_Ouest',GerMountSetup.Degagement_Ouest);
      end;

  if Reg.ValueExists('Retournement_Autorise')
    then GerMountSetup.Retournement_Autorise := Reg.ReadBool('Retournement_Autorise')
    else
      begin
        GerMountSetup.Retournement_Autorise :=
                         FileIni.ReadBool('Pointage','Retournement_Autorise',false);
        Reg.WriteBool('Retournement_Autorise',GerMountSetup.Retournement_Autorise);
      end;

  if Reg.ValueExists('Declinaison_Critique_Est')
    then GerMountSetup.Declinaison_Critique_Est :=
                 Reg.ReadFloat('Declinaison_Critique_Est')
    else
      begin
        GerMountSetup.Declinaison_Critique_Est :=
                FileIni.Readfloat('Pointage', 'Declinaison_Critique_Est', 0);
        Reg.WriteFloat('Declinaison_Critique_Est',GerMountSetup.Declinaison_Critique_Est);
      end;

  if Reg.ValueExists('Declinaison_Critique_Ouest')
    then GerMountSetup.Declinaison_Critique_Ouest :=
                 Reg.ReadFloat('Declinaison_Critique_Ouest')
    else
      begin
        GerMountSetup.Declinaison_Critique_Ouest :=
                FileIni.Readfloat('Pointage', 'Declinaison_Critique_Ouest', 0);
        Reg.WriteFloat('Declinaison_Critique_Ouest',
                                                GerMountSetup.Declinaison_Critique_Ouest);
      end;

  if Reg.ValueExists('TelescopeZenithOuest')
    then GerMountSetup.TelescopeZenithOuest := Reg.ReadBool('TelescopeZenithOuest')
    else GerMountSetup.TelescopeZenithOuest := False;

  LongLat.Long:=Longitude;
  Reg.WriteFloat('Longitude', LongLat.Long);

  LongLat.Lat:=Latitude;
  Reg.WriteFloat('Latitude', LongLat.Lat);

  Logproc(procName,Format('Init lat=%1.5f long=%1.5f',[LongLat.Lat,LongLat.Long]));

  Backlash.Saved_Park := Backlash.Activated;
  Backlash.StepAD     := round(SpeedIntMCMT.Resol_AD  * Backlash.Value / 60 / 360);
  Backlash.StepDEC    := round(SpeedIntMCMT.Resol_DEC * Backlash.Value / 60 / 360);

  Reg.CloseKey;

  Finally
  Reg.Free;
  FileIni.free;
  end;

  ErrCode := RegOpenKeyEx(HKEY_LOCAL_MACHINE,'Software\ASCOM\Telescope Drivers\MCMTII.Telescope',0,KEY_READ,KeyHandle);

  if (ErrCode = ERROR_SUCCESS) then
  begin
    try
      Chsr := CreateOLEObject('DriverHelper.Profile');
      Try
      if Chsr.GetValue('MCMTII.Telescope', 'King_ASCOM_Activated') = 'Yes' then
        King_Ascom_Activated := true
      else
        King_Ascom_Activated := false;

      Amplitude_PEC_Ascom[0] := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Amplitude_PEC'  ));
      Amplitude_PEC_Ascom[1] := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Amplitude_PEC_1'));
      Amplitude_PEC_Ascom[2] := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Amplitude_PEC_2'));
      Amplitude_PEC_Ascom[3] := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Amplitude_PEC_3'));
      Periode_PEC_Ascom[0]   := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Periode_PEC'    ));
      Periode_PEC_Ascom[1]   := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Periode_PEC_1'  ));
      Periode_PEC_Ascom[2]   := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Periode_PEC_2'  ));
      Periode_PEC_Ascom[3]   := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Periode_PEC_3'  ));
      Phase_PEC_Ascom[0]     := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Phase_PEC'      ));
      Phase_PEC_Ascom[1]     := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Phase_PEC_1'    ));
      Phase_PEC_Ascom[2]     := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Phase_PEC_2'    ));
      Phase_PEC_Ascom[3]     := StrToFloat(Chsr.GetValue('MCMTII.Telescope', 'Phase_PEC_3'    ));

      if Chsr.GetValue('MCMTII.Telescope', 'PEC_ASCOM_Activated') = 'Yes' then
        PEC_Ascom_Activated := true
      else
        PEC_Ascom_Activated := false;

      Finally
      Chsr :=Unassigned;
      End;

    except
      Logproc(procName,'ASCOM driver are not installed!');
    end;
  end;


  If AutoComPort then
  Begin
    Logproc(procName,'Auto COM search');

    TmpPorts := TStringList.Create;

    Try
    EnumComPorts(TmpPorts);

    if (TmpPorts.Count>0) then
    begin

          For i:= 0 to TmpPorts.Count- 1 do
           Logproc(procName,'COM detected = #'+TmpPorts[i]);

          Try
          Form_scan_mcmt:=TForm_scan_mcmt.create(AOwner,TmpPorts);
          Except
          // AOwner error
          raise Exception.Create('Erreur interne DLL d''interface sur AOwner !');
          End;
          Form_scan_mcmt._Show_;

          Found         :=Form_scan_mcmt.Found_MCMT;
          NumCOMp       :=Form_scan_mcmt.NumCom;
          Form_scan_mcmt.Free;

    end
    else
    begin
     Found:=False;
     ShowMessage('Aucun port s�rie n''a �t� detect� automatiquement sur cette machine !');
    end;

    Finally
    TmpPorts.Free;
    End;

  end  // AutoSearch
  else
  Begin
    // Essaie qd meme le port COM
    Logproc(procName,Format('Try to talk to COM %d',[NumCOMp]));
    Found         := GetVersionAxes(NumCOMp, 0, Mj_ra, Mn_ra, Mi_ra);
  End;


  If not Found then
  begin

      If AutoComPort then
      Begin
        If Not User_Has_Stopped_COM_Scans then
          ShowMessage('Le programme n''a pas pu d�tecter MCMT II sur les ports COMs d�tect�s sur cette machine !')
        else
          ShowMessage('La d�tection MCMT II a �t� stopp�e par l''utilisateur !');
      End
      else
      ShowMessage(Format('Le programme n''a pas pu d�tecter MCMT II sur le port COM %d fix� par l''utilisateur!',[NumCOMp]));

      Result := ERROR;

      Logproc(procName,Format('Error -> COM %d, no MCMT II detected...',[NumCOMp]));

  end
  else
  begin
        // COM found  YES !

        // version du Soft
        ProcessingBox:=TProcessingBox.create(Aowner,'Lecture version firmware et init MCMTII....');
        ProcessingBox.Show;
        ProcessingBox.Refresh;
        GetVersionAxes(NumCOMp, 0, Mj_ra, Mn_ra, Mi_ra);
        GetVersionAxes(NumCOMp, 1, Mj_dc, Mn_dc, Mi_dc);
        ProcessingBox.Free;

        Logproc(procName,Format('RA  version %d.%d.%d',[Mj_ra, Mn_ra, Mi_ra]));
        Logproc(procName,Format('DEC version %d.%d.%d',[Mj_dc, Mn_dc, Mi_dc]));


        if not VersionOk(Mj_ra, Mn_ra, Mi_ra, Mj_dc, Mn_dc, Mi_dc) then
        begin
          Result := ERROR;
          ShowMessage   ('Erreur Fatale, La version firmware de MCMT II n''est pas la bonne pour cette DLL, mettre � jour le firmware de MCMT !');
          Logproc(procName,'Not compliant version, FATAL error...');
          Exit; // Exec finally sections
        end;

        Is_Parking := False;

        // Console creation
        {$DEFINE CONSOLE_OP}
        {$IFDEF CONSOLE_OP}
        FormConsole                          := TFormConsole.create(Aowner); // To pass Aowner is VERY important
        Logproc(procName,'Console created...');
        {$ENDIF}

        Open_Cod_Eng(NumCOMp,ok,Hcom); // ouvre le port COM ->> Reste OUVERT par la suite

        if Ok then
        begin

            Reg         := TRegistry.Create ;

            Try
            Reg.RootKey := HKEY_CURRENT_USER;
            if not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then Reg.OpenKey('SOFTWARE\MCMTII', true);

            // Regarde si ce qui a �t� lu dans la base de registres ou le fichier INI est pariel que
            // l'EEPROM de MCMT -> ALPHA
            astring       := 'K' + Ansichar(0) + Ansichar(0) + Ansichar(0) + Ansichar(0);
            receivestring := sendcommand(Hcom,astring, 48, 0);

            If (receivestring<>'') then   // Ce sont la les donn�es VITALES
            begin

                  If (ord(receivestring[2]) <> 0) or (ord(receivestring[1]) <> 0) then
                  begin
                     valueDouble:=(625000 / (ord(receivestring[3]) / 10 + ord(receivestring[2]) * 256 + ord(receivestring[1])));
                     if (valueDouble<>SpeedIntMCMT.V_Guidage_AD) then
                     begin
                      SpeedIntMCMT.V_Guidage_AD:=valueDouble;
                      Reg.WriteFloat('V_Guidage_AD', SpeedIntMCMT.V_Guidage_AD);
                     end;
                     Logproc(procName,Format('V_Guidage_AD=%1.3f / %1.3f',[SpeedIntMCMT.V_Guidage_AD,valueDouble]));
                  end;

                  If (ord(receivestring[5]) <> 0) or (ord(receivestring[4]) <> 0) then
                  begin
                   valueDouble:=(625000 / (ord(receivestring[5]) * 256 + ord(receivestring[4])));
                   if (valueDouble<>SpeedIntMCMT.V_Corr_P_AD) then
                     begin
                      SpeedIntMCMT.V_Corr_P_AD:=valueDouble;
                      Reg.WriteFloat('V_Corr_P_AD',SpeedIntMCMT.V_Corr_P_AD);
                   end;
                   Logproc(procName,Format('V_Corr_P_AD=%1.3f / %1.3f',[SpeedIntMCMT.V_Corr_P_AD,valueDouble]));
                  end;

                  if (ord(receivestring[7]) <> 0) or (ord(receivestring[6]) <> 0) then
                  begin
                    valueDouble:= (625000 / (ord(receivestring[7]) * 256 + ord(receivestring[6])));
                    if (valueDouble<>SpeedIntMCMT.V_Corr_M_AD) then
                     begin
                      SpeedIntMCMT.V_Corr_M_AD:=valueDouble;
                      Reg.WriteFloat('V_Corr_M_AD',SpeedIntMCMT.V_Corr_M_AD);
                    end;
                    Logproc(procName,Format('V_Corr_M_AD=%1.3f / %1.3f',[SpeedIntMCMT.V_Corr_M_AD,valueDouble]));
                  end;

                  if (ord(receivestring[9]) <> 0) or (ord(receivestring[8]) <> 0) then
                  begin
                   valueDouble:=(625000 / (ord(receivestring[9]) * 256 + ord(receivestring[8])) * 8);
                   if (ValueDouble<>SpeedIntMCMT.V_Point_L_AD) then
                   begin
                     SpeedIntMCMT.V_Point_L_AD:=ValueDouble;
                     Reg.WriteFloat('V_Point_L_AD',SpeedIntMCMT.V_Point_L_AD);
                   end;
                   Logproc(procName,Format('V_Point_L_AD=%1.3f / %1.3f',[SpeedIntMCMT.V_Point_L_AD,valueDouble]));
                  end;

                  if (ord(receivestring[11]) <> 0) or (ord(receivestring[10]) <> 0) then
                  begin
                   ValueDouble:=(625000 / (ord(receivestring[11]) * 256 + ord(receivestring[10])) * 32);
                   if (SpeedIntMCMT.V_Point_R_AD<>ValueDouble) then
                   begin
                     SpeedIntMCMT.V_Point_R_AD:=valueDouble;
                     Reg.WriteFloat('V_Point_R_AD',SpeedIntMCMT.V_Point_R_AD);
                   end;
                   Logproc(procName,Format('V_Point_R_AD=%1.3f / %1.3f',[SpeedIntMCMT.V_Point_R_AD,ValueDouble]));
                  end;

                  ValueInt:= ord(receivestring[12]);
                  If (ValueInt<>SpeedIntMCMT.Accel_AD) then
                  begin
                    SpeedIntMCMT.Accel_AD:=ValueInt;
                    Reg.WriteInteger('Accel_AD', SpeedIntMCMT.Accel_AD);
                  end;
                  Logproc(procName,Format('Accel_AD= %d / %d',[SpeedIntMCMT.Accel_AD,ValueInt]));

                  ValueBool:=(ord(receivestring[13])=1);
                  if (ValueBool<>SpeedIntMCMT.Inv_Codeur_AD) then
                  begin
                    SpeedIntMCMT.Inv_Codeur_AD:=ValueBool;
                    Reg.WriteBool('DIR_AD', SpeedIntMCMT.Inv_Codeur_AD);
                  end;
                  Logproc(procName,'DIR_AD = '+booltostr(SpeedIntMCMT.Inv_Codeur_AD) +' / '+booltostr(ValueBool));


                  ValueLong := 25600 * (ord(receivestring[29]) * 256 + ord(receivestring[30]));
                  if (ValueLong<>SpeedIntMCMT.Resol_AD) and (ValueLong>0) then
                  begin
                   SpeedIntMCMT.Resol_AD:=ValueLong;
                   Reg.WriteInteger('Resol_AD', SpeedIntMCMT.Resol_AD);
                  end;
                  Logproc(procName,Format('Resol_AD= %d / %d',[SpeedIntMCMT.Resol_AD,ValueLong]));


            end;

            // l'EEPROM de MCMT -> DELTA
            Receivestring := sendcommand(Hcom,astring, 48, 1);

            if (receivestring<>'') then   // Ce sont la les donn�es VITALES
            begin

                  If (ord(receivestring[2]) <> 0) or (ord(receivestring[1]) <> 0) then
                  begin
                     valueDouble:=(625000 / (ord(receivestring[3]) / 10 + ord(receivestring[2]) * 256 + ord(receivestring[1])));
                     if (valueDouble<>SpeedIntMCMT.V_Guidage_DEC) then
                     begin
                      SpeedIntMCMT.V_Guidage_DEC:=valueDouble;
                      Reg.WriteFloat('V_Guidage_DEC', SpeedIntMCMT.V_Guidage_DEC);
                     end;
                     Logproc(procName,Format('V_Guidage_DEC=%1.3f / %1.3f',[SpeedIntMCMT.V_Guidage_DEC,valueDouble]));
                  end;

                  If (ord(receivestring[5]) <> 0) or (ord(receivestring[4]) <> 0) then
                  begin
                   valueDouble:=(625000 / (ord(receivestring[5]) * 256 + ord(receivestring[4])));
                   if (valueDouble<>SpeedIntMCMT.V_Corr_P_DEC) then
                     begin
                      SpeedIntMCMT.V_Corr_P_DEC:=valueDouble;
                      Reg.WriteFloat('V_Corr_P_DEC',SpeedIntMCMT.V_Corr_P_DEC);
                   end;
                   Logproc(procName,Format('V_Corr_P_DEC=%1.3f / %1.3f',[SpeedIntMCMT.V_Corr_P_DEC,valueDouble]));
                  end;

                  if (ord(receivestring[7]) <> 0) or (ord(receivestring[6]) <> 0) then
                  begin
                    valueDouble:= (625000 / (ord(receivestring[7]) * 256 + ord(receivestring[6])));
                    if (valueDouble<>SpeedIntMCMT.V_Corr_M_DEC) then
                     begin
                      SpeedIntMCMT.V_Corr_M_DEC:=valueDouble;
                      Reg.WriteFloat('V_Corr_M_DEC',SpeedIntMCMT.V_Corr_M_DEC);
                    end;
                   Logproc(procName,Format('V_Corr_M_DEC=%1.3f / %1.3f',[SpeedIntMCMT.V_Corr_M_DEC,valueDouble]));
                  end;

                  if (ord(receivestring[9]) <> 0) or (ord(receivestring[8]) <> 0) then
                  begin
                   valueDouble:=(625000 / (ord(receivestring[9]) * 256 + ord(receivestring[8])) * 8);
                   if (ValueDouble<>SpeedIntMCMT.V_Point_L_DEC) then
                   begin
                     SpeedIntMCMT.V_Point_L_DEC:=ValueDouble;
                     Reg.WriteFloat('V_Point_L_DEC',SpeedIntMCMT.V_Point_L_DEC);
                   end;
                   Logproc(procName,Format('V_Point_L_DEC=%1.3f / %1.3f',[SpeedIntMCMT.V_Point_L_DEC,valueDouble]));
                  end;

                  if (ord(receivestring[11]) <> 0) or (ord(receivestring[10]) <> 0) then
                  begin
                   ValueDouble:=(625000 / (ord(receivestring[11]) * 256 + ord(receivestring[10])) * 32);
                   if (SpeedIntMCMT.V_Point_R_DEC<>ValueDouble) then
                   begin
                     SpeedIntMCMT.V_Point_R_DEC:=valueDouble;
                     Reg.WriteFloat('V_Point_R_DEC',SpeedIntMCMT.V_Point_R_DEC);
                   end;
                   Logproc(procName,Format('V_Point_R_DEC=%1.3f / %1.3f',[SpeedIntMCMT.V_Point_R_DEC,valueDouble]));
                  end;

                  ValueInt:= ord(receivestring[12]);
                  If (ValueInt<>SpeedIntMCMT.Accel_DEC) then
                  begin
                    SpeedIntMCMT.Accel_DEC:=ValueInt;
                    Reg.WriteInteger('Accel_DEC', SpeedIntMCMT.Accel_DEC);
                  end;
                  Logproc(procName,Format('Accel_DEC= %d / %d',[SpeedIntMCMT.Accel_DEC,ValueInt]));

                  ValueBool:=(ord(receivestring[13])=1);
                  if (ValueBool<>SpeedIntMCMT.Inv_Codeur_DEC) then
                  begin
                    SpeedIntMCMT.Inv_Codeur_DEC:=ValueBool;
                    Reg.WriteBool('DIR_DEC', SpeedIntMCMT.Inv_Codeur_DEC);
                  end;
                   Logproc(procName,'DIR_DEC = '+booltostr(SpeedIntMCMT.Inv_Codeur_DEC) +' / '+booltostr(ValueBool));

                  ValueLong := 25600 * (ord(receivestring[29]) * 256 + ord(receivestring[30]));
                  if (ValueLong<>SpeedIntMCMT.Resol_DEC) and (ValueLong>0) then
                  begin
                   SpeedIntMCMT.Resol_DEC:=ValueLong;
                   Reg.WriteInteger('Resol_DEC', SpeedIntMCMT.Resol_DEC);
                  end;
                  Logproc(procName,Format('Resol_DEC= %d / %d',[SpeedIntMCMT.Resol_DEC,ValueLong]));

            end;

            Reg.CloseKey;

            Finally
            Reg.Free;
            end;

            // Est ce que le telescope est il park� ?
            // OU le fait que le telescope est en mode STOP
            astring       := 'r' + Ansichar(0) + Ansichar(0) + Ansichar(0) + Ansichar(0);
            receivestring := sendcommand(Hcom,astring, 10, 0);
            if (ord(receivestring[10]) = 1) then Park := true else Park := false;

            if Park then
            begin
              STSTel.Alpha := CalculeTSL(Now+SpeedIntMCMT.GMT/24,LongLat.Long) - Park_AH;
              STSTel.Delta := Park_Dec;

              Logproc(procName,'Parks is Active!');

              If GerMountSetup.Monture_Allemande then
              begin
                ProcessingBox:=TProcessingBox.create(Aowner,'Telescope park� : orientation tube monture Allemande connue...');
                ProcessingBox.Width:=520;
              end
              else
                ProcessingBox:=TProcessingBox.create(Aowner,'Telescope park�...');

              ProcessingBox.Show;
              ProcessingBox.Refresh;
              Sleep(2000);
              ProcessingBox.Free;

            end
            else
            begin
              // Dans le cas d'une monture allemande, il faut savoir, lors de
              // l'initialisation des codeurs, si le t�lescope est retourn� ou non.
              // Lorsque l'on sort d'un "park", l'�tat (retourn� ou non) du t�lescope
              // est inscrit dans la base de registre, il n'est donc pas n�cessaire
              // de poser la question.

              If GerMountSetup.Monture_Allemande then
              begin
                // De quel cot� sommes nous ????
                ASK_FortelescopeOrientation(Aowner,Latitude);  // <- Toujours demander a l'INIT
              end;  // German Mount

            end; // Park Active

            /////////////////////////////////

            Logproc(procName,'5 Timers init');
            TimeCheck                      := 1000; // 1s
            PEC.FCorrectionPeriodeMs       := TimeCheck;

            // Cree les 5 TIMERS
            CreateAllTimers;

            Try
            // Initialisation de Delta et Alpha avec derni�res valeurs enregistr�es
            // Lit La valeur des encodeurs, fatal si ca ne repond pas...
            If not Calibrate(STSTel.Alpha,STSTel.Delta) then
              Raise Exception.create('MCMT II initialisation -> Erreur fatale, Calibrate a �chou� !');
            Except
              FreeAllTimers;
              Raise;
            End;

            Logproc(procName,'Calibrate done');

            Result := NOERROR;

        end;  // Le COM a pu etre OUVERT

  end; // Com found

  Logproc(procName,'Init MCMT II Complete');
  LogEndProc_DLL(ProcName);

  Except
    On E:Exception do
    Begin
     Logproc(procName,'Init failed ->'+E.Message);
     raise Exception.create('Erreur d''initialisation de MCMT II -> '+E.Message);
    End;
  End;
end;


//////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //
// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //
// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //// SETUP PANEL //


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function ReadlatfromRegistry:Double;
Var
  Reg             : TRegistry;

Begin
  Result:=Pi/4;  // + 45N

  Reg         := TRegistry.Create;
  Try
  Reg.RootKey := HKEY_CURRENT_USER;
  If Reg.OpenKey('SOFTWARE\MCMTII', False) then
  Begin
   If Reg.ValueExists('Latitude') then
    Result:=Reg.ReadFloat('Latitude');
   Reg.Closekey;
  End;
  Finally
  Reg.Free;
  End;
End;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

procedure Init_telescope_Panel(Aowner: Tcomponent; TwinHandle: Thandle; TypeSetup: TsetupEngine_encoderEnumerator); stdcall;
//   This function has to display ITSELF a form to ask eventually about :
//   COM, Ip address, initialisation parameters, etc etc....
//   Twinhandle is PRISM's caller window handler in case of use of WIN_API procs., in order to create a modalwindow in front of all the others
//   in case of Delphi DLL please use Aowner instead.

var

  SetupTelescope  : TSetupTelescope;
  Form_scan_mcmt  : TForm_scan_mcmt;
  DeviceIni       : TIniFile;
  Reg             : TRegistry;

  S               : string;
  found           : boolean;
  c,i             : Integer;
  TmpPorts        : TStringList;

  KeyHandle       : HKEY;
  ErrCode         : Integer;
  Chsr            : Variant;
  T               : string;

  Mj_ra,
  Mn_ra, Mi_ra    : Byte;
  Mj_dc, Mn_dc,
  Mi_dc           : Byte;

  COM_OK          : Boolean;
  LongSetup,
  LatSetup,
  AltSetup        : Double;
  AutoFindCOM     : Boolean;
  ProcessingBox   : TProcessingBox;
  Latitude        : Double;
  NumCOM          : Byte;

begin

  COM_OK           := False;
  AutoFindCOM      := True;
  decimalseparator := '.';

  Latitude         := ReadlatfromRegistry;

  SetupTelescope   := TSetupTelescope.create(Aowner,Latitude); // To pass Aowner is VERY important
                                                               // Otherwise the panel is not visible

  SetupTelescope.caption := 'MCMTII configuration, version : ';

  SetupTelescope.TabSheetASCOM_PEC_KING.TabVisible:=False; // Do not show (only for ASCOM)


  SetupTelescope.caption :=SetupTelescope.caption+ReturnVersionDLL_str(FileVersionProduct);


  DeviceIni   := TIniFile.Create(return_MCMT_StoreIniFilePath);
  Reg         := TRegistry.Create;

  Try
  Reg.RootKey := HKEY_CURRENT_USER;
  if not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then Reg.OpenKey('SOFTWARE\MCMTII', true);

  if Reg.ValueExists('Port') then
  begin
    NumCOM      := 1;
    NumCOM      := Reg.ReadInteger('Port')        ;
    Try
    AutoFindCOM := Reg.ReadBool   ('FindAutoPort');
    Except
    AutoFindCOM := True;
    End;
  end
  else
  begin
    NumCOM := DeviceIni.ReadInteger('Configuration', 'Port', 1);
    Reg.WriteInteger('Port', NumCOM);
  end;
  Reg.CloseKey;

  Finally
  Reg.Free;
  DeviceIni.free;
  End;

  TmpPorts := TStringList.Create;
  EnumComPorts(TmpPorts);

  // Test
  // TmpPorts.add('1');TmpPorts.add('2');TmpPorts.add('3');TmpPorts.add('5');

  Try

  if (TmpPorts.Count>0) then
   for I := 0 to TmpPorts.Count - 1 do
    SetupTelescope.ComboBoxNumCOM.Items.Add(TmpPorts[i]);

  SetupTelescope.ComboBoxNumCOM.Visible         :=AutoFindCOM;
  SetupTelescope.NbreEditPortSerieForced.Visible:=Not AutoFindCOM;
  SetupTelescope.NbreEditPortSerieForced.Text   :=Inttostr(NumCOM);


  if AutoFindCOM and (TmpPorts.Count>0) then
  Begin

      Form_scan_mcmt:=TForm_scan_mcmt.create(AOwner,TmpPorts);
      Form_scan_mcmt._Show_;
      Found:= Form_scan_mcmt.Found_MCMT;
      Form_scan_mcmt.Free;


  End
  Else
  Found:=True;

  ///////////////////

  If not Found then // toujours pas trouv�?
  begin
    If AutoFindCOM then
    Begin

      If (TmpPorts.Count>0) then
      begin
          S := '';
          // create message String, COM related !
          c := 0;
          while (c < TmpPorts.Count) do
          begin
            T := TmpPorts[c];
            if (S<>'') then
              S := S + ', COM ' + T
            else
              S := 'COM ' + T;
            inc(c);
          end;

          if User_Has_Stopped_COM_Scans then
           S:='L''utilisateur a abandonn� la rechercher des ports COM sur ' + S + ' ! Voulez-vous poursuivre quand m�me ?'
          else
           S:='Le programme n''a pas pu d�tecter MCMT sur ' + S + ' ! Voulez-vous poursuivre quand m�me ?'

      end
      else
      S:='Aucun port COM n''a �t� d�tect� automatiquement sur ce PC, Voulez-vous poursuivre quand m�me et le rentrer manuellement ?';


      If MessageDlg(S,mtConfirmation,[mbYes, mbNo], 0) = mrYes then
      begin
        SetupTelescope.ComboBoxNumCOM.ItemIndex := 0;
      end
      else
      begin
        SetupTelescope.Free; //Destroy...
        Exit; // calls finally
      end;

    End;
  end;

  ////////////////

  If Not AutoFindCOM then
  begin
    ProcessingBox:=TProcessingBox.create(Aowner,'Lecture version firmware....');
    ProcessingBox.Show;
    ProcessingBox.Refresh;
    if GetVersionAxes(NumCOM, 0, Mj_ra, Mn_ra, Mi_ra) then
    begin
     COM_OK := True ;
     Found  := False;
     ProcessingBox.Free;
    end
    else
    begin
      ShowMessage(Format('Connexion � MCMT II impossible sur ce port COM %d !',[NumCOM]));
      COM_OK := False;
      Found  := False;
      ProcessingBox.Free;
    end;
  end;


  if Found then
  Begin
     ProcessingBox:=TProcessingBox.create(Aowner,'Lecture version firmware et init MCMTII....');
     ProcessingBox.Show;
     ProcessingBox.Refresh;
     If not GetVersionAxes(NumCOM, 0, Mj_ra, Mn_ra, Mi_ra) or
        not GetVersionAxes(NumCOM, 1, Mj_dc, Mn_dc, Mi_dc) or
        not VersionOk(Mj_ra, Mn_ra, Mi_ra, Mj_dc, Mn_dc, Mi_dc) then
     begin
      ProcessingBox.Free;
      ShowMessage('AVERTISSEMENT : La version firmware de MCMTII n''est pas la bonne pour ce plugin, mettre � jour le firmware de MCMTII sous peine d''obtenir un fonctionnement �ratique !');
     end
     else
     ProcessingBox.Free;
  end;

  Finally
  TmpPorts.Free;
  end;

  Reg         := TRegistry.Create;
  DeviceIni   := TIniFile.Create(return_MCMT_StoreIniFilePath);

  Try
  Reg.RootKey := HKEY_CURRENT_USER;
  if not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then Reg.OpenKey('SOFTWARE\MCMTII', true);

  with DeviceIni do // Regarde dans le .INI
  begin

    If AutoFindCOM then
    Begin
      if not Found then
      begin
        SetupTelescope.ComboBoxNumCOM.Enabled := True; // On peut le rentrer a la main
        COM_OK := False; //  Sinon il va tenter de lire les valeurs de MCMT !!
      end
      else
      begin
        SetupTelescope.ComboBoxNumCOM.text    := inttostr(NumCOM);
        SetupTelescope.ComboBoxNumCOM.Enabled := False;
      end;
    End;

    If Reg.ValueExists('LastDelta') then
      STSTel.Delta := Reg.ReadFloat('LastDelta')
    else
    begin
      STSTel.Delta := strtofloat(ReadString('Configuration', 'LastDelta', '0'));
      Reg.WriteFloat('LastDelta', STSTel.Delta);
    end;

    if Reg.ValueExists('LastAlpha') then
      STSTel.Alpha := Reg.ReadFloat('LastAlpha')
    else
    begin
      STSTel.Alpha := strtofloat(ReadString('Configuration', 'LastAlpha', '0'));
      Reg.WriteFloat('LastAlpha', STSTel.Alpha);
    end;

    if Reg.ValueExists('FirmwareFile') then
      SetupTelescope.AfficheNomFichier.caption := Reg.ReadString('FirmwareFile')
    else
    begin
      SetupTelescope.AfficheNomFichier.caption := ReadString('Configuration', 'FirmwareFile', '');
      Reg.WriteString('FirmwareFile', SetupTelescope.AfficheNomFichier.caption);
    end;

    if Reg.ValueExists('DIR_AD') then
      SetupTelescope.CheckBox_DIR_AD.Checked := Reg.ReadBool('DIR_AD')
    else
    begin
      SetupTelescope.CheckBox_DIR_AD.Checked := ReadBool('Parameters', 'DIR_AD', False);
      Reg.WriteBool('DIR_AD', SetupTelescope.CheckBox_DIR_AD.Checked);
    end;

    if Reg.ValueExists('DIR_DEC') then
      SetupTelescope.CheckBox_DIR_DEC.Checked := Reg.ReadBool('DIR_DEC')
    else
    begin
      SetupTelescope.CheckBox_DIR_DEC.checked := ReadBool('Parameters', 'DIR_DEC', False);
      Reg.WriteBool('DIR_DEC', SetupTelescope.CheckBox_DIR_DEC.Checked);
    end;

    if Reg.ValueExists('PEC_AD') then
      SetupTelescope.CheckBox_PEC.Checked := Reg.ReadBool('PEC_AD')
    else
    begin
      SetupTelescope.CheckBox_PEC.checked := ReadBool('Parameters', 'PEC_AD', False);
      Reg.WriteBool('PEC_AD', SetupTelescope.CheckBox_PEC.Checked);
    end;

    If Reg.ValueExists('TimeOutMs_ConnectionsWrite') then
    begin
     SetupTelescope.NumberEditWriteTimeOutms.Text := Inttostr(Reg.ReadInteger('TimeOutMs_ConnectionsWrite'));
    end
    else
     SetupTelescope.NumberEditWriteTimeOutms.text:=Inttostr(TimeOutMs_ConnectionsWrite);


    If Reg.ValueExists('TimeOutMs_ConnectionsRead') then
    begin
       SetupTelescope.NumberEditReadTimeoutMs.Text  := Inttostr(Reg.ReadInteger('TimeOutMs_ConnectionsRead'));
    end
    else
      SetupTelescope.NumberEditReadTimeoutMs.Text :=Inttostr(TimeOutMs_ConnectionsRead);


    If Reg.ValueExists('PurgeCommMethod') then
     SetupTelescope.CheckBoxPurgCOMM.Checked:= Reg.ReadBool('PurgeCommMethod')
    else
     SetupTelescope.CheckBoxPurgCOMM.Checked:= False;


    if Reg.ValueExists('LogEnabled') then
    begin
      LogEnabled:= Reg.ReadBool('LogEnabled');
    end
    else
      LogEnabled:=False;

    SetupTelescope.CheckBoxLogFile.Checked:=LogEnabled;


    if Reg.ValueExists('Raq_Can') then
    if Reg.ReadBool('Raq_Can')
         then SetupTelescope.RadioGroup_Raquette.ItemIndex:=1
         else SetupTelescope.RadioGroup_Raquette.ItemIndex:=0
    else
    begin
      if ReadBool('Parameters', 'Raq_Can', False)
            then SetupTelescope.RadioGroup_Raquette.ItemIndex:=1
            else SetupTelescope.RadioGroup_Raquette.ItemIndex:=0;
         Reg.WriteBool('Raq_Can',(SetupTelescope.RadioGroup_Raquette.ItemIndex=1));
    end;


    SpeedIntMCMT.Inv_Codeur_AD  := SetupTelescope.CheckBox_DIR_AD.Checked;
    SpeedIntMCMT.Inv_Codeur_DEC := SetupTelescope.CheckBox_DIR_DEC.checked;

    if Reg.ValueExists('Resol_AD') then
      SpeedIntMCMT.Resol_AD := Reg.ReadInteger('Resol_AD')
    else
    begin
      SpeedIntMCMT.Resol_AD := ReadInteger('Parameters', 'Resol_AD', 4608000);
      Reg.WriteInteger('Resol_AD', SpeedIntMCMT.Resol_AD);
    end;
    SetupTelescope.Edit_Resol_AD.text := inttostr(SpeedIntMCMT.Resol_AD);

    if Reg.ValueExists('Resol_DEC') then
      SpeedIntMCMT.Resol_DEC := Reg.ReadInteger('Resol_DEC')
    else
    begin
      SpeedIntMCMT.Resol_DEC := ReadInteger('Parameters', 'Resol_DEC', 4608000);
      Reg.WriteInteger('Resol_DEC', SpeedIntMCMT.Resol_DEC);
    end;
    SetupTelescope.Edit_Resol_DEC.text := inttostr(SpeedIntMCMT.Resol_DEC);

    if Reg.ValueExists('ReducAlpha') then
      SetupTelescope.EditResolAlpha.text := FloatToStr(Reg.ReadFloat('ReducAlpha'))
    else
    begin
      SetupTelescope.EditResolAlpha.text := ReadString('Parameters', 'ReducAlpha', '360');
      Reg.WriteFloat('ReducAlpha', StrToFloat(SetupTelescope.EditResolAlpha.text));
    end;

    if Reg.ValueExists('ReducDelta') then
      SetupTelescope.EditResolDelta.text := FloatToStr(Reg.ReadFloat('ReducDelta'))
    else
    begin
      SetupTelescope.EditResolDelta.text := ReadString('Parameters', 'ReducDelta', '360');
      Reg.WriteFloat('ReducDelta', StrToFloat(SetupTelescope.EditResolDelta.text));
    end;

    if Reg.ValueExists('V_Guidage_AD') then
      SetupTelescope.Edit_V_Guidage_AD.text := Format('%1.3f',[Reg.ReadFloat('V_Guidage_AD')])
    else
    begin
      SetupTelescope.Edit_V_Guidage_AD.text := ReadString('Parameters', 'V_Guidage_AD', '1');
      Reg.WriteFloat('V_Guidage_AD', StrToFloat(SetupTelescope.Edit_V_Guidage_AD.text));
    end;

    if Reg.ValueExists('V_Guidage_DEC') then
      SetupTelescope.Edit_V_Guidage_DEC.text := Format('%1.2f',[Reg.ReadFloat('V_Guidage_DEC')])
    else
    begin
      SetupTelescope.Edit_V_Guidage_DEC.text := ReadString('Parameters', 'V_Guidage_DEC', '1');
      Reg.WriteFloat('V_Guidage_DEC', StrToFloat(SetupTelescope.Edit_V_Guidage_DEC.text));
    end;

    if Reg.ValueExists('V_Corr_P_AD') then
      SetupTelescope.Edit_V_Corr_P_AD.text := Format('%1.2f',[Reg.ReadFloat('V_Corr_P_AD')])
    else
    begin
      SetupTelescope.Edit_V_Corr_P_AD.text := ReadString('Parameters', 'V_Corr_P_AD', '1');
      Reg.WriteFloat('V_Corr_P_AD', StrToFloat(SetupTelescope.Edit_V_Corr_P_AD.text));
    end;

    if Reg.ValueExists('V_Corr_P_DEC') then
      SetupTelescope.Edit_V_Corr_P_DEC.text := Format('%1.2f',[Reg.ReadFloat('V_Corr_P_DEC')])
    else
    begin
      SetupTelescope.Edit_V_Corr_P_DEC.text := ReadString('Parameters', 'V_Corr_P_DEC', '1');
      Reg.WriteFloat('V_Corr_P_DEC', StrToFloat(SetupTelescope.Edit_V_Corr_P_DEC.text));
    end;

    if Reg.ValueExists('V_Corr_M_AD') then
      SetupTelescope.Edit_V_Corr_M_AD.text := Format('%1.2f',[Reg.ReadFloat('V_Corr_M_AD')])
    else
    begin
      SetupTelescope.Edit_V_Corr_M_AD.text := ReadString('Parameters', 'V_Corr_M_AD', '1');
      Reg.WriteFloat('V_Corr_M_AD', StrToFloat(SetupTelescope.Edit_V_Corr_M_AD.text));
    end;

    if Reg.ValueExists('V_Corr_M_DEC') then
      SetupTelescope.Edit_V_Corr_M_DEC.text := Format('%1.2f',[Reg.ReadFloat('V_Corr_M_DEC')])
    else
    begin
      SetupTelescope.Edit_V_Corr_M_DEC.text := ReadString('Parameters', 'V_Corr_M_DEC', '1');
      Reg.WriteFloat('V_Corr_M_DEC', StrToFloat(SetupTelescope.Edit_V_Corr_M_DEC.text));
    end;

    if Reg.ValueExists('V_Point_L_AD') then
      SetupTelescope.Edit_V_Point_L_AD.text := Format('%1.2f',[Reg.ReadFloat('V_Point_L_AD')])
    else
    begin
      SetupTelescope.Edit_V_Point_L_AD.text := ReadString('Parameters', 'V_Point_L_AD', '1');
      Reg.WriteFloat('V_Point_L_AD', StrToFloat(SetupTelescope.Edit_V_Point_L_AD.text));
    end;

    if Reg.ValueExists('V_Point_L_DEC') then
      SetupTelescope.Edit_V_Point_L_DEC.text := Format('%1.2f',[Reg.ReadFloat('V_Point_L_DEC')])
    else
    begin
      SetupTelescope.Edit_V_Point_L_DEC.text := ReadString('Parameters', 'V_Point_L_DEC', '1');
      Reg.WriteFloat('V_Point_L_DEC', StrToFloat(SetupTelescope.Edit_V_Point_L_DEC.text));
    end;

    if Reg.ValueExists('V_Point_R_AD') then
      SetupTelescope.Edit_V_Point_R_AD.text := Format('%1.2f',[Reg.ReadFloat('V_Point_R_AD')])
    else
    begin
      SetupTelescope.Edit_V_Point_R_AD.text := ReadString('Parameters', 'V_Point_R_AD', '1');
      Reg.WriteFloat('V_Point_R_AD', StrToFloat(SetupTelescope.Edit_V_Point_R_AD.text));
    end;

    if Reg.ValueExists('V_Point_R_DEC') then
      SetupTelescope.Edit_V_Point_R_DEC.text := Format('%1.2f',[Reg.ReadFloat('V_Point_R_DEC')])
    else
    begin
      SetupTelescope.Edit_V_Point_R_DEC.text := ReadString('Parameters', 'V_Point_R_DEC', '1');
      Reg.WriteFloat('V_Point_R_DEC', StrToFloat(SetupTelescope.Edit_V_Point_R_DEC.text));
    end;

    if Reg.ValueExists('A_Guidage_AD') then
      SetupTelescope.SpinEdit_A_Guidage_AD.value := Reg.ReadInteger('A_Guidage_AD')
    else
    begin
      SetupTelescope.SpinEdit_A_Guidage_AD.value := ReadInteger('Parameters', 'A_Guidage_AD', 25);
      Reg.WriteInteger('A_Guidage_AD', SetupTelescope.SpinEdit_A_Guidage_AD.value);
    end;

    if Reg.ValueExists('A_Guidage_DEC') then
      SetupTelescope.SpinEdit_A_Guidage_DEC.value := Reg.ReadInteger('A_Guidage_DEC')
    else
    begin
      SetupTelescope.SpinEdit_A_Guidage_DEC.value := ReadInteger('Parameters', 'A_Guidage_DEC', 25);
      Reg.WriteInteger('A_Guidage_DEC', SetupTelescope.SpinEdit_A_Guidage_DEC.value);
    end;

    if Reg.ValueExists('A_Lent_AD') then
      SetupTelescope.SpinEdit_A_Lent_AD.value := Reg.ReadInteger('A_Lent_AD')
    else
    begin
      SetupTelescope.SpinEdit_A_Lent_AD.value := ReadInteger('Parameters', 'A_Lent_AD', 25);
      Reg.WriteInteger('A_Lent_AD', SetupTelescope.SpinEdit_A_Lent_AD.value);
    end;

    if Reg.ValueExists('A_Lent_DEC') then
      SetupTelescope.SpinEdit_A_Lent_DEC.value := Reg.ReadInteger('A_Lent_DEC')
    else
    begin
      SetupTelescope.SpinEdit_A_Lent_DEC.value := ReadInteger('Parameters', 'A_Lent_DEC', 25);
      Reg.WriteInteger('A_Lent_DEC', SetupTelescope.SpinEdit_A_Lent_DEC.value);
    end;

    if Reg.ValueExists('A_Rapide_AD') then
      SetupTelescope.SpinEdit_A_Rapide_AD.value := Reg.ReadInteger('A_Rapide_AD')
    else
    begin
      SetupTelescope.SpinEdit_A_Rapide_AD.value := ReadInteger('Parameters', 'A_Rapide_AD', 25);
      Reg.WriteInteger('A_Rapide_AD', SetupTelescope.SpinEdit_A_Rapide_AD.value);
    end;

    if Reg.ValueExists('A_Rapide_DEC') then
      SetupTelescope.SpinEdit_A_Rapide_DEC.value := Reg.ReadInteger('A_Rapide_DEC')
    else
    begin
      SetupTelescope.SpinEdit_A_Rapide_DEC.value := ReadInteger('Parameters', 'A_Rapide_DEC', 25);
      Reg.WriteInteger('A_Rapide_DEC', SetupTelescope.SpinEdit_A_Rapide_DEC.value);
    end;

    if Reg.ValueExists('Accel_AD') then
      SetupTelescope.ComboBox_Accel_AD.ItemIndex := Reg.ReadInteger('Accel_AD')
    else
    begin
      SetupTelescope.ComboBox_Accel_AD.ItemIndex := strtoint(ReadString('Parameters', 'Accel_AD', '0'));
      Reg.WriteInteger('Accel_AD', SetupTelescope.ComboBox_Accel_AD.ItemIndex);
    end;

    if Reg.ValueExists('Accel_DEC') then
      SetupTelescope.ComboBox_Accel_DEC.ItemIndex := Reg.ReadInteger('Accel_DEC')
    else
    begin
      SetupTelescope.ComboBox_Accel_DEC.ItemIndex := strtoint(ReadString('Parameters', 'Accel_DEC', '0'));
      Reg.WriteInteger('Accel_DEC', SetupTelescope.ComboBox_Accel_DEC.ItemIndex);
    end;

    if Reg.ValueExists('Backlash_Activated') then Backlash.Activated:= Reg.ReadBool('Backlash_Activated')
    else
    begin
      Backlash.Activated:=ReadBool('Parameters', 'Backlash_Activated', false);
      Reg.WriteBool('Backlash_Activated', Backlash.Activated);
    end;
    SetupTelescope.CheckBoxActivateBacklash.Checked           := Backlash.Activated;
    SetupTelescope.GroupBox_Parametres_Correction_Jeux.Visible:= Backlash.Activated;


    if Reg.ValueExists('Backlash') then
      SetupTelescope.NbreEditBacklash.Text := FloatToStr(Reg.ReadFloat('Backlash'))
    else
    begin
      SetupTelescope.NbreEditBacklash.Text := ReadString('Parameters', 'Backlash', '20');
      Reg.WriteFloat('Backlash', StrToFloat(SetupTelescope.NbreEditBacklash.Text));
    end;

    if Reg.ValueExists('Backlash_Delta_Mode')
        then SetupTelescope.RadioGroup_Mode_Correction_Delta.ItemIndex :=
                                        Reg.ReadInteger('Backlash_Delta_Mode')
    else
    begin
      SetupTelescope.RadioGroup_Mode_Correction_Delta.ItemIndex :=ReadInteger('Parameters', 'Backlash_Delta_Mode', 0);
      Reg.WriteInteger('Backlash_Delta_Mode',SetupTelescope.RadioGroup_Mode_Correction_Delta.ItemIndex);
    end;

    if Reg.ValueExists('Backlash_Pause') then
      SetupTelescope.ComboBoxPauseBacklash.ItemIndex := Reg.ReadInteger('Backlash_Pause') div 1000
    else
    begin
      SetupTelescope.ComboBoxPauseBacklash.ItemIndex := ReadInteger('Parameters', 'Backlash_Pause', 0);
      Reg.WriteInteger('Backlash_Pause', SetupTelescope.ComboBoxPauseBacklash.ItemIndex * 1000);
    end;

     // Type de monture *****************************************************
     if Reg.ValueExists('Monture_Allemande')
          then GerMountSetup.Monture_Allemande:=Reg.ReadBool('Monture_Allemande')
        else
          begin
            GerMountSetup.Monture_Allemande :=
                 ReadBool('Pointage', 'Monture_Allemande', false);
            Reg.WriteBool('Monture_Allemande',GerMountSetup.Monture_Allemande);
          end;

        if GerMountSetup.Monture_Allemande then
        begin
            SetupTelescope.RadioGroupTypeMonture.ItemIndex :=1;
            SetupTelescope.GroupBoxMontureAllemande.Visible:=true;
            SetupTelescope.GroupBoxMonturefourche.Visible  :=false;
            If (LongLat.Lat>0) then
            begin
              SetupTelescope.Image_Pied.Visible     :=true;
//              Form_Etat_Telescope.Image_Pied.Visible:=true  pas reference ici
            end
            else
            begin
              SetupTelescope.Image_Pied_HemisphereSud.Visible     :=true
//              Form_Etat_Telescope.Image_Pied_HemisphereSud.Visible:=true pas reference ici
            end;
        end
        else
        begin
            SetupTelescope.RadioGroupTypeMonture.ItemIndex:=0;
            SetupTelescope.GroupBoxMontureAllemande.Visible:=false;
            SetupTelescope.GroupBoxMonturefourche.Visible:=true;
         end;

      // Monture � fourche *****************************************************
      if Reg.ValueExists('Passage_Interdit')
        then GerMountSetup.Passage_Interdit:=Reg.ReadBool('Passage_Interdit')
        else
          begin
            GerMountSetup.Passage_Interdit :=ReadBool('Pointage', 'Passage_Interdit', false);
            Reg.WriteBool('Passage_Interdit',GerMountSetup.Passage_Interdit);
          end;
      if GerMountSetup.Passage_Interdit
        then
          begin
            SetupTelescope.RadioGroupAlgorithmePointage.ItemIndex:=1;
            SetupTelescope.Image_Antimeridien.Visible:=false;
            SetupTelescope.Image_Meridien.Visible:=true;
          end
        else
          begin
            SetupTelescope.RadioGroupAlgorithmePointage.ItemIndex:=0;
            SetupTelescope.Image_Antimeridien.Visible:=true;
            SetupTelescope.Image_Meridien.Visible:=false;
          end;


      // Monture allemande *****************************************************
      if Reg.ValueExists('TelescopeZenithOuest')
          then GerMountSetup.TelescopeZenithOuest:=Reg.ReadBool('TelescopeZenithOuest')
          else GerMountSetup.TelescopeZenithOuest:=false;
      if GerMountSetup.TelescopeZenithOuest
        then
          begin
            SetupTelescope.RadioGroupEtatTelescope.ItemIndex:=1;
            SetupTelescope.Image_Zenith_Est.Visible:=false;
            SetupTelescope.Image_Zenith_Ouest.Visible:=true;
          end
        else
          begin
            SetupTelescope.RadioGroupEtatTelescope.ItemIndex:=0;
            SetupTelescope.Image_Zenith_Est.Visible:=true;
            SetupTelescope.Image_Zenith_Ouest.Visible:=false;
          end;

      if Reg.ValueExists('Retournement_Autorise')
          then GerMountSetup.Retournement_Autorise:=Reg.ReadBool('Retournement_Autorise')
          else
            begin
              GerMountSetup.Retournement_Autorise:=
                     ReadBool('Pointage', 'Retournement_Autorise', false);
              Reg.WriteBool('Retournement_Autorise',GerMountSetup.Retournement_Autorise)
            end;

      SetupTelescope.GroupBox_Declinaisons_critiques.Visible:=
                                                   GerMountSetup.Retournement_Autorise;
      SetupTelescope.CheckBox_Retournement_Autorise.Checked:=
                                                   GerMountSetup.Retournement_Autorise;

      if Reg.ValueExists('Degagement_Est')
        then SetupTelescope.SpinEdit_Degagement_Est.value :=
                                 Round(Reg.ReadFloat('Degagement_Est')/Pi*12*60)
        else
          begin
            SetupTelescope.SpinEdit_Degagement_Est.value :=
                  Round(ReadFloat('Pointage', 'Degagement_Est', Pi/12));
            Reg.WriteFloat('Degagement_Est',
                         SetupTelescope.SpinEdit_Degagement_Est.value*Pi/12/60);
          end;

      if Reg.ValueExists('Degagement_Ouest')
        then SetupTelescope.SpinEdit_Degagement_Ouest.value :=
                            Round(Reg.ReadFloat('Degagement_Ouest')/Pi*12*60)
        else
          begin
            SetupTelescope.SpinEdit_Degagement_Ouest.value :=
            Round(ReadFloat('Pointage', 'Degagement_Ouest', Pi/12));
            Reg.WriteFloat('Degagement_Ouest',
                       SetupTelescope.SpinEdit_Degagement_Ouest.value*Pi/12/60);
          end;

      if Reg.ValueExists('Declinaison_critique_Est')
        then SetupTelescope.SpinEdit_Declinaison_critique_Est.value :=
                         Round(Reg.ReadFloat('Declinaison_critique_Est')/Pi*180)
        else
          begin
            SetupTelescope.SpinEdit_Declinaison_critique_Est.value :=
            Round(ReadFloat('Pointage', 'Declinaison_critique_Est', 0));
            Reg.WriteFloat('Declinaison_critique_Est',
              SetupTelescope.SpinEdit_Declinaison_critique_Est.value*Pi/180);
          end;

        if Reg.ValueExists('Declinaison_critique_Ouest')
        then SetupTelescope.SpinEdit_Declinaison_critique_Ouest.value :=
                        Round(Reg.ReadFloat('Declinaison_critique_Ouest')/Pi*180)
        else
          begin
            SetupTelescope.SpinEdit_Declinaison_critique_Ouest.value :=
            Round(ReadFloat('Pointage', 'Declinaison_critique_Ouest', 0));
            Reg.WriteFloat('Declinaison_critique_Ouest',
              SetupTelescope.SpinEdit_Declinaison_critique_Ouest.value*Pi/180);
          end;

        // Longitude

        if Reg.ValueExists('Longitude') then
          LongSetup := Reg.ReadFloat('Longitude')
        else
          begin
            LongSetup := 0;
            Reg.WriteFloat('Longitude', LongSetup);
          end;
        if LongSetup<0 then
        begin
          SetupTelescope.CheckBoxEast.Checked := true;
          LongSetup:=-LongSetup;
        end
        else
          SetupTelescope.CheckBoxEast.Checked := false;

        LongSetup:=LongSetup*180/pi;
        SetupTelescope.NbreEditLongDeg.Text := IntToStr(trunc(LongSetup));
        LongSetup:=(LongSetup-int(LongSetup))*60;
        SetupTelescope.NbreEditLongMin.Text := IntToStr(trunc(LongSetup));
        LongSetup:=(LongSetup-int(LongSetup))*60;
        SetupTelescope.NbreEditLongSec.Text := IntToStr(trunc(LongSetup));
        LongSetup:=(LongSetup-int(LongSetup))*100;
        SetupTelescope.NbreEditLongSec100.Text := IntToStr(trunc(LongSetup));

        // Latitude
        If Reg.ValueExists('Latitude') then
          LatSetup := Reg.ReadFloat('Latitude')
        else
        begin
            LatSetup := 0;
            Reg.WriteFloat('Latitude',LatSetup);
        end;

        If (LatSetup<0) then
        begin
          SetupTelescope.CheckBoxSouth.Checked := true;
          LatSetup:=-LatSetup;
        end
        else
          SetupTelescope.CheckBoxSouth.Checked := false;

        LatSetup:=LatSetup*180/pi;
        SetupTelescope.NbreEditLatDeg.Text := IntToStr(trunc(LatSetup));
        LatSetup:=(LatSetup-int(LatSetup))*60;
        SetupTelescope.NbreEditLatMin.Text := IntToStr(trunc(LatSetup));
        LatSetup:=(LatSetup-int(LatSetup))*60;
        SetupTelescope.NbreEditLatSec.Text := IntToStr(trunc(LatSetup));
        LatSetup:=(LatSetup-int(LatSetup))*100;
        SetupTelescope.NbreEditLatSec100.Text := IntToStr(trunc(LatSetup));

        // Altitude

        if Reg.ValueExists('Altitude') then
          AltSetup := Reg.ReadFloat('Altitude')
        else
          begin
            AltSetup := 0;
            Reg.WriteFloat('Altitude',AltSetup);
          end;
        SetupTelescope.NbreEditAltitude.Text:= FloatToStr(AltSetup);

  end;

  Reg.CloseKey;

  Finally
  DeviceIni.free;
  Reg.Free;
  End;


  ErrCode := RegOpenKeyEx(HKEY_LOCAL_MACHINE,'Software\ASCOM\Telescope Drivers\MCMTII.Telescope',0,KEY_READ,KeyHandle);


  If (ErrCode = ERROR_SUCCESS) then
  begin
      Try
        Chsr := CreateOLEObject('DriverHelper.Profile');

        Try
        SetupTelescope.NbreEditPeriode_PEC.Text   := TestString(Chsr.GetValue('MCMTII.Telescope', 'Periode_PEC'));
        SetupTelescope.NbreEditPeriode_PEC_1.Text := TestString(Chsr.GetValue('MCMTII.Telescope', 'Periode_PEC_1'));
        SetupTelescope.NbreEditPeriode_PEC_2.Text := TestString(Chsr.GetValue('MCMTII.Telescope', 'Periode_PEC_2'));
        SetupTelescope.NbreEditPeriode_PEC_3.Text := TestString(Chsr.GetValue('MCMTII.Telescope', 'Periode_PEC_3'));
        SetupTelescope.NbreEditPhase_PEC.Text     := TestString(Chsr.GetValue('MCMTII.Telescope', 'Phase_PEC'));
        SetupTelescope.NbreEditPhase_PEC_1.Text   := TestString(Chsr.GetValue('MCMTII.Telescope', 'Phase_PEC_1'));
        SetupTelescope.NbreEditPhase_PEC_2.Text   := TestString(Chsr.GetValue('MCMTII.Telescope', 'Phase_PEC_2'));
        SetupTelescope.NbreEditPhase_PEC_3.Text   := TestString(Chsr.GetValue('MCMTII.Telescope', 'Phase_PEC_3'));
        SetupTelescope.NbreEditAmplitude_PEC.Text := TestString(Chsr.GetValue('MCMTII.Telescope', 'Amplitude_PEC'));
        SetupTelescope.NbreEditAmplitude_PEC_1.Text := TestString(Chsr.GetValue('MCMTII.Telescope', 'Amplitude_PEC_1'));
        SetupTelescope.NbreEditAmplitude_PEC_2.Text := TestString(Chsr.GetValue('MCMTII.Telescope', 'Amplitude_PEC_2'));
        SetupTelescope.NbreEditAmplitude_PEC_3.Text := TestString(Chsr.GetValue('MCMTII.Telescope', 'Amplitude_PEC_3'));

        if Chsr.GetValue('MCMTII.Telescope', 'King_ASCOM_Activated') = 'Yes' then SetupTelescope.CheckBoxActivateKing.Checked := true
        else SetupTelescope.CheckBoxActivateKing.Checked := false;

        if Chsr.GetValue('MCMTII.Telescope', 'PEC_ASCOM_Activated') = 'Yes' then SetupTelescope.CheckBoxActivatePEC.Checked := true
        else SetupTelescope.CheckBoxActivatePEC.Checked := false;

        if SetupTelescope.CheckBoxActivatePEC.Checked then
        begin
          SetupTelescope.label62.Enabled := true;
          SetupTelescope.label63.Enabled := true;
          SetupTelescope.label64.Enabled := true;
          SetupTelescope.label65.Enabled := true;
          SetupTelescope.label66.Enabled := true;
          SetupTelescope.label67.Enabled := true;
          SetupTelescope.label68.Enabled := true;
          SetupTelescope.NbreEditAmplitude_PEC.Enabled := true;
          SetupTelescope.NbreEditAmplitude_PEC_1.Enabled := true;
          SetupTelescope.NbreEditAmplitude_PEC_2.Enabled := true;
          SetupTelescope.NbreEditAmplitude_PEC_3.Enabled := true;
          SetupTelescope.NbreEditPeriode_PEC.Enabled := true;
          SetupTelescope.NbreEditPeriode_PEC_1.Enabled := true;
          SetupTelescope.NbreEditPeriode_PEC_2.Enabled := true;
          SetupTelescope.NbreEditPeriode_PEC_3.Enabled := true;
          SetupTelescope.NbreEditPhase_PEC.Enabled := true;
          SetupTelescope.NbreEditPhase_PEC_1.Enabled := true;
          SetupTelescope.NbreEditPhase_PEC_2.Enabled := true;
          SetupTelescope.NbreEditPhase_PEC_3.Enabled := true;
        end
        else
        begin
          SetupTelescope.label62.Enabled := false;
          SetupTelescope.label63.Enabled := false;
          SetupTelescope.label64.Enabled := false;
          SetupTelescope.label65.Enabled := false;
          SetupTelescope.label66.Enabled := false;
          SetupTelescope.label67.Enabled := false;
          SetupTelescope.label68.Enabled := false;
          SetupTelescope.NbreEditAmplitude_PEC.Enabled := false;
          SetupTelescope.NbreEditAmplitude_PEC_1.Enabled := false;
          SetupTelescope.NbreEditAmplitude_PEC_2.Enabled := false;
          SetupTelescope.NbreEditAmplitude_PEC_3.Enabled := false;
          SetupTelescope.NbreEditPeriode_PEC.Enabled := false;
          SetupTelescope.NbreEditPeriode_PEC_1.Enabled := false;
          SetupTelescope.NbreEditPeriode_PEC_2.Enabled := false;
          SetupTelescope.NbreEditPeriode_PEC_3.Enabled := false;
          SetupTelescope.NbreEditPhase_PEC.Enabled := false;
          SetupTelescope.NbreEditPhase_PEC_1.Enabled := false;
          SetupTelescope.NbreEditPhase_PEC_2.Enabled := false;
          SetupTelescope.NbreEditPhase_PEC_3.Enabled := false;
        end;
        Finally
        Chsr := Unassigned;
        End;
      Except
      // Driver Ascom non install�
      end;
  end;

  SetupTelescope.CheckBoxAutoCOM.Checked:=AutoFindCOM;

  // Mettre a jour avec la vraie config contenue dans MCMT II
  If COM_OK then
    SetupTelescope.Button_LireClick(nil);

  SetupTelescope.creating:=False;

  ///////////////  ///////////////  ///////////////  ///////////////  ///////////////  ///////////////
  if (SetupTelescope.ShowModal = MrOK) then // Uniquement si *Validation* !
  ///////////////  ///////////////  ///////////////  ///////////////  ///////////////  ///////////////
  begin

    ErrCode := RegOpenKeyEx(HKEY_LOCAL_MACHINE,'Software\ASCOM\Telescope Drivers\MCMTII.Telescope',0,KEY_READ,KeyHandle);

    if (ErrCode = ERROR_SUCCESS) then
    begin
      Try
        Chsr := CreateOLEObject('DriverHelper.Profile');

        Chsr.WriteValue('MCMTII.Telescope', 'Periode_PEC', SetupTelescope.NbreEditPeriode_PEC.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Periode_PEC_1', SetupTelescope.NbreEditPeriode_PEC_1.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Periode_PEC_2', SetupTelescope.NbreEditPeriode_PEC_2.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Periode_PEC_3', SetupTelescope.NbreEditPeriode_PEC_3.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Phase_PEC', SetupTelescope.NbreEditPhase_PEC.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Phase_PEC_1', SetupTelescope.NbreEditPhase_PEC_1.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Phase_PEC_2', SetupTelescope.NbreEditPhase_PEC_2.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Phase_PEC_3', SetupTelescope.NbreEditPhase_PEC_3.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Amplitude_PEC', SetupTelescope.NbreEditAmplitude_PEC.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Amplitude_PEC_1', SetupTelescope.NbreEditAmplitude_PEC_1.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Amplitude_PEC_2', SetupTelescope.NbreEditAmplitude_PEC_2.Text);
        Chsr.WriteValue('MCMTII.Telescope', 'Amplitude_PEC_3', SetupTelescope.NbreEditAmplitude_PEC_3.Text);

        if SetupTelescope.CheckBoxActivateKing.Checked then
          Chsr.WriteValue('MCMTII.Telescope', 'King_ASCOM_Activated', 'Yes')
        else
          Chsr.WriteValue('MCMTII.Telescope', 'King_ASCOM_Activated', 'No');
        if SetupTelescope.CheckBoxActivatePEC.Checked then
          Chsr.WriteValue('MCMTII.Telescope', 'PEC_ASCOM_Activated', 'Yes')
        else
          Chsr.WriteValue('MCMTII.Telescope', 'PEC_ASCOM_Activated', 'No');
      except
             // Driver Ascom NON install� !!
      end;
    end;


    DeviceIni   := TIniFile.Create(return_MCMT_StoreIniFilePath);
    Reg         := TRegistry.Create;

    Try
    Reg.RootKey := HKEY_CURRENT_USER;

    if not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then Reg.OpenKey('SOFTWARE\MCMTII', true);
    with DeviceIni do
    begin
      AutoFindCOM:= SetupTelescope.CheckBoxAutoCOM.Checked;
      if AutoFindCOM then
       NumCom     := StrToInt(SetupTelescope.ComboBoxNumCOM.text)
      else
       NumCom     := StrToInt(SetupTelescope.NbreEditPortSerieForced.text);


      WriteInteger('Configuration', 'Port', NumCOM);
      Reg.WriteInteger('Port'             , NumCOM);
      Reg.WriteBool   ('FindAutoPort'     , AutoFindCOM);

      WriteString('Configuration', 'FirmwareFile', SetupTelescope.OpenDialog1.filename);
      Reg.WriteString('FirmwareFile', SetupTelescope.OpenDialog1.filename);

      WriteBool('Parameters', 'DIR_AD', SetupTelescope.CheckBox_DIR_AD.Checked);
      Reg.WriteBool('DIR_AD', SetupTelescope.CheckBox_DIR_AD.Checked);

      WriteBool('Parameters', 'DIR_DEC', SetupTelescope.CheckBox_DIR_DEC.checked);
      Reg.WriteBool('DIR_DEC', SetupTelescope.CheckBox_DIR_DEC.checked);

      WriteBool('Parameters', 'PEC_AD', SetupTelescope.CheckBox_PEC.checked);
      Reg.WriteBool('PEC_AD', SetupTelescope.CheckBox_PEC.checked);

      WriteBool('Parameters', 'Raq_Can',(SetupTelescope.RadioGroup_Raquette.ItemIndex=1));
      Reg.WriteBool('Raq_Can',          (SetupTelescope.RadioGroup_Raquette.ItemIndex=1));


      SpeedIntMCMT.Inv_Codeur_AD := SetupTelescope.CheckBox_DIR_AD.checked;
      SpeedIntMCMT.Inv_Codeur_DEC := SetupTelescope.CheckBox_DIR_DEC.checked;

      SpeedIntMCMT.Resol_AD := strtoint(SetupTelescope.Edit_Resol_AD.text);
      WriteInteger('Parameters', 'G_Resol_AD', SpeedIntMCMT.Resol_AD);
      Reg.WriteInteger('G_Resol_AD', SpeedIntMCMT.Resol_AD);

      SpeedIntMCMT.Resol_DEC := strtoint(SetupTelescope.Edit_Resol_DEC.text);
      WriteInteger('Parameters', 'Resol_DEC', SpeedIntMCMT.Resol_DEC);
      Reg.WriteInteger('Resol_DEC', SpeedIntMCMT.Resol_DEC);

      WriteString('Parameters', 'ReducAlpha', SetupTelescope.EditResolAlpha.text);
      Reg.WriteFloat('ReducAlpha', StrToFloat(SetupTelescope.EditResolAlpha.text));

      WriteString('Parameters', 'ReducDelta', SetupTelescope.EditResolDelta.text);
      Reg.WriteFloat('ReducDelta', StrToFloat(SetupTelescope.EditResolDelta.text));

      WriteString('Parameters', 'V_Guidage_AD', SetupTelescope.Edit_V_Guidage_AD.text);
      SpeedIntMCMT.V_Guidage_AD := strtofloat(SetupTelescope.Edit_V_Guidage_AD.text);
      Reg.WriteFloat('V_Guidage_AD', SpeedIntMCMT.V_Guidage_AD);
      SpeedIntMCMT.V_Guidage_AD_SID := SpeedIntMCMT.V_Guidage_AD;

      WriteString('Parameters', 'V_Guidage_DEC', SetupTelescope.Edit_V_Guidage_DEC.text);
      SpeedIntMCMT.V_Guidage_DEC := strtofloat(SetupTelescope.Edit_V_Guidage_DEC.text);
      Reg.WriteFloat('V_Guidage_DEC', SpeedIntMCMT.V_Guidage_DEC);

      WriteString('Parameters', 'V_Corr_P_AD', SetupTelescope.Edit_V_Corr_P_AD.text);
      SpeedIntMCMT.V_Corr_P_AD := strtofloat(SetupTelescope.Edit_V_Corr_P_AD.text);
      Reg.WriteFloat('V_Corr_P_AD', SpeedIntMCMT.V_Corr_P_AD);

      WriteString('Parameters', 'V_Corr_P_DEC', SetupTelescope.Edit_V_Corr_P_DEC.text);
      SpeedIntMCMT.V_Corr_P_DEC := strtofloat(SetupTelescope.Edit_V_Corr_P_DEC.text);
      Reg.WriteFloat('V_Corr_P_DEC', SpeedIntMCMT.V_Corr_P_DEC);

      WriteString('Parameters', 'V_Corr_M_AD', SetupTelescope.Edit_V_Corr_M_AD.text);
      SpeedIntMCMT.V_Corr_M_AD := strtofloat(SetupTelescope.Edit_V_Corr_M_AD.text);
      Reg.WriteFloat('V_Corr_M_AD', SpeedIntMCMT.V_Corr_M_AD);

      WriteString('Parameters', 'V_Corr_M_DEC', SetupTelescope.Edit_V_Corr_M_DEC.text);
      SpeedIntMCMT.V_Corr_M_DEC := strtofloat(SetupTelescope.Edit_V_Corr_M_DEC.text);
      Reg.WriteFloat('V_Corr_M_DEC', SpeedIntMCMT.V_Corr_M_DEC);

      WriteString('Parameters', 'V_Point_L_AD', SetupTelescope.Edit_V_Point_L_AD.text);
      SpeedIntMCMT.V_Point_L_AD := strtofloat(SetupTelescope.Edit_V_Point_L_AD.text);
      Reg.WriteFloat('V_Point_L_AD', SpeedIntMCMT.V_Point_L_AD);

      WriteString('Parameters', 'V_Point_L_DEC', SetupTelescope.Edit_V_Point_L_DEC.text);
      SpeedIntMCMT.V_Point_L_DEC := strtofloat(SetupTelescope.Edit_V_Point_L_DEC.text);
      Reg.WriteFloat('V_Point_L_DEC', SpeedIntMCMT.V_Point_L_DEC);

      WriteString('Parameters', 'V_Point_R_AD', SetupTelescope.Edit_V_Point_R_AD.text);
      SpeedIntMCMT.V_Point_R_AD := strtofloat(SetupTelescope.Edit_V_Point_R_AD.text);
      Reg.WriteFloat('V_Point_R_AD', SpeedIntMCMT.V_Point_R_AD);

      WriteString('Parameters', 'V_Point_R_DEC', SetupTelescope.Edit_V_Point_R_DEC.text);
      SpeedIntMCMT.V_Point_R_DEC := strtofloat(SetupTelescope.Edit_V_Point_R_DEC.text);
      Reg.WriteFloat('V_Point_R_DEC', SpeedIntMCMT.V_Point_R_DEC);

      WriteInteger('Parameters', 'A_Guidage_AD', SetupTelescope.SpinEdit_A_Guidage_AD.value);
      Reg.WriteInteger('A_Guidage_AD', SetupTelescope.SpinEdit_A_Guidage_AD.value);

      WriteInteger('Parameters', 'A_Guidage_DEC', SetupTelescope.SpinEdit_A_Guidage_DEC.value);
      Reg.WriteInteger('A_Guidage_DEC', SetupTelescope.SpinEdit_A_Guidage_DEC.value);

      WriteInteger('Parameters', 'A_Lent_AD', SetupTelescope.SpinEdit_A_Lent_AD.value);
      Reg.WriteInteger('A_Lent_AD', SetupTelescope.SpinEdit_A_Lent_AD.value);

      WriteInteger('Parameters', 'A_Lent_DEC', SetupTelescope.SpinEdit_A_Lent_DEC.value);
      Reg.WriteInteger('A_Lent_DEC', SetupTelescope.SpinEdit_A_Lent_DEC.value);

      WriteInteger('Parameters', 'A_Rapide_AD', SetupTelescope.SpinEdit_A_Rapide_AD.value);
      Reg.WriteInteger('A_Rapide_AD', SetupTelescope.SpinEdit_A_Rapide_AD.value);

      WriteInteger('Parameters', 'A_Rapide_DEC', SetupTelescope.SpinEdit_A_Rapide_DEC.value);
      Reg.WriteInteger('A_Rapide_DEC', SetupTelescope.SpinEdit_A_Rapide_DEC.value);

      WriteString('Parameters', 'Accel_AD', inttostr(SetupTelescope.ComboBox_Accel_AD.ItemIndex));
      SpeedIntMCMT.Accel_AD := SetupTelescope.ComboBox_Accel_AD.ItemIndex;
      Reg.WriteInteger('Accel_AD', SpeedIntMCMT.Accel_AD);

      WriteString('Parameters', 'Accel_DEC', inttostr(SetupTelescope.ComboBox_Accel_DEC.ItemIndex));
      SpeedIntMCMT.Accel_DEC := SetupTelescope.ComboBox_Accel_DEC.ItemIndex;
      Reg.WriteInteger('Accel_DEC', SpeedIntMCMT.Accel_DEC);

      WriteBool('Parameters', 'Backlash_Activated', SetupTelescope.CheckBoxActivateBacklash.Checked);
      Backlash.Activated := SetupTelescope.CheckBoxActivateBacklash.Checked;
      Reg.WriteBool('Backlash_Activated', Backlash.Activated);

      WriteString('Parameters', 'Backlash', SetupTelescope.NbreEditBacklash.Text);
      Backlash.Value := StrToFloat(SetupTelescope.NbreEditBacklash.Text);
      Reg.WriteFloat('Backlash', Backlash.Value);

      WriteInteger('Parameters', 'Backlash_Delta_Type',SetupTelescope.RadioGroup_Mode_Correction_Delta.ItemIndex);         //JLH
      Backlash.Delta_Mode:=SetupTelescope.RadioGroup_Mode_Correction_Delta.ItemIndex;
      Reg.WriteInteger('Backlash_Delta_Mode', Backlash.Delta_Mode);

      LogEnabled:=SetupTelescope.CheckBoxLogFile.Checked;
      Reg.WriteBool('LogEnabled',LogEnabled);

      WriteInteger('Parameters', 'Backlash_Pause', SetupTelescope.ComboBoxPauseBacklash.ItemIndex);
      Backlash.Pause := SetupTelescope.ComboBoxPauseBacklash.ItemIndex * 1000;
      Reg.WriteInteger('Backlash_Pause', Backlash.Pause);

      // Type de monture ***************************************************
      if SetupTelescope.RadioGroupTypeMonture.ItemIndex=0
          then GerMountSetup.Monture_Allemande:= false
          else GerMountSetup.Monture_Allemande:= true;
      WriteBool('Pointage', 'Monture_Allemande', GerMountSetup.Monture_Allemande);
      Reg.WriteBool('Monture_Allemande', GerMountSetup.Monture_Allemande);


      // Monture � fourche***************************************************
      if SetupTelescope.RadioGroupAlgorithmePointage.ItemIndex=0
          then GerMountSetup.Passage_Interdit:=false
          else GerMountSetup.Passage_Interdit:=true;
      Reg.WriteBool('Passage_Interdit',GerMountSetup.Passage_Interdit);
      WriteBool('Pointage', 'Passage_Interdit', GerMountSetup.Passage_Interdit);


      // Monture allemande ***************************************************
      GerMountSetup.Degagement_Est:=SetupTelescope.SpinEdit_Degagement_Est.value*Pi/12/60;
      WriteFloat('Pointage', 'Degagement_Est',GerMountSetup.Degagement_Est);
      Reg.WriteFloat('Degagement_Est', GerMountSetup.Degagement_Est);

      GerMountSetup.Degagement_Ouest:=SetupTelescope.SpinEdit_Degagement_Ouest.value*Pi/720;
      WriteFloat('Pointage', 'Degagement_Ouest',GerMountSetup.Degagement_Ouest);
      Reg.WriteFloat('Degagement_Ouest', GerMountSetup.Degagement_Ouest);

      GerMountSetup.Declinaison_critique_Est:=SetupTelescope.SpinEdit_Declinaison_critique_Est.value*Pi/180;
      WriteFloat('Pointage', 'Declinaison_critique_Est',GerMountSetup.Declinaison_critique_Est);
      Reg.WriteFloat('Declinaison_critique_Est', GerMountSetup.Declinaison_critique_Est);

      GerMountSetup.Declinaison_critique_Ouest:=SetupTelescope.SpinEdit_Declinaison_critique_Ouest.value*Pi/180;
      WriteFloat('Pointage', 'Declinaison_critique_Ouest',GerMountSetup.Declinaison_critique_Ouest);
      Reg.WriteFloat('Declinaison_critique_Ouest', GerMountSetup.Declinaison_critique_Ouest);

      GerMountSetup.Retournement_autorise:=SetupTelescope.CheckBox_Retournement_autorise.checked;
      WriteBool('Pointage', 'Retournement_autorise',GerMountSetup.Retournement_autorise);
      Reg.WriteBool('Retournement_autorise', GerMountSetup.Retournement_autorise);

      GerMountSetup.TelescopeZenithOuest:=(SetupTelescope.RadioGroupEtatTelescope.ItemIndex=1);
      Reg.WriteBool('TelescopeZenithOuest',GerMountSetup.TelescopeZenithOuest);

      King_Ascom_Activated:= SetupTelescope.CheckBoxActivateKing.Checked;
      PEC_Ascom_Activated:= SetupTelescope.CheckBoxActivatePEC.Checked;

      LongSetup:=StrToFloat(SetupTelescope.NbreEditLongSec100.Text)/100;
      LongSetup:=(LongSetup+StrToFloat(SetupTelescope.NbreEditLongSec.Text))/60;
      LongSetup:=(LongSetup+StrToFloat(SetupTelescope.NbreEditLongMin.Text))/60;
      LongSetup:=(LongSetup+StrToFloat(SetupTelescope.NbreEditLongDeg.Text))/180*pi;
      if SetupTelescope.CheckBoxEast.Checked = true then LongSetup:=-LongSetup;
      Reg.WriteFloat('Longitude', LongSetup);

      LatSetup:=StrToFloat(SetupTelescope.NbreEditLatSec100.Text)/100;
      LatSetup:=(LatSetup+StrToFloat(SetupTelescope.NbreEditLatSec.Text))/60;
      LatSetup:=(LatSetup+StrToFloat(SetupTelescope.NbreEditLatMin.Text))/60;
      LatSetup:=(LatSetup+StrToFloat(SetupTelescope.NbreEditLatDeg.Text))/180*pi;
      if SetupTelescope.CheckBoxSouth.Checked = true then LatSetup:=-LatSetup;
      Reg.WriteFloat('Latitude', LatSetup);

      Reg.WriteFloat('Altitude', StrToFloat(SetupTelescope.NbreEditAltitude.Text));

      TimeOutMs_ConnectionsWrite:=StrToInt(SetupTelescope.NumberEditWriteTimeOutms.Text);
      Reg.WriteInteger('TimeOutMs_ConnectionsWrite',TimeOutMs_ConnectionsWrite);

      TimeOutMs_ConnectionsRead:=StrToInt(SetupTelescope.NumberEditReadTimeoutMs.Text  );
      Reg.WriteInteger('TimeOutMs_ConnectionsRead',TimeOutMs_ConnectionsRead);

      PurgeCommMethod:=SetupTelescope.CheckBoxPurgCOMM.Checked;
      Reg.WriteBool('PurgeCommMethod',PurgeCommMethod);


      Amplitude_PEC_Ascom[0]:= StrToFloat(SetupTelescope.NbreEditAmplitude_PEC.Text);
      Amplitude_PEC_Ascom[1]:= StrToFloat(SetupTelescope.NbreEditAmplitude_PEC_1.Text);
      Amplitude_PEC_Ascom[2]:= StrToFloat(SetupTelescope.NbreEditAmplitude_PEC_2.Text);
      Amplitude_PEC_Ascom[3]:= StrToFloat(SetupTelescope.NbreEditAmplitude_PEC_3.Text);
      Phase_PEC_Ascom[0]    := StrToFloat(SetupTelescope.NbreEditPhase_PEC.Text);
      Phase_PEC_Ascom[1]    := StrToFloat(SetupTelescope.NbreEditPhase_PEC_1.Text);
      Phase_PEC_Ascom[2]    := StrToFloat(SetupTelescope.NbreEditPhase_PEC_2.Text);
      Phase_PEC_Ascom[3]    := StrToFloat(SetupTelescope.NbreEditPhase_PEC_3.Text);
      Periode_PEC_Ascom[0]  := StrToFloat(SetupTelescope.NbreEditPeriode_PEC.Text);
      Periode_PEC_Ascom[1]  := StrToFloat(SetupTelescope.NbreEditPeriode_PEC_1.Text);
      Periode_PEC_Ascom[2]  := StrToFloat(SetupTelescope.NbreEditPeriode_PEC_2.Text);
      Periode_PEC_Ascom[3]  := StrToFloat(SetupTelescope.NbreEditPeriode_PEC_3.Text);

    end;
    Reg.CloseKey;

    Finally
    DeviceIni.free;
    Reg.Free;
    End;
  end;

  SetupTelescope.Free; //Destroy...

end;





end.
