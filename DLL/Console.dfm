object FormConsole: TFormConsole
  Left = 451
  Top = 244
  BorderStyle = bsToolWindow
  Caption = 'Console MCMT'
  ClientHeight = 355
  ClientWidth = 386
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000800000000000F00000000088888800008700000000000
    00000000888888880887880000000000000000080888888880878800F0000000
    0007000887774888087788000000000000080088788878888878080000000000
    0000008878887788087888000000000000000087888777788778880000000000
    0000008788877F888780880000000000000008878887F7787788800000000000
    0000087888877788788850000000000000000888888877877885000000000000
    0000087788888777888000000000000000000888888888888850000000000000
    00000878088888888508000000000000000008800088888850000F0000000000
    0000088000008000000000000000000000000880000000000000000000000000
    0000088008880000000000000000000000000888878880000000000000000000
    000000888F888000000000000000000000000058887888000000000000000000
    0000008487887800000000000000000000000008580780000000000000000000
    0000000008080000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000}
  OldCreateOrder = False
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageConsole: TPageControl
    Left = 0
    Top = 0
    Width = 386
    Height = 355
    ActivePage = TabSheetPEC
    Align = alClient
    Style = tsButtons
    TabOrder = 0
    OnChange = PageConsoleChange
    object TabSheetInfo: TTabSheet
      Caption = 'Infos'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label6: TLabel
        Left = 5
        Top = 162
        Width = 126
        Height = 13
        Caption = 'Vitesse MCMT ('#181'step/sec)'
      end
      object Label7: TLabel
        Left = 5
        Top = 4
        Width = 121
        Height = 13
        Caption = 'Vitesse Alpha ('#181'step/sec)'
      end
      object Label9: TLabel
        Left = 4
        Top = 104
        Width = 87
        Height = 13
        Caption = 'Facteur de vitesse'
      end
      object Label10: TLabel
        Left = 9
        Top = 274
        Width = 64
        Height = 13
        Caption = 'Angle Horaire'
      end
      object Label13: TLabel
        Left = 4
        Top = 48
        Width = 69
        Height = 13
        Caption = 'Encodeurs RA'
      end
      object Label14: TLabel
        Left = 184
        Top = 48
        Width = 76
        Height = 13
        Caption = 'Encodeurs DEC'
      end
      object Label19: TLabel
        Left = 181
        Top = 4
        Width = 119
        Height = 13
        Caption = 'Vitesse Delta ('#181'step/sec)'
      end
      object Label20: TLabel
        Left = 189
        Top = 127
        Width = 33
        Height = 13
        Caption = '           '
      end
      object EditVitesseStr: TEdit
        Left = 8
        Top = 179
        Width = 121
        Height = 21
        Hint = 'Dans M'#233'moire MCMT'
        Color = clBtnFace
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object EditGuideStr: TEdit
        Left = 8
        Top = 20
        Width = 121
        Height = 21
        Hint = 'Base mesure encodeurs'
        Color = clBtnFace
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object EditFactVitesse: TEdit
        Left = 8
        Top = 120
        Width = 121
        Height = 21
        Hint = 'Base mesure encodeurs'
        Color = clBtnFace
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object EditAngleHor: TEdit
        Left = 12
        Top = 290
        Width = 121
        Height = 21
        Color = clBtnFace
        TabOrder = 3
      end
      object GroupBox2: TGroupBox
        Left = 152
        Top = 188
        Width = 223
        Height = 137
        Caption = 'Vitesses differentielles'
        TabOrder = 4
        object Label4: TLabel
          Left = 10
          Top = 68
          Width = 79
          Height = 13
          Caption = 'Vitesse de King :'
        end
        object Label5: TLabel
          Left = 10
          Top = 86
          Width = 79
          Height = 13
          Caption = 'Vitesse de PEC :'
        end
        object LabelKing: TLabel
          Left = 98
          Top = 70
          Width = 24
          Height = 13
          Caption = '????'
        end
        object LabelPEC: TLabel
          Left = 98
          Top = 86
          Width = 24
          Height = 13
          Caption = '????'
        end
        object Label11: TLabel
          Left = 9
          Top = 110
          Width = 77
          Height = 13
          Caption = 'Vitesse relative :'
        end
        object Label12: TLabel
          Left = 182
          Top = 110
          Width = 34
          Height = 13
          Caption = '(calcul)'
        end
        object Label8: TLabel
          Left = 12
          Top = 20
          Width = 149
          Height = 13
          Caption = 'PEC PRISM position (sec d'#39'arc)'
        end
        object EditFactVitesseCalc: TEdit
          Left = 90
          Top = 106
          Width = 87
          Height = 21
          Color = clBtnFace
          TabOrder = 0
        end
        object EditPec_posi: TEdit
          Left = 16
          Top = 36
          Width = 121
          Height = 21
          Color = clBtnFace
          TabOrder = 1
        end
      end
      object EditEncRA: TEdit
        Left = 8
        Top = 64
        Width = 121
        Height = 21
        Color = clBtnFace
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
      end
      object EditEncDEC: TEdit
        Left = 184
        Top = 64
        Width = 121
        Height = 21
        Color = clBtnFace
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
      end
      object EditSpeedDECuSteps: TEdit
        Left = 184
        Top = 20
        Width = 121
        Height = 21
        Hint = 'Base mesure encodeurs'
        Color = clBtnFace
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
      end
      object ButtonDECsend: TButton
        Left = 310
        Top = 94
        Width = 57
        Height = 25
        Caption = 'Envoyer'
        TabOrder = 8
        OnClick = ButtonDECsendClick
      end
      object EditSendDA: TEdit
        Left = 184
        Top = 96
        Width = 121
        Height = 21
        TabOrder = 9
        Text = '0'
      end
    end
    object TabSheetPEC: TTabSheet
      Caption = 'PEC Interne'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GroupBox7: TGroupBox
        Left = 8
        Top = 40
        Width = 361
        Height = 97
        TabOrder = 4
        object Label15: TLabel
          Left = 32
          Top = 8
          Width = 75
          Height = 13
          Caption = 'Vitesse Actuelle'
        end
        object Label16: TLabel
          Left = 160
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Index Pec'
        end
        object Label17: TLabel
          Left = 152
          Top = 48
          Width = 67
          Height = 13
          Caption = 'Compteur Pec'
        end
        object Label18: TLabel
          Left = 248
          Top = 48
          Width = 99
          Height = 13
          Caption = 'Prochaine Correction'
        end
        object Panel7: TPanel
          Left = 24
          Top = 56
          Width = 97
          Height = 33
          BevelInner = bvLowered
          BevelOuter = bvLowered
          TabOrder = 0
          object ButtonActiverPEC: TButton
            Left = 4
            Top = 4
            Width = 90
            Height = 25
            Caption = 'Activer'
            TabOrder = 0
            OnClick = ButtonActiverPECClick
          end
        end
      end
      object Panel_vitesse: TPanel
        Left = 32
        Top = 64
        Width = 97
        Height = 17
        TabOrder = 1
      end
      object Panel_Index_Pec: TPanel
        Left = 144
        Top = 64
        Width = 97
        Height = 17
        TabOrder = 0
      end
      object Panel_compteur_Pec: TPanel
        Left = 144
        Top = 104
        Width = 97
        Height = 17
        TabOrder = 2
      end
      object Panel_prochaine_correction: TPanel
        Left = 256
        Top = 104
        Width = 97
        Height = 17
        TabOrder = 3
      end
      object Panel23: TPanel
        Left = 8
        Top = 140
        Width = 77
        Height = 33
        BevelInner = bvLowered
        BevelOuter = bvLowered
        TabOrder = 6
        object ButtonCHargerPEC: TButton
          Left = 4
          Top = 4
          Width = 69
          Height = 25
          Hint = 
            'Prendre les donn'#233'es d'#39'un fichier et les mettre dans le PEC inter' +
            'ne'
          Caption = 'Charger Pec'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = ButtonCHargerPECClick
        end
      end
      object Panel22: TPanel
        Left = 88
        Top = 140
        Width = 77
        Height = 33
        BevelInner = bvLowered
        BevelOuter = bvLowered
        TabOrder = 7
        object ButtonSauverPEC: TButton
          Left = 4
          Top = 4
          Width = 69
          Height = 25
          Hint = 'Prendre les donn'#233'es du PEC interne et les mettre dans in fichier'
          Caption = 'Sauver Pec '
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = ButtonSauverPECClick
        end
      end
      object Panel1: TPanel
        Left = 168
        Top = 140
        Width = 77
        Height = 33
        BevelInner = bvLowered
        BevelOuter = bvLowered
        TabOrder = 8
        object ButtonEffacerMem: TButton
          Left = 4
          Top = 4
          Width = 69
          Height = 25
          Hint = 
            'Il est conseill'#233' d'#39'effacer la m'#233'moire avant de lancer l'#39'enregist' +
            'rement!'
          Caption = 'Effacer PEC'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = ButtonEffacerMemClick
        end
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 8
        Width = 361
        Height = 33
        Caption = 'Info PEC'
        TabOrder = 9
        object Panel_info_pec: TPanel
          Left = 56
          Top = 9
          Width = 297
          Height = 20
          BevelOuter = bvLowered
          TabOrder = 0
        end
      end
      object Panel24: TPanel
        Left = 248
        Top = 140
        Width = 120
        Height = 33
        BevelInner = bvLowered
        BevelOuter = bvLowered
        TabOrder = 5
        object ButtonEnregistrerPEC: TButton
          Left = 4
          Top = 4
          Width = 113
          Height = 25
          Caption = 'Enregistrer Corrections'
          TabOrder = 0
          OnClick = ButtonEnregistrerPECClick
        end
      end
    end
  end
  object OpenDialog2: TOpenDialog
    DefaultExt = 'ini'
    Filter = 'INI|*.INI'
    Left = 308
    Top = 456
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'ini'
    Filter = 'INI|*.INI'
    Left = 252
    Top = 480
  end
  object Timer: TTimer
    Enabled = False
    OnTimer = TimerTimer
    Left = 236
    Top = 230
  end
end
