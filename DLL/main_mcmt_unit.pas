Unit main_mcmt_unit;
{$R+}
{$DEFINE CONSOLE_OP}

interface

uses
  Fastmm4,
  Forms,
  Dialogs,
  SysUtils,
  Math,
  Classes,
  Controls,
  Windows,
  IniFiles,
  Registry,
  ExtCtrls,
  SyncObjs,

  Console,
  FeuRouge,
  FeuVert,
  COMM,
  Utils,
  Etat_Telescope,
  Initphases,
  VarGlobal;


(*  SUIVI DES MODIFICATIONS
    SVP TENIR A JOUR

  14/02/10 : Elimination du postmessage sur la console qui posait de GROS GROS probl�mes.
  28/02/10 : Fusion avec Michel sur le TDM
  01/04/10 : Rendu comptible avec Windows 7 et son UCAC

  v3.3
  03/01/12 : Elimination de la gestion du TDM
             Gestion de cette erreur (bug du Pic)
                 Raise Exception.create(Format('Erreur codeurs, diff�rence trop importante par rapport � il y a 1s -> %d>%f or %d>%f',
                                              [AlphaRawPrevious,(Resol_AD/10),DeltaRawPrevious,(Resol_DEC/10)]))
             Apres 5 erreur consecutives de ce type : erreur fatale.

*)


function  Calibrate(Alpha, Delta: Double): boolean; stdcall;
function  CloseCodeurs: boolean; stdcall;
function  getAlpha(var Alpha: Double): boolean; stdcall;
function  getDelta(var Delta: Double): boolean; stdcall;
function  RetrieveDataAlphaDelta: boolean; stdcall;
function  AcceptRawPositionCodeurs: Boolean; stdcall;
function  GetRawPositionAlphaInit_arcsecCodeurs: Double; stdcall;
function  GetRawPositionDeltaInit_arcsecCodeurs: Double; stdcall;
function  GetRawPositionAlpha_arcsecCodeurs: Double; stdcall;
function  GetRawPositionDelta_arcsecCodeurs: Double; stdcall;
function  InitTelescopeSeeker_DLL(var Alpha, Delta, Latitude, Longitude, Accuracy: Double): Boolean; stdcall;
function  Execute_TelescopeSeeker_DLL(var Alpha, Delta: Double): Boolean; stdcall;
function  StopSlewing_TelescopeSeekerDevice_DLL: Boolean; stdcall;
function  CloseTelescopeSeeker_DLL: Boolean; stdcall;
function  InitTelescopeEngine_DLL(var EngineStructConf: TEngineStruct): Boolean; stdcall;
function  ShowConsoleEngine(Aowner: Tcomponent): Boolean; stdcall;
function  ShowSpeedEngine(Aowner: Tcomponent): Boolean; stdcall;
function  GetSpeedAlpha(Speed: TSpeed; var RelativeSpeed: Integer): Boolean; stdcall;
function  GetSpeedDelta(Speed: TSpeed; var RelativeSpeed: Integer): Boolean; stdcall;
function  STOPTelescope(Alpha, Delta: Boolean): Boolean; stdcall;
function  MoveTelescope(Direction: TDirection; Speed: TSpeed): Boolean; stdcall;
function  MoveEventTelescope: Boolean; stdcall;
function  STOP_ALL_Telescope: Boolean; stdcall;
function  MoveOffsetDirectAD(AlphaD, DeltaD: Double; Speed: TSpeed): Boolean; stdcall;
function  ReturnToSideralSpeed: Boolean; stdcall;
function  StopFreeSpeed: Boolean; stdcall;
function  StartSolarEngine: Boolean; stdcall;
function  StartMoonEngine: Boolean; stdcall;
function  CloseEngine: Boolean; stdcall;
function  StartKingDevice(latitude_rad, Longitude_rad, Altitude_m, pression_mb: Double): Boolean; stdcall;
function  StopKingDevice: Boolean; stdcall;
function  PulseGuideAscom(RaInd, DecInd, MsRa, MsDec: Word): Boolean; stdcall;
function  PulseGuide(RaInd, DecInd, MsRa, MsDec: Word): Boolean; stdcall;
function  IsPulseGuiding: boolean; stdcall;
function  GetCurrentSpeedRA(var D: Double): Boolean; stdcall;
function  GetCurrentSpeedDEC(var D: Double): Boolean; stdcall;
function  StartPecSpeedEngine(Periode_arcsec, Phase_arcsec, Amplitude_arcsec: array of Double): Boolean; stdcall;
procedure GetSpeed(var Guidage_AD, Guidage_DEC, Corr_P_AD, Corr_P_DEC, Corr_M_AD, Corr_M_DEC, Point_L_AD, Point_L_DEC, Point_R_AD, Point_R_DEC: double); stdcall;
procedure GetKingPEC(var King: boolean; var Amplitude, Periode, Phase: array of double; var PEC: boolean); stdcall;
function  GetRAWAlphaDelta(var CountsRA, CountsDEC: Integer): Boolean; stdcall;
function  SetFreeSpeed(AlphaSpeed_rad_H, DeltaSpeed_rad_H: Double): Boolean; stdcall;
function  StopPec: Boolean; stdcall;
function  END_TelescopeSeeker: boolean; stdcall;


function  UnParkTel_From: Boolean; stdcall;
function  ParkTel_at(AHPark, Dec2000: Double): Boolean; stdcall;
function  IsParked(var Parked: boolean): boolean; stdcall;
function  IsParking(var Parking: boolean): boolean; stdcall;
function  TelescopeOuest : boolean; stdcall;
function  GermanMount : boolean; stdcall;

// NOT EXPORTED

function  IsMCMTSlewing(Const Hcom:Thandle;Axis: integer): boolean;
function  MoveOffsetDirectStep(AlphaDSteps, DeltaDSteps: Integer): Boolean;
procedure ASK_FortelescopeOrientation(Aowner:Tcomponent;Latitude:Double);
function  IsAxisSlewing_Backlash(Hcom:Thandle;Axis: integer): boolean;

Procedure NilTimers;
Procedure CreateAllTimers;
Procedure FreeAllTimers;



IMPLEMENTATION

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// All MEMORY Allocation must be done within the DLL

uses HiResTim;


Type


  TTimerBacklash = class(TTimer)
    procedure TimerEvent(Sender: TObject);
  private
    Flag: integer;
    Alpha, Delta: boolean;
    { Private declarations }
  public
    Finished: boolean;
    { Public declarations }
  end;

  TTimerPark = class(TTimer)
    procedure TimerEvent(Sender: TObject);
  private
    { Private declarations }
    Flag: integer;
    AngleHour, Delta: double;
  public
    Finished: boolean;
    { Public declarations }
  end;

  TTimerRetardateur = class(TTimer)
    procedure TimerEvent(Sender: TObject);
  private
    { Private declarations }
    Declanchement: double;
    DabordAlpha, DabordDelta : Boolean ;
    AlphaCroissant, DeltaCroissant : Boolean;
    MoveSteps : integer;
  public
    Finished: boolean;
    { Public declarations }
  end;

  TTimerPulse = class(THiResTimer)
    procedure TimerEvent(Sender: TObject);
  private
    StopOrderAlpha,
    StopOrderDecli: boolean;
    { Private declarations }
  public
    IsPulse: boolean;
    { Public declarations }
  end;


var
  TimerAlphaPulse   : TTimerPulse;
  TimerDecliPulse   : TTimerPulse;

  TimerBacklash     : TTimerBacklash;
  TimerPark         : TTimerPark;
  TimerRetardateur  : TTimerRetardateur;

Const
  Stopping:Integer=0;


// Fordward declarations

Function  TelescopeMove   (const Hcom:Thandle;MoveStepsAlpha, MoveStepsDelta: integer): Boolean; forward;
Procedure TelescopeMoveAxe(const Hcom:Thandle;MoveSteps, Axe: integer); forward;
Procedure CalculePositionMonture; forward;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Procedure TTimerPulse.TimerEvent(Sender: TObject);  // Arrive apres X ms
begin
  Self.Enabled := false;
  STOPTelescope(StopOrderAlpha, StopOrderDecli);
  IsPulse := false;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Procedure TTimerBacklash.TimerEvent(Sender: TObject);
var
  MoveStepsAlpha,
  MoveStepsDelta : integer;

const
  procName='TTimerBacklash.TimerEvent';

  (* Main Thread *)

begin
  LogEnterProc(procName);

  Try

      if flag <> 0 then
      begin
        dec(Flag);
        if Flag = 0 then
        begin
          MoveStepsAlpha := 0;
          MoveStepsDelta := 0;

          if Alpha then MoveStepsAlpha := BackLash.StepAD;
          if Delta then
            if BackLash.North then
              MoveStepsDelta := -BackLash.StepDEC
            else
              MoveStepsDelta :=  BackLash.StepDEC;

          TelescopeMove (Hcom,MoveStepsAlpha,MoveStepsDelta);
          Logproc(procName,'Backslash moving pending (1)');

          Enabled := false;
        end;
      end
      else
      begin
        Logproc(procName,'Waiting for backslash');

        if ((IsMCMTSlewing(Hcom,C_Alpha) = false) and (IsMCMTSlewing(Hcom,C_Delta) = false)) then
        begin
          Flag := BackLash.Pause div 1000;

          Logproc(procName,'Trig pause before backslash');
          if Flag = 0 then
          begin
            MoveStepsAlpha := 0;
            MoveStepsDelta := 0;
            if Alpha then MoveStepsAlpha := BackLash.StepAD;
            if Delta then
              if BackLash.North then
                MoveStepsDelta := -BackLash.StepDEC
              else
                MoveStepsDelta :=  BackLash.StepDEC;

            TelescopeMove (Hcom,MoveStepsAlpha, MoveStepsDelta);
            Logproc(procName,'Backslash moving pending (2)');
            Enabled := false;
          end;
        end;
      end;


  LogEndProc(procName);

  Finally
  End;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Procedure TTimerPark.TimerEvent(Sender: TObject);
var
  Alpha_Temp,
  Delta_Temp,
  Accuracy       : Double;

const
  procName='TTimerPark.TimerEvent';

  (* Main thread *)

begin

    if ((IsAxisSlewing_Backlash(Hcom,C_Alpha) = false) and (IsAxisSlewing_Backlash(Hcom,C_Delta) = false)) then
    begin
      case flag of
        1:
          begin
            Alpha_Temp:=CalculeTSL(Now+SpeedIntMCMT.GMT/24,LongLat.Long)-AngleHour;
            Delta_Temp := Delta;

            if BackLash.Saved_Park then
            begin
              Alpha_Temp := Alpha_Temp + 0.002; // To avoid backlash correction in the last seeker
              if BackLash.North then
              begin
                Delta_Temp := Delta_Temp + 0.002; // To avoid backlash correction in the last seeker
                if Delta_Temp > Pi_d_2 then Delta_Temp := Pi_d_2;
              end
              else
              begin
                Delta_Temp := Delta_Temp - 0.002; // To avoid backlash correction in the last seeker
                if Delta_Temp < -Pi_d_2 then Delta_Temp := -Pi_d_2;
              end;

            end;

            Remet2Pi(Alpha_Temp);
            Logproc(procName,'Park: 2nd slewing ' + FloatToStr(Alpha_Temp) + ' ' + FloatToStr(Delta_Temp));
            InitTelescopeSeeker_DLL(Alpha_Temp, Delta_Temp, LongLat.Lat, LongLat.Long, Accuracy);
            Flag := 0;
            Logproc(procName,'Park: end 2nd slewing');
          end;
        0:
          begin
            Alpha_Temp:=CalculeTSL(Now+SpeedIntMCMT.GMT/24,LongLat.Long)-AngleHour;
            Remet2Pi(Alpha_Temp);
            Delta_Temp := Delta;
            Logproc(procName,'Park: 3rd slewing ' + FloatToStr(Alpha_Temp) + ' ' + FloatToStr(Delta_Temp));
            Enabled := false;

            InitTelescopeSeeker_DLL(Alpha_Temp, Delta_Temp, LongLat.Lat, LongLat.Long, Accuracy);
            while ((IsAxisSlewing_backlash(Hcom,C_Alpha) = True) or (IsAxisSlewing_backlash(Hcom,C_Delta) = True)) do WaitMainthread(procName);

            Logproc(procName,'Park: End 3rd slewing');
            STOP_ALL_Telescope;

            is_Parking        := False;
            Park              := True;
            BackLash.Activated:= BackLash.Saved_Park;
          end;
      end;
    end;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Procedure TTimerRetardateur.TimerEvent(Sender: TObject);
begin

  Try
    CalculePositionMonture;

    if DabordAlpha
           {Le mouvement Alpha est d�j� d�clanch�, le mouvement Delta est retard�}
       and ((GerMountSetup.Monture_Allemande
           {test d'attente du d�clanchement Delta pour une monture allemande}
              and (DeltaCroissant xor (GerMountSetup.LongitudeHorairePointage<Declanchement))
              and (GerMountSetup.LongitudeHorairePointage< Pi_d_2+GerMountSetup.Degagement_Ouest)
              and (GerMountSetup.LongitudeHorairePointage>-Pi_d_2-GerMountSetup.Degagement_Est))
           or
           (not(GerMountSetup.Monture_Allemande)
           {test d'attente du d�clanchement Delta pour une monture � fourche}
            and (AlphaCroissant xor (GerMountSetup.AngleHorairePointage<Declanchement))))

      then  {D�clanchement du mouvement Delta et arr�t du timer}
        begin
          TelescopeMoveAxe(Hcom,MoveSteps, C_Delta);
          Enabled:=false
        end;

    if DabordDelta
          { Le mouvement Delta est d�j� d�clanch�, le mouvement Alpha est retard�
             Cela ne peut se produire que dans une proc�dure de retournement
             d'une monture allemande}
       and (DeltaCroissant xor (GerMountSetup.AnglePolairePointage<Declanchement))

      then  {D�clanchement du mouvement Alpha et arr�t du timer}
        begin
          TelescopeMoveAxe(Hcom,MoveSteps, C_Alpha);
          Enabled:=false
        end;

  Finally
  End;
end;


Procedure NilTimers;
Begin
  TimerAlphaPulse  :=Nil;
  TimerDecliPulse  :=Nil;
  TimerRetardateur :=Nil;
  TimerBacklash    :=Nil;
  TimerPark        :=Nil;
end;

Procedure CreateAllTimers;
Begin
    TimerAlphaPulse                := TTimerPulse.Create(nil);
    TimerAlphaPulse.Enabled        := False;
    TimerAlphaPulse.Resolution     := 50;
    TimerAlphaPulse.OnTimer        := TimerAlphaPulse.TimerEvent;
    TimerAlphaPulse.StopOrderAlpha := True;  // for the stoptelescope order
    TimerAlphaPulse.StopOrderDecli := False; // for the stoptelescope order
    TimerAlphaPulse.IsPulse        := False;

    TimerDecliPulse                := TTimerPulse.Create(nil);
    TimerDecliPulse.Enabled        := False;
    TimerAlphaPulse.Resolution     := 50;
    TimerDecliPulse.OnTimer        := TimerDecliPulse.TimerEvent;
    TimerDecliPulse.StopOrderAlpha := False; // for the stoptelescope order
    TimerDecliPulse.StopOrderDecli := True;  // for the stoptelescope order
    TimerDecliPulse.IsPulse        := False;

    TimerRetardateur               := TTimerRetardateur.Create(nil);
    TimerRetardateur.Enabled       := False;
    TimerRetardateur.OnTimer       := TimerRetardateur.TimerEvent;

    TimerBacklash                  := TTimerBacklash.Create(nil);
    TimerBacklash.Enabled          := False;
    TimerBacklash.OnTimer          := TimerBacklash.TimerEvent;

    TimerPark                      := TTimerPark.Create(nil);
    TimerPark.Enabled              := False;
    TimerPark.OnTimer              := TimerPark.TimerEvent;
End;

Procedure FreeAllTimers;
Begin
 TimerBacklash   .Free;
 TimerDecliPulse .Free;
 TimerAlphaPulse .Free;
 TimerPark       .Free;
 TimerRetardateur.Free;
End;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// A function that tell you if an axis is moving, but only for the point of vue
// MCMT

Function IsMCMTSlewing(Const Hcom:Thandle;Axis: integer): boolean;
Const
  procName='IsMCMTSlewing';

var
  RecBuff : array[0..7] of byte;
  SendBuff: array[0..3] of byte;
  abyte,
  numbyte,
  retry   : byte;
  NbOfBytesWrite,
  NbOfBytesRead : LongWord;
  deviceready   : integer ;

begin
  LogEnterProc(ProcName);

  Result      := False;

  numbyte     := 2;
  SendBuff[0] := AxeZero + Axis;
  SendBuff[1] := 255;
  retry       := 3;
  result      := False;

  // Read a single byte from Interface
  Repeat
    FlushWriteRead(Hcom,SendBuff, numbyte, NbOfBytesWrite, RecBuff, 1, NbOfBytesRead);
    abyte := RecBuff[0];
    retry := retry - 1;
  until (retry = 0) or (NbOfBytesRead <> 0);

  if (abyte=1) then deviceready := 1
               else deviceready := 0;

  If (NbOfBytesRead=0) then raise Exception.Create('Erreur lecture �tat mouvement MCMT II, pas de r�ponse !');

  Result:=(deviceready=0); // 0 = pas de mouvements

  LogEndProc(ProcName);
end;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function IsAxisSlewing_Backlash(Hcom:Thandle;Axis: integer): boolean;
begin
 result := IsMCMTSlewing(Hcom,Axis) or TimerBacklash.Enabled;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function END_TelescopeSeeker: boolean; stdcall;
Const
  procName='END_TelescopeSeeker';
begin
  LogEnterProc_DLL(ProcName);
  result := (not (IsAxisSlewing_BackLash(Hcom,C_Alpha))) and (not (IsAxisSlewing_BackLash(Hcom,C_Delta)));
  if result then Logproc(procName,'Telescope is not slewing') else Logproc(procName,'Telescope slewing');
  LogEndProc_DLL(ProcName);
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function Calibrate(Alpha, Delta: Double): boolean; stdcall;

var
  AlphaRaw,
  DeltaRaw: longInt;

Const
  procName='Calibrate';

begin
    LogEnterProc_DLL(ProcName);

    result    := ERROR;

    Logproc(procName,'Start Calibrate '+FloatToStr(Alpha)+' '+FloatToStr(Delta));

    If (IsAxisSlewing_BackLash(Hcom,C_Alpha) = false) then
    begin

          If GerMountSetup.Monture_Allemande then
          begin

            // Cannot be put like that here because this can be asked during image recentering for instance
            // Put something more intelligent...
            // ASK_FortelescopeOrientation(Nil);

            If TelescopeOuest then
            begin
             Delta:=Pi-Delta;
             Alpha:=Alpha+Pi;
            end;
          end;

          Logproc(procName,'Calibrate1 '+FloatToStr(Alpha)+' '+FloatToStr(Delta));

          Remet2Pi(Alpha);
          ReadAlphaDeltaEncoder(hcom,AlphaRaw, DeltaRaw);

          SectionSTSTel.Enter;
          Try
          STSTel.AlphaRawPrevious := AlphaRaw;
          STSTel.DeltaRawPrevious := DeltaRaw;

          STSTel.AlphaCodeurInit  := T_pi * frac(AlphaRaw / SpeedIntMCMT.Resol_AD);
          STSTel.DeltaCodeurInit  := T_pi * frac(DeltaRaw / SpeedIntMCMT.Resol_DEC);
          Logproc(procName,'Calibrate2 '+FloatToStr(STSTel.AlphaCodeurInit)+' '+FloatToStr(STSTel.DeltaCodeurInit));

          STSTel.AlphaInit := Alpha;
          STSTel.DeltaInit := Delta;
          if (STSTel.DeltaInit<0) then
            STSTel.DeltaInit:=T_pi + STSTel.DeltaInit;

          Logproc(procName,'Calibrate3 '+FloatToStr(STSTel.AlphaInit)+' '+FloatToStr(STSTel.DeltaInit));
          STSTel.Alpha := Alpha;
          STSTel.Delta := Delta;

          STSTel.FirstTime := Now;
          STSTel.atime     := Now;

          Finally
          SectionSTSTel.Leave;
          end;

          result    := NOERROR;

    end;

    Logproc(procName,'End Calibrate');
    LogEndProc_DLL(ProcName);

end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

procedure ASK_FortelescopeOrientation(Aowner:Tcomponent;Latitude:Double);
var
  Form_Etat_Telescope: TForm_Etat_Telescope;
  Reg                : TRegistry;
  ShowInterFace      : Boolean;

Const
  procName='ASK_FortelescopeOrientation';

begin
          Try
          ShowInterFace:=True;

          Reg         := TRegistry.Create;
          Reg.RootKey := HKEY_CURRENT_USER;

          If not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then
                  Reg.OpenKey('SOFTWARE\MCMTII', True);

          Try


          If ShowInterFace then
          begin

              Form_Etat_Telescope   := TForm_Etat_Telescope.create(Aowner,Latitude);

              Try
              If TelescopeOuest then
              begin
                    Logproc(procName,'Telescope_Ouest');
                    Form_Etat_Telescope.RadiogroupEtatTelescope.ItemIndex:=1;
                    Form_Etat_Telescope.Image_Zenith_Ouest.Visible:=True;
                    Form_Etat_Telescope.Image_Zenith_Est.Visible  :=false;
              end
              else
              begin
                    Logproc(procName,'Telescope_Est');
                    Form_Etat_Telescope.RadiogroupEtatTelescope.ItemIndex:=0;
                    Form_Etat_Telescope.Image_Zenith_Ouest.Visible:=false;
                    Form_Etat_Telescope.Image_Zenith_Est.Visible  :=True;
              end;

              Form_Etat_Telescope.Top:=Form_Etat_Telescope.Top-Form_Etat_Telescope.height;
              Form_Etat_Telescope.BringTofront;
              Form_Etat_Telescope.ShowModal;

              GerMountSetupSem.Enter;
              If (Form_Etat_Telescope.RadiogroupEtatTelescope.ItemIndex=1)
                then GerMountSetup.TelescopeZenithOuest:=True
                else GerMountSetup.TelescopeZenithOuest:=False;
              GerMountSetupSem.Leave;

              // Save
              Reg.WriteBool   ('TelescopeZenithOuest',TelescopeOuest);
              Finally
              Form_Etat_Telescope.free;
              End;
          end;

          Reg.WriteInteger('Handle_Caller'       ,integer(Aowner));
          Reg.CloseKey;
          Finally
          Reg.Free;
          end;


          Except
          GerMountSetup.TelescopeZenithOuest:=True;
          Logproc(procName,'Telescope_Est');
          End;
end;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Procedure DisableTimers;
begin
  Try
  If (TimerAlphaPulse  <> nil) then TimerAlphaPulse.Enabled :=False;
  if (TimerDecliPulse  <> nil) then TimerDecliPulse.Enabled :=False;
  if (TimerBacklash    <> nil) then TimerBacklash.Enabled   :=False;
  if (TimerPark        <> nil) then TimerPark.Enabled       :=False;
  if (TimerRetardateur <> nil) then TimerRetardateur.Enabled:=False;
  Except
  End;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CloseCodeurs: boolean; stdcall;
var
  DeviceIni: TIniFile;
  Reg      : TRegistry;

Const
  procName='CloseCodeurs';


begin
  Try
  LogEnterProc_DLL(ProcName);

  FormConsole.StopTimer;

  DisableTimers;

  if TimerAlphaPulse <> nil then
  begin
    TimerAlphaPulse.Free;
    TimerAlphaPulse := nil;
  end;

  if TimerDecliPulse <> nil then
  begin
    TimerDecliPulse.Free;
    TimerDecliPulse:= nil;
  end;

  if TimerBacklash <> nil then
  begin
    TimerBacklash.Free;
    TimerBacklash := nil;
  end;

  if TimerPark <> nil then
  begin
    TimerPark.Free;
    TimerPark := nil;
  end;

  if TimerRetardateur <> nil then
  begin
    TimerRetardateur.Free;
    TimerRetardateur := nil;
  end;

  Closecom(Hcom);

  Reg         := TRegistry.Create;
  Try
  Reg.RootKey := HKEY_CURRENT_USER;
  if not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then Reg.OpenKey('SOFTWARE\MCMTII', True);

  SectionSTSTel.Enter;
  Try
  Logproc(procName,'Close codeur '+FloatToStr(STSTel.Alpha)+' '+FloatToStr(STSTel.Delta));

  Reg.WriteFloat('LastAlpha', STSTel.Alpha);
  Reg.WriteFloat('LastDelta', STSTel.Delta);

  Finally
  SectionSTSTel.Leave;
  End;



  Reg.WriteBool ('TelescopeZenithOuest',TelescopeOuest);

  Reg.CloseKey;

  Finally
  Reg.Free;
  End;

  DeviceIni := TIniFile.Create(return_MCMT_StoreIniFilePath);

  SectionSTSTel.Enter;
  Try
  with DeviceIni do
  begin
    WriteString('Configuration', 'LastDelta', floattostr(STSTel.Delta));
    WriteString('Configuration', 'LastAlpha', floattostr(STSTel.Alpha));
  end;
  Finally
  SectionSTSTel.Leave;
  DeviceIni.free;
  End;

  if (FormConsole <> nil) then FormConsole.free;

  Result := True;

  LogEndProc_DLL(ProcName);

  Except
  result := True;
  End;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getAlpha(var Alpha: Double): boolean; stdcall;
begin
  result := True;
  SectionSTSTel.Enter;
  Try
  Alpha  := STSTel.Alpha;
  Finally
  SectionSTSTel.Leave;
  End;

end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function getDelta(var Delta: Double): boolean; stdcall;
begin
  result := True;
  SectionSTSTel.Enter;
  Try
  Delta  := STSTel.Delta;
  Finally
  SectionSTSTel.Leave;
  End;

end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Const
 ErrorCountRAce:Word= 0;
 _ERROR_            =-1;

// NOT IN MAIN THREAD ,DO NOT USE VCL OBJECTS in this function

Function RetrieveDataAlphaDelta_Internal: boolean; // NOT IN VCL THREAD
var
  AlphaTemp, DeltaTemp  : Double;
  i                     : Integer;
  astring,receivestring : ShortString;
  SIGNE, MSB, LSB, LLSB : Byte;
  tempPEC,temp,
  U                     : Double;
  FactVitesse           : Double;
  DeltaA                : Double;
  DeltaD                : Double;
  Readtemp              : Integer;
  H__, M__, S__         : Double ;
  Index_PecC            : Integer;
  CasePanel_info_pec    : Integer;
  CasePecState2         : Integer;
  CasePecState1         : Integer;
  position_PECD         : Double ;
  delta_time            : Double ;
  AH_Park               : Double ;
  LTelescopeZenithOuest : Boolean;
  L_DeltaRaw            : Longint;
  L_AlphaRaw            : Longint;
  L_AlphaRawPrevious    : Longint;
  L_DeltaRawPrevious    : Longint;
  L_ATime               : Double ;
  L_Alpha               : Double ;
  L_Delta               : Double ;
  L_AlphaInit           : Double ;
  L_AlphaCodeurInit     : Double ;
  L_DeltaInit           : Double ;
  L_DeltaCodeurInit     : Double ;
  S_err                 : String ;


Const
  procName='RetrieveDataAlphaDelta_Internal';

begin

  Try

  Try
      Result:=False;

      LogEnterproc(procName);



      // delta_time = variation du temps sid�ral depuis l''initialisation des codeurs.
      // En JOURS

      SectionSTSTel.Enter;
      Try
      STSTel.ATime      := Now;
      L_AlphaRawPrevious:= STSTel.AlphaRawPrevious;
      L_DeltaRawPrevious:= STSTel.DeltaRawPrevious;
      L_ATime           := STSTel.ATime;
      L_AlphaInit       := STSTel.AlphaInit;
      L_AlphaCodeurInit := STSTel.AlphaCodeurInit;
      L_DeltaInit       := STSTel.DeltaInit;
      L_DeltaCodeurInit := STSTel.DeltaCodeurInit;

      Delta_time        := (STSTel.ATime - STSTel.FirstTime) * OmegaSid; // Bug corrig� 06 MARS 08, bug de minuit!

      Finally
      SectionSTSTel.Leave;
      End;

      // Delta Time en RADIANS  2Pi = 23h56m04s Temps Local

      Logproc(procName,Format('Delta Time=%1.10f rad',[delta_time]));

      // Lecture encodeurs
      // AlphaRaw et DeltaRow : position des codeurs sur quatre octets, en micropas

      If (ReadAlphaDeltaEncoder(Hcom,L_AlphaRaw,L_DeltaRaw)=_ERROR_) then //MCMT toujours branch�?
      begin
        Try
        Result :=True ; // Important sinon mets un message generique
        DisableTimers ;
        Finally
        Raise Exception.create('Le port COM ne r�pond plus, pas de communication avec MCMT....');
        End;
      end
      else
      Logproc(procName,Format('ReadAlphaDeltaEncoder %d , %d',[L_AlphaRaw,L_DeltaRaw]));

      DeltaA:=L_AlphaRaw - L_AlphaRawPrevious;
      DeltaD:=L_DeltaRaw - L_DeltaRawPrevious;

      Logproc(procName,Format('Difference previous -> %1.1f , %1.1f',[DeltaA,DeltaD]));

      (**)

      // Si c'est une diff�rence trop forte
      // avec la derni�re fois ==> erreur  (plus d'un dizi�me de tour, soit 36�)
      If (abs(DeltaA) > (SpeedIntMCMT.Resol_AD  / 10)) or
         (abs(DeltaD) > (SpeedIntMCMT.Resol_DEC / 10)) then
      begin
        Logproc(procName,Format('diff�rence trop forte (%d) !',[ErrorCountRAce]) );

        If (ErrorCountRAce>5) then
        Begin
         DisableTimers ;
         S_err:=Format('Erreur critique de lecture des codeurs MCMTII, la diff�rence entre 5 lectures cons�cutives est trop importante '+
                                       ':  RAi=%d,RAf=%d <-> DECi=%d,DECf=%d',
                                       [L_AlphaRawPrevious,L_AlphaRaw,L_DeltaRawPrevious,L_DeltaRaw]);
         Logproc(procName,S_err);
         Raise Exception.create(S_err)
        end
        else
        Begin
         Inc(ErrorCountRAce);
         Result        := True; // Ok fine BUT
         Exit;                  // Warning so Exit this proc for another trial
        End;
      End;

      ErrorCountRAce   := 0; // Ok reset le compteur

      Logproc(procName,'diff�rence OK');

      Try

      ConsoleDataSem.Enter;
      Try
      InfosConsoleVar.NbStepPer_sec_meas_Alpha := (L_AlphaRaw - InfosConsoleVar.AlphaRaw_Previous) / (frac(L_atime - InfosConsoleVar.previous_Time) * 24 * 3600);
      InfosConsoleVar.AlphaRaw_Previous        :=  L_AlphaRaw;

      InfosConsoleVar.NbStepPer_sec_meas_Delta := (L_DeltaRaw - InfosConsoleVar.DeltaRaw_Previous) / (frac(L_atime - InfosConsoleVar.previous_Time) * 24 * 3600);
      InfosConsoleVar.DeltaRaw_Previous        :=  L_DeltaRaw;
      Finally
      ConsoleDataSem.Leave;
      End;


      Except
      // Sans importance...
      On E:Exception do
       Logproc(procName,'Avertissement (non fatal), probl�me de calcul lignes  : 928 � 937 -> '+E.message);
      End;


      Try (* Trace d'une erreur vue par Ren� Roy, assez dur a voir...  *)

      // RA
      AlphaTemp := 0;
      AlphaTemp := T_pi * Frac(L_AlphaRaw / SpeedIntMCMT.Resol_AD);

      If not SpeedIntMCMT.Inv_Codeur_AD then
        L_Alpha := T_pi * Frac((L_AlphaInit + AlphaTemp - L_AlphaCodeurInit + delta_time) / T_pi)
      else
        L_Alpha := T_pi * Frac((L_AlphaInit - AlphaTemp + L_AlphaCodeurInit + delta_time) / T_pi);

      While (L_Alpha >= T_pi) do
        L_Alpha := L_Alpha - T_pi; // G_Alpha est alors compris dans [0,T_pi]

      // DEC
      DeltaTemp := 0;
      DeltaTemp := T_pi*Frac(L_DeltaRaw / SpeedIntMCMT.Resol_DEC);


      Except
        On E:Exception do
        begin
         S_err :=Format('Erreur de calcul : L_AlphaRaw=%d, '            +
                                           'SpeedIntMCMT.Resol_AD=%d, ' +
                                           'AlphaTemp=%1.5f, '          +
                                           'L_AlphaInit=%1.5f, '        +
                                           'L_AlphaCodeurInit=%1.5f, '  +
                                           'delta_time=%1.5f, '         +
                                           'L_Alpha=%1.5f, '            +

                                           'L_DeltaRaw=%d, '            +
                                           'SpeedIntMCMT.Resol_DEC=%d, '+
                                           'DeltaTemp=%1.5f, '          +
                                           'Exception=%s',

                          [L_AlphaRaw             ,
                           SpeedIntMCMT.Resol_AD  ,
                           AlphaTemp              ,
                           L_AlphaInit            ,
                           L_AlphaCodeurInit      ,
                           delta_time             ,
                           L_Alpha                ,

                           L_DeltaRaw             ,
                           SpeedIntMCMT.Resol_DEC ,
                           DeltaTemp              ,
                           E.Message              ]);

         Logproc(procName,S_err);
         raise Exception.Create(S_err);
        end;
      End;

      Logproc(procName,Format('AlphaTemp=%1.6f DeltaTemp=%1.6f',[AlphaTemp,DeltaTemp]));

      (**)

      If SpeedIntMCMT.Inv_Codeur_DEC then
      begin
        L_Delta := L_DeltaInit + DeltaTemp - L_DeltaCodeurInit;
        Logproc(procName,'GDelta1 ' + FloatToStr(L_DeltaInit) + ' ' + FloatToStr(DeltaTemp)+ ' ' + FloatToStr(L_DeltaCodeurInit));
      end
      else
      begin
        L_Delta := -DeltaTemp + L_DeltaInit + L_DeltaCodeurInit;
        Logproc(procName,'GDelta2 ' + FloatToStr(L_DeltaInit) + ' ' + FloatToStr(DeltaTemp)+ ' ' + FloatToStr(L_DeltaCodeurInit));
      end;

      L_Delta := (T_pi) * frac(L_Delta / (T_pi));

      Remet2Pi(L_Delta);

      if (L_Delta >Pi_d_2) and (L_Delta <= 3*Pi_d_2)  then
      begin
        if GerMountSetup.Monture_Allemande then LTelescopeZenithOuest:=True;
        L_Delta := Pi - L_Delta;
        L_Alpha := L_Alpha + Pi;
      end
      else
      begin
        if L_Delta >= (3 * Pi / 2) then L_Delta := L_Delta - T_pi;
        if GerMountSetup.Monture_Allemande then
        begin
         LTelescopeZenithOuest:=false;
         Logproc(procName,'Met � est');
        end;
      end;


      // Sauve l etat du telescope
      if GerMountSetup.Monture_Allemande then
        WriteBoolToReg('TelescopeZenithOuest',LTelescopeZenithOuest);

      GerMountSetupSem.enter;
      GerMountSetup.TelescopeZenithOuest:=LTelescopeZenithOuest;
      GerMountSetupSem.Leave;

      Remet2Pi(L_Alpha);  // G_Alpha est alors compris dans [0;T_pi]

      FactVitesse                         := SpeedIntMCMT.V_Guidage_AD;
      ConsoleDataSem.Enter;
      InfosConsoleVar .FactVitesseDisplay := FactVitesse ;
      ConsoleDataSem.Leave;

      //AH_Park     := AH(long, G_Alpha, GMT);
      //GAlpha_Park := G_Alpha;
      //GDelta_Park := G_Delta;


      If Not IsAxisSlewing_BackLash(Hcom,C_Alpha) then
      begin

              Logproc(procName,'No Slewing...');

              astring       := 'r' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
              receivestring := sendcommand(Hcom,astring, 10, 0);

              If (receivestring<>'') then
                if (ord(receivestring[2]) <> 0) or (ord(receivestring[1]) <> 0) then
                begin

                  ConsoleDataSem.Enter;
                  InfosConsoleVar.VitesseCurStepPer_sec := 625000 / (ord(receivestring[3]) / 10 +
                                                                     ord(receivestring[2]) +
                                                                     256 * ord(receivestring[1]));
                  ConsoleDataSem.Leave;

                  PECSection.Enter;
                  Try
                  Index_PecC     := ord(receivestring[4]);
                  PEC.PEC_Step   := ord(receivestring[6]) + 256 * ord(receivestring[5]);

                  if not SpeedIntMCMT.Inv_Codeur_AD then
                    PEC.PEC_Step := 25600 - PEC.PEC_Step;
                  Finally
                  PECSection.Leave;
                  End;


                  readtemp := 2 * (ord(receivestring[8]) + 256 * ord(receivestring[7]));
                  if not SpeedIntMCMT.Inv_Codeur_AD then
                    readtemp := 25600 - readtemp;
                end;

              CasePanel_info_pec := (ord(receivestring[9 ]) and 15);
              CasePecState1      :=  ord(receivestring[9 ]);
              CasePecState2      :=  ord(receivestring[10]);

              Logproc(procName,'Index Pec done...');

              temp := AH(LongLat.long, L_Alpha, SpeedIntMCMT.GMT) * 12 / pi;

              H__ := int(temp);
              M__ := int((abs(temp) - abs(H__)) * 60);
              S__ := 60 * frac((abs(temp) - abs(H__)) * 60);


              if ((KING_Activated) or (PEC_Activated)) and (IsAxisSlewing_BackLash(Hcom,C_Alpha) = false) then
              begin

                    if King_Activated then
                    begin
                      FactVitesse := FactVitesse / getKingSpeedCorrection(AH_Park, L_Delta, LongLat.Lat);
                      Logproc(procName,Format('King Update : %1.10f',[FactVitesse]));
                    end;

                    if PEC_Activated then
                    begin
                      Logproc(procName,'PEC update start');

                      PECSection.Enter;
                      Try
                      position_PECD := PEC.Periode_PEC[1] * frac(PEC.PEC_Step / 25600);

                      // This is the derivative function of X + A1*cos(2Pi/Per*(X + Phi1)) + A2*cos(2Pi/Per*(X + Phi2))

                      tempPEC := 0;

                      For i := 1 to MaxPECHarmos do
                      begin
                        U := (Pi / PEC.Periode_PEC[i]) *
                          (2 * PEC.Amplitude_PEC[i]) *
                          sin(((T_pi) / PEC.Periode_PEC[i]) * (i * (position_PECD + PEC.Phase_PEC[i]) +
                           (15.041 * PEC.FCorrectionPeriodeMs / 1000)));
                        tempPEC := tempPEC + U;
                      end;

                      Finally
                      PECSection.Leave;
                      End;


                      // Sinus derivative is negative !
                      // 1 is derivatif of X

                      tempPEC := 1 - tempPEC;

                      FactVitesse := FactVitesse * tempPEC;
                    end;

                    ConsoleDataSem.Enter;
                    InfosConsoleVar.FactVitesseDisplay := FactVitesse; // For Display
                    ConsoleDataSem.Leave;

                    FactVitesse := 625000 / FactVitesse;
                    MSB   := (round(abs(FactVitesse)) shr 8) and $FF;
                    LSB   := (round(abs(FactVitesse))) and $FF;
                    LLSB  := round(abs(10 * frac(FactVitesse))) and $FF;
                    SIGNE := 0;

                    if (FactVitesse>0) then SIGNE := 1;
                    astring := 'R' + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB) + AnsiChar(SIGNE);
                    sendcommand(Hcom,astring, 0, 0);

                    Logproc(procName,Format('PEC Update : %1.10f',[FactVitesse]));

              end; // KING_Activated PEC_Activated

              McmtPecSem.Enter;
              Try
              if InternalMCMT_PEC.ActiverPEC then
              begin
                InternalMCMT_PEC.ActiverPEC    := false;
                astring       := 'L' + AnsiChar(47) + AnsiChar(1) + AnsiChar(0) + AnsiChar(0);
                receivestring := sendcommand(Hcom,astring, 0, 0);
              end;

              if InternalMCMT_PEC.DesactiverPEC then
              begin
                InternalMCMT_PEC.DesactiverPEC := false;
                astring       := 'L' + AnsiChar(47) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                receivestring := sendcommand(Hcom,astring, 0, 0);
              end;

              if InternalMCMT_PEC.EffacerPEC then
              begin
                InternalMCMT_PEC.EffacerPEC    := false;
                astring       := 'e' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                receivestring := sendcommand(Hcom,astring, 0, 0);
              end;

              if InternalMCMT_PEC.EnregistrerPEC then
              begin
                InternalMCMT_PEC.EnregistrerPEC := false;
                astring        := 'E' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                receivestring  := sendcommand(Hcom,astring, 0, 0);
              end;

              Finally
              McmtPecSem.Leave;
              End;

      end; // RA moves

      SectionSTSTel.Enter;
      STSTel.Delta            := L_Delta;
      STSTel.Alpha            := L_Alpha;
      STSTel.AlphaRaw         := L_AlphaRaw;
      STSTel.DeltaRaw         := L_DeltaRaw;
      STSTel.AlphaPointage    := L_Alpha;
      STSTel.DeltaPointage    := L_Delta;
      STSTel.AlphaRawPrevious := L_AlphaRaw; // Previous data
      STSTel.DeltaRawPrevious := L_DeltaRaw;
      SectionSTSTel.Leave;

      Logproc(procName,'End RetrieveAlphaDelta ' + FloatToStr(L_Alpha) + ' ' + FloatToStr(L_Delta));

      ConsoleDataSem.Enter;
      InfosConsoleVar.previous_Time       :=L_Atime;
      InfosConsoleVar .AlphaRaw           :=L_AlphaRaw;
      InfosConsoleVar .DeltaRaw           :=L_DeltaRaw;
      InfosConsoleVar .M__                :=M__     ;
      InfosConsoleVar .H__                :=H__     ;
      InfosConsoleVar .S__                :=S__     ;
      InfosConsoleVar .position_PECD      :=position_PECD      ;
      InfosConsoleVar .Index_PecC         :=Index_PecC         ;
      InfosConsoleVar .Readtemp           :=Readtemp           ;
      InfosConsoleVar .CasePanel_info_pec :=CasePanel_info_pec ;
      InfosConsoleVar .CasePecState2      :=CasePecState2      ;
      InfosConsoleVar .CasePecState1      :=CasePecState1      ;

      PECSection.Enter;
      InfosConsoleVar .PEC_Step           :=PEC.PEC_Step       ;
      PECSection.Leave;

      ConsoleDataSem.Leave;

      Logproc(procName,'InfosConsoleVar console var done');

      Result        := True; // Ok fine

      // raise EAccessViolation.Create('Test');

  Finally
  End;

  LogEndproc(procName);

  Except
    On E:Exception do
    begin
      Logproc(procName,'Erreur fatale, exception globale : '+E.Message);
      Raise Exception.create('Erreur MCMTII, lecture p�riodique des codeurs -> '+E.Message);
    end;
  End;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function RetrieveDataAlphaDelta: boolean; stdcall;
Const procName='RetrieveDataAlphaDelta';
begin
  LogEnterProc_DLL(ProcName);
  Result := RetrieveDataAlphaDelta_Internal;     // Ceci cause au COMS
  LogEndProc_DLL(ProcName);
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function AcceptRawPositionCodeurs: Boolean; stdcall;
begin
  result := NOError;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GetRawPositionAlphaInit_arcsecCodeurs: Double; stdcall;
begin
  PECSection.Enter;
  Try
  PEC.PEC_Step_Old           := PEC.PEC_Step;
  PEC.PEC_Step_Codeur_Arcsec := 3600 * (9216000 / SpeedIntMCMT.Resol_AD) * frac(PEC.PEC_Step / 25600);
  result                     := 0;
  Finally
  PECSection.Leave;
  End;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GetRawPositionDeltaInit_arcsecCodeurs: Double; stdcall;
begin
  result := 0;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GetRawPositionAlpha_arcsecCodeurs: Double; stdcall;
var
  Temp: double;

begin
  PECSection.Enter;
  Try
  if (PEC.PEC_Step < PEC.PEC_Step_Old) then
    temp := PEC.PEC_Step + 25600 - PEC.PEC_Step_Old
  else
    temp := PEC.PEC_Step - PEC.PEC_Step_Old;

  temp                   := 3600 * (9216000 / SpeedIntMCMT.Resol_AD) * frac(temp / 25600);
  PEC.PEC_Step_Codeur_Arcsec := PEC.PEC_Step_Codeur_Arcsec + temp;
  Result                 := PEC.PEC_Step_Codeur_Arcsec;
  PEC.PEC_Step_Old           := PEC.PEC_Step;
  Finally
  PECSection.Leave;
  End;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GetRawPositionDelta_arcsecCodeurs: Double; stdcall;
begin
  result := 0;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GetRAWAlphaDelta(var CountsRA, CountsDEC: Integer): Boolean; stdcall;
begin
   (*
      This function aims at providing the raw counts of the encoders
      if Encoders systems supports
   *)
  Try
  SectionSTSTel.Enter;
  CountsRA  := STSTel.AlphaRaw;
  CountsDEC := STSTel.DeltaRaw;
  SectionSTSTel.Leave;
  Result    := NOError;
  Except
  End;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
// POINTAGE AUTO
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /

procedure TelescopeMoveAxe(Const Hcom:Thandle;MoveSteps, Axe: integer);
var
  MMSB, MSB, LSB, LLSB: byte;
  astring             : shortstring;

Const
  procName='TelescopeMoveAxe';


begin
  Logproc(procName,'TelescopeMoveAxe '+IntToStr(Axe)+' '+IntToStr(MoveSteps));

  If (abs(MoveSteps) > 0) then
  begin
    MMSB := (abs(MoveSteps) shr 24) and $7F;
    MSB  := (abs(MoveSteps) shr 16) and $FF;
    LSB  := (abs(MoveSteps) shr 8)  and $FF;
    LLSB :=  abs(MoveSteps) and $FF;

    If MoveSteps < 0 then MMSB := MMSB or $80;
    While IsMCMTSlewing(Hcom,Axe) do WaitMainthread(procName);

    astring := 'p' + AnsiChar(MMSB) + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB);
    sendcommand(Hcom,astring, 0, Axe);
  end;

  Logproc(procName,'End TelescopeMoveAxe');
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function TelescopeMove(Const Hcom:Thandle;MoveStepsAlpha, MoveStepsDelta: integer): Boolean;
Const
  procName='TelescopeMove';


begin
  Logproc(procName,'TelescopeMove '+IntToStr(MoveStepsAlpha)+' '+IntToStr(MoveStepsDelta));

  TelescopeMoveAxe(Hcom,MoveStepsDelta, C_Delta);
  TelescopeMoveAxe(Hcom,MoveStepsAlpha, C_Alpha);

  Logproc(procName,'End TelescopeMove');
  result := True;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

(*
  D�termine les angles courants de la monture
  DeltaPointage            -Pi_d_2 < Delta < Pi_d_2
  AnglePolairePointage       -Pi < Theta < Pi
  AlphaPointage                0 < Alpha < T_pi
  AngleHorairePointage       -Pi <  AH   < Pi
  LongitudeHorairePointage   -Pi <  LH   < Pi
*)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Procedure CalculePositionMonture;
Const
  procName='CalculePositionMonture';

  (* Main thread only *)

Var
  L_DeltaPointage : Double;
  L_AlphaPointage : Double;

begin

  RetrieveDataAlphaDelta; // get position from hardware

  SectionSTSTel.Enter;
  L_AlphaPointage:=STSTel.AlphaPointage;
  L_DeltaPointage:=STSTel.DeltaPointage;
  SectionSTSTel.Leave;

  GerMountSetup.AngleHorairePointage:=AH(LongLat.Long,L_AlphaPointage,SpeedIntMCMT.GMT);

  Logproc(procName,'CalculePositionMonture->AH                  =' + FloatToStr(LongLat.Long)+' '+FloatToStr(L_AlphaPointage)+' '+FloatToStr(SpeedIntMCMT.GMT));
  Logproc(procName,'CalculePositionMonture->AngleHorairePointage=' + FloatToStr(GerMountSetup.AngleHorairePointage));


  {-Pi < AngleHorairePointage < +Pi}
  if TelescopeOuest then
      // T�lescope retourn�
  begin
        GerMountSetup.LongitudeHorairePointage:=GerMountSetup.AngleHorairePointage+Pi_d_2;

  {-Pi_d_2 < LongitudeHorairePointage < +3*Pi_d_2}
        if GerMountSetup.LongitudeHorairePointage>(3*Pi_d_2)-GerMountSetup.Degagement_Est
          then GerMountSetup.LongitudeHorairePointage:=GerMountSetup.LongitudeHorairePointage-T_pi;

  {-Pi_d_2-Degagement_Est < LongitudeHorairePointage < +3*Pi_d_2-Degagement_Est}
        GerMountSetup.AnglePolairePointage:=Sign(LongLat.Lat)*Pi_d_2-L_DeltaPointage;      //H�misph�re Sud
  end
  else  // T�lescope non retourn�
  begin

        GerMountSetup.LongitudeHorairePointage:=GerMountSetup.AngleHorairePointage-Pi_d_2;

  {-3*Pi_d_2 < LongitudeHorairePointage < Pi_d_2}
        if GerMountSetup.LongitudeHorairePointage<(-3*Pi_d_2)+GerMountSetup.Degagement_Ouest
          then GerMountSetup.LongitudeHorairePointage:=GerMountSetup.LongitudeHorairePointage+T_pi;

  {-3*Pi_d_2+Degagement_Ouest < LongitudeHorairePointage < Pi_d_2+Degagement_Ouest}
        GerMountSetup.AnglePolairePointage:=L_DeltaPointage-Sign(LongLat.Lat)*Pi_d_2;      //H�misph�re Sud
  end;

  Logproc(procName,'CalculePositionMonture->LongitudeHorairePointage=' + FloatToStr(GerMountSetup.LongitudeHorairePointage));
  Logproc(procName,'CalculePositionMonture->AnglePolairePointage    =' + FloatToStr(GerMountSetup.AnglePolairePointage));
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function InitTelescopeSeeker_DLL(var Alpha, Delta, Latitude, Longitude, Accuracy: Double): Boolean; stdcall;
var
  MoveStepsAlpha, MoveStepsDelta  : Integer;
  Alphatemp, Deltatemp            : Double ;
  L_AlphaPointage,L_DeltaPointage : Double ;

  // Test de retardement du mouvement delta
  // Montures allemandes
  RetournementEnCours, Test                  : Boolean;
  LongitudeHoraire, AnglePolaire             : double;


const
  // On ne s'approche pas � plus de 5 degr�s du danger, c'est plus s�r
  PiedDePilote = Pi/36;
  procName     = '';

Begin

  (* ALWAYS IN MAIN THREAD *)


  // Alpha     : RA Distination coordinates
  // Delta     : DEC Distination coordinates
  // Longitude : observation Longitude place coordinates
  // Latitude  : observation Latitude place coordinates
  // Accuracy  : Desired pointing accuracy

  LogEnterProc_DLL(ProcName);
  Logproc(procName,'InitTelescopeSeeker ' + FloatToStr(Alpha) + ' ' + FloatToStr(Delta)+' '+FloatToStr(Latitude)+' '+FloatToStr(Longitude));

  SectionSTSTel.Enter;
  L_AlphaPointage:=STSTel.AlphaPointage;
  L_DeltaPointage:=STSTel.DeltaPointage;
  SectionSTSTel.Leave;


  // Par ce test, AlphaPointage et DeltaPointage sont d�termin�s
  if not RetrieveDataAlphaDelta_Internal then
  begin
    result := false;
    Logproc(procName,'End Slew with error!');
    Exit;
  end;

  GerMountSetup.AngleHoraire:=AH(Longitude,Alpha,SpeedIntMCMT.GMT);          {Angle horaire de la cible}
  Logproc(procName,'AH='+FloatToStr(Longitude)+' '+FloatToStr(Alpha)+' '+FloatToStr(SpeedIntMCMT.GMT));
  Logproc(procName,'Angle Horaire ='+FloatToStr(GerMountSetup.AngleHoraire));

  TimerRetardateur.DabordAlpha  := False;
  TimerRetardateur.DabordDelta  := False;
  TimerRetardateur.Declanchement:= 0;
  RetournementEnCours           := False;

  /////

  // *********************** Monture allemande *********************************

  if GerMountSetup.Monture_Allemande then
  begin
        LongLat.Long:=Longitude;
        CalculePositionMonture;
        // Calcul de la longitude horaire et de l''angle polaire de la cible

        if (GerMountSetup.AngleHoraire<-GerMountSetup.Degagement_Est) or
           (GerMountSetup.AngleHoraire>=Pi-GerMountSetup.Degagement_Est)
        then      {T�lescope sur Z�nith Ouest : retourn�}
        begin
              LongitudeHoraire:=GerMountSetup.AngleHoraire+Pi_d_2;
              if LongitudeHoraire>Pi
                then LongitudeHoraire:=LongitudeHoraire-T_pi;
              AnglePolaire:=Sign(LongLat.Lat)*Pi_d_2-Delta;               //H�misph�re Sud
         end
         else     {T�lescope sur Z�nith Est : non retourn�}
         begin
              LongitudeHoraire:=GerMountSetup.AngleHoraire-Pi_d_2;
              if LongitudeHoraire<-Pi
                then LongitudeHoraire:=LongitudeHoraire+T_pi;
              AnglePolaire:=Delta-Sign(LongLat.Lat)*Pi_d_2;               //H�misph�re Sud
         end;

        (*  Si la cible est � moins d'une minute d'arc
          et que l'on ne se trouve pas en proc�dure de Park
          et que le Retournement est autoris�  *)

        if (abs(L_AlphaPointage-Alpha)<0.0003)
             and (abs(L_DeltaPointage-Delta)<0.0003)
             and GerMountSetup.Retournement_Autorise
             and not(is_Parking)
          then
            begin
              RetournementEnCours:=True;
             {il est imp�ratif de quitter la section critique,
              sinon cela produit t�t ou tard un plantage g�n�ral de prism}
              if (GerMountSetup.AnglePolairePointage+Sign(LongLat.Lat)*                //H�misph�re Sud
                  GerMountSetup.Declinaison_Critique_Est<Pi_d_2)
                  and (GerMountSetup.AnglePolairePointage-Sign(LongLat.Lat)*           //H�misph�re Sud
                       GerMountSetup.Declinaison_Critique_Ouest>-Pi_d_2)
                  and (GerMountSetup.LongitudeHorairePointage<Pi_d_2-GerMountSetup.Degagement_Est)
                  and (GerMountSetup.LongitudeHorairePointage>-Pi_d_2+GerMountSetup.Degagement_Ouest)
                then
                  begin
                    Form_FeuRouge := TForm_FeuRouge.create(nil);
                    Form_FeuRouge.show;
                    Daccord_Renoncer:=false;
                    Daccord_Perseverer:=false;
                    while not(Daccord_Renoncer) and not(Daccord_Perseverer)
                      do Application.ProcessMessages;
                    if Daccord_Renoncer
                      then
                        begin
                          result := True;
                          Form_FeuRouge.free;
                          Form_FeuRouge:=Nil;
                          Exit;                {c'est plus sage en effet}
                        end;
                    Form_FeuRouge.free;
                    Form_FeuRouge:=Nil;
                  end
                else
                  begin
                    Form_FeuVert := TForm_FeuVert.create(nil);
                    Form_FeuVert.show;
                    Daccord_Confirmer:=false;
                    Daccord_Annuler:=false;
                   while not(Daccord_Confirmer) and not(Daccord_Annuler)
                      do Application.ProcessMessages;
                    if Daccord_Annuler
                      then
                        begin
                          result := True;
                          Form_FeuVert.free;
                          Form_FeuVert:=Nil;
                          Exit;
                        end;
                    Form_FeuVert.free;
                    Form_FeuVert:=Nil;
                  end;

              AnglePolaire:=-GerMountSetup.AnglePolairePointage;
              if TelescopeOuest
                then LongitudeHoraire:=GerMountSetup.LongitudeHorairePointage-Pi
                else LongitudeHoraire:=GerMountSetup.LongitudeHorairePointage+Pi ;
            end;

        AlphaTemp:=LongitudeHoraire-GerMountSetup.LongitudeHorairePointage;
        DeltaTemp:=AnglePolaire-GerMountSetup.AnglePolairePointage;
        TimerRetardateur.Declanchement:=GerMountSetup.LongitudeHorairePointage;

         (* premier test : uniquement pour la proc�dure de retournement.
         Sommes-nous en avance horaire de la zone principale de pointage ?
         Si oui, il faut retarder le d�clanchement du mouvement delta  *)
        if ((GerMountSetup.LongitudeHorairePointage<-Pi_d_2-GerMountSetup.Degagement_Est)
            or (GerMountSetup.LongitudeHorairePointage>Pi_d_2+GerMountSetup.Degagement_Ouest))
            and (GerMountSetup.AnglePolairePointage*Sign(LongLat.Lat)<0)               //H�misph�re Sud
          then
            begin
              TimerRetardateur.DabordAlpha:=True;
              TimerRetardateur.Declanchement:=-Pi_d_2-GerMountSetup.Degagement_Est+PiedDePilote;
            end;

          (* Deuxi�me test: sommes-nous en zone de poursuite horaire critique ?
          Si oui, il faut retarder le d�clanchement du mouvement delta *)
        if ((GerMountSetup.LongitudeHorairePointage>Pi_d_2+GerMountSetup.Degagement_Ouest)
            or (GerMountSetup.LongitudeHorairePointage<-Pi_d_2-GerMountSetup.Degagement_Est))
            and (GerMountSetup.AnglePolairePointage*Sign(LongLat.Lat)>0)               //H�misph�re Sud
          then
            begin
              TimerRetardateur.DabordAlpha:=True;
              TimerRetardateur.Declanchement:=Pi_d_2+GerMountSetup.Degagement_Ouest-PiedDePilote;
            end;

          (* Troisi�me test: se rapproche-t-on de l'horizon ?
          et peut-on encore retarder le d�clanchement du mouvement delta ?
          si oui, il faudra le faire *)
        if (TelescopeOuest and (DeltaTemp*Sign(LongLat.Lat)>0)
                                 and (AlphaTemp>DeltaTemp*Sign(LongLat.Lat)))
            or
            (not(TelescopeOuest) and (DeltaTemp*Sign(LongLat.Lat)<0)
                                 and (AlphaTemp<DeltaTemp*Sign(LongLat.Lat)))
          then
            begin
              TimerRetardateur.DabordAlpha:=True;
              TimerRetardateur.Declanchement:=LongitudeHoraire-DeltaTemp;
            end;

          (* Quatri�me test : T�lescope sur Z�nith Est, sommes-nous en zone
          principale de pointage, plus bas que la d�clinaison critique Ouest ?
          si oui, il faudra retarder le mouvement en alpha *)
        if (GerMountSetup.LongitudeHorairePointage>-Pi_d_2-GerMountSetup.Degagement_Est)
            and (Sign(LongLat.Lat)*                                      //H�misph�re Sud
                       (GerMountSetup.Declinaison_Critique_Ouest-GerMountSetup.AnglePolairePointage)>Pi_d_2)
            and RetournementEnCours
          then
            begin
              TimerRetardateur.DabordDelta:=True;
              TimerRetardateur.Declanchement:= -GerMountSetup.AnglePolairePointage
                    -Sign(LongLat.Lat)*                                  //H�misph�re Sud
                     (Pi_d_2-GerMountSetup.LongitudeHorairePointage+GerMountSetup.Degagement_Ouest-PiedDePilote);
            end;

          (* Cinqui�me test : T�lescope sur Z�nith Ouest, sommes-nous en zone
          principale de pointage, plus bas que la d�clinaison critique Est ?
          si oui, il faudra retarder le mouvement en alpha *)

        if (GerMountSetup.LongitudeHorairePointage<Pi_d_2-GerMountSetup.Degagement_Est)
            and (Sign(LongLat.Lat)*                                      //H�misph�re Sud
                         (GerMountSetup.Declinaison_Critique_Est+GerMountSetup.AnglePolairePointage)>Pi_d_2)
            and RetournementEnCours
          then
            begin
              TimerRetardateur.DabordDelta:=True;
              TimerRetardateur.Declanchement:= -GerMountSetup.AnglePolairePointage
                   +Sign(LongLat.Lat)*                                   //H�misph�re Sud
                     (Pi_d_2+GerMountSetup.LongitudeHorairePointage+GerMountSetup.Degagement_Est-PiedDePilote);
            end;

  end
  else      // *********************** Monture � fourche ************************
  begin

        AnglePolaire:=Delta-  Pi_d_2;
        DeltaTemp   :=Delta - L_DeltaPointage;

        if GerMountSetup.Passage_Interdit
          then
            (*l'algorithme est tel que le t�lescope ne peut pas faire des tours
             complets autour de l'axe horaire.*)
            begin
              if (L_Alphapointage=STSTel.AlphaOldPoint)
                  and (L_DeltaPointage=STSTel.DeltaOldPoint)
                then GerMountSetup.AngleHorairePointage:=GerMountSetup.AngleHorairePointagePrecedent+
                  (now-GerMountSetup.DatePointagePrecedent)*OmegaSid
                else GerMountSetup.AngleHorairePointage:=AH(Longitude,L_AlphaPointage,SpeedIntMCMT.GMT);

              AlphaTemp := GerMountSetup.AngleHoraire-GerMountSetup.AngleHorairePointage;
            end
          else
            (* Algorithme plus rapide autorisant le t�lescope � tourner librement
            autour de l'axe alpha, ce qui peut �tre un inconv�nient. *)
            begin
              AlphaTemp := L_AlphaPointage;
              if Alpha - Pi > L_AlphaPointage
                then AlphaTemp := AlphaTemp +  T_pi
                else
                if L_AlphaPointage - Pi > Alpha then Alpha := Alpha +  T_pi ;
              AlphaTemp := AlphaTemp - Alpha;
            end;

         (*test: se rapproche-t-on de l'horizon ?
          et peut-on retarder le d�clanchement du mouvement delta ?
          si oui, il faudra le faire *)

        if (Delta<L_DeltaPointage)
            and (abs(AlphaTemp)>abs(DeltaTemp))
          then
            begin
              TimerRetardateur.DabordAlpha:=True;
              if (alphatemp>0)
                then TimerRetardateur.Declanchement:=GerMountSetup.AngleHoraire+DeltaTemp
                else TimerRetardateur.Declanchement:=GerMountSetup.AngleHoraire-DeltaTemp;
            end;
  end;

  MoveStepsDelta := round(SpeedIntMCMT.Resol_DEC * DeltaTemp / ( T_pi ));
  MoveStepsAlpha := round(SpeedIntMCMT.Resol_AD * AlphaTemp  / (T_pi));
  Logproc(procName,'Movesteps ' + IntToStr(MoveStepsAlpha) + ' ' + FloatToStr(MoveStepsDelta));

  TimerBacklash.Alpha := false;
  TimerBacklash.Delta := false;


  Try
  if BackLash.Activated then
  begin
      If (MoveStepsAlpha<0) then
      begin
         TimerBacklash.Alpha := True;
         Dec(MoveStepsAlpha, BackLash.StepAD);
      end;

      Case BackLash.Delta_Mode of
      0:  ;          //  Backlash Delta d�activ�
      1:  begin      //  Correction lors d''un d�placement vers le Nord
               if ((MoveStepsDelta > 0)
               and (L_DeltaPointage < (90 - BackLash.Value / 60) * pi / 180))
              then
                begin
                  TimerBacklash.Delta := True;
                  Inc(MoveStepsDelta, BackLash.StepDEC);
                  BackLash.North:=True;
                end;
          end;
      2:  begin      // Correction lors d''un d�placement vers le Sud
            if ((MoveStepsDelta < 0)
                and (L_DeltaPointage > (-90 + Backlash.value / 60) * pi / 180))
              then
                begin
                  TimerBacklash.Delta := True;
                  Dec(MoveStepsDelta, BackLash.StepDEC);
                  BackLash.North:=false;
                end;
          end;
      3:  begin      //  Correction lors d''un d�placement vers le Z�nith
            Test:=(tan(Latitude)<tan(Delta)*cos(GerMountSetup.AngleHoraire))
                          xor (AnglePolaire<0);
            if (MoveStepsDelta>0) and Test
              then
                begin
                  TimerBacklash.Delta := True;
                  Inc(MoveStepsDelta,BackLash.StepDEC);
                  BackLash.North:=True;
                end;
            if (MoveStepsDelta<0) and not(Test)
              then
                begin
                  TimerBacklash.Delta := True;
                  Dec(MoveStepsDelta,BackLash.StepDEC);
                  BackLash.North:=false;
                end;
          end;
     4:  begin      //  Correction lors d'un d�placement vers l'Horizon
            Test:=(tan(Latitude)<tan(Delta)*cos(GerMountSetup.AngleHoraire))
                          xor (AnglePolaire<0);
            if (MoveStepsDelta>0) and not(Test)
              then
                begin
                  TimerBacklash.Delta := True;
                  Inc(MoveStepsDelta,BackLash.StepDEC);
                  BackLash.North:=True;
                end;
            if (MoveStepsDelta<0) and Test
              then
                begin
                  TimerBacklash.Delta := True;
                  Dec(MoveStepsDelta,BackLash.StepDEC);
                  BackLash.North:=false;
                end;
          end;
      end; {case}
  end; // Backslash activated

  if TimerRetardateur.DabordDelta or TimerRetardateur.DabordAlpha  then
  begin
   TimerRetardateur.Interval      := 1000;
   TimerRetardateur.AlphaCroissant:=(AlphaTemp>0);
   TimerRetardateur.DeltaCroissant:=(DeltaTemp>0);

   if TimerRetardateur.DabordDelta then           {D�clanchement mouvement Delta, Alpha en attente}
   begin
        TelescopeMoveAxe(Hcom,MoveStepsDelta, C_Delta);
        TimerRetardateur.Movesteps := MovestepsAlpha;
        TimerRetardateur.Enabled := True;
    end
    else          {D�clanchement mouvement Alpha, Delta en attente}
    begin
        TelescopeMoveAxe(Hcom,MoveStepsAlpha, C_Alpha);
        TimerRetardateur.Movesteps := MovestepsDelta;
        TimerRetardateur.Enabled := True;
    end;
  end
  else               {D�clanchement simultan� Alpha et Delta}
   TelescopeMove(Hcom,MoveStepsAlpha,MoveStepsDelta);

  Finally
  End;


  if TimerBacklash.Alpha or TimerBacklash.Delta then
  begin
    TimerBacklash.Interval := 1000;
    TimerBacklash.Flag     := 0;
    TimerBacklash.Enabled  := True;
    Logproc(procName,'Backslash enabled');
  end;

  STSTel.AlphaOldPoint := Alpha;  // always in this proc.
  STSTel.DeltaOldPoint := Delta;


  GerMountSetup.AngleHorairePointagePrecedent:=GerMountSetup.AngleHoraire;
  GerMountSetup.DatePointagePrecedent:=now;

  if GerMountSetup.monture_allemande then
  begin
    GerMountSetupSem.Enter;
    GerMountSetup.TelescopeZenithOuest:= (AnglePolaire*Sign(LongLat.Lat)>0);         // H�misph�re sud
    GerMountSetupSem.Leave;
    WriteBoolToReg('TelescopeZenithOuest',TelescopeOuest);
  end;


 // Must be send back
  Accuracy := DegToRad(1 / 30);
  result   := True;

  Logproc(procName,'End Slewing');
  LogEndProc_DLL(ProcName);

end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Here proceeds actions according to latest telescope position.
// This is called by a separed PROCESS/THREAD, so be very carefull here, do not DISPLAY
// any messages, or don't be stuck here, otherwise he could make crash PRISM
// Don't take care of pointing accuracy here, because PRISM does it for you and
// stops the PROCESS/THREAD if necessary.
// Sometimes, nothing has to be done here, because a simple order to the device can be
// provided thru InitTelescopeSeeker_DLL, so leave it like that. (ie lx200 case)

function Execute_TelescopeSeeker_DLL(var Alpha, Delta: Double): Boolean; stdcall;
begin
  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Executes here code to stop forcefully the telescope slewing...
// Do not put graphic stuff here... this things goes into a thread.

function StopSlewing_TelescopeSeekerDevice_DLL: Boolean; stdcall;

begin
  TimerBacklash.Enabled    := false;
  TimerRetardateur.Enabled := false;

  Try
  StopSlewing(Hcom,C_Alpha);
  StopSlewing(Hcom,C_Delta);
  Finally
  End;

  result := True;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Close what is necessary to be closed after pointing completion
// Sometimes, nothing has to be closed, so leave it like that.

function CloseTelescopeSeeker_DLL: Boolean; stdcall;
begin
  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Telescope drives


function InitTelescopeEngine_DLL(var EngineStructConf: TEngineStruct): Boolean; stdcall;
Const
 procname='InitTelescopeEngine_DLL';

begin
  LogEnterProc_DLL(ProcName);


  with EngineStructConf do
  begin
    AbleTo_WidthStand_SpeedChanges := True; // If True
                                              // ReturnToSideralSpeed must be filled with appropriate code
                                              // SetDriveSpeed must be filled with appropriate code


    AbleTo_ApplyOffsets := True; // If True
                                // MoveOffsetDirectAD must be filled with appropriate code

    AbleTo_Stop_All := True; // If True
                            // STOP_ALL_Telescope must be filled with appropriate code

    Able_ToSupportEmbeddedSpeeds := True; // Can the drives has solar,Mpec and moon speeds embedded
                                         // If True
                                         // ReturnToSideralSpeed
                                         // StartPecEngine            name 'StartPecSpeedEngineDevice_DLL',
                                         // StartSolarEngine          name 'StartSolarSpeedEngineDevice_DLL',
                                         // StartMoonEngine           name 'StartMoonSpeedEngineDevice_DLL',
                                         // Must be filled with appropriate code

    HasAConsole      := True;  // Must be able to show a Console panel...
    HasASpeedpanel   := False; // Must be able to show a Speed panel...
    AbleToPulseGuide := True;
    RateEvent        := 10000; // Msec
    AbleToPark       := True;

  end;

  result := True;

  LogEndProc_DLL(ProcName);
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If HasAConsole=False this function does not need to exist

function ShowConsoleEngine(Aowner: Tcomponent): Boolean; stdcall;
begin
  try
    FormConsole.PageConsole.ActivePageIndex := 0;
    FormConsole.Show;
    result := NOError;
  except
    result := Error;
  end;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If HasASpeedpanel=False this function does not need to exist

function ShowSpeedEngine(Aowner: Tcomponent): Boolean; stdcall;
begin
  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GetSpeedAlpha(Speed: TSpeed; var RelativeSpeed: Integer): Boolean; stdcall;
begin
 // Ce sont les vitesses de centrage qui sont repr�sent�es ici, c''est-�-dire celles
 // qui servent dans la raquette du t�lescope


 case Speed of // With respect to the sideral speed (ie = 1x);
    SMax    : RelativeSpeed := round(SpeedIntMCMT.V_Point_R_AD * 86164.101 / SpeedIntMCMT.Resol_AD);

      // R�cup�rer les vitesses par rapport � la vitesse sid�rale ( voir l''unit� Vitesses.pas ) :
      // Comme RelativeSpeed doit necessairement �tre entier, on est oblig� de la consid�rer
      // �gal � 1 pour des vitesses r�ellement inf�rieure � 1 fois la vitesse sid�rale :
    SMedium : RelativeSpeed := round(SpeedIntMCMT.V_Point_L_AD * 86164.101 / SpeedIntMCMT.Resol_AD);
      // idem mais en fonction de la vitesse de centrage m�dium

    SSideral: RelativeSpeed := round(SpeedIntMCMT.V_Corr_P_AD * 86164.101 / SpeedIntMCMT.Resol_AD);
      // idem mais en fonction de la vitesse de centrage minimum

    SNone   : RelativeSpeed := 1;
 end;

 if RelativeSpeed = 0 then RelativeSpeed := 1;

 result := NOERROR;
end; //Fait

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GetSpeedDelta(Speed: TSpeed; var RelativeSpeed: Integer): Boolean; stdcall;
begin
  Try
  Case Speed of // With respect to the sideral speed (ie = 1x);
    SMax    : RelativeSpeed  := round(SpeedIntMCMT.V_Point_R_DEC * 86164.101 / SpeedIntMCMT.Resol_DEC);
    SMedium : RelativeSpeed  := round(SpeedIntMCMT.V_Point_L_DEC * 86164.101 / SpeedIntMCMT.Resol_DEC);
    SSideral: RelativeSpeed  := round(SpeedIntMCMT.V_Corr_P_DEC  * 86164.101 / SpeedIntMCMT.Resol_DEC);
    SNone   : RelativeSpeed  := 1;
  end;

  if RelativeSpeed = 0 then RelativeSpeed := 1;
  result := NOERROR;
  Except
   On E:Exception do raise Exception.Create('Erreur GetSpeedDelta -> '+E.Message);
  End;
end;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function MoveTelescope(Direction: TDirection; Speed: TSpeed): Boolean; stdcall;
var
  astring: shortstring ;
  LSpeeds: Tspeeds     ;
  OutC   : Integer     ;

Const
  procName='MoveTelescope';


begin
  LogEnterProc_DLL(ProcName);
  result := ERROR;


  If (InterlockedCompareExchange(Stopping,1,1)=1) then   // Do not enter because stopping is pending
  begin
   result := NOERROR;
   Logproc(procName,'Exits because a stopping is still pending');
   LogEndProc_DLL(ProcName);
   Exit;
  end;

  SpeedsSection.Enter;
  LSpeeds:=Speeds;
  SpeedsSection.Leave;

  Logproc(procName,'Move_Telescope start');

  //  Monture allemande ***********************************************
  if GerMountSetup.Monture_Allemande and TelescopeOuest then
  begin
      case Direction of
        DNord  : Direction:=DSud  ;
        DSud   : Direction:=DNord ;
        DEst   : ;
        DOuest : ;
      end;
  end;

  Try

      case Direction of
        DNord: begin
                // On a demand� d''aller vers le Nord : Delta augmente
            case Speed of
              SMax: begin //Fournir la vitesse VMax :
                  while IsAxisSlewing_BackLash(Hcom,C_Delta) do WaitMainthread(procName);

                  astring := 'X' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 1);
                  LSpeeds.SpeedDelta := SMax;
                end;
              SMedium: begin //Fournir la vitesse VMed :
                  while IsAxisSlewing_BackLash(Hcom,C_Delta) do WaitMainthread(procName);

                  astring := 'G' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 1);
                  LSpeeds.SpeedDelta := SMedium;
                end;
              SSideral: begin // Fournir la vitesse VMin :
                  while IsAxisSlewing_BackLash(Hcom,C_Delta) do WaitMainthread(procName);

                  astring := 'D' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 1);
                  LSpeeds.SpeedDelta := SSideral;
                end;
            end;
          end;

        DSud: begin
                // On a demand� d''aller vers le Sud : Delta diminue
            case Speed of
              SMax: begin //Fournir la vitesse VMax :
                  while IsAxisSlewing_BackLash(Hcom,C_Delta) do WaitMainthread(procName);

                  astring := 'W' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 1);
                  LSpeeds.SpeedDelta := SMax;
                end;
              SMedium: begin //Fournir la vitesse VMed :
                  while IsAxisSlewing_BackLash(Hcom,C_Delta) do WaitMainthread(procName);

                  astring := 'F' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 1);
                  LSpeeds.SpeedDelta := SMedium;
                end;
              SSideral: begin // Fournir la vitesse VMin :
                  while IsAxisSlewing_BackLash(Hcom,C_Delta) do WaitMainthread(procName);

                  astring := 'Q' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 1);
                  LSpeeds.SpeedDelta := SSideral;
                end;
            end;
          end;

        DEst: begin
                // On a demand� d'aller vers l'Est : Alpha augmente
            case Speed of
              SMax: begin //Fournir la vitesse VMax :
                  while IsAxisSlewing_BackLash(Hcom,C_Alpha) do WaitMainthread(procName);         // Attend la fin de rampe

                  astring := 'W' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 0);
                  LSpeeds.SpeedAlpha := SMax;
                end;
              SMedium: begin //Fournir la vitesse VMed :
                  while IsAxisSlewing_BackLash(Hcom,C_Alpha) do WaitMainthread(procName);

                  astring := 'F' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 0);
                  LSpeeds.SpeedAlpha := SMedium;
                end;
              SSideral: begin // Fournir la vitesse VMin :
                  while IsAxisSlewing_BackLash(Hcom,C_Alpha) do WaitMainthread(procName);

                  astring := 'Q' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 0);
                  LSpeeds.SpeedAlpha := SSideral;
                end;
            end;
          end;

        DOuest: begin
                 // On a demand� d'aller vers l'Ouest : Alpha diminue
            case Speed of
              SMax: begin //Fournir la vitesse VMax :
                  while IsAxisSlewing_BackLash(Hcom,C_Alpha) do WaitMainthread(procName);

                  astring := 'X' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 0);
                  LSpeeds.SpeedAlpha := SMax;
                end;
              SMedium: begin //Fournir la vitesse VMed :
                  while IsAxisSlewing_BackLash(Hcom,C_Alpha) do WaitMainthread(procName);

                  astring := 'G' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 0);
                  LSpeeds.SpeedAlpha := SMedium;
                end;
              SSideral: begin // Fournir la vitesse VMin :
                  while IsAxisSlewing_BackLash(Hcom,C_Alpha) do WaitMainthread(procName);

                  astring := 'D' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
                  sendcommand(Hcom,astring, 0, 0);
                  LSpeeds.SpeedAlpha := SSideral;
                end;
            end;
          end;
      end;


  Finally
  End;

  result := NOERROR;

  Logproc(procName,'Move_Telescope end');

  SpeedsSection.Enter;
  Speeds:=LSpeeds;
  SpeedsSection.Leave;

  LogEndProc_DLL(ProcName);
end;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Stops the telescope (Alpha;delta : axis) and put it back in tracking mode

function STOPTelescope(Alpha, Delta: Boolean): Boolean; stdcall;

// Stoppe le t�lescope depuis les instructions envoy�es par la raquette du t�lescope
// une fois la commande stop faite, il faut un certain temps que la rampe descende pour que ca rende la main
// Ca rends la raquette pas bien reactive...


var
  astring : shortstring ;
  LSpeeds : Tspeeds     ;

Const
  procName='STOPTelescope';

begin
  InterlockedIncrement(Stopping);

  LogEnterProc_DLL(ProcName);

  SpeedsSection.Enter;
  LSpeeds:=Speeds;
  SpeedsSection.Leave;



  TimerBacklash.Enabled    := False;
  TimerRetardateur.Enabled := False;

  Logproc(procName,'Stop_Telescope start');

  Try

  If Alpha then // Si l''arr�t en Alpha a �t� demand� ...
  begin
    case LSpeeds.SpeedAlpha of // With respect to the sideral speed (ie = 1x);

      SMax:
        begin
          if IsAxisSlewing_BackLash(hcom,C_Alpha) then
          begin
           StopSlewing(hcom,C_Alpha);
           while IsAxisSlewing_BackLash(hcom,C_Alpha) do      // Attends la fin de la rampe
             WaitMainthread(procName);
          end;
        end;

      SMedium:
        begin
          if IsAxisSlewing_BackLash(hcom,C_Alpha) then
          begin
           StopSlewing(hcom,C_Alpha);
           while IsAxisSlewing_BackLash(hcom,C_Alpha) do  // Attends la fin de la rampe
             WaitMainthread(procName);
          end;
        end;

      SSideral: begin
          //while IsAxisSlewing_OC(hcom,C_Alpha) do WaitMainthread(procName);
          astring := 'S' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
          sendcommand(hcom,astring, 0, 0);
        end;

      SNone: ;
    end;
  end;

  If Delta then // Si l''arr�t en Delta a �t� demand�
  begin
    case LSpeeds.SpeedDelta of // With respect to the sideral speed (ie = 1x);

      SMax:
        begin
          If IsAxisSlewing_BackLash(Hcom,C_Delta) then
          begin
           StopSlewing(Hcom,C_Delta);
           while IsAxisSlewing_BackLash(hcom,C_Delta) do  // Attends la fin de la rampe
             WaitMainthread(procName);
          end;
        end;

      SMedium:
        begin
          If IsAxisSlewing_BackLash(Hcom,C_Delta) then
          begin
           StopSlewing(Hcom,C_Delta);
           while IsAxisSlewing_BackLash(hcom,C_Delta) do  // Attends la fin de la rampe
             WaitMainthread(procName);
          end;
        end;

      SSideral: begin
          //while IsAxisSlewing_BackLash(Hcom,C_Delta) do WaitMainthread(procName);
          astring := 'S' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
          sendcommand(Hcom,astring, 0, 1);
        end;

      SNone: ;
    end;
  end;

  Finally
  InterlockedDecrement(Stopping);
  End;

  Logproc(procName,'Stop_Telescope end');
  LogEndProc_DLL(ProcName);

  Result := True;
end;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This function is called by a timer every RateEvent ms, this timer
// has his own PROCESS/THREAD so do not display any form here
// Can be used to check to some extent the telescope status
// If not used leave result:=NOError;

function MoveEventTelescope: Boolean; stdcall;
begin
  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Stops completely the telescope
// If AbleTo_Stop_All is false, leave it as it is.

function STOP_ALL_Telescope: Boolean; stdcall;
Const
  procName='STOP_ALL_Telescope';

var
  astring : ShortString;

begin
  Result := False;

  while IsAxisSlewing_BackLash(Hcom,C_Alpha) do WaitMainthread(procName);
  astring := 'P' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0); // Commande Park pour AD
  sendcommand(Hcom,astring, 0, 0);

  while IsAxisSlewing_BackLash(Hcom,C_Delta) do WaitMainthread(procName);
  astring := 'R' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0); // Commande vitesse = 0 en DEC;
  sendcommand(Hcom,astring, 0, 1);
  Result := True;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Applies an offset to the telescope
// If AbleTo_ApplyOffsets is false, leave it as it is.

function MoveOffsetDirectAD(AlphaD, DeltaD: Double; Speed: TSpeed): Boolean; stdcall;
// NE SUPPORTE Qu''une SEULE vitesse !

Const
  procName='MoveOffsetDirectAD';

var
  NumOfSteps: longint;
  astring   : ShortString;
  MMSB, MSB,
  LSB, LLSB : byte;


begin
  Try

  NumOfSteps := round(SpeedIntMCMT.Resol_AD * AlphaD / (T_pi));

  if abs(NumOfSteps) > 0 then
  begin
    MMSB := (abs(NumOfSteps) shr 24) and $FF;
    MSB  := (abs(NumOfSteps) shr 16) and $FF;
    LSB  := (abs(NumOfSteps) shr 8 ) and $FF;
    LLSB :=  abs(NumOfSteps) and $FF;
    // Bit de signe
    if NumOfSteps < 0 then MMSB := MMSB or $80;

    while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(ProcName);
    astring := 'p' + AnsiChar(MMSB) + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB);
    sendcommand(Hcom,astring, 0, 0);
  end;


  NumOfSteps := round(SpeedIntMCMT.Resol_DEC * DeltaD / (T_pi));
  if abs(NumOfSteps) > 0 then
  begin
    MMSB := (abs(NumOfSteps) shr 24) and $FF;
    MSB := (abs(NumOfSteps)  shr 16) and $FF;
    LSB := (abs(NumOfSteps)  shr 8 ) and $FF;
    LLSB := abs(NumOfSteps)  and $FF;
    // Bit de signe
    if NumOfSteps < 0 then MMSB := MMSB or $80;

    while (IsAxisSlewing_BackLash(Hcom,C_Delta)) do WaitMainthread(ProcName);
    astring := 'p' + AnsiChar(MMSB) + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB);
    sendcommand(Hcom,astring, 0, 1);
  end;

  Finally
  End;

  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If AbleTo_WidthStand_SpeedChanges is false, leave it as it is.
// Otherwise allows the telescope to come back to sideral speed
// If PEC correction is activated, here you should use this function to desactivate it
// as so as the moon and solar speeds.

function ReturnToSideralSpeed: Boolean; stdcall;
var
  astring   : shortstring;

Const
  ProcName='ReturnToSideralSpeed';

begin
  Try

  while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(ProcName);
  astring := 'N' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0); // Commande No Park
  sendcommand(Hcom,astring, 0, 0);

  while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(ProcName);
  astring := 'S' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0); // Vitesse Siderale en AD
  sendcommand(Hcom,astring, 0, 0);

  while (IsAxisSlewing_BackLash(Hcom,C_Delta)) do WaitMainthread(ProcName);
  astring := 'S' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
  sendcommand(Hcom,astring, 0, 1);

  SpeedIntMCMT.V_Guidage_AD := SpeedIntMCMT.V_Guidage_AD_SID;

  Finally
  End;

  result := True;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If AbleTo_WidthStand_SpeedChanges is false, leave it as it is.
// Otherwise allows the telescope to come back to sideral speed
// If PEC correction is activated, here you should use this function to desactivate it
// as so as the moon and solar speeds.

function StopFreeSpeed: Boolean; stdcall;
var
  astring   : shortstring;

Const
  Procname='StopFreeSpeed';

begin
  Try

  while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(Procname);

  astring := 'N' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0); // Commande No Park
  sendcommand(Hcom,astring, 0, 0);

  while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(Procname);

  astring := 'S' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
  sendcommand(Hcom,astring, 0, 0);

  while (IsAxisSlewing_BackLash(Hcom,C_Delta)) do WaitMainthread(Procname);

  astring := 'S' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
  sendcommand(Hcom,astring, 0, 1);

  SpeedIntMCMT.V_Guidage_AD := SpeedIntMCMT.V_Guidage_AD_SID;

  Finally
  End;

  result := True;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If AbleTo_WidthStand_SpeedChanges is false, leave it as it is.
// en rad/heures et pour un objet mobile, type comete ou aster

function SetFreeSpeed(AlphaSpeed_rad_H, DeltaSpeed_rad_H: Double): Boolean; stdcall;
var
  astring : ShortString;
  SIGNE,
  MSB,
  LSB,LLSB: byte;
  aval    : Double;
  DepSky  : Double;
  Rapport : Double;

Const
  Procname='SetFreeSpeed';

begin
  Try

  if AlphaSpeed_rad_H <> 0 then
  begin
    DepSky := (T_pi) / (86164.0) * 3600; // rad par heure
    Rapport := (DepSky + AlphaSpeed_rad_H) / DepSky;

    SpeedIntMCMT.V_Guidage_AD := SpeedIntMCMT.V_Guidage_AD * Rapport;
    aval := SpeedIntMCMT.V_Guidage_AD;

    aval := 625000 / aval;
    MSB := (round(abs(aval)) shr 8) and $FF;
    LSB := (round(abs(aval))) and $FF;
    LLSB := round(abs(10 * frac(aval))) and $FF;
    SIGNE := 0;

    if aval > 0 then SIGNE := 1;
    while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(Procname);
    astring := 'R' + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB) + AnsiChar(SIGNE);
    sendcommand(Hcom,astring, 0, 0);
  end;

  if DeltaSpeed_rad_H <> 0 then
  begin
    aval := DeltaSpeed_rad_H * SpeedIntMCMT.Resol_DEC / (2 * 3600 * PI); // �setp per seconds
    if (aval > 10) and (aval < 5000) then
    begin
      aval := 625000 / aval;
      MSB := (round(abs(aval)) shr 8) and $FF;
      LSB := (round(abs(aval))) and $FF;
      LLSB := round(abs(10 * frac(aval))) and $FF;
      SIGNE := 0;
      if aval > 0 then SIGNE := 1;
      while (IsAxisSlewing_BackLash(Hcom,C_Delta)) do WaitMainthread(Procname);
      astring := 'R' + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB) + AnsiChar(SIGNE);
      sendcommand(Hcom,astring, 0, 1);
    end
    else
    begin

    end;
  end;


  Finally
  End;

  result := True;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start Solar speed engine ... stopped by ReturnToSideralSpeed

function StartSolarEngine: Boolean; stdcall;

Const
  procName='StartSolarEngine';

var
  astring   : ShortString;
  SIGNE,
  MSB,
  LSB, LLSB : byte;
  aval      : Double;


begin
  Try

  // Va vers l''EST donc ralenti un peu la vitesse
  SpeedIntMCMT.V_Guidage_AD := round(SpeedIntMCMT.V_Guidage_AD_SID * ((360-0.9856) /360.0) );

  aval  := SpeedIntMCMT.V_Guidage_AD;
  aval  := 625000 / aval;
  MSB   := (round(abs(aval)) shr 8) and $FF;
  LSB   := (round(abs(aval))) and $FF;
  LLSB  := round(abs(10 * frac(aval))) and $FF;
  SIGNE := 0;

  if (aval>0) then SIGNE:=1;

  while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(procName);
  astring := 'R' + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB) + AnsiChar(SIGNE);
  sendcommand(Hcom,astring, 0, 0);

  finally
  end;
  result := True;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start Moon speed engine ... stopped by ReturnToSideralSpeed

function StartMoonEngine: Boolean; stdcall;
Const
  procName='StartMoonEngine';


var
  astring   : ShortString;
  SIGNE,
  MSB, LSB,
  LLSB      : byte;
  aval      : Double;

begin

  Try
  SpeedIntMCMT.V_Guidage_AD := round(SpeedIntMCMT.V_Guidage_AD_SID * 0.9662);
  aval         := SpeedIntMCMT.V_Guidage_AD;
  aval         := 625000 / aval;
  MSB          := (round(abs(aval)) shr 8) and $FF;
  LSB          := (round(abs(aval))) and $FF;
  LLSB         := round(abs(10 * frac(aval))) and $FF;
  SIGNE        := 0;
  if aval > 0 then SIGNE := 1;

  while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(procName);

  astring := 'R' + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB) + AnsiChar(SIGNE);
  sendcommand(Hcom,astring, 0, 0);
  finally
  end;

  result := True;
end;

// Closes the telescope engine
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function CloseEngine: Boolean; stdcall;
begin
 //Pas la peine de la fermer puisque le port est ferme des que l''appel est termin� !
  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// King

function StartKingDevice(latitude_rad, Longitude_rad, Altitude_m, pression_mb: Double): Boolean; stdcall;
begin
  King_Activated := True;
  result         := NOERROR;
end;

function StopKingDevice: Boolean; stdcall;
begin
  King_Activated := false;
  ReturnToSideralSpeed;
  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function PulseGuideAscom(RaInd, DecInd, MsRa, MsDec: Word): Boolean; stdcall;
//   RaInd=0 = ALpha+
//   RaInd=1 = ALpha-

begin
  Try
  Result := False;

  If (MsRa>0) then
  begin
    If (RaInd=0) then MoveTelescope(DEst, SSideral)
                 else MoveTelescope(DOuest, SSideral);

    TimerAlphaPulse.Interval := MsRa;
    TimerAlphaPulse.Enabled  := True;
    TimerAlphaPulse.IsPulse  := True;
  end;

  if (MsDec>0) then
  begin
    If (DecInd=0) then MoveTelescope(DNord, SSideral)
                  else MoveTelescope(DSud, SSideral);

    TimerDecliPulse.Interval := MsDec;
    TimerDecliPulse.Enabled  := True;
    TimerDecliPulse.IsPulse  := True;
  end;

  Result := True;
  Except
  End;
end;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function IsPulseGuiding: boolean; stdcall;
begin
  result := (TimerAlphaPulse.IsPulse or TimerDecliPulse.IsPulse);
end;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function PulseGuide(RaInd, DecInd, MsRa, MsDec: Word): Boolean; stdcall;
// function(RaInd,DecInd,MsRa,MsDec:Word):Boolean; stdcall;
//   RaInd=0 = ALpha+
//   RaInd=1 = ALpha-


Const
  StopOrderAlpha = True;
  StopOrderDecli = True;


begin
  // BLOCKING
  Result := True;

  If (MsRa=0) and (MsDec=0) then Exit;


  If (MsRa>0) then
  begin
    If (RaInd=0) then MoveTelescope(DEst  , SSideral)
                 else MoveTelescope(DOuest, SSideral);
  end;

  If (MsDec>0) then
  begin
    If (DecInd=0) then MoveTelescope(DNord, SSideral)
                  else MoveTelescope(DSud , SSideral);
  end;

  If (MsRa>MsDec) then
  Begin
   If (MsDec>0) then
   begin
    Sleep(MsDec);
    STOPTelescope(Not StopOrderAlpha, StopOrderDecli);
   end;

   Sleep(MsRa-MsDec);
   STOPTelescope(StopOrderAlpha, Not StopOrderDecli);
  End
  else
  begin
   If (MsRa>0) then
   begin
    Sleep(MsRa);
    STOPTelescope(StopOrderAlpha,Not StopOrderDecli);
   end;

   Sleep(MsDec-MsRa);
   STOPTelescope(Not StopOrderAlpha, StopOrderDecli);
  end;

end;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function IsParked(var Parked: boolean): boolean; stdcall;
begin
  Parked := Park;
  result := True;
end;

function IsParking(var Parking: boolean): boolean; stdcall;
begin
  Parking := is_Parking;
  result := True;
end;


function GetCurrentSpeedRA(var D: Double): Boolean; stdcall;
begin
  // Return current speed : 1.00 is sidereal speed ie 15.04 arsec/sec
  Result := True;

  Try
  Try

  ConsoleDataSem .Enter;
  If (InfosConsoleVar.NbStepPer_sec_meas_Alpha<>0) then
   D:= (InfosConsoleVar.NbStepPer_sec_meas_Alpha/SpeedIntMCMT.V_Guidage_AD)
  else
   D:=0;
  Finally
  ConsoleDataSem.Leave;
  End;

  Except
  D:=0;
  End;
end;

function GetCurrentSpeedDEC(var D: Double): Boolean; stdcall;
begin
  // Return current speed : 1.00 is sidereal speed ie 15.04 arsec/sec
  Result := True;

  Try
  Try

  ConsoleDataSem .Enter;
  If (InfosConsoleVar.NbStepPer_sec_meas_Delta<>0) then
   D:= (InfosConsoleVar.NbStepPer_sec_meas_Delta/SpeedIntMCMT.Resol_DEC)*(360*3600/15.041)
  else
   D:=0;
  Finally
  ConsoleDataSem.Leave;
  End;


  Except
  D:= 1;
  End;
end;


//////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
// PEC

function StartPecSpeedEngine(Periode_arcsec, Phase_arcsec, Amplitude_arcsec: array of Double): Boolean; stdcall;
var i: Integer;

begin
  MaxPECHarmos := length(Periode_arcsec);
  PECSection.Enter;
  for i := 1 to MaxPECHarmos do
  begin
    PEC.Phase_PEC    [i] := Phase_arcsec    [i - 1];
    PEC.Periode_PEC  [i] := Periode_arcsec  [i - 1];
    PEC.Amplitude_PEC[i] := Amplitude_arcsec[i - 1];
  end;
  PECSection.Leave;

  PEC_Activated := True;
  FormConsole.TabSheetPEC.Enabled := False;
  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function StopPec: Boolean; stdcall; // Stoppe le PEC
begin
  PEC_Activated := false;
  ReturnToSideralSpeed;
  FormConsole.TabSheetPEC.Enabled := True;
  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function UnParkTel_From: Boolean; stdcall;
var
  astring   : shortstring;
  Reg       : TRegistry;
  Alpha_Temp,
  Decli_Temp: double;



begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CURRENT_USER;
  if not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then Reg.OpenKey('SOFTWARE\MCMTII', True);

  if Reg.ValueExists('Park_AH') then
    Alpha_Temp := Reg.ReadFloat('Park_AH')
  else Alpha_Temp := 0;

  if Reg.ValueExists('Park_Dec') then
    Decli_Temp := Reg.ReadFloat('Park_Dec')
  else Decli_Temp := 0;

  Reg.CloseKey;
  Reg.Free;

  Try
  astring := 'N' + AnsiChar(0) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0); // Commande No Park
  sendcommand(hcom,astring, 0, 0);
  finally
  end;


  BackLash.Activated := BackLash.Saved_Park; // restore backlash state
  Park               := False;

  Alpha_Temp := CalculeTSL(Now+SpeedIntMCMT.GMT/24,LongLat.Long)-Alpha_Temp;
  Remet2Pi(Alpha_Temp);

  Calibrate(Alpha_Temp, Decli_Temp);
  Result := True;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ParkTel_at(AHPark, Dec2000: Double): Boolean; stdcall;
var
  Accuracy  : Double;
  Reg       : TRegistry;
  AlphaPark : Double;

Const
  procName='ParkTel_at';

begin

  LogEnterProc_DLL(ProcName);

  Logproc(procName,'Park: 1st slew ' + FloatToStr(AHPark) + ' ' + FloatToStr(Dec2000));
  is_Parking := True;

  TimerPark.AngleHour := AHPark;
  TimerPark.Delta     := Dec2000;
  BackLash.Saved_Park := BackLash.Activated; // We desactivate backlash, but keep the state in mind
  BackLash.Activated  := False;

  AlphaPark:=CalculeTSL(Now+SpeedIntMCMT.GMT/24,LongLat.Long)-AHPark;
  Result   :=InitTelescopeSeeker_DLL(AlphaPark,Dec2000,LongLat.Lat,LongLat.Long,Accuracy);

  TimerPark.Interval := 1000;
  TimerPark.Flag     := 1;
  TimerPark.Enabled  := True;


  Reg         := TRegistry.Create;
  Reg.RootKey := HKEY_CURRENT_USER;

  if not (Reg.OpenKey('SOFTWARE\MCMTII', False)) then
    Reg.OpenKey('SOFTWARE\MCMTII', True);

  Reg.WriteFloat('Park_AH',  AHPark );
  Reg.WriteFloat('Park_Dec', Dec2000);

  Reg.CloseKey;
  Reg.Free;

  Logproc(procName,'Park: end order 1st slew');
  LogEndProc_DLL(ProcName);
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function TelescopeOuest : boolean; stdcall;
begin
  GerMountSetupSem.Enter;
  Result:=GerMountSetup.TelescopeZenithOuest;
  GerMountSetupSem.leave;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GermanMount : boolean; stdcall;
begin
  Result:=GerMountSetup.Monture_Allemande; // Never changes
end;


// allow driver for ASCOM to get the different speeds in radian/sec

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

procedure GetSpeed(var Guidage_AD, Guidage_DEC, Corr_P_AD, Corr_P_DEC,Corr_M_AD, Corr_M_DEC, Point_L_AD, Point_L_DEC,
                   Point_R_AD, Point_R_DEC: double); stdcall;
begin
  Try
  Guidage_AD  := SpeedIntMCMT.V_Guidage_AD  / SpeedIntMCMT.Resol_AD  * T_pi;
  Guidage_DEC := SpeedIntMCMT.V_Guidage_DEC / SpeedIntMCMT.Resol_DEC * T_pi;
  Corr_P_AD   := SpeedIntMCMT.V_Corr_P_AD   / SpeedIntMCMT.Resol_AD  * T_pi;
  Corr_P_DEC  := SpeedIntMCMT.V_Corr_P_DEC  / SpeedIntMCMT.Resol_DEC * T_pi;
  Corr_M_AD   := SpeedIntMCMT.V_Corr_M_AD   / SpeedIntMCMT.Resol_AD  * T_pi;
  Corr_M_DEC  := SpeedIntMCMT.V_Corr_M_DEC  / SpeedIntMCMT.Resol_DEC * T_pi;
  Point_L_AD  := SpeedIntMCMT.V_Point_L_AD  / SpeedIntMCMT.Resol_AD  * T_pi;
  Point_L_DEC := SpeedIntMCMT.V_Point_L_DEC / SpeedIntMCMT.Resol_DEC * T_pi;
  Point_R_AD  := SpeedIntMCMT.V_Point_R_AD  / SpeedIntMCMT.Resol_AD  * T_pi;
  Point_R_DEC := SpeedIntMCMT.V_Point_R_DEC / SpeedIntMCMT.Resol_DEC * T_pi;
  Except
   On E:Exception do
    raise Exception.Create('Erreur GetSpeed -> '+E.Message);
  End;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// allow driver for ASCOM to get information for King and PEC

procedure GetKingPEC(var King:boolean;var Amplitude,Periode,Phase: array of double;var PEC:boolean); stdcall;
var i:integer;
begin
  King:=King_Ascom_Activated;
  PEC :=PEC_Ascom_Activated ;
  for i:=0 to 3 do
  begin
    Amplitude[i]:=Amplitude_PEC_Ascom[i];
    Periode[i]:=Periode_PEC_Ascom[i];
    Phase[i]:=Phase_PEC_Ascom[i];
  end;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function MoveOffsetDirectStep(AlphaDSteps, DeltaDSteps: Integer): Boolean;
// NE SUPPORTE Qu''une SEULE vitesse !

Const
  procName='MoveOffsetDirectStep';

var
  NumOfSteps: longint;
  astring   : ShortString;
  MMSB, MSB,
  LSB, LLSB : byte;


begin
  Try

  NumOfSteps := AlphaDSteps;

  if abs(NumOfSteps) > 0 then
  begin
    MMSB := (abs(NumOfSteps) shr 24) and $FF;
    MSB  := (abs(NumOfSteps) shr 16) and $FF;
    LSB  := (abs(NumOfSteps) shr 8 ) and $FF;
    LLSB :=  abs(NumOfSteps) and $FF;
    // Bit de signe
    if NumOfSteps < 0 then MMSB := MMSB or $80;

    while (IsAxisSlewing_BackLash(Hcom,C_Alpha)) do WaitMainthread(procName);
    astring := 'p' + AnsiChar(MMSB) + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB);
    sendcommand(Hcom,astring, 0, 0);
  end;


  NumOfSteps := DeltaDSteps;

  if abs(NumOfSteps) > 0 then
  begin
    MMSB := (abs(NumOfSteps) shr 24) and $FF;
    MSB  := (abs(NumOfSteps) shr 16) and $FF;
    LSB  := (abs(NumOfSteps) shr 8)  and $FF;
    LLSB :=  abs(NumOfSteps) and $FF;
    // Bit de signe
    if NumOfSteps < 0 then MMSB := MMSB or $80;

    while (IsAxisSlewing_BackLash(Hcom,C_Delta)) do WaitMainthread(procName);
    astring := 'p' + AnsiChar(MMSB) + AnsiChar(MSB) + AnsiChar(LSB) + AnsiChar(LLSB);
    sendcommand(Hcom,astring, 0, 1);
  end;

  finally
  end;

  result := NOERROR;
end;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

End.

