unit Console;
{$R+}

(*///////////////////////////////////////////////////////////////////////
/////////    Console pour commande autres que celles impl�ment�es  //////
/////////                     dans PRiSM                           //////
////////////////////////////////////////////////////////////////////////*)

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  COMM,
  ExtCtrls,
  IniFiles,
  ComCtrls,
  Syncobjs,
  Utils,
  VarGlobal;



type
  TFormConsole = class(TForm)
    OpenDialog2: TOpenDialog;
    SaveDialog1: TSaveDialog;
    PageConsole: TPageControl;
    TabSheetInfo: TTabSheet;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    EditVitesseStr: TEdit;
    EditGuideStr: TEdit;
    EditFactVitesse: TEdit;
    EditAngleHor: TEdit;
    TabSheetPEC: TTabSheet;
    GroupBox7: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Panel_vitesse: TPanel;
    Panel_Index_Pec: TPanel;
    Panel_compteur_Pec: TPanel;
    Panel_prochaine_correction: TPanel;
    Panel24: TPanel;
    ButtonEnregistrerPEC: TButton;
    Panel23: TPanel;
    ButtonCHargerPEC: TButton;
    Panel22: TPanel;
    ButtonSauverPEC: TButton;
    Panel1: TPanel;
    ButtonEffacerMem: TButton;
    GroupBox1: TGroupBox;
    Panel_info_pec: TPanel;
    Panel7: TPanel;
    ButtonActiverPEC: TButton;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    LabelKing: TLabel;
    LabelPEC: TLabel;
    EditFactVitesseCalc: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    EditEncRA: TEdit;
    Label13: TLabel;
    Label14: TLabel;
    EditEncDEC: TEdit;
    EditSpeedDECuSteps: TEdit;
    Label19: TLabel;
    EditPec_posi: TEdit;
    Label8: TLabel;
    ButtonDECsend: TButton;
    EditSendDA: TEdit;
    Label20: TLabel;
    Timer: TTimer;
    procedure ButtonEnregistrerPECClick(Sender: TObject);
    procedure ButtonCHargerPECClick(Sender: TObject);
    procedure ButtonSauverPECClick(Sender: TObject);
    procedure ButtonEffacerMemClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ButtonActiverPECClick(Sender: TObject);
    procedure HandleMessageUpdate(var Message: TMessage); message WM_HandleMessageUpdate;
    procedure PageConsoleChange(Sender: TObject);
    procedure ButtonDECsendClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    { Private declarations }
  public
    procedure StartTimer;
    procedure StopTimer;
  end;


Type
  TinfosConsoleVars=record
     NbStepPer_sec_meas_Alpha:Double;
     AlphaRaw_Previous       :Double;
     DeltaRaw_Previous       :Double;
     NbStepPer_sec_meas_Delta:Double;
     Previous_Time           :Double;
     VitesseCurStepPer_sec   :Double;
     FactVitesseDisplay      :Double;

     AlphaRaw                :Integer;
     DeltaRaw                :Integer;
     M__,H__,S__             :Double;
     position_PECD           :Double;
     Index_PecC              :Integer;
     PEC_Step                :Integer;
     Readtemp                :Integer;
     CasePanel_info_pec      :Integer;
     CasePecState2           :Integer;
     CasePecState1           :Integer;
  end;

var
  FormConsole     : TFormConsole     ;
  InfosConsoleVar : TinfosConsoleVars;
  ConsoleDataSem  : TcriticalSection ;
  McmtPecSem      : TcriticalSection ;

/////////////////////////////////////////////////////////////////////////////////////////////////////////

Implementation

Uses Main_mcmt_unit;

{$R *.DFM}

procedure TFormConsole.ButtonActiverPECClick(Sender: TObject);
begin
  McmtPecSem.Enter;
  if Panel_info_pec.caption = 'Correction MCMT des erreurs p�riodiques active' then
    InternalMCMT_PEC.DesactiverPEC := true
  else
    InternalMCMT_PEC.ActiverPEC := true;
  McmtPecSem.Leave;
end;

procedure TFormConsole.ButtonEnregistrerPECClick(Sender: TObject);
begin
  McmtPecSem.Enter;
  InternalMCMT_PEC.EnregistrerPEC := True;
  McmtPecSem.Leave;
end;

procedure TFormConsole.ButtonEffacerMemClick(Sender: TObject);
begin
  McmtPecSem.Enter;
  InternalMCMT_PEC.EffacerPEC := True;
  McmtPecSem.Leave;
end;


procedure TFormConsole.ButtonCHargerPECClick(Sender: TObject);
Var
  DeviceIni: TIniFile;
  i,c,
  valeur : Integer;
  astring: ShortString;

begin
 if (Hcom=0) then Exit;

 if OpenDialog2.Execute then // Display Save dialog box
 begin
  DeviceIni := TIniFile.Create(OpenDialog2.FileName);
  i := 49;
  c := 01;

  Try
  while (i<249) do
  begin
   with DeviceIni do valeur := ReadInteger('PEC', 'BASE_' + inttostr(c), 0);

   while IsAxisSlewing_backLash(Hcom,C_Alpha) do
      Application.ProcessMessages;

   astring := 'L' + AnsiChar(i) + AnsiChar(valeur) + AnsiChar(valeur shr 8) + AnsiChar(0);
   sendcommand(Hcom,astring, 0, 0);
   i := i + 2;
   c := c + 1;
  end;

  Finally
  End;

  DeviceIni.free;
 end;
end;


procedure TFormConsole.ButtonSauverPECClick(Sender: TObject);
Var
  DeviceIni      : TIniFile;
  i,c,valeur     : Integer;
  astring        : shortstring;
  receivestring  : ShortString;
  achar          : AnsiChar;

begin
   if SaveDialog1.Execute then // Display Save dialog box
   begin

     DeviceIni := TIniFile.Create(SaveDialog1.FileName);
     i      := 49;
     c      := 1;
     valeur := 0;

     Try

     while i < 249 do
     begin
      if IsAxisSlewing_BackLash(Hcom,C_Alpha) = false then
      begin
        astring       := 'J' + AnsiChar(i) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
        receivestring := sendcommand(Hcom,astring, 1, 0);
        achar         := receivestring[1];
        valeur        := ord(achar);
      end;

      if IsAxisSlewing_BackLash(Hcom,C_Alpha) = false then
      begin
        astring       := 'J' + AnsiChar(i + 1) + AnsiChar(0) + AnsiChar(0) + AnsiChar(0);
        receivestring := sendcommand(Hcom,astring, 1, 0);
        achar         := receivestring[1];
        valeur        := valeur + 256 * ord(achar);
      end;

      with DeviceIni do WriteInteger('PEC', 'BASE_' + inttostr(c), valeur);
      i := i + 2;
      c := c + 1;
     end;

     Finally
     End;

     DeviceIni.free;
   end;
end;

procedure TFormConsole.FormHide(Sender: TObject);
begin
   StopTimer;
end;

procedure TFormConsole.FormShow(Sender: TObject);
begin
  PageCOnsole.ActivePageIndex := 0;
  StartTimer;
end;


procedure TFormConsole.HandleMessageUpdate(var Message: TMessage);

       Function InttostrP(N:Integer):String;
       Var
         Negatif:Boolean;

       Begin
        Negatif:=(N<0);
        If N<10 then
        begin
         If not negatif then result:='+ 0'+Inttostr(N) else
                             result:='- 0'+Inttostr(Abs(N));
        end
        else
        begin
         If not negatif then result:='+ '+Inttostr(N) else
                             result:='- '+Inttostr(Abs(N));
        end;
       end;

       ///////////////////////////////////////////////

       Function InttostrB(N:Word):String;
       Begin
        If N<10 then
        begin
         result:='0'+Inttostr(N);
        end
        else
        begin
         result:=Inttostr(N);
        end;
       end;




Const
   procName='TFormConsole.HandleMessageUpdate';

Var
   LInfosConsoleVars:TInfosConsoleVars;


begin

 Try

 ConsoleDataSem.Enter;
 Try
 LInfosConsoleVars.NbStepPer_sec_meas_Alpha:=InfosConsoleVar.NbStepPer_sec_meas_Alpha;
 LInfosConsoleVars.AlphaRaw_Previous       :=InfosConsoleVar.AlphaRaw_Previous;
 LInfosConsoleVars.DeltaRaw_Previous       :=InfosConsoleVar.DeltaRaw_Previous;
 LInfosConsoleVars.NbStepPer_sec_meas_Delta:=InfosConsoleVar.NbStepPer_sec_meas_Delta;
 LInfosConsoleVars.Previous_Time           :=InfosConsoleVar.Previous_Time;
 LInfosConsoleVars.VitesseCurStepPer_sec   :=InfosConsoleVar.VitesseCurStepPer_sec;
 LInfosConsoleVars.FactVitesseDisplay      :=InfosConsoleVar.FactVitesseDisplay;

 LInfosConsoleVars.AlphaRaw                :=InfosConsoleVar.AlphaRaw;
 LInfosConsoleVars.DeltaRaw                :=InfosConsoleVar.DeltaRaw;
 LInfosConsoleVars.M__                     :=InfosConsoleVar.M__;
 LInfosConsoleVars.H__                     :=InfosConsoleVar.H__ ;
 LInfosConsoleVars.S__                     :=InfosConsoleVar.S__;
 LInfosConsoleVars.position_PECD           :=InfosConsoleVar.position_PECD;
 LInfosConsoleVars.Index_PecC              :=InfosConsoleVar.Index_PecC;
 LInfosConsoleVars.PEC_Step                :=InfosConsoleVar.PEC_Step;
 LInfosConsoleVars.Readtemp                :=InfosConsoleVar.Readtemp;
 LInfosConsoleVars.CasePanel_info_pec      :=InfosConsoleVar.CasePanel_info_pec;
 LInfosConsoleVars.CasePecState2           :=InfosConsoleVar.CasePecState2;
 LInfosConsoleVars.CasePecState1           :=InfosConsoleVar.CasePecState1;
 Finally
 ConsoleDataSem.Leave;
 End;


 EditGuideStr.text         := Format     ('%1.5f',[LInfosConsoleVars.NbStepPer_sec_meas_Alpha]);
 EditVitesseStr.text       := Format     ('%1.5f',[LInfosConsoleVars.VitesseCurStepPer_sec    ]);
 EditEncRA.text            := Inttostr   (LInfosConsoleVars.AlphaRaw);

 Try
   if EditVitesseStr.text<>'PARK!' then
   EditFactVitesse.Text := Format('%1.5f',[Abs(LInfosConsoleVars.VitesseCurStepPer_sec/LInfosConsoleVars.NbStepPer_sec_meas_Alpha)]);
 Except
   EditFactVitesse.Text :='!!!!';
 end;

 // DEC
 EditSpeedDECuSteps.text    := Format('%1.5f',[LInfosConsoleVars.NbStepPer_sec_meas_Delta]);
 EditEncDEC.text            := Inttostr(LInfosConsoleVars.DeltaRaw);

 // MISC
 EditAngleHor.Text := inttostrP(round(LInfosConsoleVars.H__)) + ':' +
                      inttostrB(round(LInfosConsoleVars.M__)) + ':' +
                      Format('%2.2f',[LInfosConsoleVars.S__]);

 // PEC
 If PEC_Activated  then
  EditPec_posi.text := inttostr(round(LInfosConsoleVars.position_PECD))
 else
  EditPec_posi.text := 'Non disponible';


 If KING_Activated then LabelKing.Caption:='Actif' else LabelKing.Caption:='Inactif';
 If PEC_Activated  then LabelPEC.Caption:='Actif'  else LabelPEC.Caption :='Inactif';
 Try
 EditFactVitesseCalc.Text:=Format('%1.7f',[LInfosConsoleVars.FactVitesseDisplay/SpeedIntMCMT.V_Guidage_AD]);
 Except
 EditFactVitesseCalc.Text:='???';
 End;

 Panel_Index_Pec.caption            := Inttostr(LInfosConsoleVars.Index_PecC);
 Panel_compteur_Pec.caption         := Inttostr(LInfosConsoleVars.PEC_Step);
 Panel_prochaine_correction.caption := inttostr(LInfosConsoleVars.Readtemp);
 Panel_vitesse.caption              := FormConsole.EditVitesseStr.text;


 Case LInfosConsoleVars.CasePanel_info_pec of
  0: Panel_info_pec.caption := 'Correction MCMT des erreurs p�riodiques inactive';
  1: Panel_info_pec.caption := 'Correction MCMT des erreurs p�riodiques active';
  2: Panel_info_pec.caption := 'En attente du passage � z�ro pour synchronisation!';
  3: Panel_info_pec.caption := 'Enregistrement des corrections en cours !';
 end;

 If (LInfosConsoleVars.CasePecState2=1) then EditVitesseStr.text := 'PARK!';

 if (LInfosConsoleVars.CasePecState1<2) then
 begin
   ButtonActiverPEC.Enabled := true;
   if (LInfosConsoleVars.CasePecState1=0) then
   begin
    ButtonActiverPEC.caption      := 'Activer';
    ButtonEnregistrerPEC.Enabled  := true;
    ButtonEffacerMem.Enabled      := true;
    ButtonSauverPEC.Enabled       := true;
    ButtonCHargerPEC.Enabled      := true;
   end
   else
   begin
    ButtonActiverPEC.caption      := 'D�sactiver';
    ButtonEnregistrerPEC.Enabled  := false;
    ButtonEffacerMem.Enabled      := false;
    ButtonSauverPEC.Enabled       := false;
    ButtonCHargerPEC.Enabled      := false;
   end;
  end
  else
  begin
   ButtonActiverPEC.caption       := 'D�sactiver';
   ButtonActiverPEC.Enabled       := false;
   ButtonEnregistrerPEC.Enabled   := false;
   ButtonEffacerMem.Enabled       := false;
   ButtonSauverPEC.Enabled        := false;
   ButtonCHargerPEC.Enabled       := false;
  end;

 Except
  On E:Exception do
   Logproc(procName,'Exception dans HandleMessageUpdate : '+E.message);
 End;
end;




procedure TFormConsole.PageConsoleChange(Sender: TObject);
begin
 If PageConsole.ActivePage=TabSheetInfo then
   Self.Height:=375
 else
 If PageConsole.ActivePage=TabSheetPEC then
   Self.Height:=233;
end;


procedure TFormConsole.TimerTimer(Sender: TObject);
Var Message:TMessage;
begin
 HandleMessageUpdate(Message);
end;


procedure TFormConsole.ButtonDECsendClick(Sender: TObject);
Var DeltaDSteps:Integer;

begin
 Try
 DeltaDSteps:=StrToInt(EditSendDA.text);
 MoveOffsetDirectStep(0,DeltaDSteps);
 Except
 end;
end;


procedure TFormConsole.StartTimer;
Begin
 Timer.Enabled:=True;
End;

procedure TFormConsole.StopTimer;
Begin
 Try
 Timer.Enabled:=False;
 Except
 End;
End;

////////////////////////////////////////////////////////////////////////////////////////////////////

Initialization
 ConsoleDataSem  :=TcriticalSection.Create;
 McmtPecSem      :=TcriticalSection.Create;

 InfosConsoleVar.NbStepPer_sec_meas_Alpha:=0;
 InfosConsoleVar.NbStepPer_sec_meas_Delta:=0;

 InfosConsoleVar.AlphaRaw_Previous :=0;
 InfosConsoleVar.DeltaRaw_Previous :=0;
 InfosConsoleVar.Previous_Time     :=0;

////////////////////////////////////////////////////////////////////////////////////////////////////

Finalization
 ConsoleDataSem.Free;
 McmtPecSem    .Free;
end.
