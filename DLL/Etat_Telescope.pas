unit Etat_Telescope;
{R+}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TForm_Etat_Telescope = class(TForm)
    RadioGroupEtatTelescope: TRadioGroup;
    Label10: TLabel;
    validation: TButton;
    Label11: TLabel;
    Image_Zenith_Est: TImage;
    Image_Pied: TImage;
    Image_Zenith_Ouest: TImage;
    Image_Pied_HemisphereSud: TImage;
    procedure RadioGroupEtatTelescopeClick(Sender: TObject);
  private
    Flatitude:Double;
  public
    Constructor create(Aowner:Tcomponent;Latitude:Double); reintroduce;
  end;



implementation

{$R *.dfm}

Constructor TForm_Etat_Telescope.create(Aowner:Tcomponent;Latitude:Double);
begin
 Inherited create(Aowner);
 Flatitude:=Latitude;
end;


procedure TForm_Etat_Telescope.RadioGroupEtatTelescopeClick(Sender: TObject);
begin
   If (FLatitude>0) then
   begin
     Image_Pied.Visible := true;
     Image_Pied_HemisphereSud.Visible := false ;
   end
   else
   begin
     Image_Pied.Visible := false;
     Image_Pied_HemisphereSud.Visible := true ;
   end;

   If (RadioGroupEtatTelescope.ItemIndex=0)   then
   begin
        If (FLatitude>0) then
        begin
           Image_Zenith_Ouest.Visible := false;
           Image_Zenith_Est.Visible   := true;
        end
        else
        begin
          Image_Zenith_Ouest.Visible := true;
          Image_Zenith_Est.Visible   := false;
        end;

    end
    else
    begin
        If (FLatitude>0) then
        begin
          Image_Zenith_Ouest.Visible := true;
          Image_Zenith_Est.Visible   := false;
        end
        else
        begin
          Image_Zenith_Ouest.Visible := false;
          Image_Zenith_Est.Visible   := true;
        end;
    end;
end;


end.
