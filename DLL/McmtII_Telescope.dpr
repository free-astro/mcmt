library McmtII_Telescope;

//{$M minstacksize,maxstacksize}
// Default {$M 16384,1048576}
//{$M 1048576,4048576}

// DLL pour MCMT avec firmware "MCMTII"
// De fa�on g�n�rale, la communication se fera par le port COM. Ce port sera ouvert
// � chaque envoi de commande et ferm� juste apr�s !

{$ALIGN 8}

(* Changes record

  Aug 2001 : BEWARE OF ALIGNMENT !!
             Delphi6 is A8 by default, Delphi5 is A4
             Aowner in InitCodeurs may cause problems for non Delphi languages, in that case leave it to NIL and
             use file or registry to set up the hardware.

  Oct 2001 : CHANGED from
             Function InitCodeurs(Var TimeCheck:Integer;Aowner:Tcomponent;Var EnCodersCaps:TEnCodersCaps):boolean; stdcall;
             To
             Function InitCodeurs(Var TimeCheck:Integer;Aowner:Tcomponent;Latitude,Longitude:Double):boolean; stdcall;


  janv 2004 : No setup panel poping-up during InitCodeurs procedure !

              added :
                Init_telescope_Panel(Aowner:Tcomponent;TwinHandle:Thandle;TypeSetup:TsetupEngine_encoderEnumerator); stdcall;

               To allow setup panel to be called by PRISM setup telescope...


  18 JUIN 06 : Cyril  passage du PEC a plus d'harmoniques

 20 Juillet 07 : Cyril pb lecture de la config MCMT lorsqu'on change de telescope et donc de config de MCMT, et on peut rentrer des pas arbitraire en delta.

  04 Janv 08  : Cyril Reprise complete de la facon de dialoguer avec la COM

 *)

uses
  FastMM4,
  COMM in 'COMM.pas',
  Console in 'Console.pas' {FormConsole},
  EditNbre in 'EditNbre.pas',
  Etat_Telescope in 'Etat_Telescope.pas' {Form_Etat_Telescope},
  FeuRouge in 'FeuRouge.pas' {Form_FeuRouge},
  FeuVert in 'FeuVert.pas' {Form_FeuVert},
  FinderCom_Unit in 'FinderCom_Unit.pas',
  HiresTim in 'HiresTim.pas',
  main_mcmt_unit in 'main_mcmt_unit.pas',
  NumberEdit in 'NumberEdit.pas',
  PanelLongLat in 'PanelLongLat.pas',
  ProcessBox_mcmtII in 'ProcessBox_mcmtII.pas' {ProcessingBox},
  scan_mcmt in 'scan_mcmt.pas' {Form_scan_mcmt},
  Setup in 'Setup.pas' {SetupTelescope},
  Utils in 'Utils.pas',
  VarGlobal in 'VarGlobal.pas',
  Initphases in 'Initphases.pas';

{$R *.RES}


//////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
//////// ///////// ///////// ///////// ///////// ///////// ///////// /
(* These functions MUST be exported to Prism as it is *)

exports
 //General
  GetDLLVer name 'GetDLLVer',

 //Encoders
  InitCodeurs   name 'InitCodeursDevice_DLL',
  CloseCodeurs  name 'CloseCodeursDevice_DLL',
  Calibrate     name 'CalibrateCodeursDevice_DLL',
  GetAlpha      name 'GetAlphaCodeursDevice_DLL',
  GetDelta      name 'GetDeltaCodeursDevice_DLL',

  RetrieveDataAlphaDelta                name 'CheckCodeursCodeursDevice_DLL',
  GetRAWAlphaDelta                      name 'GetRAWAlphaDeltaCodeursDevice_DLL',
  GetRawPositionAlphaInit_arcsecCodeurs name 'GetRawPositionAlphaInit_arcsecCodeursDevice_DLL',
  GetRawPositionDeltaInit_arcsecCodeurs name 'GetRawPositionDeltaInit_arcsecCodeursDevice_DLL',
  GetRawPositionAlpha_arcsecCodeurs     name 'GetRawPositionAlpha_arcsecCodeursDevice_DLL',
  GetRawPositionDelta_arcsecCodeurs     name 'GetRawPositionDelta_arcsecCodeursDevice_DLL',

 //Telescope Slewing

  InitTelescopeSeeker_DLL               name 'InitTelescopeSeekerDevice_DLL',
  Execute_TelescopeSeeker_DLL           name 'Execute_TelescopeSeekerDevice_DLL',
  StopSlewing_TelescopeSeekerDevice_DLL name 'StopSlewing_TelescopeSeekerDevice_DLL',
  CloseTelescopeSeeker_DLL              name 'CloseTelescopeSeekerDevice_DLL',

 // telescope Engine -> Raquette
  InitTelescopeEngine_DLL name 'InitTelescopeEngineDevice_DLL',
  GetSpeedAlpha           name 'GetSpeedAlphaEngineDevice_DLL',
  GetSpeedDelta           name 'GetSpeedDeltaEngineDevice_DLL',
  MoveOffsetDirectAD      name 'MoveOffsetDirectADEngineDevice_DLL',
  MoveTelescope           name 'MoveEngineDevice_DLL',
  MoveEventTelescope      name 'MoveEventEngineDevice_DLL',
  STOPTelescope           name 'StopTelescopeEngineDevice_DLL',
  STOP_ALL_Telescope      name 'Stop_ALL_TelescopeEngineDevice_DLL',
  ReturnToSideralSpeed    name 'ReturnToSideralSpeedEngineDevice_DLL',
  SetFreeSpeed            name 'SetFreeSpeedDevice_DLL',
  StopFreeSpeed           name 'StopFreeSpeedDevice_DLL',
  StartSolarEngine        name 'StartSolarSpeedEngineDevice_DLL',
  StartMoonEngine         name 'StartMoonSpeedEngineDevice_DLL',
  ShowConsoleEngine       name 'ShowConsoleEngineDevice_DLL',
  ShowSpeedEngine         name 'ShowSpeedEngineDevice_DLL',
  CloseEngine             name 'CloseEngineTelescopeDevice_DLL',

  GetCurrentSpeedRA  name 'GetCurrentSpeedRADevice_DLL',
  GetCurrentSpeedDEC name 'GetCurrentSpeedDECDevice_DLL',

  PulseGuideAscom           name 'PulseGuideAscomDevice_DLL',
  PulseGuide                name 'PulseGuideDevice_DLL',
  IsPulseGuiding            name 'IsPulseGuidingDevice_DLL',

  IsParked                  name 'IsParkedDevice_DLL',
  ParkTel_at                name 'ParkTel_DLL',
  UnparkTel_from            name 'UnParkTel_DLL',
  IsParking                 name 'IsParkingDevice_DLL',

 //King

  StartKingDevice name 'StartKingDevice_DLL',
  StopKingDevice  name 'StopKingDevice_DLL',

 // PEC
  AcceptRawPositionCodeurs name 'AcceptRawPositionCodeursDevice_DLL',
  StartPecSpeedEngine      name 'StartPecSpeedEngineDevice_DLL',
  StopPec                  name 'StopPec_DLL',

 // PANEL d'INIT
  Init_telescope_Panel     name 'Init_telescope_Panel_DLL',

 // FOR ASCOM
  END_TelescopeSeeker      name 'END_TelescopeSeekerDevice_DLL',
  GetSpeed                 name 'GetSpeed_DLL',
  GetKingpec               name 'GetKingPEC_DLL',

  // German Mounts

  TelescopeOuest           name 'TelescopeOuest_DLL',
  GermanMount              name 'GermanMount_DLL',

  ReturnDLLinfos           name 'ReturnDLLinfos';

Begin
end.
