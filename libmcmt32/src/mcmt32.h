/* This file is part of libmcmt32
 * Licence: GPLv3
 * Project page: http://free-astro.vinvin.tf/index.php/MCMT32
 */

#ifndef MCMT32_H
#define MCMT32_H

#include <stdio.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/******************* TELESCOPE CONFIGURATION (MANDATORY) *******************/
// All 4 gear parameters (usteps, steps, reducer, gear) are stored
// in the controller eeprom for each axis FM 20160210
// So for Latitude and Longitude
/* number of teeth on the right-ascension (sidereal rotation) gear */
//#define NB_TEETH_RA	200
/* number of teeth on the declination gear */
//#define NB_TEETH_DEC 200
/* reducer ratio between motor and worm ; 1 if no reducer */
//#define MOTOR_RATIO 12
/* number of micro-steps per step ; must match the configuration 
   of the stepper driver */
//#define NB_MICROSTEP	16

/* number of steps for one motor revolution (typically 200) */
//#define NB_STEPPERREV	200

/* Latitude and longitude of the telescope, in degrees */
//#define LATITUDE	43.619604	/* positive is north */
//#define LONGITUDE	7.039153	/* positive is east */
/* = 43d37'11", 7d02'21" */

/***************************************************************************/

/* TODO
 * FLIP:
 *	set _flip_status where it should
 *	how to display the current position while flipping?
 *		= when to do the +pi on HA/RA and DEC values?
 *	
 *
 * set longitude (used in HA) and latitude (not used yet) from external source
 * backlash (DEC only when guiding, all when slewing)
 *
 * the cable problem on many turns, avoid flipping by the bad direction too
 *
 * what's the behavior of requesting slewing while already slewing?
 *
 * update park mode - from 'r' command (done: 'K') or better, 'J'
 *
 * goto_relative(angle, angle)
 *
 *
 * TOTEST
 * values of park mode and PEC enabled from EEPROM read where not correctly
 * displayed in first test
 *
 */

#define MCMT32_WRITE_TIMEOUT_SEC	1	/* seconds */
#define MCMT32_WRITE_TIMEOUT_USEC	500000	/* microseconds */
#define MCMT32_READ_TIMEOUT_SEC		2	/* seconds */
#define MCMT32_READ_TIMEOUT_USEC	0	/* microseconds */

#define MCMT32_RET_OK		     0
#define MCMT32_RET_ERR		    -1
#define MCMT32_RET_ARGERR	    -2
#define MCMT32_RET_TIMEOUT	    -3
#define MCMT32_RET_UNINIT	    -4
#define MCMT32_RET_INCOMPLETE	-5
#define MCMT32_RET_EEPROMERR	-6

#define MCMT32_NBPARAM_IN_EEPROM 22
#define CASINDEG 360000 // nb  of centiarcsec in a degree
struct mcmt32_eeprommap {unsigned char addr; unsigned char length; unsigned char name[24];};

typedef enum {
    VM_GUIDAGE,
    VM_CPLUS,
    VM_CMINUS,
    VM_SLOW,
    VM_FAST,
    VM_ACCE,
    DIR_GUIDAGE,
    E_USTEPS,
    E_STEPS,
    E_RATIO,
    E_GEAR,
    E_LATITUDE,
    E_LONGITUDE,
    E_ALTITUDE,
    E_FLIP,
    PAS_CODEUR,
    PARK_MODE,
    RAQ_TYPE_CAN_ADDRESS,
    PEC_ENABLED,
    PARK_HA,
    PARK_HGT,
    PARK_SIDE
    } mcmt32_mapindex;

char *mcmt32_strerror(int errno);

/* The two axis of the mount, MCMT32 has a controller for each */
typedef enum {
	MCMT32_AXIS_RA	= 0xe0,
	MCMT32_AXIS_DEC	= 0xe1
} mcmt32_axis;

/* serial link management */

int mcmt32_serial_link_open(const char *port_device);
void mcmt32_serial_link_close(int serial_link_fd);
int mcmt32_serial_link_list(char **device_name, int max_devices);


/* low-level commands */

int mcmt32_device_ready(int fd, mcmt32_axis axis);
int mcmt32_get_version(int fd, mcmt32_axis axis, char *version);

int mcmt32_read_encoder(int fd, mcmt32_axis axis, unsigned int *encoder_value);
int mcmt32_move_number_of_steps(int fd, mcmt32_axis axis, int number_of_steps);

int mcmt32_park_mode(int fd, mcmt32_axis axis, int enable);
int mcmt32_stop_slewing(int fd, mcmt32_axis axis);

int mcmt32_get_from_eeprom(int fd, mcmt32_axis axis, mcmt32_mapindex index, uint32_t *data);
int mcmt32_read_eeprom(int fd, mcmt32_axis axis, uint32_t *eeprom);
int mcmt32_init_eeprom(int fd, mcmt32_axis axis, uint32_t *eeprom);
int mcmt32_write_uchar8_eeprom(int fd, mcmt32_axis axis, unsigned char address, unsigned char data);
int mcmt32_write_uint16_eeprom(int fd, mcmt32_axis axis, unsigned char address, uint16_t data);
int mcmt32_write_uint32_eeprom(int fd, mcmt32_axis axis, unsigned char address, uint32_t data);
int mcmt32_write_eeprom(int fd, mcmt32_axis axis, int index, uint32_t data);
int mcmt32_write_eeprom_full(int fd, mcmt32_axis axis, uint32_t *eeprom);
int mcmt32_change_sidereal_speed(int fd, double nbsps) ;
int mcmt32_get_speednpark(int fd, mcmt32_axis axis, uint32_t *current_speed, uint8_t *parked) ;
int mcmt32_set_current_speed(int fd, mcmt32_axis axis, double new_speed, unsigned char dir) ;

/* high-level commands */

typedef enum {
	MCMT32_SPEED_GUID,	/* guiding (aka sidereal) speed */
	MCMT32_SPEED_MIN,	/* correction speed */
	MCMT32_SPEED_MED,	/* slow slewing speed */
	MCMT32_SPEED_MAX	/* fast slewing speed */
} mcmt32_speed;

typedef enum {
	MCMT32_NORTH,
	MCMT32_SOUTH,
	MCMT32_EAST,
	MCMT32_WEST
} mcmt32_direction;

struct mcmt32_angle {
	unsigned char degrees;
	unsigned char minutes;
	unsigned char seconds;
	unsigned char centiseconds;
};

int mcmt32_move(int fd, mcmt32_direction dir, mcmt32_speed speed);
int mcmt32_guide(int fd, mcmt32_direction dir, mcmt32_speed speed);
int mcmt32_stop_sidereal_speed(int fd);
void mcmt32_stop_all_slewing(int fd);

int mcmt32_set_current_position(int fd, double ra, double dec, double lon);
int mcmt32_get_current_ha_position(int fd, double *ha, double *dec);
int mcmt32_get_current_position(int fd, double *ra, double *dec, double lon);

int mcmt32_slew_absolute(int fd, double ra, double dec, double lon, int allow_flip);
//int mcmt32_slew_absolute2(int fd, double ra, double dec);	// flip allowed
//int mcmt32_slew_relative(int fd, double ra, double dec);

typedef enum {
	MCMT32_FLIP_UNKNOWN,
	MCMT32_FLIP_NONE, MCMT32_FLIP_FLIPPING,
	MCMT32_FLIP_FLIPPED, MCMT32_FLIP_UNFLIPPING
} mcmt32_flip_status;

mcmt32_flip_status mcmt32_get_flip_status();

void mcmt32_set_flip_status(mcmt32_flip_status st);
int mcmt32_guess_flip_status(int fd);

//int mcmt32_get_latitude_longitude(int fd, double *lat, double *lon);

int mcmt32_init_ratios(int fd);
int mcmt32_init_pos(int fd, double *lat, double *lon, double *alt);

int mcmt32_is_parked(int fd);
void mcmt32_print_eeprom_configuration(FILE *output, uint32_t *eeprom);


/* angle/encoder conversion */

void mcmt32_conversion_set_reference(mcmt32_axis axis, unsigned int steps, double angle, double lon);
void mcmt32_conversion_reset_reference();
int mcmt32_conversion_is_init();

int mcmt32_convert_is_over_meridian(double ha, double dec, double lon);

/* data structure */

#ifdef __cplusplus
}
#endif

#endif

