
#include <stdlib.h>	/* for memcpy */
#include <unistd.h>	/* for memcpy */
#include <string.h>	/* for memcpy */
#include <byteswap.h>
#include "mcmt32.h"
#include "low_level_commands.h"
#include "log.h"

// MCMT32 STATUS OK TESTED
/* map structure to ease access to the values in the eeprom
 * and ease later modifications ; 
 * first value is the offset from base eeprom address, counted as bytes 
 * second value is the length in byte of the parameter at this address
 * third filed contains a description of the content */
struct mcmt32_eeprommap eeprom_map[MCMT32_NBPARAM_IN_EEPROM]={
        { 0, 4, "VM_Guidage"},
        { 4, 4, "VM_Corect_Plus"},
        { 8, 4, "VM_Corect_Moins"},
        {12, 4, "VM_Corect_Lent"},
        {16, 4, "VM_Corect_Rapide"},
        {20, 1, "VM_Corect_Acce"},
        {21, 1, "DIR_Guidage"},
        {22, 2, "Microsteps_per_step"},
        {24, 2, "Steps_per_rev"},
        {26, 2, "Reducer_ratio"},
        {28, 2, "Gear_ratio"},
        {30, 4, "Latitude"},
        {34, 4, "Longitude"},
        {38, 2, "Altitude"},
        {41, 1, "PARK_Status"},
        {42, 1, "RAQ_Type_can_addr"},
        {43, 2, "PAS_Codeur"},
        {47, 1, "PEC_Status"},
        {48, 4, "Park_ha"},
        {52, 4, "Park_hgt"},
        {56, 1, "Park_side"}
    };

/*************** internal data ***************/

_mcmt32_mode _current_mode_ra = MCMT32_MODE_UNINIT;
_mcmt32_mode _current_mode_dec = MCMT32_MODE_UNINIT;

mcmt32_flip_status _flip_status;

double ra_deg_per_microstep;
double ra_deg_per_microstep;
double dec_deg_per_microstep;
uint16_t ra_r_usteps, ra_r_steps, ra_r_reducer, ra_r_gear;
uint16_t dec_r_usteps, dec_r_steps, dec_r_reducer, dec_r_gear;
uint8_t RA_driver_status, DEC_driver_status;
/*************** internal functions ***************/

// MCMT32 STATUS OK TESTED
/* This method provides the proper interface to 2 bytes interrupting commands */
//
int _request_response_2byte_command(int fd, unsigned char *request,
		unsigned char *response, int expected_response_size) {
	if (request[1] <= 0x7f) {
		fprintf(stderr, "Wrongly made 2-byte command\n");
		return MCMT32_RET_ARGERR;
	}
	return _request_response(fd, request, 2,
			response, expected_response_size);
}

/* MCMT32 STATUS OK TESTED
 * Adaptation de _request_response_9byte_command pour MCMT32 fm@dulle.fr
 *
 * This method provides the proper interface to 9 bytes ASCII commands.
 * request should be 7 bytes: the axis, the command ASCII char, and 5 data bytes.
 * expected_response_size must not include the ACK, it's already managed here. 
 * Returns MCMT32_RET_OK when the response size is the expected response size
 * and the ACK has been received (removed from the response),
 * MCMT32_RET_INCOMPLETE if the response is smaller with ACK,
 * MCMT32_RET_ERR else and for any error.
 */
int _request_response_9byte_command(int fd, 
                                    unsigned char *request, 
                                    unsigned char *response, 
                                    int expected_response_size) {
	unsigned char ninebc[9];	/* nine-byte command */
	unsigned char *response_buf;
	int i, retval, my_retval;
#ifdef TESTING
	return MCMT32_RET_OK;
#endif
	if (request[1] > 0x7f || expected_response_size < 0) {
		fprintf(stderr, "Wrongly made 9-byte command\n");
		return MCMT32_RET_ARGERR;
	}
	ninebc[0] = request[0];	/* axis	*/
	ninebc[1] = request[1];	/* command id (ASCII letter) */
	ninebc[7] = 0;			/* heavyweight bits */
	ninebc[8] = 0;			/* checksum */
	/* we need to remove the eighth bit to bytes [2;6] */
	for (i=2; i<=6; i++) {
		ninebc[i] = request[i] & 0x7f;
		if (request[i] & 0x80)
			ninebc[7] |= 1 << (4-(i-2)); /* WARNING 4 in MCMT32, 3 in MCMTII !! */
	}
	/* checksum */
	for (i=0; i<=7; i++)
		ninebc[8] += ninebc[i];
	ninebc[8] = ninebc[8] & 0x7f;

	expected_response_size++;
	response_buf = malloc(expected_response_size);
	/* do the request */
	retval = _request_response(fd, ninebc, 9, response_buf, expected_response_size);
	/* handle return values and the ACK */
	if (retval < 0) my_retval = retval;	/* keep upstream errors */
	else my_retval = MCMT32_RET_ERR;
	if (retval >= 1 && _is_ack_retval(response_buf[0])) {
		/* copy anyway if not everything is here */
		for (i=1; i<retval; i++){
			response[i-1] = response_buf[i];
            }
		if (retval == expected_response_size)
			my_retval = MCMT32_RET_OK;
		else my_retval = MCMT32_RET_INCOMPLETE;
	} else {
		/* not an ack? should not happen! */
	}
	free(response_buf);
	return my_retval;
}

/* MCMT32 STATUS OK TESTED
 * The main input/output method.
 * Return value is the number of bytes read (>= 0) in response in case of success;
 * error return codes are MCMT32_RET_ERR for failure, MCMT32_RET_ARGERR for invalid
 * argument, and MCMT32_RET_TIMEOUT in case of read or write timeout */
int _request_response(int fd, unsigned char *request, int request_size,
		unsigned char *response, int expected_response_size) {
	fd_set fds;
	struct timeval tv;
	int retval, my_retval, read_bytes;

	if (request_size != 2 && request_size != 9) {
		/* case not handled: 16-byte commands for EEPROM update */
		fprintf(stderr, "Command type not handled (%d bytes)\n", request_size);
		return MCMT32_RET_ARGERR;
	}
#if 0
    else {
		fprintf(stderr, "ok in request_respo\n");
    }
#endif

	/* Sending the request */
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	tv.tv_sec = MCMT32_WRITE_TIMEOUT_SEC;
	tv.tv_usec = MCMT32_WRITE_TIMEOUT_USEC;
	retval = select(fd+1, NULL, &fds, NULL, &tv);
	if (retval == -1) {
		perror("mcmt32 internal select write");
		return MCMT32_RET_ERR;
	}
	if (retval == 0 || !FD_ISSET(fd, &fds)) {
		fprintf(stderr, "MCMT32 timeout, retry writing to it later (0x%x 0x%x) fd %d\n",request[0], request[1], fd );
		return MCMT32_RET_TIMEOUT;
	}

	retval = write(fd, request, request_size);
	mcmt32_log_binary("raw write", (unsigned char *)request, request_size);

	if (retval == -1) {
		perror("mcmt32 internal write");
		return MCMT32_RET_ERR;
	}
	if (retval == 0) {	/* end of file */
		fprintf(stderr, "Write error: end of file. The connection was probably cut.\n");
		return MCMT32_RET_ERR;
	}
	if (retval != request_size) {
		fprintf(stderr, "mcmt32 internal write: request has not been fully "
				"sent, leaving the link in an inconsistent state.\n");
		return MCMT32_RET_ERR;
	}

	/* Receiving the response */
	read_bytes = 0;
	my_retval = 0;
	do {	/* loop because it may not read it all the first time */
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		tv.tv_sec = MCMT32_READ_TIMEOUT_SEC;
		tv.tv_usec = MCMT32_READ_TIMEOUT_USEC;
		retval = select(fd+1, &fds, NULL, NULL, &tv);
		if (retval == -1) {
			perror("mcmt32 internal read");
			my_retval = MCMT32_RET_ERR;
			break;
		}
		if (retval == 0 || !FD_ISSET(fd, &fds)) {
			fprintf(stderr, "MCMT32 is busy after reading %d bytes, "
					"retry reading from it later\n", read_bytes);
			my_retval = MCMT32_RET_TIMEOUT;
			break;
		}

		retval = read(fd, response+read_bytes, expected_response_size-read_bytes);
		if (retval == -1) {
			perror("mcmt32 internal read");
			my_retval = MCMT32_RET_ERR;
			break;
		}
		if (retval == 0) {	/* end of file */
			fprintf(stderr, "Read error: end of file. The connection was probably cut.\n");
			my_retval = MCMT32_RET_ERR;
			break;
		}
		read_bytes += retval;
	} while (read_bytes < expected_response_size) ;
	mcmt32_log_binary("raw read", (unsigned char *)response, read_bytes);

	if (read_bytes > 0) {
		if (read_bytes != expected_response_size) {
			fprintf(stderr, "mcmt32 internal: short read (%d/%d).\n",
					read_bytes, expected_response_size);
		}
		/* override timeout error if something has been read */
		if (my_retval == MCMT32_RET_TIMEOUT || my_retval == 0)
			my_retval = read_bytes;
        
	}
	return my_retval;
}

/* MCMT32 STATUS OK UNCHANGED TESTED */
unsigned char _axis_to_byte(mcmt32_axis axis) {
	return (unsigned char) axis;
}

/* MCMT32 STATUS OK UNCHANGED TESTED */
int _is_ack_retval(unsigned char byte) {
	if (byte == MCMT32_ACK)
		return 1;
	if (byte == MCMT32_NOACK)
		return 0;
	return MCMT32_RET_ERR;
}

/********************************************************************************
 *                   PUBLIC FUNCTIONALITIES (listed in mcmt32.h)                *
 ********************************************************************************/

/* MCMT32 STATUS OK TESTED */
/* returns <0 on error, 1 if device is ready, 0 otherwise */
// PingReturn= pointage << 7 + guidage << 6 + tracking << 5 + rapide << 1 + dir

int mcmt32_device_ready(int fd, mcmt32_axis axis) {
	/* request:	axis_id, FF
	 * response:	01 */
	unsigned char resp[1];
	int retval;
#ifndef TESTING
	unsigned char req[2];
	req[0] = _axis_to_byte(axis);
	req[1] = 0xff;
	retval = _request_response_2byte_command(fd, req, resp, 1);
#else
	resp[0] = 0x01;
    retval = 1;
#endif
    if (retval < 1)
        return retval;

    if (resp[0] & 0x20) {
        if (axis == MCMT32_AXIS_RA){
            _current_mode_dec = MCMT32_MODE_GUIDING;
            RA_driver_status=resp[0];
            }
        else {	
            _current_mode_dec = MCMT32_MODE_GUIDING;
            DEC_driver_status=resp[0];
            }
        /* update flip status after the flip */
        if ((_flip_status == MCMT32_FLIP_FLIPPING ||
                    _flip_status == MCMT32_FLIP_UNFLIPPING) &&
                _current_mode_ra == MCMT32_MODE_GUIDING &&
                _current_mode_dec == MCMT32_MODE_GUIDING) {
            if (_flip_status == MCMT32_FLIP_FLIPPING)
                _flip_status = MCMT32_FLIP_FLIPPED;
            else _flip_status = MCMT32_FLIP_NONE;
        }
    }

    if (resp[0] & 0x80) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_SLEWING;
		else 	
            _current_mode_dec = MCMT32_MODE_SLEWING;
	}
	return (resp[0] == 0x20);
}

/* MCMT32 STATUS OK UNCHANGED TESTED */
/* Gets the current value of the encoder and stores it in the pointer encoder_value.
 * Returns MCMT32_RET_OK on success, MCMT32_RET_ERR on error */
int mcmt32_read_encoder(int fd, mcmt32_axis axis, unsigned int *encoder_value) {
	/* 
     * request:	axis_id, F1 + byte number [0..3]
	 * (must start with F4, this copies the value in the 4 byte buffer for
	 * subsequent requests)
	 * response:	the requested byte from the int32 value
    Above 5 lines were OBSOLETED after rev 78 and switch to 115200 bauds :
        any of 0xf1 0xf2 0xf3 0xf4 will return the 4 bytes MSB 1st from the encoder 


	 */
#ifdef TESTING
	*encoder_value = 1073741824;
	return MCMT32_RET_OK;
#endif
	uint8_t req[2];
	uint8_t resp[4];

	uint32_t value = 0;
	int reqsize=4, retval;

	req[0] = _axis_to_byte(axis);
    req[1] = 0xf1;
    retval = _request_response_2byte_command(fd, req, resp, reqsize);
    if (retval != reqsize)
        return MCMT32_RET_ERR;
    
	*encoder_value = (resp[0]<<24) + (resp[1]<<16) + (resp[2]<<8) + resp[3];
	return MCMT32_RET_OK;
}

int mcmt32_init_pos(int fd, double *lat, double *lon, double *alt){
    //
    // Reads r_altitude, r_longitude, r_altitude
    //
    uint32_t buffer; 
    int retval;

    retval=mcmt32_get_from_eeprom(fd, MCMT32_AXIS_RA, E_LATITUDE, &buffer); 
    *lat=((double)buffer)/CASINDEG;

    retval=mcmt32_get_from_eeprom(fd, MCMT32_AXIS_RA, E_LONGITUDE, &buffer); 
    *lon=((double)buffer)/CASINDEG;

    retval=mcmt32_get_from_eeprom(fd, MCMT32_AXIS_RA, E_ALTITUDE, &buffer);
    //
    // FIXME if in Death Valley... this is probably broken if altitude is negative 
    //
    *alt=(double)buffer; 
    return retval;
    }

int mcmt32_init_ratios(int fd){
    //
    // Reads usteps/step, step/rev, reducer ratio, gear ratio from eeprom for both axis
    // computes *_deg_per_microstep 
    // Must be called before any slewing action
    //
    uint32_t data; 
    int retval;

    mcmt32_get_from_eeprom(fd, MCMT32_AXIS_RA, E_USTEPS, &data); 
    ra_r_usteps=(uint16_t) data;
    mcmt32_get_from_eeprom(fd, MCMT32_AXIS_RA, E_STEPS, &data); 
    ra_r_steps=(uint16_t) data;
    mcmt32_get_from_eeprom(fd, MCMT32_AXIS_RA, E_RATIO, &data); 
    ra_r_reducer=(uint16_t) data;
    mcmt32_get_from_eeprom(fd, MCMT32_AXIS_RA, E_GEAR, &data); 
    ra_r_gear=(uint16_t) data;
    ra_deg_per_microstep=(double)(360./(ra_r_usteps*ra_r_steps*ra_r_reducer*ra_r_gear));
    
    mcmt32_get_from_eeprom(fd, MCMT32_AXIS_DEC, E_USTEPS, &data); 
    dec_r_usteps=(uint16_t) data;
    mcmt32_get_from_eeprom(fd, MCMT32_AXIS_DEC, E_STEPS, &data); 
    dec_r_steps=(uint16_t) data;
    mcmt32_get_from_eeprom(fd, MCMT32_AXIS_DEC, E_RATIO, &data); 
    dec_r_reducer=(uint16_t) data;
    retval=mcmt32_get_from_eeprom(fd, MCMT32_AXIS_DEC, E_GEAR, &data); 
    dec_r_gear=(uint16_t) data;
    dec_deg_per_microstep=(double)(360./(dec_r_usteps*dec_r_steps*dec_r_reducer*dec_r_gear));
    
    return retval;
    }

/* MCMT32 STATUS OK UNCHANGED */
/* interrupts the current slewing operation (when speed is medium or max) */
int mcmt32_stop_slewing(int fd, mcmt32_axis axis) {
	/* request:	axis_id, F0
	 * response:	01 */
	unsigned char req[2];
	unsigned char resp[1];
	int retval;
	if ((axis == MCMT32_AXIS_RA && _current_mode_ra == MCMT32_MODE_UNINIT) ||
			(axis == MCMT32_AXIS_DEC && _current_mode_dec == MCMT32_MODE_UNINIT)) {
		retval = mcmt32_device_ready(fd, axis);
		if (retval < 0) return retval;
	}
	if ((axis == MCMT32_AXIS_RA && _current_mode_ra == MCMT32_MODE_GUIDING) ||
			(axis == MCMT32_AXIS_DEC && _current_mode_dec == MCMT32_MODE_GUIDING))
		return MCMT32_RET_OK;
	/* stop slewing does not reply if it's not slewing, so we need to be sure
	 * of the current state to avoid waiting for the timeout. */

	req[0] = _axis_to_byte(axis);
	req[1] = 0xf0;
	retval = _request_response_2byte_command(fd, req, resp, 1);
	if (retval == MCMT32_RET_ERR || retval == MCMT32_RET_ARGERR)
		return MCMT32_RET_ERR;
	if (retval == MCMT32_RET_TIMEOUT ||
			(retval == 1 && _is_ack_retval(resp[0]))) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_GUIDING;
		else _current_mode_dec = MCMT32_MODE_GUIDING;
		/* flip status is returned to previous value if flipping is
		 * interrupted. That can be fixed afterwards w/ mcmt32_set_flip_status() */
		if (_flip_status == MCMT32_FLIP_FLIPPING)
			_flip_status = MCMT32_FLIP_NONE;
		else if (_flip_status == MCMT32_FLIP_UNFLIPPING)
			_flip_status = MCMT32_FLIP_FLIPPED;
		return MCMT32_RET_OK;
	}
	fprintf(stderr, "libmcmt32 stop slewing: WEIRD CASE HAPPENED! retval=%d\n", retval);
	return MCMT32_RET_ERR;	/* not an ACK?, should not happen */
}

/* MCMT32 STATUS FIXME */
/* move number of steps: slewing at max speed (the sign gives the direction) */
int mcmt32_move_number_of_steps(int fd, mcmt32_axis axis, int number_of_steps)
{
	unsigned char req[7];
	int retval;
	req[0] = _axis_to_byte(axis);
	req[1] = 'p';
	/* fix the reversed axis on the telescope */
//	if (axis == MCMT32_AXIS_DEC && _flip_status == MCMT32_FLIP_FLIPPED)
//		number_of_steps = -number_of_steps;
	if (number_of_steps >= 0) {
		number_of_steps = __bswap_32(number_of_steps);
		memcpy(req+2, &number_of_steps, 4);
	} else {
		/* negative value is in fact transferred as a positive value
		 * but with the heavy bit set, that's not a regular swap */
		number_of_steps = __bswap_32(-number_of_steps);
		memcpy(req+2, &number_of_steps, 4);
		req[2] |= 0x80;
	}
	retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval != MCMT32_RET_ERR) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_UNINIT;
		else 	_current_mode_dec = MCMT32_MODE_UNINIT;
		/* mode is set to uninit ^ because we don't know if next use will
		 * be before or after the completion of the slewing */
	}
	return retval;
}

/********************************************************************************
 *                           VIRTUAL HAND CONTROLLER                            *
 ********************************************************************************/

/* MCMT32 STATUS FIXME */
int _move_NW_fast(int fd, mcmt32_axis axis) {
	/* request:	'X', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[7];
	int retval;
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'X';
	retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval == MCMT32_RET_OK) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_SLEWING;
		else	_current_mode_dec = MCMT32_MODE_SLEWING;
	}
	return retval;
}

/* MCMT32 STATUS FIXME */
int _move_NW_medium(int fd, mcmt32_axis axis) {
	/* request:	'G', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[7];
	int retval;
    
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'G';
	retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval == MCMT32_RET_OK) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_SLEWING;
		else	_current_mode_dec = MCMT32_MODE_SLEWING;
	}
	return retval;
}

/* MCMT32 STATUS FIXME */
/* guiding correction, WARNING: cannot be done in slewing mode */
int _move_NW_slow(int fd, mcmt32_axis axis) {
	/* request:	'D', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[7];
	int retval;
	if ((axis == MCMT32_AXIS_RA && _current_mode_ra == MCMT32_MODE_SLEWING) ||
			(axis == MCMT32_AXIS_DEC && _current_mode_dec == MCMT32_MODE_SLEWING))
		return MCMT32_RET_ERR;
    
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'D';
	retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval == MCMT32_RET_OK) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_GUIDING;
		else	_current_mode_dec = MCMT32_MODE_GUIDING;
	}
	return retval;
}

/* MCMT32 STATUS FIXME */
int _move_SE_fast(int fd, mcmt32_axis axis) {
	/* request:	'W', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[7];
	int retval;
    
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'W';

	retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval == MCMT32_RET_OK) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_SLEWING;
		else	_current_mode_dec = MCMT32_MODE_SLEWING;
	}
	return retval;
}

/* MCMT32 STATUS FIXME */
int _move_SE_medium(int fd, mcmt32_axis axis) {
	/* request:	'F', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[7];
	int retval;
    
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'F';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval == MCMT32_RET_OK) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_SLEWING;
		else	_current_mode_dec = MCMT32_MODE_SLEWING;
	}
	return retval;
}

/* MCMT32 STATUS FIXME */
/* guiding correction, WARNING: cannot be done in slewing mode */
int _move_SE_slow(int fd, mcmt32_axis axis) {
	/* request:	'Q', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[7];
	int retval;

    if ((axis == MCMT32_AXIS_RA && _current_mode_ra == MCMT32_MODE_SLEWING) ||
			(axis == MCMT32_AXIS_DEC && _current_mode_dec == MCMT32_MODE_SLEWING))
		return MCMT32_RET_ERR;
    
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'Q';
	
    retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval == MCMT32_RET_OK) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_GUIDING;
		else	_current_mode_dec = MCMT32_MODE_GUIDING;
	}
	return retval;
}

/* MCMT32 STATUS TOBETESTED */
/* stop corrections and resume sidereal speed, WARNING: cannot be done in slewing mode */
int _resume_sidereal(int fd, mcmt32_axis axis) {
	/* request:	'S', 0, 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[7];
	int retval;
	if ((axis == MCMT32_AXIS_RA && _current_mode_ra == MCMT32_MODE_SLEWING) ||
			(axis == MCMT32_AXIS_DEC && _current_mode_dec == MCMT32_MODE_SLEWING))
		return MCMT32_RET_ERR;

    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'S';
	
    retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval == MCMT32_RET_OK) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_GUIDING;
		else	_current_mode_dec = MCMT32_MODE_GUIDING;
	}
	return retval;
}

/* MCMT32 STATUS TOBECHECKED */
/* Update guiding speed, can only be set while device is already in sidereal speed.
 * The new speed setting is not saved in the EEPROM, but restored when changing speed,
 * for example using the 'S' command (return to sidereal) or {XGDWFQ} commands.
 * The new speed setting is returned by the 'r' command if it hasn't been reset yet.
 * Updating the guiding speed is generally used periodically to compensate
 * errors like PEC and KING.
 *
 * The format of new_speed is:
 * bytes 0 and 1 2 3 for DELAY_M, MSB first
 * byte 4, low bit, for direction of movement (0 for reverse)
 *
 * this ^ conversion is due to the fact that argument is speed in steps per seconds
 * but the MCMT32 uses a value corresponding to the delay between (micro?)steps
 */
int mcmt32_set_current_speed(int fd, mcmt32_axis axis, double new_sps, unsigned char dir) {
	/* request:	'R', DELAY_M{1,2}, DELAY_M_LSB, DIR_M
	 * response:	ack */
	unsigned char req[8];
    uint32_t delay_m;

	if ((axis == MCMT32_AXIS_RA && _current_mode_ra == MCMT32_MODE_SLEWING) ||
			(axis == MCMT32_AXIS_DEC && _current_mode_dec == MCMT32_MODE_SLEWING))
		return MCMT32_RET_ERR;
	req[0] = _axis_to_byte(axis);
	req[1] = 'R';
	if (new_sps < (double) 0.001) {
		/* 0, 0, 0, 0 */
		memset(req+2, 0, 4);
	} else {
        mcmt32_log_text("setting ", " %lf sps on axis %u\n", req[0], new_sps);
		delay_m = (uint32_t)((double) 80000000 / new_sps);
        mcmt32_log_binary("sid delay_m", (uint8_t *)&delay_m, 4);

        req[2] = (uint8_t)(delay_m >> 24);
        req[3] = (uint8_t)(delay_m >> 16);
        req[4] = (uint8_t)(delay_m >> 8);
        req[5] = (uint8_t)(delay_m >> 0);

        req[6] = dir;
	}
	return _request_response_9byte_command(fd, req, NULL, 0);
}

/* MCMT32 STATUS TOBECHECKED */
/* Gets the value of some key parameters :
 *   4 bytes = current speed
 *   6 bytes = unused
 *   1 byte  = parked status
 */
int mcmt32_get_speednpark(int fd, mcmt32_axis axis, uint32_t *current_speed, uint8_t *parked) {
	/* request:	'r', 
	 * response:	11 bytes ack */
	unsigned char req[7];
	unsigned char resp[11];
    int retval;

	if ((axis == MCMT32_AXIS_RA && _current_mode_ra == MCMT32_MODE_SLEWING) ||
			(axis == MCMT32_AXIS_DEC && _current_mode_dec == MCMT32_MODE_SLEWING))
		return MCMT32_RET_ERR;
    
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'r';
	
    retval=_request_response_9byte_command(fd, req, resp, 11);
    mcmt32_log_binary("read speednpark", resp, 11);
	if (retval == MCMT32_RET_OK) {
        *current_speed=(resp[0]<<24)+(resp[1]<<16)+(resp[2]<<8)+resp[3];
        *parked=resp[10];
        }
    mcmt32_log_text("current speed", "%u", *current_speed);
    mcmt32_log_text("parked ", "%u", *parked);
	return retval;
}

/* MCMT32 STATUS TOBECONFIRMED */
/* enable or disable park mode */
int mcmt32_park_mode(int fd, mcmt32_axis axis, int enable) {
	/* request:	'P', 0, 0, 0, 0		park mode
	 * or request:	'N', 0, 0, 0, 0		nopark mode
	 * response:	ack */
	unsigned char req[7];
	int retval;
	if ((axis == MCMT32_AXIS_RA && _current_mode_ra == MCMT32_MODE_PARKED) ||
			(axis == MCMT32_AXIS_DEC && _current_mode_dec == MCMT32_MODE_PARKED))
		return MCMT32_RET_OK;
    
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = enable ? 'P' : 'N';
	
    retval = _request_response_9byte_command(fd, req, NULL, 0);
	if (retval == MCMT32_RET_OK) {
		if (axis == MCMT32_AXIS_RA)
			_current_mode_ra = MCMT32_MODE_PARKED;
		else _current_mode_dec = MCMT32_MODE_PARKED;
	}
	return retval;
}

/********************************************************************************
 *                           SETUP-RELATED COMMANDS                             *
 ********************************************************************************/

/* MCMT32 STATUS OK TESTED */
/* request version number for the microcontroller's code.
 * It will be stored in version, which must be at least 80 characters wide.
 * Return value is 0 (MCMT32_RET_OK) on success, -1 (MCMT32_RET_ERR) on error.
 */
int mcmt32_get_version(int fd, mcmt32_axis axis, char *version) {
	/* request:	'V', 0, 0, 0, 0, 0
	 * response:	a string allocated to 80 characters */
	unsigned char req[8];
	int retval;
	if (version == NULL) return MCMT32_RET_ERR;
	memset(version, 0, 80);		/* fix the short read nul byte issue */
	
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'V';

	retval = _request_response_9byte_command(fd, req,
			(unsigned char*)version, 14);
	mcmt32_log_binary("version_b", (unsigned char *)version, 14);
	version[79] = '\0';
	if (retval != MCMT32_RET_OK && retval != MCMT32_RET_INCOMPLETE)
		return MCMT32_RET_ERR;
	fprintf(stdout, "MCMT32 version: %s\n", version); 
	return MCMT32_RET_OK;
}

/* MCMT32 STATUS OK TESTED */
//#define SWAP_UINT32_BYTES(n) ((n & 0xff) << 24) + ((n & 0xff00) << 8 ) + ((n >> 8) & 0xff00) + ((n >> 24) & 0xff )
uint32_t swap_uint32_bytes(unsigned char *buffer){
	return (uint32_t)(buffer[0]+(buffer[1]<<8)+(buffer[2]<<16)+(buffer[3]<<24));
    }

#define SWAP_SHORT_BYTES(n) ((n & 255) << 8) + ((n >> 8) & 255)

/* MCMT32 STATUS OK TBC */
int mcmt32_get_from_eeprom(int fd, mcmt32_axis axis, mcmt32_mapindex index, uint32_t *data) {
    /* gets the value mapped at 'index' position in the eeprom map from the eeprom 
     * if index points to a byte value, we do a J command to get a single byte
     * otherwise (int16 or int32) we do a full eeprom read K command */
    int retval; 
    
    if (eeprom_map[index].length==1){
        unsigned char byte;
        retval=_read_byte_from_eeprom(fd, axis, eeprom_map[index].addr, &byte);
        *data=(uint32_t)byte;
        }
    else{
        uint32_t buffer_eeprom[MCMT32_NBPARAM_IN_EEPROM];
        retval=mcmt32_read_eeprom(fd, axis, buffer_eeprom);
        *data=buffer_eeprom[index];
        }
    return retval;
    }

/* MCMT32 STATUS OK TBC 
 * Converts an entire raw 48 byte buffer from the eeprom to a uint32_t table 
 * using the eeprom_map mechanism
 */

void _buffer_to_eeprom(unsigned char *buffer, uint32_t *eeprom){
    int i;
    
    for (i=0;i<MCMT32_NBPARAM_IN_EEPROM; ++i) {
        eeprom[i]=buffer[eeprom_map[i].addr];/* on lit tout comme des unsigned char, et on remappe ensuite
                                               les uint16 et les uint32  */
        if (eeprom_map[i].length==2){
            eeprom[i] = (*(uint16_t *)(buffer+eeprom_map[i].addr));
            }
        if (eeprom_map[i].length==4){
            //eeprom[i] = swap_uint32_bytes(buffer+eeprom_map[i].addr);
            eeprom[i] = (*(uint32_t *)(buffer+eeprom_map[i].addr));
            }
        }
// eeprom[VM_GUIDAGE] = swap_uint32_bytes(buffer+eeprom_map[VM_GUIDAGE].addr);
// eeprom[VM_CPLUS]   = swap_uint32_bytes(buffer+eeprom_map[VM_CPLUS].addr);
// eeprom[VM_CMINUS]  = swap_uint32_bytes(buffer+eeprom_map[VM_CMINUS].addr);
// eeprom[VM_SLOW]    = swap_uint32_bytes(buffer+eeprom_map[VM_SLOW].addr);
// eeprom[VM_FAST]    = swap_uint32_bytes(buffer+eeprom_map[VM_FAST].addr);
// eeprom[E_LATITUDE] = swap_uint32_bytes(buffer+eeprom_map[E_LATITUDE].addr);
// eeprom[E_LONGITUDE]= swap_uint32_bytes(buffer+eeprom_map[E_LONGITUDE].addr);
// eeprom[E_ALTITUDE] = SWAP_SHORT_BYTES (*(uint16_t *)(buffer+eeprom_map[E_ALTITUDE].addr));
// eeprom[PAS_CODEUR] = SWAP_SHORT_BYTES (*(uint16_t *)(buffer+eeprom_map[PAS_CODEUR].addr));
}

/* MCMT32 STATUS OK TBC */
/* Return value is MCMT32_RET_OK on success, MCMT32_RET_ERR on error. 
 * Reads the eeprom and maps it to a uint32_t table.
*/
int mcmt32_read_eeprom(int fd, mcmt32_axis axis, uint32_t *eeprom) {
	/* request:	'K', 0, 0, 0, 0, 0
	 * response:	48 bytes from the EEPROM
	 */
	unsigned char buffer[48];
	unsigned char req[6];
	int retval;
	
    if (eeprom == NULL) return MCMT32_RET_ERR;

    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'K';
	
    retval = _request_response_9byte_command(fd, req, buffer, 48);
	if (retval != MCMT32_RET_OK)
		return MCMT32_RET_ERR;
	_buffer_to_eeprom(buffer, eeprom);

	if ((axis == MCMT32_AXIS_RA &&
				_current_mode_ra != MCMT32_MODE_UNINIT &&
				((eeprom[PARK_MODE] == 0x01) !=
				 (_current_mode_ra == MCMT32_MODE_PARKED))) ||
			(axis == MCMT32_AXIS_DEC &&
			 _current_mode_dec != MCMT32_MODE_UNINIT &&
			 ((eeprom[PARK_MODE] == 0x01) !=
			  (_current_mode_dec == MCMT32_MODE_PARKED))))
		fprintf(stderr, "Park mode mismatch between known value and EEPROM's\n");
	/* if the EEPROM says the telescope is in park mode, we should write it as such,
	 * but if it is in fact moving, it may lead to an uncontrollable situation.
	 if (axis == MCMT32_AXIS_RA)
		_current_mode_ra = MCMT32_MODE_PARKED;
	else _current_mode_dec = MCMT32_MODE_PARKED; */
	/* initialize conversion data */
	//ra_per_step = 
	return MCMT32_RET_OK;
}


/* MCMT32 STATUS OK TESTED */
int mcmt32_write_eeprom(int fd, mcmt32_axis axis, int index, uint32_t data){
    int retval;

    switch(eeprom_map[index].length){
        case  1:
            retval=mcmt32_write_uchar8_eeprom(fd, axis, eeprom_map[index].addr, (unsigned char) data);
            break;

        case  2:
            retval=mcmt32_write_uint16_eeprom(fd, axis, eeprom_map[index].addr, (uint16_t) data);

            break;

        case  4:
            retval=mcmt32_write_uint32_eeprom(fd, axis, eeprom_map[index].addr, (uint32_t)data);
            break;

        default:
            retval=MCMT32_RET_EEPROMERR;
        }
    return retval;
    }

/* MCMT32 STATUS OK TESTED */
int mcmt32_write_eeprom_full(int fd, mcmt32_axis axis, uint32_t *eeprom){
    int i, retval;
    
    for (i=0;i<MCMT32_NBPARAM_IN_EEPROM; ++i){
        retval=mcmt32_write_eeprom(fd, axis, i, eeprom[i]);
        if (retval!=MCMT32_RET_OK){
            mcmt32_log_text("eeprom write NOK", mcmt32_strerror(retval));
            return retval;
            }
	    mcmt32_log_text("eeprom write OK", "%s %u", (char *)eeprom_map[i].name, eeprom[i] );
        usleep(100000);
        }
    return retval;
    }

/* MCMT32 STATUS OK TESTED */
/* write an unsigned char to eeprom at given address offest (0-47)
 * data is an unsigned char */
int mcmt32_write_uchar8_eeprom(int fd, mcmt32_axis axis,
		unsigned char address, unsigned char data) {
	/* request:	'L', address, byte
	 * response:	FIXME the byte from EEPROM at requested address */
	unsigned char req[6];
    unsigned char resp;
	if (address > 0x2f ) 
        return MCMT32_RET_ARGERR;
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'L';
	req[2] = address;
	req[3] = data; 
	mcmt32_log_binary("uchar8 eeprom write ", req , 7);

	return _request_response_9byte_command(fd, req, &resp, 0);
    }

/* MCMT32 STATUS OK TOBETESTED */
/* write an uint16_t to eeprom at given address offest (0-47)
 * data is a uint16_t */
int mcmt32_write_uint16_eeprom(int fd, mcmt32_axis axis,
		unsigned char address, uint16_t data) {
	/* request:	'L', address, MSB, LSB
	 * response:	FIXME the byte from EEPROM at requested address */
// mcmt32 uc code is broken for 2byte-write so we make 2 byte writes FM 20160127
//  unsigned char req[6];
//  unsigned char resp;
//  if (address > 0x2f ) 
//      return MCMT32_RET_ARGERR;
    mcmt32_write_uchar8_eeprom(fd, axis, address, data%256);
    usleep(100000);
    return mcmt32_write_uchar8_eeprom(fd, axis, address+1, data/256);
//   
//   memset(req, 0, 7);
//   req[0] = _axis_to_byte(axis);
//   req[1] = 'L';
//   req[2] = address;
//   req[3] = (uint8_t)(data >> 8);
//   req[4] = (uint8_t)(data >> 0);
//   mcmt32_log_binary("int16 eeprom write ", req , 7);
//   mcmt32_log_text("int16 eeprom write ", "%u" , data);
//   return _request_response_9byte_command(fd, req, &resp, 0);
    }

/* MCMT32 STATUS OK TOBETESTED */
/* write an uint_32t to eeprom at given address offest (0-47)
 * data is the address of a uint_32t */
    int mcmt32_write_uint32_eeprom(int fd, mcmt32_axis axis, unsigned char address, uint32_t data) {
	/* request:	'l', address, MSB, mSB, lSB, LSB
	 * response:	FIXME the byte from EEPROM at requested address
	 */
	unsigned char req[6];
    unsigned char resp;

	if (address > 0x2f) 
        return MCMT32_RET_ARGERR;
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'l';
	req[2] = address;

//req[3] = (data & 0xff); 
//req[4] = (data & 0xff00)>>8; 
//req[5] = (data & 0xff0000)>>16;
//req[6] = (data & 0xff000000)>>24;
    req[3] = (uint8_t)(data >> 24);
    req[4] = (uint8_t)(data >> 16);
    req[5] = (uint8_t)(data >> 8);
    req[6] = (uint8_t)(data >> 0);
    
	mcmt32_log_text("long eeprom write ", "%u %u %u %u\n", req[3], req[4], req[5], req[6]);
	mcmt32_log_binary("long eeprom write ", req , 7);

	return _request_response_9byte_command(fd, req, &resp, 0);
}

/* FIXME 20160119 
 * alternative to the read_eeprom, where only one byte is read from EEPROM.
 * data is the address of a single byte, not an array. */
int _read_byte_from_eeprom(int fd, mcmt32_axis axis,
		unsigned char address, unsigned char *data) {
	/* request:	'J', address, 0, 0, 0
	 * response:	the byte from EEPROM at requested address
	 */
	unsigned char req[6];
	if (address > 0x2f || data == NULL) return MCMT32_RET_ARGERR;
    memset(req, 0, 7);
	req[0] = _axis_to_byte(axis);
	req[1] = 'J';
	req[2] = address;
	
    return _request_response_9byte_command(fd, req, data, 1);
}

char *mcmt32_strerror(int errno) {
	switch (errno) {
		case MCMT32_RET_OK:
			return "no error";
		case MCMT32_RET_ERR:
			return "general error";
		case MCMT32_RET_ARGERR:
			return "bad argument";
		case MCMT32_RET_TIMEOUT:
			return "timeout";
		case MCMT32_RET_UNINIT:
			return "operation cannot be done because it relies on some uninitialized date";
		case MCMT32_RET_INCOMPLETE:
			return "response is incomplete but successful otherwise";
		default:
			return "unknown error code";
	}
}
/* MCMT32 STATUS OK TESTED */
/* Printing EEPROM data information (decimal values) */
void mcmt32_print_eeprom_configuration(FILE *output, uint32_t *eeprom) {
    int i;
	if (eeprom == NULL) {
		fprintf(stderr, "EEPROM data structure has not been initialized (NULL)\n");
		return;
        }

    for (i=0; i < MCMT32_NBPARAM_IN_EEPROM; ++i){
	    fprintf(output, "%20s : %12u (0x%.8x)\n", eeprom_map[i].name, eeprom[i], eeprom[i]);
        }
/*	fprintf(output, "Guiding delay (speed): 32-bit : %u ", eeprom[VM_GUIDAGE]);
	fprintf(output, "\tsps : %.3f\n",
			eeprom[VM_GUIDAGE] == 0 ? 0.0 : 80000000.0 / (double)eeprom[VM_GUIDAGE]);

	fprintf(output, "Correcting speed +: %u, sps : %.2f\n",
			eeprom[VM_CPLUS], 80000000.0 / (double)eeprom[VM_CPLUS]);

	fprintf(output, "Correcting speed -: %u, sps : %.2f\n",
			eeprom[VM_CMINUS], 80000000.0 / (double)eeprom[VM_CMINUS]);

	fprintf(output, "Slewing speed (low): %u, sps : %.2f\n",
			eeprom[VM_SLOW], 80000000.0 / (double)eeprom[VM_SLOW]);

	fprintf(output, "Slewing speed (high): %u, sps : %.2f\n",
			eeprom[VM_FAST], 80000000.0 / (double)eeprom[VM_FAST]);

	fprintf(output, "Acceleration: %u\n", eeprom[VM_ACCE] );

	fprintf(output, "Rotation direction: %d (%s)\n", eeprom[DIR_GUIDAGE] & 1,
			eeprom[DIR_GUIDAGE] & 1 ? "positive" : "negative");

	fprintf(output, "Resolution: %d\n", 25600 * eeprom[PasCodeur);

	fprintf(output, "Park mode %s\n", eeprom[PARK_MODE] & 1 ? "enabled" : "disabled");
	fprintf(output, "PEC is %s\n", eeprom[PEC_ENABLED] & 1 ? "enabled" : "disabled");
    */
}

