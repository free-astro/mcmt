#include <math.h>	/* floor() */
#include <time.h>	/* localtime(), time() */
#include "mcmt32.h"
#include "low_level_commands.h"
#include "conversion.h"

/* the MCMT32 gives us a number of steps for each axis.
 * We need to convert them to right ascension value or declination angle.
 *
 * INDI uses double values.
 *
 * The Stellarium Telescope Protocol version 1.0 uses:
 * http://free-astro.vinvin.tf/images/b/b7/Stellarium_telescope_protocol.txt
 * RA	(4 bytes, unsigned integer): (J2000)
 * 		a value of 0x100000000 = 0x0 means 24h=0h,
 *		a value of 0x80000000 means 12h
 * DEC	(4 bytes, signed integer): (J2000)
 *		a value of -0x40000000 means -90degrees,
 *		a value of 0x0 means 0degrees,
 *		a value of 0x40000000 means 90degrees
 */

#if 0
typedef struct {
	char dec_deg;	/* positive is NORTH */
	char dec_min, dec_sec, dec_csec;
	char ra_hour;	/* positive is EAST */
	char ra_min, ra_sec, ra_csec;
	struct timeval tv;
} _mount_position_t;

extern _mount_position_t _current_mount_position;

_mount_position_t _current_mount_position = { 0, 0, 0, 0, 0, 0, 0, 0, { 0, 0 } };
#endif

/* Converting requires a reference position, which is taken from the first slewing
 * or sync request, and the telescope moving angle for each (micro-)step */
#define CONVERSION_ANGLE_UNKNOWN -1.0

extern double ra_deg_per_microstep;      // Must be set at lower level
extern double dec_deg_per_microstep;     // Must be set at lower level 

typedef struct { unsigned int steps; double value; } reference_position;

/* reference cannot be set on RA, needs to be HA */
reference_position reference_HA = { 0, 0.0 };	/* value is a hour angle */
reference_position reference_DEC = { 0, 0.0 };	/* value is a degree angle */

void mcmt32_conversion_set_reference(mcmt32_axis axis, unsigned int steps, double value, double lon) {
	if (mcmt32_conversion_is_init(axis))
		fprintf(stderr, "MCMT32 conversion: the reference position is "
				"already set, overwriting\n");

	if (axis == MCMT32_AXIS_RA) {
		while (value < 0.0) value += 24.0;
		while (value >= 24.0) value -= 24.0;
		value = ra_to_ha(value, lon);
		if (_flip_status == MCMT32_FLIP_FLIPPED) {
			value += 12.0;
			if (value >= 24.0) value -= 24.0;
		}
		fprintf(stderr, "MCMT32 conversion: setting the reference position "
				"for the RA axis to (%d steps, %g HA)\n", steps, value);
		reference_HA.steps = steps;
		reference_HA.value = value;
	} else {
		if (_flip_status == MCMT32_FLIP_FLIPPED)
			value += 180.0;
		fprintf(stderr, "MCMT32 conversion: setting the reference position "
				"for the DEC axis to (%d steps, %g)\n", steps, value);
		reference_DEC.steps = steps;
		reference_DEC.value = value;
	}
}

void _mcmt32_conversion_reset_reference(mcmt32_axis axis) {
	if (axis == MCMT32_AXIS_RA) {
		fprintf(stderr, "MCMT32 conversion: reseting RA reference position\n");
		reference_HA.steps = 0; reference_HA.value = 0.0;
	} else {
		fprintf(stderr, "MCMT32 conversion: reseting DEC reference position\n");
		reference_DEC.steps = 0; reference_DEC.value = 0.0;
	}
}

/* returns true if there are RA and DEC reference positions available */
int _mcmt32_conversion_is_init(mcmt32_axis axis) {
	if (axis == MCMT32_AXIS_RA)
		return reference_HA.steps != 0 && reference_HA.value != 0.0;
	else return reference_DEC.steps != 0 && reference_DEC.value != 0.0;
}

int mcmt32_conversion_is_init() {
	return _mcmt32_conversion_is_init(MCMT32_AXIS_RA) &&
		_mcmt32_conversion_is_init(MCMT32_AXIS_DEC);
}

void mcmt32_conversion_reset_reference() {
	_mcmt32_conversion_reset_reference(MCMT32_AXIS_RA);
	_mcmt32_conversion_reset_reference(MCMT32_AXIS_DEC);
}

/* conversion to double */

/* this is the tricky part. See http://www.astrosurf.com/mcmt32/fonctionnement.htm
 * for information about the number of steps and motor characteristics.
 * steps per motor revolution: 200
 * micro steps per step: 128
 * micro steps per motor revolution = 200*128 = 25600
 *
 * 86164.09 seconds per earth rotation (sidereal)
 * => 360/86164.09 = 0.004178075 degree per second = 15.041 arcsec/sec
 *
 * for a value x of usteps per second for the MCMT32's sidereal speed, we have:
 * 15.041/x arcsec per ustep = 15.041/3600x degree per ustep
 *
 * for x = 221.804, we have 1.8837e-6 degree per ustep for RA
 *
 * Using number of teeth n in the gear, 360/n = angle for each tooth, and so
 * 360/n is the angle for a complete rotation of the motor, we thus have
 * (360/n)/nb_steps = axis angle per motor step, and (nb_steps = 200)
 * (360/n)/nb_usteps = axis angle per motor ustep (nb_usteps= 25600)
 *
 * TFG: RA=720 gear teeth, DEC=370
 *	720 teeth => (360/720)/200 = 0.025 deg/step or 9"/step
 *	(360/720)/25600 = 0.000019531degdeg/ustep or 0.0703125"/ustep
 *	sidereal speed requires 15.041/0.0703125 = 213.916 ustep/s
 */

/*IfIamnotmistakenthesemustbesetinlow_level
void_conversion_initialize_data(){
ra_deg_per_microstep=mcmt32_get_ratio(MCMT32_AXIS_RA);
//360.0/(double)NB_TEETH_RA)/(double)(NB_MICROSTEP*NB_STEPPERREV*MOTOR_RATIO);

dec_deg_per_microstep=mcmt32_get_ratio(MCMT32_AXIS_DEC);
//(360.0/(double)NB_TEETH_DEC)/(double)(NB_MICROSTEP*NB_STEPPERREV*MOTOR_RATIO);
fprintf(stderr,"Conversiondegreepermicrostepdatainitialized\n");
}
*/

/* converts an absolute number of microsteps (encoder value) to an actual HA
 * value (RA axis only) in double format */
double _convert_microsteps_to_ha(unsigned int encoder_value) {
	double ha;
	unsigned int delta;
	if (encoder_value == reference_HA.steps) {
		ha = reference_HA.value;
	}
	else if (encoder_value > reference_HA.steps) {
		delta = encoder_value - reference_HA.steps;
		ha = reference_HA.value + ((double)delta * ra_deg_per_microstep)/15.0;
		if (ha >= 24.0) ha -= 24.0;
	} else {
		delta = reference_HA.steps - encoder_value;
		ha = reference_HA.value - ((double)delta * ra_deg_per_microstep)/15.0;
		if (ha < 0.0) ha += 24.0;
	}
	/* in any case, if flipped, remove 12 hours */
	//if (_flip_status == MCMT32_FLIP_FLIPPED) {
	//	ha -= 12.0;
	//	if (ha < 0.0) ha += 24.0;
	//}
	return ha;
}

/* converts an absolute number of microsteps (encoder value) to an actual RA or
 * DEC value in double format */
double _convert_microsteps_to_dec(unsigned int encoder_value) {
	int delta;
	double dec;
	/* WARNING: this is unsigned arithmetics, we need to manage the sign
	 * explicitly.
	 * We assume that encoder_value given by the MCMT32 will never cycle
	 * below 0 or over 2^32-1 */
	if (encoder_value == reference_DEC.steps) {
		dec = reference_DEC.value;
	}
	else {
	    delta=encoder_value - reference_DEC.steps;
	    dec=reference_DEC.value + (double)delta * dec_deg_per_microstep;
	}
	if (dec<-90.) dec+=180.; 
	if (dec>90.) dec-=180.; 
	//
	//	else if (encoder_value > reference_DEC.steps) {
	//		delta = encoder_value - reference_DEC.steps;
	//		/* values are inverted when flipped */
	//		if (_flip_status == MCMT32_FLIP_FLIPPED)
	//			dec = reference_DEC.value - ((double)delta * dec_deg_per_microstep);
	//		else dec = reference_DEC.value + ((double)delta * dec_deg_per_microstep);
	//	} else {
	//		delta = reference_DEC.steps - encoder_value;
	//		if (_flip_status == MCMT32_FLIP_FLIPPED)
	//			dec = reference_DEC.value + ((double)delta * dec_deg_per_microstep);
	//		else dec = reference_DEC.value - ((double)delta * dec_deg_per_microstep);
	//i	}
	/* in any case, if flipped, remove 180 degrees */
	//if (_flip_status == MCMT32_FLIP_FLIPPED)
	//	dec -= 180.0;
	return dec;
}

/* converts an absolute RA or DEC value in double format to an absolute number
 * of microsteps (encoder value)
 * allow_short_path is used to force the rotation on the longest direction.
 * This can be useful to force the flip in one direction and avoid turns in wires
 */
int _convert_double_to_microsteps(mcmt32_axis axis,
		double value, unsigned int *steps, int allow_short_path, double lon) {
	if (!_mcmt32_conversion_is_init(axis))
		return MCMT32_RET_UNINIT;
	if (axis == MCMT32_AXIS_RA) {
		double ha, delta_hours;
		fprintf(stderr, "RA convert %g hours to microsteps\n", value);
		/* if flipped, remove 180deg to the target angle */
		/*if (_flip_status == MCMT32_FLIP_FLIPPED)
			value -= 12.0;*/
		/* Right ascension ranges from 0 to 24 (hours) */
		while (value < 0.0) value += 24.0;
		while (value >= 24.0) value -= 24.0;
		
		/* convert RA to HA */
		ha = ra_to_ha(value, lon);

		/* avoid doing more than 12h/180deg, always go the short way */
		/* XXX POSSIBLE CABLE PROBLEM HERE
		 * XXX FLIP PROBLEM HERE: it will unflip the other way around if
		 * this optimization is activated.
		 */
		if (_flip_status != MCMT32_FLIP_FLIPPING &&
			       _flip_status != MCMT32_FLIP_UNFLIPPING &&
			       fabs(ha - reference_HA.value) > 12.0) {
			/* don't go the short way when flipping */
			if (ha > reference_HA.value)
				delta_hours = -(24.0-ha) - reference_HA.value;
			else delta_hours = -(24.0-reference_HA.value) - ha;
		} else {
			delta_hours = ha - reference_HA.value;
			/* force >0 value when flipping and <0 when unflipping */
			if (_flip_status == MCMT32_FLIP_FLIPPING && delta_hours < 0.0)
				delta_hours += 24.0;
			else if (_flip_status == MCMT32_FLIP_UNFLIPPING && delta_hours > 0.0)
				delta_hours -= 24.0;
		}
		fprintf(stderr, "\tDelta hours: %g\n", delta_hours);

		/* calculate number of steps */
		*steps = reference_HA.steps + (delta_hours * 15.0) / ra_deg_per_microstep;
	} else {
		/* Declination ranges from -90.0 to 90.0 degrees, but with the
		 * flip it may be -270 to 270 */
		double delta_degrees;
		fprintf(stderr, "DEC convert %g degrees to microsteps\n", value);
		delta_degrees = value - reference_DEC.value;
		fprintf(stderr, "\tDelta degrees v0: %g\n", delta_degrees);
		/* force >0 value when flipping and <0 when unflipping */
		if (_flip_status == MCMT32_FLIP_FLIPPING && delta_degrees < 0.0)
			delta_degrees += 360.0;
		else if (_flip_status == MCMT32_FLIP_UNFLIPPING && delta_degrees > 0.0)
			delta_degrees -= 360.0;
		/* avoid 360 degrees values */
		else if (delta_degrees <= -270.0)
			delta_degrees += 360.0;
		else if (delta_degrees > 270.0)
			delta_degrees -= 360.0;
		fprintf(stderr, "\tDelta degrees vf: %g\n", delta_degrees);
		*steps = reference_DEC.steps + delta_degrees / dec_deg_per_microstep;
	}
	return MCMT32_RET_OK;
}

/*************************** CONVERSION OF RA TO HA ***************************/
/* see: http://jean-paul.cornec.pagesperso-orange.fr/gmst0.htm
 * Angle horaire d'une etoile H  = angle horaire du point g -
 *					ascension droite de l'etoile a
 */

/* step 1, Julian day calculation. */
#define MJD0  2415020.0
#define J2000 (2451545.0 - MJD0)      /* yes, 2000 January 1 at 12h */

/* Code from XEphem. Given a date in months, mn, days, dy, years, yr,
 * return the modified Julian date (number of days elapsed since 1900 jan 0.5),
 * *mjd.
 * days between 1 and 31, months between 1 and 12
 */
void cal_mjd (int mn, double dy, int yr, double *mjp) {
	static double last_mjd, last_dy;
	static int last_mn, last_yr;
	int b, d, m, y;
	long c;

	if (mn == last_mn && yr == last_yr && dy == last_dy) {
		*mjp = last_mjd;
		return;
	}

	m = mn;
	y = (yr < 0) ? yr + 1 : yr;
	if (mn < 3) {
		m += 12;
		y -= 1;
	}

	if (yr < 1582 || (yr == 1582 && (mn < 10 || (mn == 10 && dy < 15))))
		b = 0;
	else {
		int a;
		a = y/100;
		b = 2 - a + a/4;
	}

	if (y < 0)
		c = (long)((365.25*y) - 0.75) - 694025L;
	else
		c = (long)(365.25*y) - 694025L;

	d = (int)(30.6001*(m+1));

	*mjp = b + c + d + dy - 0.5;

	last_mn = mn;
	last_dy = dy;
	last_yr = yr;
	last_mjd = *mjp;
}

/* given an mjd, truncate it to the beginning of the whole day */
double mjd_day(double mj) {
	return (floor(mj-0.5)+0.5);
}

/* given an mjd, return the number of hours past midnight of the whole day */
double mjd_hr(double mj) {
	return ((mj-mjd_day(mj))*24.0);
}


/* step 2, greenwich mean sidereal time */
#define SIDRATE         .9972695677

/* insure 0 <= *v < r. */
void range (double *v, double r) {
	*v -= r*floor(*v/r);
}

/* gmst0() - return Greenwich Mean Sidereal Time at 0h UT; stern */
double gmst0 (
		double mj)      /* date at 0h UT in Julian days since MJD0 */
{
	double T, x;

	T = ((int)(mj - 0.5) + 0.5 - J2000)/36525.0;
	x = 24110.54841 +
		(8640184.812866 + (0.093104 - 6.2e-6 * T) * T) * T;
	x /= 3600.0;
	range(&x, 24.0);
	return (x);
}

/* given a modified Julian date, mj, and a universally coordinated time, utc,
 * return greenwich mean sidereal time, *gst.
 * N.B. mj must be at the beginning of the day.
 * Current GST can be seen here: http://www.dur.ac.uk/john.lucey/users/lst.html
 */
void utc_gst (double mj, double utc, double *gst) {
	static double lastmj = -18981;
	static double t0;

	if (mj != lastmj) {
		t0 = gmst0(mj);
		lastmj = mj;
	}
	*gst = (1.0/SIDRATE)*utc + t0;
	range (gst, 24.0);
}

/* step 3, local sidereal time, in hours, which is the local meridian's hour
 * angle too */

#define degrad(x)       ((x)*M_PI/180.)
#define raddeg(x)       ((x)*180./M_PI)
#define deghr(x)        ((x)/15.)
#define radhr(x)        deghr(raddeg(x))

/* given the modified Julian date, mj, find the mean obliquity of the
 * ecliptic, *eps, in radians.
 *
 * IAU expression (see e.g. Astron. Almanac 1984); stern
 */
void obliquity (double mj, double *eps)
{
	static double lastmj = -16347, lasteps;

	if (mj != lastmj) {
		double t = (mj - J2000)/36525.;     /* centuries from J2000 */
		lasteps = degrad(23.4392911 +       /* 23^ 26' 21".448 */
				t * (-46.8150 +
					t * ( -0.00059 +
						t * (  0.001813 )))/3600.0);
		lastmj = mj;
	}
	*eps = lasteps;
}

/* local sidereal time from modified Julian day, the high level method from
 * XEphem */
void now_lst(double mjd, double *lstp, double lon) {
	static double last_mjd = -23243, last_lst = 0.0;
	double lst, eps, deps, dpsi;
	//double hour, min, sec;
	if (last_mjd == mjd) {
		*lstp = last_lst;
		return;
	}
	utc_gst(mjd_day(mjd), mjd_hr(mjd), &lst);
	lst += lon/15.0;	// hours

	obliquity(mjd, &eps);
	nutation(mjd, &deps, &dpsi);
	lst += radhr(dpsi*cos(eps+deps));

	range (&lst, 24.0);
	/*hour = floor(lst);
	min = floor((lst-hour)*60);
	sec = floor((lst-hour-min/60)*3600);
	fprintf(stderr, "\tLocal sidereal time: %g (%02d:%02d:%02d)\n",
			lst, (int)hour, (int)min, (int)sec);*/
	last_mjd = mjd;
	*lstp = last_lst = lst;
}


/********************** OUR HIGH LEVEL CONVERSION METHODS **********************/

double get_lst(double lon) {
	double mjd, lst;
	time_t now;
	struct tm *timenow;
	now = time(NULL);
	timenow = gmtime(&now);
	/*fprintf(stderr, "\tUTC: %02d:%02d:%02d on %d/%d/%d\n",
			timenow->tm_hour, timenow->tm_min, timenow->tm_sec,
			timenow->tm_mon+1, timenow->tm_mday, timenow->tm_year+1900);*/
	cal_mjd(timenow->tm_mon+1, timenow->tm_mday, timenow->tm_year+1900, &mjd);
	/* add current time to the Julian date, used to calculate the Greenwich
	 * Sidereal Time, so it needs to be UTC, not local time */
	mjd += (double)timenow->tm_hour / 24. + (double)timenow->tm_min / 1440. +
		(double)timenow->tm_sec / 86400.;
	now_lst(mjd, &lst, lon);	// get local sidereal time using GST and longitude
	return lst;
}

double ra_to_ha(double ra, double lon) {
	double ha, lst = get_lst(lon);
	// RA + HA = LST
	ha = lst - ra;
	if (ha < 0.0) ha += 24.0;
	if (ha >= 24.0) ha -= 24.0;
	fprintf(stderr, "\tRA to HA: RA = %g hours => HA = %g hours\n", ra, ha);
	return ha;
}

double ha_to_ra(double ha, double lon) {
	double ra, lst = get_lst(lon);
	// RA + HA = LST
	ra = lst - ha;
	if (ra < 0.0) ra += 24.0;
	if (ra >= 24.0) ra -= 24.0;
	/*fprintf(stderr, "\tHA to RA: HA = %g hours => RA = %g hours\n", ha, ra);*/
	return ra;
}

/* hour angle to azimuth, used to check meridian crossing for the mount flip */
double ha_dec_to_az(double ha, double dec, double latitude) {
	double sin_alt, lat, alt, cos_az, az;
	/* convert degrees to radians */
	ha = ha*15.0*M_PI/180.0;
	dec = dec*M_PI/180.0;
	lat = latitude*M_PI/180.0;

	/* compute altitude in radians */
	sin_alt = sin(dec)*sin(lat) + cos(dec)*cos(lat)*cos(ha);
	alt = asin(sin_alt);
    
	/* compute azimuth in radians
	 * divide by zero error at poles or if alt = 90 deg */
	if (cos(alt) == 0.0) {
		az = 0.0;
	} else {
		cos_az = (sin(dec) - sin(alt)*sin(lat))/(cos(alt)*cos(lat));
		az  = acos(cos_az);
	}

	/* convert radians to degrees */
	/* alt = alt*180.0/M_PI; */
	az  = az*180.0/M_PI;

    	/* choose hemisphere */
	if (sin(ha) > 0)
		az  = 360.0 - az;
	return az;
}

int mcmt32_convert_is_over_meridian(double ha, double dec, double lat) {
	double az = ha_dec_to_az(ha, dec, lat);
	fprintf(stderr, "calculated azimuth: %g deg\n", az);
	return (az >= 180.0);
}

/* [0 ; 180[ is non flipped side
 * [180 ; 360[ is flipped side */
