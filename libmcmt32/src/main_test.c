#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>
#include "mcmt32.h"

#define MODE_CONSOLE	0
extern struct mcmt32_eeprommap eeprom_map[18];

uint32_t eeprom_current_ra[MCMT32_NBPARAM_IN_EEPROM];
uint32_t to_eeprom_ra[MCMT32_NBPARAM_IN_EEPROM];

uint32_t eeprom_current_dec[MCMT32_NBPARAM_IN_EEPROM];
uint32_t to_eeprom_dec[MCMT32_NBPARAM_IN_EEPROM];

char *devices[20];
int nb_devices = 0;
int mcmt32_fd = -1;
int run = 1;
int current_mode = MODE_CONSOLE;
void console_mode();
int calculate_ms_from_timeval(struct timeval *start, struct timeval *stop);


/* MCMT32 STATUS OK TESTED */
int mcmt32_init_eeprom_ra(int fd){
	to_eeprom_ra[VM_GUIDAGE]           = 0x000db206; /*   0	vitesse moyenne de guidage         */
	to_eeprom_ra[VM_CPLUS]             = 0x00223d10; /*   4	vitesse moyenne de correction +    */
	to_eeprom_ra[VM_CMINUS]            = 0x00088f44; /*   8	vitesse moyenne de correction -    */
	to_eeprom_ra[VM_SLOW]              = 0x00015e9a; /*  12	vitesse moyenne de pointage lent   */
	to_eeprom_ra[VM_FAST]              = 0x00000703; /*  16	vitesse moyenne de pointage rapide */
	to_eeprom_ra[VM_ACCE]              = 0x05;       /*  20	*/
	to_eeprom_ra[DIR_GUIDAGE]          = 0x01;       /*  21	*/
	to_eeprom_ra[E_USTEPS]             = 0x0020;     /*  22  */
	to_eeprom_ra[E_STEPS]              = 0x0064;     /*  24  */
	to_eeprom_ra[E_RATIO]              = 0x000c;     /*  26  */
	to_eeprom_ra[E_GEAR]               = 0x00c8;     /*  28  */
	to_eeprom_ra[E_LATITUDE]           = 0x00000101; /*  30  */
	to_eeprom_ra[E_LONGITUDE]          = 0x00000102; /*  34  */
	to_eeprom_ra[E_ALTITUDE]           = 0x0103;     /*  38  */
	to_eeprom_ra[PARK_MODE]            = 0x00;       /*  41	premier bit: park mode activated */
	to_eeprom_ra[RAQ_TYPE_CAN_ADDRESS] = 0x00;       /*  42	*/
    to_eeprom_ra[PAS_CODEUR]           = 0x7c01;     /*  43  */
    to_eeprom_ra[PEC_ENABLED]          = 0x00;       /*  47	*/
    return mcmt32_write_eeprom_full(fd, MCMT32_AXIS_RA, to_eeprom_ra);
    }

int mcmt32_init_eeprom_dec(int fd){
	to_eeprom_dec[VM_GUIDAGE]           = 0x00000000; /*   0  vitesse moyenne de guidage */
	to_eeprom_dec[VM_CPLUS]             = 0x0016d360; /*   4  vitesse moyenne de correction + */
	to_eeprom_dec[VM_CMINUS]            = 0x0016d360; /*   8  vitesse moyenne de correction - */
	to_eeprom_dec[VM_SLOW]              = 0x00015e9a; /*  12  vitesse moyenne de pointage lent */
	to_eeprom_dec[VM_FAST]              = 0x00000703; /*  16  vitesse moyenne de pointage rapide */
	to_eeprom_dec[VM_ACCE]              = 0x05;       /*  20  */
	to_eeprom_dec[DIR_GUIDAGE]          = 0x01;       /*  21  */
	to_eeprom_dec[E_USTEPS]             = 0x0020;     /*  22  */
	to_eeprom_dec[E_STEPS]              = 0x0064;     /*  24  */
	to_eeprom_dec[E_RATIO]              = 0x000c;     /*  26  */
	to_eeprom_dec[E_GEAR]               = 0x00c8;     /*  28  */
	to_eeprom_dec[E_LATITUDE]           = 0x00000101; /*  30  */
	to_eeprom_dec[E_LONGITUDE]          = 0x00000102; /*  34  */
	to_eeprom_dec[E_ALTITUDE]           = 0x0103;     /*  38  */
	to_eeprom_dec[PARK_MODE]            = 0x00;       /*  41  premier bit: park mode activated */
	to_eeprom_dec[RAQ_TYPE_CAN_ADDRESS] = 0x00;       /*  42  */
    to_eeprom_dec[PAS_CODEUR]           = 0x7c01;     /*  43  */
    to_eeprom_dec[PEC_ENABLED]          = 0x00;       /*  47  */
    return mcmt32_write_eeprom_full(fd, MCMT32_AXIS_DEC, to_eeprom_dec);
    }


int main() {
	int i;
    nb_devices = mcmt32_serial_link_list(devices, 20);
    fprintf(stdout, "List of available serial devices:\n");
    for (i=0; i<nb_devices; i++) {
        fprintf(stdout, "%2d: %s\n", i, devices[i]);
    }
    fprintf(stdout,"%ld", sizeof(uint32_t));
    mcmt32_fd = mcmt32_serial_link_open("/dev/ttyS0");
	do {
		if (current_mode == MODE_CONSOLE)
			console_mode();
	} while (run) ;

	if (mcmt32_fd != -1)
		mcmt32_serial_link_close(mcmt32_fd);
	return 0;
}

void display_help() {
	fprintf(stdout, "Available commands:\n"
			"\topen dev_num:     open serial device dev_num in the displayed list\n"
			"\thelp (or h):      display this help\n"
			"\tping (or ready):  get MCMT32's status\n"
			"\tversion:          get MCMT32's versions\n"
			"\tencoders (or e):  get current encoders values (mount position)\n"
			"\tinit 0|1:         initialize EEPROM (0 for RA, 1 for DEC)\n"
			"\teeprom 0|1:       get and display EEPROM values (0 for RA, 1 for DEC)\n"
			"\tmoveRA nb_steps:  move signed nb_steps on the RA axis\n"
			"\tmoveDEC nb_steps: move signed nb_steps on the DEC axis\n"
			"\tsidguid:          retour vitesse siderale sur 2 axes \n"
			"\tsetspd 0|1 stps dir slot : writes speed in eeprom (slot 0=sid, 1=CPLUS...)\n"
			"\tgetspeed O|1 :    reads current speed and parked status\n"
			"\tsetstps O|1 nbsps dir : sets current sps \n"
			"\tsetpos O|1 lat lon alt : sets cordinates (lat lon in .1, alt in m) \n"
			"\tgetpos : gets coordinates \n"
			"\tsetstps O|1 nbsps dir : sets current sps \n"
			"\tstopall (or s):   stop moving before steps are reached\n");
	fprintf(stdout, "quit with 'q' or ^d\n");
}

int initnwrite_eeprom(int fd, mcmt32_axis axis){
    int retval;

    switch(axis){
        case MCMT32_AXIS_RA:
            retval=mcmt32_init_eeprom_ra(mcmt32_fd);
            mcmt32_read_eeprom(mcmt32_fd, axis, eeprom_current_ra);
            mcmt32_print_eeprom_configuration(stdout, eeprom_current_ra);
            break;

        case MCMT32_AXIS_DEC:
            retval=mcmt32_init_eeprom_dec(mcmt32_fd);
            mcmt32_read_eeprom(mcmt32_fd, axis, eeprom_current_dec);
            mcmt32_print_eeprom_configuration(stdout, eeprom_current_dec);
            break;
    }
    return retval;
}

void console_mode() {
	char command[200], *command_ptr;

	do {
		fprintf(stdout, "> ");
		command_ptr = fgets(command, 200, stdin);

		if (!command_ptr || !strcasecmp(command, "q\n")) {
			if (!command_ptr)
				fputc('\n', stdout);
			run = 0;
			return ;
		}
		if (command[0] == '\n') continue;

		/* OPEN THE SERIAL LINK */
		if (!strncasecmp(command, "open", 4)) {
			if (strlen(command) > 6 && command[4] == ' ' &&
					command[5] >= '0' && command[5] <= '9') {
				int dev_num = atoi(command+5);
				char device[50];
				if (dev_num >= nb_devices) {
					fprintf(stdout, "wrong device number\n");
				} else {
					sprintf(device, "/dev/%s", devices[dev_num]);
					fprintf(stdout, "opening serial device %s for MCMT32\n",
							devices[dev_num]);
					mcmt32_fd = mcmt32_serial_link_open(device);
					if (mcmt32_fd != -1)
						fprintf(stdout, "serial link successfully opened.\n");
				}
			} else {
				fprintf(stdout, "malformed command. use 'open number', with"
						" number being the index in the displayed list"
						" of serial devices\n");
			}
		}
		else if (!strcasecmp(command, "h\n") || !strcasecmp(command, "help\n")) {
			display_help();
		}
		else if (mcmt32_fd == -1) {
			fprintf(stdout, "connect to the MCMT32 first, use open\n");
		}
		/* IS DEVICE READY (PING) */
		else if (!strcasecmp(command, "ping\n") || !strcasecmp(command, "ready\n")) {
			int retval;
			struct timeval start, stop;
			gettimeofday(&start, NULL);
			retval = mcmt32_device_ready(mcmt32_fd, MCMT32_AXIS_RA);
			if (retval >= 0) {
				int ms;
				gettimeofday(&stop, NULL);
				ms = calculate_ms_from_timeval(&start, &stop);
				fprintf(stdout, "right ascension axis is %s. Ping: %dms\n",
						retval ? "ready (guiding)" : "not ready (slewing)",
						ms);
			}
			else fprintf(stdout, "right ascension axis did not respond (%s)\n",
					mcmt32_strerror(retval));

			gettimeofday(&start, NULL);
			retval = mcmt32_device_ready(mcmt32_fd, MCMT32_AXIS_DEC);
			if (retval >= 0) {
				int ms;
				gettimeofday(&stop, NULL);
				ms = calculate_ms_from_timeval(&start, &stop);
				fprintf(stdout, "declination axis is %s. Ping: %dms\n",
					       retval ? "ready (guiding)" : "not ready (slewing)",
					       ms);
			}
			else fprintf(stdout, "declination axis did not respond (%s)\n",
					mcmt32_strerror(retval));
		}
		/* READ MCMT32 VERSION */
		else if (!strcasecmp(command, "version\n")) {
			char ver[80];
			if (mcmt32_get_version(mcmt32_fd, MCMT32_AXIS_RA, ver) == MCMT32_RET_OK)
				fprintf(stdout, "RA version: %s\n", ver);
			else fprintf(stdout, "could not get RA version\n");
			if (mcmt32_get_version(mcmt32_fd, MCMT32_AXIS_DEC, ver) == MCMT32_RET_OK)
				fprintf(stdout, "DEC version: %s\n", ver);
			else fprintf(stdout, "could not get DEC version\n");
		}
		/* READ ENCODER VALUE */
		else if (!strcasecmp(command, "e\n") || !strcasecmp(command, "encoders\n")) {
			unsigned int encoder_val;
			if (mcmt32_read_encoder(mcmt32_fd, MCMT32_AXIS_RA, &encoder_val) ==
					MCMT32_RET_OK)
				fprintf(stdout, "RA encoder value: %u\n", encoder_val);
			else fprintf(stdout, "RA encoder reading failed\n");
			if (mcmt32_read_encoder(mcmt32_fd, MCMT32_AXIS_DEC, &encoder_val) ==
					MCMT32_RET_OK)
				fprintf(stdout, "DEC encoder value: %u\n", encoder_val);
			else fprintf(stdout, "DEC encoder reading failed\n");
		}

		else if (!strncasecmp(command, "nbsps ", 6) ) {
			double nbsps;
            unsigned char dir=1;

			if (strlen(command) > 6 && sscanf(command+6, "%lf %hhd", &nbsps, &dir) >= 1){
                mcmt32_change_sidereal_speed(mcmt32_fd, nbsps); 
            }
		}

		/* READ EEPROM DATA AND DISPLAY IT */
		else if (!strncasecmp(command, "eeprom", 6)) {
            uint32_t *eeprom_current;
			if (strlen(command) > 8 && command[6] == ' ' &&
					(command[7] == '0' || command[7] == '1')) {
			    if (command[7] == 0)
                    eeprom_current=eeprom_current_ra;
                else
                    eeprom_current=eeprom_current_dec;
                    
				if (mcmt32_read_eeprom(mcmt32_fd,
						command[7] == '0' ? MCMT32_AXIS_RA : MCMT32_AXIS_DEC,
						eeprom_current) == MCMT32_RET_OK)
					mcmt32_print_eeprom_configuration(stdout, eeprom_current);
				else fprintf(stdout, "Error getting the EEPROM data\n");
			} else {
				fprintf(stdout, "malformed command. use 'eeprom 0|1' (0 for RA, 1 for DEC)\n");
			}
		}
		else if (!strncasecmp(command, "init", 4)) {
			if (strlen(command) > 4 && command[4] == ' ' &&
					(command[5] == '0' || command[5] == '1')) {
			    mcmt32_axis axis = (command[5] == '0' ? MCMT32_AXIS_RA : MCMT32_AXIS_DEC);	

                switch(axis){
                    case MCMT32_AXIS_RA:
                        mcmt32_init_eeprom_ra(mcmt32_fd);
                        mcmt32_read_eeprom(mcmt32_fd, axis, eeprom_current_ra);
                        mcmt32_print_eeprom_configuration(stdout, eeprom_current_ra);
                        break;

                    case MCMT32_AXIS_DEC:
                        mcmt32_init_eeprom_dec(mcmt32_fd);
                        mcmt32_read_eeprom(mcmt32_fd, axis, eeprom_current_dec);
                        mcmt32_print_eeprom_configuration(stdout, eeprom_current_dec);
                        break;
                    }

			} 

				else fprintf(stdout, "Error getting the EEPROM data\n");
		}
		else if (!strncasecmp(command, "accel", 5)) {
            uint32_t accel;

			if (strlen(command) > 7 && command[5] == ' ' && sscanf(command+8, "%u", &accel) == 1){
			    mcmt32_axis axis = (command[7] == '0' ? MCMT32_AXIS_RA : MCMT32_AXIS_DEC);	
                if (mcmt32_write_eeprom(mcmt32_fd, axis, VM_ACCE, accel) == MCMT32_RET_OK){
                    fprintf(stdout, "Setting Accel %u on axis %c", accel, command[7]);
                    }
				else fprintf(stdout, "Malformed command ; use accel <ACCEL_RATE>\n");
		    }
        }



		/* Read current speed and park*/
		else if (!strncasecmp(command, "getspeed", 8)) {
			if (strlen(command) > 8 && command[8] == ' ' && (command[9] == '0' || command[9] == '1')) {
                uint32_t current_speed;
                uint8_t parked;

			    mcmt32_axis axis = (command[9] == '0' ? MCMT32_AXIS_RA : MCMT32_AXIS_DEC);	

                if(mcmt32_get_speednpark(mcmt32_fd, axis, &current_speed, &parked)== MCMT32_RET_OK){
                    fprintf(stdout, "speed %u \nparked %u\n", current_speed, parked);
                    }
                }
            }

		/* Retour vitesse siderale */
		else if (!strncasecmp(command, "sidguid", 7)) {
				if (mcmt32_guide(mcmt32_fd, MCMT32_WEST, MCMT32_SPEED_GUID) == MCMT32_RET_OK)
					fprintf(stdout, "RA  sidereal guiding... OK\n");
				else fprintf(stdout, "error when setting  sidereal guiding.\n");
				if (mcmt32_guide(mcmt32_fd, MCMT32_NORTH, MCMT32_SPEED_GUID) == MCMT32_RET_OK)
					fprintf(stdout, "DEC sidereal guiding... OK\n");
				else fprintf(stdout, "error when setting  sidereal guiding.\n");
		}

		/* GOTO: MOVE NUMBER OF STEPS */
		else if (!strncasecmp(command, "movera", 6)) {
			if (strlen(command) > 8 && command[6] == ' ' &&
					((command[7] >= '0' && command[7] <= '9') || command[7] == '-')) {
				int steps = atoi(command+7);
				if (mcmt32_move_number_of_steps(mcmt32_fd, MCMT32_AXIS_RA, steps) ==
						MCMT32_RET_OK)
					fprintf(stdout, "moving %d steps on RA. Use 's' to stop\n",
							steps);
				else fprintf(stdout, "error when trying to move RA.\n");
			} else {
				fprintf(stdout, "malformed command. use 'movera nb_steps'\n");
			}
		}
		else if (!strncasecmp(command, "movedec", 7)) {
			if (strlen(command) > 9 && command[7] == ' ' &&
					((command[8] >= '0' && command[8] <= '9') || command[8] == '-')) {
				int steps = atoi(command+8);
				if (mcmt32_move_number_of_steps(mcmt32_fd, MCMT32_AXIS_DEC, steps) ==
						MCMT32_RET_OK)
					fprintf(stdout, "moving %d steps on DEC. Use 's' to stop\n",
							steps);
				else fprintf(stdout, "error when trying to move DEC.\n");
			} else {
				fprintf(stdout, "malformed command. use 'movedec nb_steps'\n");
			}
		}
		else if (!strncasecmp(command, "setstps", 7)) {
			uint32_t nbsps ;
            uint8_t dir=1;
            uint32_t delay;

			if (strlen(command) > 9 && command[7] == ' ' &&
                    sscanf(command+10, "%u %hhu", &nbsps, &dir) == 2){
			    mcmt32_axis axis = (command[8] == '0' ? MCMT32_AXIS_RA : MCMT32_AXIS_DEC);
                delay=(uint32_t)((double)80000000/(double)nbsps);
				if (mcmt32_set_current_speed(mcmt32_fd, axis, delay, dir) ==
						MCMT32_RET_OK)
					fprintf(stdout, "setting %u steps/sec axis %hhu\n", nbsps, axis);
                else fprintf(stdout, "error when trying to set speed axis %u\n", axis);
            } else {
                fprintf(stdout, "malformed command. use 'setstps axis nb_steps dir'\n");
            }
        }
        else if (!strncasecmp(command, "getpos", 6)) {
            uint32_t data;
            double lat, lon;

            if (mcmt32_get_from_eeprom(mcmt32_fd, MCMT32_AXIS_RA, E_LATITUDE, &data) !=MCMT32_RET_OK){
                fprintf(stderr, "error getting latitude via get_from_eeprom\n");
            }
            else{
                lat=(double)data/CASINDEG;
                fprintf(stdout,"latitude via get_from_eeprom %.3lf \n", lat);
            }
            if (mcmt32_get_from_eeprom(mcmt32_fd, MCMT32_AXIS_DEC, E_LONGITUDE, &data) !=MCMT32_RET_OK){
                fprintf(stderr, "error getting longitude via get_from_eeprom\n");
            }
            else{
                lon=(double)data/CASINDEG;
                fprintf(stdout,"longitude via get_from_eeprom %.3lf \n", lon);
            }

        }
        else if (!strncasecmp(command, "setpos", 6)) {
			double lat, lon;
            int16_t alt;
            int32_t coord;

			if (strlen(command) > 8 && command[6] == ' ' &&
                    sscanf(command+9, "%lf %lf %hu", &lat, &lon, &alt) == 3){
			    mcmt32_axis axis = (command[7] == '0' ? MCMT32_AXIS_RA : MCMT32_AXIS_DEC);
                coord=(uint32_t)(lat*CASINDEG);
                if (mcmt32_write_eeprom( mcmt32_fd, axis, E_LATITUDE, coord) == MCMT32_RET_OK)
					fprintf(stdout, "setting lat %f (%d)\n", lat, coord);
				else fprintf(stdout, "error when trying to set lat\n");

                usleep(100000);
                coord=(uint32_t)(lon*CASINDEG);
                if (mcmt32_write_eeprom( mcmt32_fd, axis, E_LONGITUDE, coord) == MCMT32_RET_OK)
					fprintf(stdout, "setting lat %f (%d)\n", lon, coord);
				else fprintf(stdout, "error when trying to set lat\n");
                usleep(100000);

                if (mcmt32_write_eeprom( mcmt32_fd, axis, E_ALTITUDE, alt) == MCMT32_RET_OK)
					fprintf(stdout, "setting lat %d \n", alt);
				else fprintf(stdout, "error when trying to set alt \n");
			} else {
				fprintf(stdout, "malformed command. use 'setpos axis lat lon alt'\n");
			}
		}
		else if (!strncasecmp(command, "setspd", 6)) {
            //
            // writes in eeprom one of the five speeds 
            //  parameter axis=0/RA axis=1/DEC
            //  parameter nbsps = steps per second
            //  parameter dir : dir 0=reverse 1=direct (only relevant for slot=0 VM_GUIDAGE)
            //  parameter slot=0..4 :
            //      0 = [VM_GUIDAGE] = 0x000ad08e; 
            //      1 = [VM_CPLUS]   = 0x0009d500; 
            //      2 = [VM_CMINUS]  = 0x000c1600; 
            //      3 = [VM_SLOW]    = 0x00039c00; 
            //      4 = [VM_FAST]    = 0x00000308; 
            // 
            //
			float nbsps ;
            uint8_t dir=1;
            uint8_t slot=0;
            uint32_t delay;

			if (strlen(command) > 8 && command[6] == ' ' &&
                    sscanf(command+9, "%f %hhu %hhu", &nbsps, &dir, &slot) == 3){
			    mcmt32_axis axis = (command[7] == '0' ? MCMT32_AXIS_RA : MCMT32_AXIS_DEC);
                
                delay=(uint32_t)((double)80000000/(double)nbsps);
                if (nbsps==0){
                    delay=0;
                    }
                if (mcmt32_write_eeprom( mcmt32_fd, axis, slot, delay) == MCMT32_RET_OK)
					fprintf(stdout, "setting %s %.2f steps/sec (0x%x) axis %hhu in eeprom\n", 
                        eeprom_map[slot].name,nbsps,delay, axis);
				else fprintf(stdout, "error when trying to set sidspeed axis %u\n", axis);
			} else {
				fprintf(stdout, "malformed command. use 'setspd axis[=0|1]  nb_steps dir[=0|1] slot[=0-4]'\n");
			}
		}
		else if (!strcasecmp(command, "s\n") ||
				!strcasecmp(command, "stopall\n")) {
			fprintf(stdout, "stopping all slewing\n");
			mcmt32_stop_all_slewing(mcmt32_fd);
		}
		else {
			fprintf(stdout, "command not found, use `help' for help, `q' to quit\n");
		}

	} while (1) ;
}

int calculate_ms_from_timeval(struct timeval *start, struct timeval *stop) {
	if (!start || !stop)
		return 9999999;
	time_t delta_seconds      = stop->tv_sec - start->tv_sec;
	time_t delta_milliseconds = (stop->tv_usec - start->tv_usec) / 1000;

	if (delta_milliseconds < 0) { /* manually carry a one from the seconds field */
		delta_milliseconds += 1000;
		-- delta_seconds;
	}
	return (delta_seconds * 1000) + delta_milliseconds;
}

