#include <stdio.h>
#include <stdarg.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include "log.h"

FILE *log_fd = NULL;

void mcmt32_log_open_file() {
	time_t now_sec;
	char date[32];
	if (log_fd != NULL) {
		fprintf(stderr, "log already opened!\n");
		return;
	}
	log_fd = fopen(LOGFILE, "a");
	if (log_fd == NULL) {
		perror("mcmt32 log fopen");
		log_fd = stderr;
		return;
	}
	now_sec = time(NULL);
	ctime_r(&now_sec, date);
	date[strlen(date)-1] = '\0';	/* remove NL */
	mcmt32_log_text("log", "------ LOG OPENED (%s) ------", date);
}

void mcmt32_log_close_file() {
	time_t now_sec;
	char date[32];
	if (log_fd == NULL || log_fd == stderr) {
		log_fd = NULL;
		fprintf(stderr, "log was not opened!\n");
		return;
	}
	now_sec = time(NULL);
	ctime_r(&now_sec, date);
	date[strlen(date)-1] = '\0';	/* remove NL */
	mcmt32_log_text("log", "------ LOG CLOSED (%s) ------", date);
	fclose(log_fd);
	log_fd = NULL;
}

void _log_print_date() {
}

void _log_print_timestamp() {
	struct timeval tv;
	struct tm *now;
	gettimeofday(&tv, NULL);
	now = localtime(&tv.tv_sec);
	fprintf(log_fd, "%.2d:%.2d:%.2d.%.3u ",
			now->tm_hour, now->tm_min, now->tm_sec,
			(int)tv.tv_usec/1000);
}

void mcmt32_log_text(char *title, const char *format, ...) {
	va_list args;
	va_start(args, format);
	_log_print_timestamp();
	fprintf(log_fd, "%s: ", title);
	vfprintf(log_fd, format, args);
	va_end(args);
	fputc('\n', log_fd);
	fflush(log_fd);
}

void mcmt32_log_binary(char *title, unsigned char *buf, int buflen) {
	int i;
	_log_print_timestamp();
	if (buflen > 8)
		fprintf(log_fd, "%s (%d %s):\n", title, buflen,
				buflen <= 1 ? "byte" : "bytes");
	else if (buflen <= 0) {
		fprintf(log_fd, "%s (empty)\n", title);
		return;
	}
	else fprintf(log_fd, "%s (%d %s): ", title, buflen,
				buflen <= 1 ? "byte" : "bytes");
	
	for (i=0; i<buflen; i++) {
		if (buflen > 8 && i % 8 == 0)
			fputc('\t', log_fd);
		fprintf(log_fd, "%.2X", buf[i]);
		if (i % 8 == 7)
			fputc('\n', log_fd);
		else fputc(' ', log_fd);
	}
	if (i > 0 && i % 8 != 0)
		fputc('\n', log_fd);
	fflush(log_fd);
}
