#ifndef _MCMT32_LOG_H
#define _MCMT32_LOG_H

#define LOGFILE "libmcmt32.log"

void mcmt32_log_open_file();
void mcmt32_log_close_file();
void mcmt32_log_text(char *title, const char *format, ...);
void mcmt32_log_binary(char *title, unsigned char *buf, int buflen);

#endif
