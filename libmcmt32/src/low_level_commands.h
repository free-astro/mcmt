#ifndef _MCMT32_LOW_LEVEL_H
#define _MCMT32_LOW_LEVEL_H

#include <unistd.h>
#include "mcmt32.h"	/* for mcmt32 types */

#define MCMT32_ACK	0x06
#define MCMT32_NOACK	0x15


/* internal functions for the MCMT32 library */

unsigned char _axis_to_byte(mcmt32_axis axis);

int _request_response_8byte_command(int fd, unsigned char *request,
		unsigned char *response, int expected_response_size);

int _request_response(int fd, unsigned char *request, int request_size,
		unsigned char *response, int expected_reponse_size);

int _is_ack_retval(unsigned char byte);


void _buffer_to_eeprom(unsigned char *buffer, uint32_t  *eeprom);
char * _eeprom_current_to_string(unsigned char current);

int _read_byte_from_eeprom(int fd, mcmt32_axis axis,
		unsigned char address, unsigned char *data);

/* hand controller commands */

int _move_NW_fast(int fd, mcmt32_axis axis);
int _move_NW_medium(int fd, mcmt32_axis axis);
int _move_SE_fast(int fd, mcmt32_axis axis);
int _move_SE_medium(int fd, mcmt32_axis axis);


/* guiding commands */

int _move_NW_slow(int fd, mcmt32_axis axis);
int _move_SE_slow(int fd, mcmt32_axis axis);
int _resume_sidereal(int fd, mcmt32_axis axis);

int _update_current_speed(int fd, mcmt32_axis axis, double new_speed, unsigned char dir);


/* libmcmt32 internal status data *
 * representing the physical status of the mount/MCMT32 */

typedef enum {
	MCMT32_MODE_UNINIT, MCMT32_MODE_SLEWING,
	MCMT32_MODE_GUIDING, MCMT32_MODE_PARKED
} _mcmt32_mode;
extern _mcmt32_mode _current_mode_ra, _current_mode_dec;

extern mcmt32_flip_status _flip_status;

extern uint8_t RA_driver_status, DEC_driver_status;
extern double ra_deg_per_microstep;
extern double dec_deg_per_microstep;
extern uint16_t ra_r_usteps, ra_r_steps, ra_r_reducer, ra_r_gear;
extern uint16_t dec_r_usteps, dec_r_steps, dec_r_reducer, dec_r_gear;


#ifndef INT_MAX
#define INT_MAX       2147483647
#endif

#endif
