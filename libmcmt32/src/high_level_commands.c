#include <stdio.h>
#include "mcmt32.h"
#include "low_level_commands.h"
#include "conversion.h"

/* MCMT32 STATUS UNCHANGED TOBECHECKED */
/* virtual hand controller for the slewing mode
 * use MCMT32_SPEED_GUID to stop slewing */
int mcmt32_move(int fd, mcmt32_direction dir, mcmt32_speed speed) {
	int retval = -1;
	mcmt32_axis axis;
	switch (dir) {
		case MCMT32_NORTH:
		case MCMT32_SOUTH:
			axis = MCMT32_AXIS_DEC;
			break;
		case MCMT32_EAST:
		case MCMT32_WEST:
			axis = MCMT32_AXIS_RA;
			break;
	}
	if (dir == MCMT32_NORTH || dir == MCMT32_WEST) {
		switch (speed) {
			case MCMT32_SPEED_GUID:
				mcmt32_stop_slewing(fd, axis);
				break;
			case MCMT32_SPEED_MIN:
				return MCMT32_RET_ARGERR;
			case MCMT32_SPEED_MED:
				retval = _move_NW_medium(fd, axis);
				break;
			case MCMT32_SPEED_MAX:
				retval = _move_NW_fast(fd, axis);
				break;
		}
	} else {
		switch (speed) {
			case MCMT32_SPEED_GUID:
				mcmt32_stop_slewing(fd, axis);
				break;
			case MCMT32_SPEED_MIN:
				return MCMT32_RET_ARGERR;
			case MCMT32_SPEED_MED:
				retval = _move_SE_medium(fd, axis);
				break;
			case MCMT32_SPEED_MAX:
				retval = _move_SE_fast(fd, axis);
				break;
		}
	}
	return retval;
}

/* MCMT32 STATUS UNCHANGED TOBECHECKED */
/* virtual hand controller for the guiding mode */
int mcmt32_guide(int fd, mcmt32_direction dir, mcmt32_speed speed) {
	int retval = -1;
	mcmt32_axis axis;
	switch (dir) {
		case MCMT32_NORTH:
		case MCMT32_SOUTH:
			if (_current_mode_dec == MCMT32_MODE_SLEWING)
				return MCMT32_RET_ERR;
			axis = MCMT32_AXIS_DEC;
			break;
		case MCMT32_EAST:
		case MCMT32_WEST:
			if (_current_mode_ra == MCMT32_MODE_SLEWING)
				return MCMT32_RET_ERR;
			axis = MCMT32_AXIS_RA;
			break;
	}
	if (dir == MCMT32_NORTH || dir == MCMT32_WEST) {
		switch (speed) {
			case MCMT32_SPEED_GUID:
				retval = _resume_sidereal(fd, axis);
				break;
			case MCMT32_SPEED_MIN:
				retval = _move_NW_slow(fd, axis);
				break;
			case MCMT32_SPEED_MED:
			case MCMT32_SPEED_MAX:
				return MCMT32_RET_ARGERR;
		}
	} else {
		switch (speed) {
			case MCMT32_SPEED_GUID:
				retval = _resume_sidereal(fd, axis);
				break;
			case MCMT32_SPEED_MIN:
				retval = _move_SE_slow(fd, axis);
				break;
			case MCMT32_SPEED_MED:
			case MCMT32_SPEED_MAX:
				return MCMT32_RET_ARGERR;
		}
	}
	return retval;
}

/* MCMT32 STATUS OK */
/* Changing sidereal speed
 * Used for tuning purposes 
 * The original sidereal speed can be reset using mcmt32_move(MCMT32_SPEED_GUID) */
int mcmt32_change_sidereal_speed(int fd, double nbsps) {
	return mcmt32_set_current_speed(fd, MCMT32_AXIS_RA, nbsps, 1);
}


/* MCMT32 STATUS CHANGED OK */
/* when telescope is moving at sidereal speed, it can be stopped using this method.
 * The original sidereal speed can be reset using mcmt32_move(MCMT32_SPEED_GUID) */
int mcmt32_stop_sidereal_speed(int fd) {
	return mcmt32_set_current_speed(fd, MCMT32_AXIS_RA, (double)0, 1);
}

/* MCMT32 STATUS UNCHANGED */
/* When telescope is slewing (using mcmt32_move_number_of_steps() or mcmt32_move()),
 * the move can be stopped by using this command */
void mcmt32_stop_all_slewing(int fd) {
	mcmt32_stop_slewing(fd, MCMT32_AXIS_RA);
	mcmt32_stop_slewing(fd, MCMT32_AXIS_DEC);
}

/* MCMT32 STATUS UNCHANGED */
/* setting reference position for angle conversion and relative slewing. The
 * current position is taken as reference with the angles passed in argument.
 * If the flip status is unknown when calling this function, it is guessed from
 * the passed coordinates (assuming flipped above meridian). To force it to
 * another value, use mcmt32_set_flip_status() before calling this function. */
int mcmt32_set_current_position(int fd, double ra, double dec, double lon) {
	unsigned int enc_val;
	if (_flip_status == MCMT32_FLIP_UNKNOWN) {
		/* guess flip status from coordinates */
		double ha = ra_to_ha(ra, lon);
		if (mcmt32_convert_is_over_meridian(ha, dec, lon))
			_flip_status = MCMT32_FLIP_FLIPPED;
		else _flip_status = MCMT32_FLIP_NONE;
	}
	if (mcmt32_read_encoder(fd, MCMT32_AXIS_RA, &enc_val) == MCMT32_RET_OK)
		mcmt32_conversion_set_reference(MCMT32_AXIS_RA, enc_val, ra, lon);
	else return MCMT32_RET_ERR;

	if (mcmt32_read_encoder(fd, MCMT32_AXIS_DEC, &enc_val) == MCMT32_RET_OK)
		mcmt32_conversion_set_reference(MCMT32_AXIS_DEC, enc_val, dec, lon);
	else return MCMT32_RET_ERR;
	return MCMT32_RET_OK;
}

/* MCMT32 STATUS UNCHANGED */
/* get the current position of the telescope and convert it to RA/DEC values */
int mcmt32_get_current_ha_position(int fd, double *ha, double *dec) {
	unsigned int enc_val;
	double _ha, _dec;	/* avoid writing nonsense on errors */
	if (mcmt32_read_encoder(fd, MCMT32_AXIS_RA, &enc_val) == MCMT32_RET_OK){
		_ha = _convert_microsteps_to_ha(enc_val);
		fprintf(stderr,"usteps RA %u _flip_status %d %g\n", enc_val , _flip_status, _ha);
	}
	else return MCMT32_RET_ERR;

	if (mcmt32_read_encoder(fd, MCMT32_AXIS_DEC, &enc_val) == MCMT32_RET_OK) {
	    _dec = _convert_microsteps_to_dec(enc_val);
	    fprintf(stderr,"usteps DEC  %u flip_status %d %g\n", enc_val, _flip_status, _dec );
	}
	else return MCMT32_RET_ERR;
	*ha = _ha; *dec = _dec;
	return MCMT32_RET_OK;
}

/* MCMT32 STATUS UNCHANGED */
/* get the current position of the telescope and convert it to RA/DEC values */
int mcmt32_get_current_position(int fd, double *ra, double *dec, double lon) {
	double ha, _dec;
	int retval;
	if (!mcmt32_conversion_is_init())
		return MCMT32_RET_UNINIT;
	if ((retval = mcmt32_get_current_ha_position(fd,
					&ha, &_dec)) == MCMT32_RET_OK) {
		*ra = ha_to_ra(ha, lon);
		*dec = _dec;
	}
	return retval;
}

/* MCMT32 STATUS FIXME */
/* The GOTO method
 * Moves the telescope to designated RA/DEC values. RA is in hours, DEC in degrees */
int mcmt32_slew_absolute(int fd, double ra, double dec, double lon, int allow_flip) {
	unsigned int current_enc_val_ra, current_enc_val_dec,
		     remote_enc_val_ra, remote_enc_val_dec, tmp;
	int nb_steps, is_flipping = 0;
	if (!mcmt32_conversion_is_init())
		return MCMT32_RET_UNINIT;
	/* while flipping, deny other moves and new targets */
	if (_flip_status == MCMT32_FLIP_FLIPPING ||
			_flip_status == MCMT32_FLIP_UNFLIPPING) {
		fprintf(stderr, "Please wait until flip is over to give a new target\n");
		/* TODO the flipping status is removed only when calling
		 * mcmt32_device_ready(), so clients should call it periodically.
		 * It also means that there will be some latency between the end
		 * of the flip and being allowed to slew again.
		 * Solutions would be to add these calls above OR here and check
		 * again flip status:
		 * mcmt32_device_ready(fd, MCMT32_AXIS_RA);
		 * mcmt32_device_ready(fd, MCMT32_AXIS_DEC)
		 */
		return MCMT32_RET_ERR;
	}
	/* TODO: while slewing, what to do? */

	if (0 && allow_flip) {
		int is_over_meridian;
		double target_ha = ra_to_ha(ra, lon);
		is_over_meridian = mcmt32_convert_is_over_meridian(target_ha, dec, lon);
		if (_flip_status == MCMT32_FLIP_NONE && is_over_meridian) {
			/* needs to flip */
			ra += 12.0;
			dec += 180.0;
			if (ra >= 24.0) ra -= 24.0;
			if (dec >= 180.0) dec -= 360.0;
			fprintf(stderr, "-- FLIPPING, RA = %g, DEC = %g\n", ra, dec);
			_flip_status = MCMT32_FLIP_FLIPPING;
			is_flipping = 1;
		} else if (_flip_status == MCMT32_FLIP_FLIPPED && !is_over_meridian) {
			/* needs to unflip: nothing to do */
			/*ra -= 12.0;
			dec -= 180.0;
			if (ra < 0.0) ra += 24.0;
			if (dec < -180.0) dec += 360.0;*/
			fprintf(stderr, "-- UNFLIPPING, RA = %g, DEC = %g\n", ra, dec);
			_flip_status = MCMT32_FLIP_UNFLIPPING;
			is_flipping = 1;
		}
	}
	/* modify target when flipped */
//	if (_flip_status == MCMT32_FLIP_FLIPPED) {
//		ra += 12.0;
//		dec += 180.0;
//		if (ra >= 24.0) ra -= 24.0;
//		/*if (dec >= 360.0) dec -= 360.0;*/
    fprintf(stderr, "MCMT32: flip_status %d dec %g\n", _flip_status, dec);

	_convert_double_to_microsteps(MCMT32_AXIS_RA, ra, &remote_enc_val_ra, !is_flipping, lon);
	_convert_double_to_microsteps(MCMT32_AXIS_DEC, dec, &remote_enc_val_dec, !is_flipping, lon);
	fprintf(stderr, "MCMT32: slewing to %gh (%u steps), %g deg (%u steps)\n",
			ra, remote_enc_val_ra, dec, remote_enc_val_dec);

	/* RA axis. Managing the unsigned int - unsigned int given to an int arg */
	if (mcmt32_read_encoder(fd, MCMT32_AXIS_RA, &current_enc_val_ra) == MCMT32_RET_OK) {
		if (remote_enc_val_ra > current_enc_val_ra)
			tmp = remote_enc_val_ra - current_enc_val_ra;
		else tmp = current_enc_val_ra - remote_enc_val_ra;
		if (tmp > INT_MAX) {
			fprintf(stderr, "MCMT32: we have an integer overflow, that "
					"should not happen often, but it did. If you "
					"are not trying to slew on the other side of "
					"the sky, you may need to restart the MCMT32.\n");
			return MCMT32_RET_ERR;
		}
		nb_steps = (int)tmp;
		if (remote_enc_val_ra < current_enc_val_ra)
			nb_steps = -nb_steps;
		if (nb_steps != 0) {
			fprintf(stderr, "\t-> RA moving %d microsteps\n", nb_steps);
			if (mcmt32_move_number_of_steps(fd, MCMT32_AXIS_RA, nb_steps)
					!= MCMT32_RET_OK)
				return MCMT32_RET_ERR;
		}
	} else return MCMT32_RET_ERR;

	/* DEC axis. Managing the unsigned int - unsigned int given to an int arg */
	if (mcmt32_read_encoder(fd, MCMT32_AXIS_DEC, &current_enc_val_dec) == MCMT32_RET_OK) {
		fprintf(stderr, "target steps: %d, current steps: %d (t-c=%d)\n",
				remote_enc_val_dec, current_enc_val_dec,
				remote_enc_val_dec-current_enc_val_dec);
		if (remote_enc_val_dec > current_enc_val_dec)
			tmp = remote_enc_val_dec - current_enc_val_dec;
		else tmp = current_enc_val_dec - remote_enc_val_dec;
		if (tmp > INT_MAX) {
			fprintf(stderr, "MCMT32: we have an integer overflow, that "
					"should not happen often, but it did. If you "
					"are not trying to slew on the other side of "
					"the sky, you may need to restart the MCMT32.\n");
			return MCMT32_RET_ERR;
		}
		nb_steps = (int)tmp;
		if (remote_enc_val_dec < current_enc_val_dec)
			nb_steps = -nb_steps;
		if (nb_steps != 0) {
			fprintf(stderr, "\t-> DEC moving %d microsteps\n", nb_steps);
			if (mcmt32_move_number_of_steps(fd, MCMT32_AXIS_DEC, nb_steps)
					!= MCMT32_RET_OK)
				return MCMT32_RET_ERR;
		}
	} else return MCMT32_RET_ERR;
	return MCMT32_RET_OK;
}

/* MCMT32 STATUS UNCHANGED */
int mcmt32_slew_absolute2(int fd, double ra, double dec, double lon) {
	return mcmt32_slew_absolute(fd, ra, dec, lon, 1);
}

/* MCMT32 STATUS UNCHANGED */
int mcmt32_slew_relative(int fd, double ra, double dec) {
	return MCMT32_RET_ERR;
}

mcmt32_flip_status mcmt32_get_flip_status() {
//	_flip_status=MCMT32_FLIP_NONE; 
	if (!mcmt32_conversion_is_init())
		return MCMT32_FLIP_UNKNOWN;
	return _flip_status;
}

/* MCMT32 STATUS UNCHANGED OK */
/* may be used after a flip abort, or when resuming operations */
void mcmt32_set_flip_status(mcmt32_flip_status st) {
	_flip_status = st;
}

#if 0
/* guess the flip status from position (assuming flipped above meridian) */
/* MCMT32 STATUS UNCHANGED */
int mcmt32_guess_flip_status(int fd) {
	double current_ha, current_dec;
	if (_flip_status == MCMT32_FLIP_UNKNOWN) {
		if (mcmt32_get_current_ha_position(fd, &current_ha,
					&current_dec) != MCMT32_RET_OK)
			return MCMT32_RET_ERR;
		if (mcmt32_convert_is_over_meridian(current_ha, current_dec))
			_flip_status = MCMT32_FLIP_FLIPPED;
		else _flip_status = MCMT32_FLIP_NONE;
		fprintf(stderr, "Initializing flip status from current location: "
				"STATUS IS %s\n", _flip_status == MCMT32_FLIP_FLIPPED ?
				"FLIPPED" : "NOT FLIPPED");
	}
	return MCMT32_RET_OK;
}
#endif


/* MCMT32 STATUS OK */
/* asks the MCMT32 if the axes are in park mode.
 * Returns:	MCMT32_RET_ERR if anything goes wrong,
 *		1 if park mode is activated on AT LEAST ONE axis,
 *		0 else.
 */
int mcmt32_is_parked(int fd) {
    uint32_t park_mode_ra, park_mode_dec;

    if (mcmt32_get_from_eeprom(fd, MCMT32_AXIS_RA, PARK_MODE, &park_mode_ra) !=MCMT32_RET_OK)
		return MCMT32_RET_ERR;

    if (mcmt32_get_from_eeprom(fd, MCMT32_AXIS_DEC, PARK_MODE, &park_mode_dec) !=MCMT32_RET_OK)
		return MCMT32_RET_ERR;
        
	park_mode_ra &= 0x01; 
    park_mode_dec &= 0x01;

	if (park_mode_ra) _current_mode_ra = MCMT32_MODE_PARKED;
	if (park_mode_dec) _current_mode_dec = MCMT32_MODE_PARKED;

	if (park_mode_ra != park_mode_dec)
		fprintf(stderr, "Park mode mismatch between RA and DEC axes\n");
	return (park_mode_ra == 0x01 || park_mode_dec == 0x01);
}


