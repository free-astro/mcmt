/****************************************************************
*                  Definition des Output                        *
****************************************************************/
/*
 *      Bits de control des LED
 *
 */
#ifdef AD
const int Out_LED_A_Moins =  33;    // the number of the LED pin
const int Out_LED_A_Plus =  32;     // the number of the LED pin
#else
const int Out_LED_A_Moins =  32;    // the number of the LED pin
const int Out_LED_A_Plus =  33;     // the number of the LED pin
#endif
const int Out_LED_T1 =  31;         // the number of the LED pin
const int Out_LED_T2 =  30;         // the number of the LED pin
const int Out_Control_TX =  29;     // Controle de la ligne TX pour MCMT Laurent B.
const int Out_HorlogeMoteur = 7;
const int Out_DIR = 11;
/****************************************************************
*                  Definition des Input                         *
****************************************************************/
const int In_PEC1 =  28;      // Controle de la ligne TX pour MCMT Laurent B.
const int In_RAQ_Moins = 36;     /* bit 6 du port D */
const int In_RAQ_Plus = 37;      /* bit 7 du port D */
const int In_ST4_Moins = 27;     /* bit 1 du port E */
const int In_ST4_Plus = 26;      /* bit 0 du port E */
const int In_VIT1 = 8;
const int In_VIT2 = 35;
// const int PB3 = 34;
const int Out_LED1 = 13;      // pour debugage
//const int LED2 = 0;      // pour debugage
#define EAST 0
#define WEST 1
/****************************************************************
*     Adresses des data dans la memoire EEPROM                  *
****************************************************************/
#define E_VM_GUIDE   		     0
#define E_VM_COREC_PLUS    	     4
#define E_VM_COREC_MINUS    	 8
#define E_VM_SLEW_SLOW        	12
#define E_VM_SLEW_FAST          16
#define E_VM_ACCE   		    20
#define E_DIR_GUIDE  		    21

// R_MICROSTEPS byte number of usteps per step  (defaults to 128)
#define E_MICROSTEPS            22
//        
// R_STEPS byte number of steps per motor rev (defaults to 200)
#define E_STEPS                 24
//
// R_REDUCER_RATIO ratio of a possible additional reducer 
// between motor and worm gear (defaults to 1 if none) 
#define E_REDUCER_RATIO         26
//
// R_GEAR_RATIO byte : number of teeth of the worm gear (defaults to 200)
#define E_AXIS_RATIO            28
//
// LATITUDE signed int32, positive North. Unit is 0.01 arcsecond (~0.3 m)
//                                        Divide by 360000 to get degrees
// useless for motor driving, only here as storage for uppper level soft
#define E_LATITUDE              30
//
// LONGITUDE signed int32, positive East. Unit is 0.01 arcsecond (~0.2 m at 45 deg)
//                                        Divide by 360000 to get degrees
// useless for motor driving, only here as storage for uppper level soft
#define E_LONGITUDE             34
//
// ALTITUDE signed int16. East. Unit is 1 meter
//                                        
// useless for motor driving, only here as storage for uppper level soft
#define E_ALTITUDE   		    38
// For german mounts parameter at E_FLIP address is EOM if the scope is 
// east of the mount WOM if west, NAP if not applicable (not a german mount)
#define E_FLIP   		        40

#define E_PARK_MODE 		    41
#define E_RAQ_TYPE_CAN          42
#define E_PAS_CODEUR 		    43
#define	E_PEC_ENABLED		    47
// 
// hour angle, height and side of pier of the park position
#define PARK_HA                 48
#define PARK_HGT                52
#define PARK_SIDE               56

/****************************************************************
*     Speed mode definitions
****************************************************************/
#define SPD_SIDEREAL     0
#define SPD_COR_PLUS     1
#define SPD_SLOW_PLUS    2
#define SPD_FAST_PLUS    3
#define SPD_COR_MINUS    4
#define SPD_SLOW_MINUS   5
#define SPD_FAST_MINUS   6

#define AXIS_POSITION_INIT 0x40000000
// 
// For german mounts, scope position regarding mount
//
// NAP : mount is not a german mount
#define	NAP	             0	    
// EOM : scope is East Of Mount
#define	EOM	             1	    
// WOM : scope is West Of Mount
#define	WOM	             2	    

#define	URXDA                    0
#define	OERR                     1

/****************************************************************
*     		D?finition de constantes               		*
****************************************************************/

#define		NOACK	0x15
#define		ACK	0x06

#include	"table.c" 

/****************************************************************
*                  Definition des Output                        *
****************************************************************/

/*
 *      Bits de control des moteurs *      RA0..3 pour la phase A
 *      RD0..3 pour la phase B
 *
 */
 

/***************************************************************************
 ******         Definition des variables        ****************************
 ***************************************************************************/

/*
 * Variables globales
 *
 */

// Drapeau pour le positionnement en pas

static byte	Dir_M;       // Motion direction ; to be sent to the driver               
static uint32_t Delay_M; // Delay that will be used 

static byte	Pointage;    // flag ; set to 1 if one of SPD_SLOW_* or SPD_FAST_* is requested
static byte PingReturn;  //  pointage << 7 + guidage << 6 + tracking << 5 + rapide << 1 + dir;
                         //  in order to provide some info when answering 0xff

static byte	Guide;       // flag 1 if guiding, 0 if slewing 
static byte	Change_Mode; // flag indicating whether a speed change has been requested 

static byte Num_Carte;   // 0xe0 RA, 0xe1 for DEC, 0xe2 for focus
static byte Clavier;        // paddle/ST4 state 
static byte Temp;           // used to store paddle/ST4 state before testing key rebound

static byte Mode_Vitesse;   // one SPD_* values defined above.
static byte	Old_Mode_Vitesse;


/***************************************************************************
 ******         Serial communication   **********************
 ***************************************************************************/
static byte	NbRS232;
static byte	RS232Received;	
static byte Buf_Count; 
static byte Buf_Serial[9];
static byte	Command_Rec; // flag to indicate whether an rs232 command is to be processed

/***************************************************************************
 ******         Slewing related variables variables   **********************
 ***************************************************************************/
static uint32_t Num_Posi; // Absolute position of the motor unit is microsteps
                          // initialized at AXIS_POSITION_INIT in setup()
                          // then is updated at each motor microstep 

static uint32_t Temp_Num_Posi;  // freezed version of Num_Posi used when sending
                                // the 4 bytes over the serial line

static uint32_t Delay_Target;   // 
static uint32_t Num_Pointage;   // Initialized upon reception of a slexing action ('p' command)
                                // upon recption, it is a signed number of usteps 
                                // (sign is extracted and then only absolute value is used)
static uint16_t Debug;		
static uint32_t Debugl;		

static byte	Accu_Add;    // flag indicating the direction of a slewing 1 means positive
                         // 0 means negative 
static byte Accel_M;      // Acceleration rate used to start and end slewing. 
                          // Values between 0 and 15 look ok (or not)

static byte Act_Corr_pointage; // flag indicating whether or not sidereal compensation
                               // (while slewing) should occur
static char Pointage_Corr;  // used to store accumulated sidereal steps that will be
                            // used to account for sidereal drift while slewing
static byte	Stop;
static byte	Flip;           // flag for the scope position / mount (german mounts)
                            // either NAP, WOM or EOM. Is read from eeprom at boot time
                            // Can be updated by client software during runtime

/***************************************************************************
 ******         managing direction LEDs
 ***************************************************************************/
static word	Compt_Flash, Flash_Init;

/***************************************************************************
 ******         PEC related variables
 ***************************************************************************/
static byte	Pec_Base, Pec_Count;
static word Pec_Step, Pec_Comp, Temp_Pec_Step, Temp_Pec;    
static byte	Pec_Activated, Pec_Write, Pec_Start, Dec_Mode;
static byte	Init_PEC, Pos_Pec1;

static byte Focus_Mode, Park, Raq_Type_Can, OKstep;


/***************************************************************************
 ******         steps, motor and gear informations
 ***************************************************************************/
static byte R_Microsteps;   // number of microsteps. Must match stepper driver configuration.
                            // is read from eeprom in setup(), defaults to 128 if 0 in eeprom.

static byte R_Steps;// number of steps per motor revolution. 
                    // is read from eeprom in setup(), defaults to 200 if 0 in eeprom.

static byte R_Reducer_Ratio;// optional reducer ratio between motor ang gear.
                            // is read from eeprom in setup(), defaults to 1 if 0 in eeprom.

static byte R_Axis_Ratio;// ratio between motor and axis (number of teeth of the wheel on axis)
                         // is read from eeprom in setup(), defaults to 200 if 0 in eeprom.

static uint32_t Microsteps_Per_Rev;   // total ratio between usteps and motor axis
                                  // is computed once in setup from the R_* values read in eeprom
                                  // default values (all 0 in eeprom) result in usteps_per_rev=25600

