#include <IOShieldEEPROM.h>
#include <Wire.h>
/****************************************************************
 *               Definition de la version                        *
 ****************************************************************/

#define version         "20160225svnR75"
// 1.1.2 remise a zero buffer RS232 en cas de depassement
// 1.1.3 gestion correcte de la raquette
// 1.1.4 integration des ordres ST4
// 1.1.5 integration du PEC avec capteur inductif
// 1.1.6 remise a zero buffer RS232 en cas de depassement bis
// 1.1.7 correction bug ST4
// 1.1.8 correction ST4, bouton raquette avec changement de port et pullup
// 1.1.9 correction tests raquette et ST4
// 1.1.10 changement majeur de gestion de l'ordre 'R' (changement vitesse)
// 1.1.11 verification de la coh?rence de la raquette et des ordres rs2
// 1.1.12 code pour UECIDE
// 1.1.13 preparation pour monture azimutale
// 1.1.1R fork libmcmt32 
/***************************************************************/

//#define AD                  //METTRE EN COMENTAIRE POUR LA CARTE DECLI
#include "Variables.h"
#ifdef AD
#define Carte 0             
#else
#define Carte 1             
#endif
#define UN 1
#define ZERO 0
#define LED1ON  digitalWrite(LED1,HIGH);
#define LED1OFF digitalWrite(LED1,LOW);
#define LED2ON  digitalWrite(LED2,HIGH);
#define LED2OFF digitalWrite(LED2,LOW);
#define TABLELENGTH 2000
#define getcUART1         (char)ReadUART1
#define ReadUART1()       (U1RXREG)
#define putcUART1(c)      do{while(!U1STAbits.TRMT); WriteUART1((int)(c));}while(0)
#define WriteUART1(data)  U1TXREG = (data)

/****************************************************************
 *       Function Name:      getch                *
 *       Return Value:   None                    *
 *       Parametres:     None                    *
 *       Description:    gets a character from the serial port    *
 ****************************************************************/

byte getch(void) {
    byte data;
    data=getcUART1();
    return (data);    
}

/****************************************************************
 *       Function Name:      putch                *
 *       Return Value:   None                    *
 *       Parametres:     None                    *
 *       Description:    writes a character to the serial port    *
 ****************************************************************/

void putch(byte c) {
    digitalWrite(Out_Control_TX,HIGH);
    putcUART1(c);
    while (!bitRead(U1STA,8));  
    digitalWrite(Out_Control_TX,LOW);
}

/****************************************************************
 *       Function Name:  write_eeprom                        *
 *       Return Value:   none                                    *
 *       Parametres:     adresse de la variable, data            *
 *       Description:    ecrit un octet ds eeprom                *
 ****************************************************************/

void write_eeprom(byte data_address, byte datavalue) {
    IOShieldEEPROM.write(data_address,datavalue);
}

/****************************************************************
 *       Function Name:  write_eeprom_int                        *
 *       Return Value:   void                                    *
 *       Parametres:     unsigned int                          *
 *       Description:    ecriture 16 bits ds eeprom            *
 ****************************************************************/

void write_eeprom_int(byte address, word datalongvalue) {
    byte * var_pointeur;

    var_pointeur = (byte *)&datalongvalue;
    write_eeprom( address, var_pointeur[0] );
    write_eeprom( address+1, var_pointeur[1] );
}

/****************************************************************
 *       Function Name:  write_eeprom_long                        *
 *       Return Value:   void                                    *
 *       Parametres:     unsigned long                          *
 *       Description:    ecriture 16 bits ds eeprom            *
 ****************************************************************/

void write_eeprom_long(byte address, unsigned long datalongvalue) {
    byte * var_pointeur;

    var_pointeur = (byte *)&datalongvalue;
    write_eeprom( address, var_pointeur[0] );
    write_eeprom( address+1, var_pointeur[1] );
    write_eeprom( address+2, var_pointeur[2] );
    write_eeprom( address+3, var_pointeur[3] );
}

/****************************************************************
 *       Function Name:  read_eeprom                         *
 *       Return Value:   byte                                    *
 *       Parametres:     adresse de la variable                  *
 *       Description:    Lit un octet ds eeprom                  *
 ****************************************************************/
byte read_eeprom(byte data_address) {          
    byte Data;
    Data = IOShieldEEPROM.read(data_address);
    return(Data);
}

/****************************************************************
 *       Function Name:  read_eeprom_int                         *
 *       Return Value:   int                                     *
 *       Parametres:     adresse de la variable                  *
 *       Description:    lecture 16 bits ds eeprom            *
 ****************************************************************/

word read_eeprom_int(byte address) {
    /*        byte * var_pointeur;
              word RESULT;

              var_pointeur = (byte *)&RESULT;
              var_pointeur[0] = read_eeprom( address );
              var_pointeur[1] = read_eeprom( address+1 ); */

    return((read_eeprom( address+1 )<<8)+read_eeprom( address));
}

/****************************************************************
 *       Function Name:  read_eeprom_long                         *
 *       Return Value:   unsigned long                                     *
 *       Parametres:     adresse de la variable                  *
 *       Description:    lecture 16 bits ds eeprom            *
 ****************************************************************/

unsigned long read_eeprom_long(byte address) {
    /*        byte * var_pointeur;
              word RESULT;

              var_pointeur = (byte *)&RESULT;
              var_pointeur[0] = read_eeprom( address );
              var_pointeur[1] = read_eeprom( address+1 ); */

    return((read_eeprom( address+3 )<<24)+(read_eeprom( address+2)<<16)+
            (read_eeprom( address+1 )<<8)+read_eeprom( address));
}

void sync_from_eeprom(byte address){
    //
    // Apres une ecriture en eeprom, certaines variables 
    // doivent etre synchronisees avec l'EEPROM 
    //    on pourrait meme encapsuler toutes les lectures eeprom ici
    //
    switch (address){
        case E_VM_GUIDE:   
            Delay_M=read_eeprom_long(E_VM_GUIDE);
            // Delay_M_Temp=Delay_M;
            break;

        case E_VM_COREC_PLUS: 
            Delay_M=read_eeprom_long(E_VM_COREC_PLUS);
            // Delay_M_Temp=Delay_M;
            break;

        case E_VM_COREC_MINUS: 
            Delay_M=read_eeprom_long(E_VM_COREC_MINUS);
            // Delay_M_Temp=Delay_M;
            break;

        case E_VM_SLEW_SLOW:       
            Delay_Target=read_eeprom_long(E_VM_SLEW_SLOW);
            break;

        case E_VM_SLEW_FAST:    
            Delay_Target=read_eeprom_long(E_VM_SLEW_FAST);
            break;

        case E_PARK_MODE:
            Park=read_eeprom(E_PARK_MODE);
            break;

        case E_DIR_GUIDE:
            Dir_M=read_eeprom(E_DIR_GUIDE);
            break;

        case E_VM_ACCE:
            Accel_M=read_eeprom(E_VM_ACCE);
            break;

        case E_RAQ_TYPE_CAN:
            Raq_Type_Can=read_eeprom(E_RAQ_TYPE_CAN);
            break;

        case E_MICROSTEPS:
            R_Microsteps=read_eeprom(E_MICROSTEPS); 
            break;

        case E_STEPS:
            R_Steps=read_eeprom(E_STEPS);
            break;

        case E_REDUCER_RATIO:
            R_Reducer_Ratio=read_eeprom(E_REDUCER_RATIO);
            break;

        case E_AXIS_RATIO:
            R_Axis_Ratio=read_eeprom(E_AXIS_RATIO);
            break;

        case E_PEC_ENABLED:
            Pec_Activated=read_eeprom(E_PEC_ENABLED);
            break;
    }
}


/****************************************************************
 *       Function Name:  step_moteur                             *
 *       Return Value:   void                                    *
 *       Parametres:     Numero selon la table des pas et moteur *
 *       Description:    Generation des pas d'un moteur          *
 ****************************************************************/

void step_moteur(void) {
    digitalWrite(Out_HorlogeMoteur, LOW);
    digitalWrite(Out_HorlogeMoteur, LOW);
    //
    // Manage scope position/german mount at lowest level to ease
    // task for upper level software
    // If Dir_M positive or we are on DEC and EOM 
    //
    //
    // We are on AD board and then Dir_M is allways canonically ok
    // or we are on DEC and in canonical (arbitrary) EOM and Dir_M is ok too.
    //    (requires checking at assembly time that Dir_M positive corresponds 
    //    to DEC motor wiring.
    // if Dir_M is positive and etiher we are on AD board or the scope is in the
    // canonical position.
#ifdef AD
    // on AD, Dir_M is ok.
    if(Dir_M) 
#else
    // on DEC, Dir_M is ok if mount is not flipped, otherwise it is reversed
    if(Dir_M && Flip != WOM ) 
#endif
    {
        digitalWrite(Out_DIR,HIGH);
        Num_Posi++;
        Pec_Step++;
        if(Pec_Step >= Microsteps_Per_Rev)
        {
            Pec_Step -= Microsteps_Per_Rev;
        }
    }
    else
    {
    //
    // 
    //
        digitalWrite(Out_DIR,LOW);
        Num_Posi--;
        if(Pec_Step < 1) {
            Pec_Step += Microsteps_Per_Rev - 1;
        }
        Pec_Step--;
    }
    digitalWrite(Out_HorlogeMoteur, LOW);
    digitalWrite(Out_HorlogeMoteur, LOW);
    digitalWrite(Out_HorlogeMoteur, LOW);
    digitalWrite(Out_HorlogeMoteur, HIGH);
}

/****************************************************************
 *       Function Name:  MoveNumOfUSteps            *
 *       Return Value:   none                    *
 *       Parameters:     none                    *
 *       Description:    Move motor x steps in dir Out_DIR        *
 ****************************************************************/

void MoveNumOfUSteps(void) {
    word temp, temp2, m, y0, index;
    unsigned long i, delay_init, _delay;
    byte en_arret, decel, respond, achar;
    int c;  //signe!!
    /*    Correction en temps pour le pointage - utilise TMR23 comme reference    */

    Clavier = (lowByte(PORTD) & 0xC0 );   /* on ne teste que la raquette */

    if (Delay_M>0) /* if guiding speed is not 0, then 
                           sidereal drift must be taken into account */
        Act_Corr_pointage = 1;
    else
        Act_Corr_pointage = 0;

    mT23ClearIntFlag(); // Clear interrupt flag

    delay_init=read_eeprom_long(E_VM_COREC_PLUS);

    if(delay_init == 0){        // if COR_PLUS delay read from eeprom is 0  */ 
        delay_init=2560000;     // then sets delay_init to 2560000 */
                                // that value is barely used since it will be overwritten
                                // in the first step of slewing 
    }

    _delay=delay_init;

    /*                   */
    /*                   */
    /* acceleration ramp */
    /*                   */
    /*                   */
    y0=60000;

    mT23IntEnable(1);  // int23 possible, to account for sidereal drift during correction */
                    
    PR4=(delay_init & 0x0000FFFF);  // adjusts timer45 delay for initial speed
    PR5=(delay_init & 0xFFFF0000) >> 16;

    i=0;            // i counts steps until half slewing. It then decreases back to 0.

    c=1;            // c is a scale step used to rate progression in the acceleration table
                    //
                    // it ponders the delay change between 2 consecutive values of the table 
                    // it is defined modulo 256, increases at Accel_M rate ; rollover  
                    // triggers index increase or decrease
                    //
    index=0;        // points to the position in the table where y0 will take its value
    Stop=0;         // indicates whether a command or a paddle action has requested a stop
    en_arret=0;     
    decel=0;        // flag to indicate deceleration or acceleration phase
    m=0;

    while(Num_Pointage>1){              // Boucle de positionnement
    //
    // This is the first phase of a slewing action :
    //      it takes care of managing acceleration ;
    //      delay is progressively lowered from the initial 
    //      value delay_init (read from eeprom address E_VM_COREC_PLUS)
    //      to the target delay (either E_VM_SLEW_SLOW or E_VM_SLEW_FAST)
    //
    //  The condition Num_Pointage>1 just makes sure a Stop has not been issued
    //      and has no numerical meaning
    //
        _delay=(y0-c*m) << 2;           // Delai2 evalue selon une droite par rapport      
                                        // A[i] = (i^0.5 - (i - 1)^0.5)A[0] 
        PR4=(_delay & 0x0000FFFF);      // Valeur du delai transferee dans le registre comparateur
        PR5=(_delay & 0xFFFF0000) >> 16;
        TMR4=0;                         // Remise a zero du TIMER45
        TMR5=0;                         // Remise a zero du TIMER45
        bitWrite(IFS0,20,0);            // Remise a zero du drapeau de comparaison
        mT45ClearIntFlag();             // Clear interrupt flag
        if(Stop) {                      // Stop is set by either paddle or rs232 command 0xF0
            Act_Corr_pointage=0;
            Num_Pointage=i-1;
            Num_Pointage++;
            Stop=0;
            en_arret=1;
        }

        Num_Pointage--;

        if (Pointage_Corr!=0) { // if T23 has generated corrections to account for sidereal drift
            mT23IntEnable(0);  // disable int23 
            Num_Pointage+=Pointage_Corr;    // Adjust Num_Pointage
            Pointage_Corr=0;                // and resets it
            mT23IntEnable(1);  // enable int23 (Timer23 counter has been initialized
                               // once and for all in setup() for sidereal delay
        }

        step_moteur();

        if (bitRead(U1STA,URXDA)) {         // checking rs232 for short commands only 
            if(Buf_Serial[0] == Num_Carte) {     
                Buf_Serial[1]=getcUART1();
                switch(Buf_Serial[1]) {
                    case 0xff:              // answers ping
                        putcUART1(PingReturn);
                        break;

                    case 0xf1:                                    
                    case 0xf2: 
                    case 0xf3:  // at 115200 we try and send the 4 bytes at once
                    case 0xf4:              
                        Temp_Num_Posi=Num_Posi;                    // freezes motor position 
                        putch((Temp_Num_Posi&0xFF000000)>>24); // sends MSB
                        putch((Temp_Num_Posi&0x00FF0000)>>16); // sends freezed MLSB 
                        putch((Temp_Num_Posi&0x0000FF00)>>8);  // sends freezed LMSB 
                        putch((Temp_Num_Posi&0x000000FF));     // sends freezed LSB 
                        break;
                                                                  
                    case 0xf0:                                     // stops slewing
                        Stop=1;
                        putcUART1(ACK);
                        break;
                }
                Buf_Serial[0] = '\0';
                Buf_Serial[1] = 0;
            }
            else {
                Buf_Serial[0]=getcUART1();
            }
        }

        if (bitRead(U1STA,URXDA)){    //OVERRUN du buffer entree de la RS232, on reinitialise
            bitClear(U1STA,URXDA);
        }

        if ((i<Num_Pointage) && (_delay > Delay_Target)) {  
            //
            // if still in first half of slewing and delay_target not reached ;
            // num_pointage is decreasing and i is increasing at the same rate; 
            // so the 2 variables cross at mid-slewing)
            //
            if(decel)                                       // if in deceleration mode
                Act_Corr_pointage = 0;

            i++;            // Conditions pour changer le delai de generation des pas
            c++;            // increment counter (? in case Accel_M is 0 ?)
            c += Accel_M;   // advance counter
            if(c>256)           // if its time to progress in table
            {
                index++;        // advance in table
                c=0;            // reinit counter 
                if(index < TABLELENGTH - 1) // if still in acceleration or deceleration 
                                            // (ie index has not yet reach TABLELENGTH)
                                            // m and y0 are updated according to table
                                            // and ramp. Once index exceeds TABLELENGTH
                                            // it goes on increasing without effect on the 
                                            // delay but ensures that deceleration will
                                            // happen symmetrically and in due time
                {
                    m=TABLE_ACC[index]-TABLE_ACC[index+1];// Delai evalue selon une droite par rapport
                    m >>= 8;                              // a la courbe d'acceleration - approximation de                   
                    y0=TABLE_ACC[index];                  // A[i] = (i^0.5 - (i - 1)^0.5)A[0]
                }
            }
        }
        else    // if in second half of the slewing or target_delay reached
            if(i>Num_Pointage) { // if in second half of the slewing
                                 // Then we start decelerating. 
                                 // 
                                 // From now on, i > Num_Pointage and both variables decrease 
                                 // at the same rate (does this wiggle a bit because of sidereal compensation (?).
                                 // (it has probably no consequences)
                                 //
                                 // index might have increase far above TABLELENGTH, but new value
                                 // for y0 and hence _delay will be updated only when index becomes
                                 // relevant regarding table
                decel=1;         // Start decel phase. 
                i--;
                c--;
                c -= Accel_M;
                if(c<1) {
                    index--;
                    c=256;
                    if(index < TABLELENGTH - 1) { // deceleration is virtual as long as index points
                                                  // beyond acceleration table upper range
                        m=TABLE_ACC[index]-TABLE_ACC[index+1];// Delai evalue selon une droite par rapport
                        m >>= 8;                              // a la courbe d'acceleration - approximation de                   
                        y0=TABLE_ACC[index];                  // A[i] = (i^0.5 - (i - 1)^0.5)A[0]
                    }
                }
            }

        while(!bitRead(IFS0,20)){                  // Waiting the end of timer 45
                                                   // En attente de la fin du delai     
            if (Clavier!=(lowByte(PORTD) & 0xC0)){ // on ne test que la raquette  
                Stop=1;                            // Ordre d'arreter le mouvement
            }
            else {
                if ((Clavier!=0xC0) && (en_arret)) {
                    en_arret=0;
                    decel=0;
                    Num_Pointage=0xFF000000;
                }
            }  
        } 
    } 

    // fin de while(Num_Pointage>1)
    // end of slewing phase

    mT23IntEnable(0);  // int23 impossible, stops guiding speed compensation
    Act_Corr_pointage=0;
    PR4=(delay_init & 0x0000FFFF);    // Valeur du delai transferee dans le registre comparateur
    PR5=(delay_init & 0xFFFF0000) >> 16;

    //
    // In case some steps are left (?)
    //
    if((Num_Pointage>0) && (!en_arret)) {
        while(Num_Pointage>0) {
            Num_Pointage--;
            step_moteur();
            TMR4=0;                     // Remise a zero du TIMER3
            TMR5=0;                     // Remise a zero du TIMER3
            mT45ClearIntFlag();         // Clear interrupt flag
            bitWrite(IFS0,20,0);        // Remise a zero du drapeau de comparaison
            while(!bitRead(IFS0,20));   // En attente de la fin du delai
        }        
    }
    Num_Pointage = 0;
}

/****************************************************************
 *       Function Name:  Raquette                                *
 *       Return Value:   void                                    *
 *       Parametres:                                             *
 *       Description:     Commande de la raquette           *
 ****************************************************************/

void Raquette(void) {
    byte new_command;
    if (digitalRead(In_ST4_Plus)==ZERO)         //ST4+     // Priorite ST4  //
    {
        new_command = SPD_COR_PLUS;
    }
    else if (digitalRead(In_ST4_Moins)==ZERO)     //ST4-
    {
        new_command = SPD_COR_MINUS;
    }
    else if (digitalRead(In_RAQ_Plus)==ZERO)     
    {
        new_command = SPD_COR_PLUS;   // Guidage +
        if (!Raq_Type_Can)
        {
            if(digitalRead(In_VIT1)==ZERO)
            {
                if(digitalRead(In_VIT2)==UN)
                    new_command = SPD_SLOW_PLUS;  // lent +
                else
                    new_command = SPD_FAST_PLUS;  // Rapide +
            }
        }
        else
        {
            if (digitalRead(In_VIT1)==ZERO)
                new_command = SPD_SLOW_PLUS;   // lent +
            else     
                if (digitalRead(In_VIT2)==UN)
                    new_command = SPD_FAST_PLUS;  // Rapide +
        }               
    }
    else if (digitalRead(In_RAQ_Moins)==ZERO) //A- ATTENTION a corriger apres car sur MCMT logique invers?e
    {
        new_command = SPD_COR_MINUS;
        if (!Raq_Type_Can)
        {
            if(digitalRead(In_VIT1)==ZERO)
            {
                if(digitalRead(In_VIT2)==UN)
                    new_command = SPD_SLOW_MINUS;  // lent -
                else
                    new_command = SPD_FAST_MINUS;  // Rapide -
            }
        }
        else
        {
            if (digitalRead(In_VIT1)==ZERO)
                new_command = SPD_SLOW_MINUS;   // lent -
            else     
                if (digitalRead(In_VIT2)==UN)
                    new_command = SPD_FAST_MINUS;  // Rapide -
        }               
    }
    else   // Etat normal
        new_command = SPD_SIDEREAL; //S

    if(new_command != Mode_Vitesse)
    {
        Mode_Vitesse = new_command;
        Change_Mode = 1;
    }
}


/****************************************************************
 *       Function Name:  traite_serial                           *
 *       Return Value:   void                                    *
 *       Parametres:                                             *
 *       Description:    Traitement des commandes du port serie  *
 ****************************************************************/

void traite_serial(void) {
    byte achar,respond,address;
    unsigned long data;
    respond = ACK;

    // Attention modification de la structure des valeurs selon que le bit de poids fort
    // de chacun des octets Buf_Serial[2] a  Buf_Serial[6] se retrouve dans l'octet
    // Buf_Serial[7] de facon a ce que chaque octet ne puisse depasser en valeur 0x7F
    if (bitRead(Buf_Serial[7],0)) bitWrite(Buf_Serial[6],7,1);
    if (bitRead(Buf_Serial[7],1)) bitWrite(Buf_Serial[5],7,1);
    if (bitRead(Buf_Serial[7],2)) bitWrite(Buf_Serial[4],7,1);
    if (bitRead(Buf_Serial[7],3)) bitWrite(Buf_Serial[3],7,1);
    if (bitRead(Buf_Serial[7],4)) bitWrite(Buf_Serial[2],7,1);

    switch (Buf_Serial[1]) {
        case 0x44:  //D     Correction +
            putch(respond);
            Mode_Vitesse=SPD_COR_PLUS;
            Change_Mode=1;
            break;

        case 0x45:    //E     Enregistrer PEC
            putch(respond);
            Pec_Start=1;
            break;

        case 0x46:    //F      Lent -
            putch(respond);
            Mode_Vitesse=SPD_SLOW_MINUS;
            Change_Mode=1;
            break;

        case 0x47:  //G      Lent +
            putch(respond);
            Mode_Vitesse=SPD_SLOW_PLUS;
            Change_Mode=1;
            break;

        case 0x4A:  //J       Lecture octet dans EEPROM
            putch(respond);
            address=Buf_Serial[2];
            achar=read_eeprom(address);
            putch(achar);
            break;    

        case 0x4B:    //K       Lecture des param?tres en batch
            putch(respond);
            for(address=0; address<48; address++)
            {
                achar=read_eeprom(address);
                putch(achar);
            }
            break;    

        case 0x4C:    //L  Ecriture EEPROM
            putch(respond);
            address=E_VM_GUIDE+Buf_Serial[2];
            write_eeprom(address,Buf_Serial[3]);
            if(address==E_PAS_CODEUR){             // WARNING exception for the only
                                                // int parameter in EEPROM
                write_eeprom(address+1,Buf_Serial[4]);
            }
            switch (address) {
                case E_VM_ACCE:         
                    Accel_M=read_eeprom(E_VM_ACCE);
                    break;
                case E_DIR_GUIDE:    
                    Dir_M=read_eeprom(E_DIR_GUIDE);
                    break;
                case E_PEC_ENABLED:    
                    Pec_Activated=read_eeprom(E_PEC_ENABLED);
                    break;
                case E_RAQ_TYPE_CAN:    
                    Raq_Type_Can=read_eeprom(E_RAQ_TYPE_CAN);
                    break;
            }
            Change_Mode=1;
            break;

        case 0x4E:    //N     No Park Mode
            putch(respond);
            Park=0;
            write_eeprom(E_PARK_MODE,0);
            break;

        case  0x50:    //P     Park Mode
            putch(respond);
            Park=1;
            write_eeprom(E_PARK_MODE,1);
            break;

        case  0x51:    //Q     Correction -
            putch(respond);
            Mode_Vitesse=SPD_COR_MINUS;
            Change_Mode=1;
            break;

        case  0x52:    //R  Ecriture Valeur Delay_M et Dir_M
            putch(respond);
            if(Mode_Vitesse==SPD_SIDEREAL) {
                Delay_M=(Buf_Serial[2]<<24)+(Buf_Serial[3]<<16)+
                    (Buf_Serial[4]<<8)+Buf_Serial[5];
                //                 mT45IntEnable(0);  // int45 impossible
                LATFSET=0x0001;    //section critique avec operation atomique
                // Delay_M_Temp = Delay_M;
                Buf_Serial[6]=Buf_Serial[6] & 0x01;
                if (Buf_Serial[6]==1){
                    Dir_M=read_eeprom(E_DIR_GUIDE);
                }
                else{
                    Dir_M=!read_eeprom(E_DIR_GUIDE);
                }
                LATFCLR=0x0001;  //sorti de section critique
                //                  mT45IntEnable(1);  // int45 possible
            }
            break;

        case  0x53:    //S       Sid?rale - 25600 micro-pas
            putch(respond);
            Mode_Vitesse=SPD_SIDEREAL;
            Change_Mode=1;
            break;

        case  0x56:    //V  Lecture Version
            putch(respond);
            Serial.print(version);
            break;

        case  0x57:    //W    Rapide -
            putch(respond);
            Mode_Vitesse=SPD_FAST_MINUS;
            Change_Mode=1;
            break;

        case  0x58:    //X      Rapide +
            putch(respond);
            Mode_Vitesse=SPD_FAST_PLUS;
            Change_Mode=1;
            break;

        case  0x65:    //e   Effacer MEM PEC
            putch(respond);

            for(address=0; address<200; address++){
                write_eeprom_int(E_PEC_ENABLED+2+address,0);
            }
            break;        

        case  0x66:    //f   Switch Auxilaire 1     ???????????????
            putch(respond);
            /* if(AUX_1==0)
               AUX_1 = 1;
               else
               AUX_1 = 0;  */
            break;

        case  0x67:    //g   Switch Auxilaire 1    ?????????????????
            putch(respond);
            /* if(AUX_2==0)
               AUX_2=1;
               else
               AUX_2=0;  */
            break;

        case  0x6C:    //l  Ecriture EEPROM d'un longword
            putch(respond);
            address=E_VM_GUIDE+Buf_Serial[2];
            data=((((Buf_Serial[3] << 8) + Buf_Serial[4]) << 8) + 
                     Buf_Serial[5] << 8) + Buf_Serial[6];
            write_eeprom_long(address,data);
            break;

        case  0x72:    // 'r' : Lecture Valeur Delay_M, Pec_Base, Pec_Step, 
                       // Pec_Comp, PEC_STATE et Park
            putch(respond);
            Temp_Pec_Step=Pec_Step;//0x000AD08D
            putch(lowByte((Delay_M&0xFF000000)>>24));
            putch(lowByte((Delay_M&0x00FF0000)>>16));
            putch(lowByte((Delay_M&0x0000FF00)>>8));
            putch(lowByte((Delay_M&0x000000FF)));
            putch(Pec_Base);
            putch(highByte(Temp_Pec_Step));putch(lowByte(Temp_Pec_Step));
            putch(highByte(Pec_Comp));putch(lowByte(Pec_Comp));
            achar=0;
            if(Pec_Start){
                achar=2;
                }
            else{
                if(Pec_Write){
                    achar=3;
                }
                else{
                    if(Pec_Activated){
                        achar = 1;
                    }
                }
            }
            putch(achar);
            putch(Park);
            break;

        case  0x70:  //p   POSITIONNEMENT
            putch(respond);
            Num_Pointage=((Buf_Serial[2]&0x7F)<<24)+(Buf_Serial[3]<<16)+ (Buf_Serial[4]<<8)+Buf_Serial[5];
            if((Buf_Serial[2] & 0x80) == 0x80){
                Mode_Vitesse=SPD_FAST_MINUS;
            }
            else{
                Mode_Vitesse=SPD_FAST_PLUS;
            }
            if(Num_Pointage!=0) {
                Change_Mode=1;
                Act_Corr_pointage=1;
            }
            break;

        case  0x78:  //x   pour le debug, lecture d'une valeur word
            putch(respond);
            putch(highByte(Debug));
            putch(lowByte(Debug));
            break;

        case  0x79:  //y   pour le debug, lecture d'une valeur long
            putch(respond);
            putch(lowByte((Debugl&0xFF000000)>>24));
            putch(lowByte((Debugl&0x00FF0000)>>16));
            putch(lowByte((Debugl&0x0000FF00)>>8));
            putch(lowByte((Debugl&0x000000FF)));
            break;

        default  :  //respond = NOACK;        // Commande non conforme
            //putch(respond);
            break;
    }
    Buf_Serial[0]='\0';
    Buf_Serial[1]=0;
    Command_Rec=0;
}

/****************************************************************
 *       Function Name:  Change_Mode_Vitesse                     *
 *       Return Value:   void                                    *
 *       Parametres:                                             *
 *       Description:    Changement de la vitesse        *
 ****************************************************************/

void Change_Mode_Vitesse(void) {
    Change_Mode = 0;

    Dir_M = read_eeprom(E_DIR_GUIDE);
    if (Dir_M) {
        digitalWrite(Out_DIR,HIGH);
        }
    else {
        digitalWrite(Out_DIR,LOW);
        }
    digitalWrite(Out_LED_A_Plus,LOW);
    digitalWrite(Out_LED_A_Moins,LOW);

    switch(Mode_Vitesse) {
        case SPD_SIDEREAL:  
            sync_from_eeprom(E_VM_GUIDE);                    // tracking
            PingReturn=0x20;// pointage << 7 + guidage << 6 +tracking << 5 + rapide << 1 + dir
            break;

        case SPD_COR_PLUS:  
            sync_from_eeprom(E_VM_COREC_PLUS);                 //guidage +
            digitalWrite(Out_LED_A_Plus,HIGH);
            mT45IntEnable(0);  // int45 impossible
            PR4=Delay_M & 0x0000FFFF;    
            TMR4=0;    
            PR5=(Delay_M & 0xFFFF0000) >> 16;    
            TMR5=0;    
            mT45IntEnable(1);  // int45 possible
            PingReturn=0x40;// pointage << 7 + guidage << 6 + rapide << 1 + dir
            break;

        case SPD_SLOW_PLUS:  
            sync_from_eeprom(E_VM_SLEW_SLOW);                //pointage lent +
            digitalWrite(Out_LED_A_Plus,HIGH);
            Pointage=1;
            PingReturn=0x80; // pointage << 7 + guidage << 6 +  rapide << 1 + dir
            break;

        case SPD_FAST_PLUS:  
            sync_from_eeprom(E_VM_SLEW_FAST);                //pointage rapide +
            digitalWrite(Out_LED_A_Plus,HIGH);
            Pointage=1;
            PingReturn=0x82;// pointage << 7 + guidage << 6 + rapide << 1 + dir
            break;    

        case SPD_COR_MINUS:  
            sync_from_eeprom(E_VM_COREC_MINUS);                //guidage-
            digitalWrite(Out_LED_A_Moins,HIGH);
            mT45IntEnable(0);  // int45 impossible
            PR4=Delay_M & 0x0000FFFF;    
            TMR4=0;    
            PR5=(Delay_M & 0xFFFF0000) >> 16;    
            TMR5=0;    
            mT45IntEnable(1);  // int45 possible
            PingReturn=0x41;// pointage << 7 + guidage << 6 rapide << 1 + dir
            break;

        case SPD_SLOW_MINUS:  
            sync_from_eeprom(E_VM_SLEW_SLOW);                //pointage lent -
            digitalWrite(Out_LED_A_Moins,HIGH);
            Pointage=1;
            PingReturn=0x81; // pointage << 7 + guidage << 6 +  rapide << 1 + dir
            break;

        case SPD_FAST_MINUS:  
            sync_from_eeprom(E_VM_SLEW_FAST);                //pointage rapide -
            digitalWrite(Out_LED_A_Moins,HIGH);
            Pointage=1;
            PingReturn=0x83; // pointage << 7 + guidage << 6 +  rapide << 1 + dir
            break;

        default:  
            break;
    }

    if(Mode_Vitesse >= SPD_COR_MINUS){        // si sens - (if backwards)            
        Accu_Add=0; // Accu_Add is used in T23 (which compensates sidereal steps and flashes led)
        if(Dec_Mode || (Mode_Vitesse > SPD_COR_MINUS)) {
            /*  
            adjusts Dir signal on the stepper driver according to 
            the direction of motion for MINUS speedmodes 
                (except for SPD_COR_MINUS on RA, where SPD_COR_MINUS is supposed to be
                 the same DIR as SPD_SIDEREAL and lower than SPD_SIDEREAL) */
            if(Dir_M) {
                Dir_M=0;
                digitalWrite(Out_DIR,LOW);
            }
            else {
                Dir_M=1;
                digitalWrite(Out_DIR,HIGH);
            }
        }
    }
    else {         // si sens + 
        Accu_Add=1;  // Accu_Add is used in T23 (which compensates sidereal steps and flashes led)
    }

    if(Pointage) {
        Guide=0;
        if(Num_Pointage==0){
            Num_Pointage = 0xFF000000;
        }
    }
    else {
        Guide = 1;
        }

    if(Park && Mode_Vitesse > 0) {
        Park=0;
        write_eeprom(E_PARK_MODE,0);
    }
    // when entering slewing (Pointage=1), build a byte 
    // containing direction, speed, to be returned to ping
}


/****************************************************************
 *       Function Name:  serial_receive                          *
 *       Return Value:   void                                    *
 *       Parametres:                                             *
 *       Description:    Reception du port serie                 *
 ****************************************************************/

void serial_receive(void) {    
    byte compteur,cheksum,temp;
    long timeout_int;

    if(Buf_Serial[0]==Num_Carte) {
        Buf_Serial[1]=getcUART1();
        switch(Buf_Serial[1]) {
            case  0xff:  
                putch(PingReturn);    // MCMT ready?
                break;

            case  0xf1:  
            case  0xf2:  
            case  0xf3:  
            case  0xf4:  
                Temp_Num_Posi=Num_Posi; 
                putch((Temp_Num_Posi&0xFF000000)>>24);
                putch((Temp_Num_Posi&0x00FF0000)>>16);
                putch((Temp_Num_Posi&0x0000FF00)>>8);
                putch((Temp_Num_Posi&0x000000FF));
                break;

            default:  
                if(!Command_Rec) {
                    compteur=7;      // 7
                    Buf_Count=1;
                    while(compteur>0)
                    {
                        compteur--;
                        Command_Rec=0;
                        timeout_int=655360;
                        while ((timeout_int!=0) && (!Command_Rec)) 
                        //only check the msb of the int for being 0, it saves space, see always.h for macro
                        {
                            //CLRWDT();
                            timeout_int--;
                            if bitRead(U1STA,0)
                            {
                                Buf_Serial[++Buf_Count]=getcUART1();
                                Command_Rec=1;
                            }
                        }
                        if(!Command_Rec)
                        {
                            compteur=0;
                        }
                    }
                    if(Command_Rec)
                    {
                        cheksum=0;
                        for(compteur=0; compteur<8; compteur++)      //8
                            cheksum += Buf_Serial[compteur];

                        bitClear(cheksum,7);    //cheksum &= 0x7F;    // 128 bits
                        if(cheksum!=Buf_Serial[8])     //8
                            Command_Rec=0;
                    }
                }
                break;    
        }
        if(!Command_Rec) {    
            Buf_Serial[0]='\0';
            Buf_Serial[1]=0;
        }
    }
    else {
        Buf_Serial[0]=getcUART1();
    }
    if (bitRead(U1STA,URXDA)){    //OVERRUN du buffer entr?e de la RS232, on r?initialise
        bitClear(U1STA,URXDA);
    }
    //clear_usart_errors_inline;
}

void init_eeprom(void) { // Configuration 32 usteps / 100 steps / 12 red / 200 gear 
#ifdef AD
    write_eeprom_long(E_VM_GUIDE,0x000db206);         // sidereal speed
    write_eeprom_long(E_VM_COREC_PLUS,0x00223d10);    // .4 * sid
    write_eeprom_long(E_VM_COREC_MINUS,0x00088f44);   // 1.6 * sid
    write_eeprom_long(E_VM_SLEW_SLOW,0x00015e9a);     // 10 * sid
    write_eeprom_long(E_VM_SLEW_FAST,0x0000703);      // 500 * sid  
    write_eeprom(E_VM_ACCE,0x5);
    write_eeprom(E_DIR_GUIDE,0x01);
    write_eeprom_int(E_PAS_CODEUR,0x7C01);
    write_eeprom(E_FLIP,EOM);
    write_eeprom(E_PARK_MODE,0x00);
    write_eeprom(E_RAQ_TYPE_CAN,0x00);
    write_eeprom(E_MICROSTEPS,0x20);
    write_eeprom(E_STEPS,0x64);
    write_eeprom(E_REDUCER_RATIO,0x0c);
    write_eeprom(E_AXIS_RATIO,0xc8);
    write_eeprom(E_PEC_ENABLED,0x00);
#else
    write_eeprom_long(E_VM_GUIDE,0x0000);            //   0
    write_eeprom_long(E_VM_COREC_PLUS,0x00016d360);  //  .6 * sid
    write_eeprom_long(E_VM_COREC_MINUS,0x00016d360); //  .6 * sid
    write_eeprom_long(E_VM_SLEW_SLOW,0x00015e9a);    //  10 * sid
    write_eeprom_long(E_VM_SLEW_FAST,0x0000703);     // 500 * sid  
    write_eeprom(E_VM_ACCE,0x5);
    write_eeprom(E_DIR_GUIDE,0x01);
    write_eeprom_int(E_PAS_CODEUR,0x7C01);
    write_eeprom(E_FLIP,EOM);
    write_eeprom(E_PARK_MODE,0x00);
    write_eeprom(E_RAQ_TYPE_CAN,0x00);
    write_eeprom(E_MICROSTEPS,0x20);
    write_eeprom(E_STEPS,0x64);
    write_eeprom(E_REDUCER_RATIO,0x0c);
    write_eeprom(E_AXIS_RATIO,0xc8);
    write_eeprom(E_PEC_ENABLED,0x00);
#endif
}

void __attribute__((interrupt)) T23() {
    //
    // Timer T23 interrupt handler
    // is used only during slewing to
    //
    // 1. flash leds
    // 2. compensate for sidereal speed during the acceleration phase
    //

    mT23ClearIntFlag(); // Clear interrupt flag
    if(++Compt_Flash == Flash_Init) {    /* If counter reaches steps/second (?) 
                                            change led state */
        Compt_Flash = 0;
        if(!Accu_Add)       /* If negative dir (one of SPD_*_MINUS is active) */
        {    
            if(digitalRead(Out_LED_A_Moins)==1)
                digitalWrite(Out_LED_A_Moins,LOW);
            else
                digitalWrite(Out_LED_A_Moins,HIGH);
        }
        else
        {
            if(digitalRead(Out_LED_A_Plus)==1)
                digitalWrite(Out_LED_A_Plus,LOW);
            else
                digitalWrite(Out_LED_A_Plus,HIGH);
        }
    }

    if (Act_Corr_pointage) { // Takes into account sid drift while slewing 
        if(!Accu_Add) {      /* If slewing is in anti-sidereal dir (one of SPD_*_MINUS) */
            if(Num_Pointage > 1){
                Pointage_Corr--;
            }
            //        Num_Pointage --;
        }
        else {
            Pointage_Corr++; /* If slewing is in sidereal dir (one of SPD_*_PLUS) */
            //      Num_Pointage ++;
            //      Compt_Correc++;        
        }
    }  
}

void __attribute__((interrupt)) T45() {
    word temp;

    mT45ClearIntFlag(); // Clear interrupt flag

    PR4=Delay_M & 0x0000FFFF;    
    PR5=(Delay_M & 0xFFFF0000) >> 16;    

    step_moteur();
    OKstep = 1;
}

void setup() {
    //  init_eeprom();
    pinMode(Out_LED_A_Moins,OUTPUT);
    pinMode(Out_LED_A_Plus,OUTPUT);
    pinMode(Out_LED_T1,OUTPUT);
    pinMode(Out_LED_T2,OUTPUT);

    TRISFCLR=0x0001;  //port F bit 0 en output
    LATFCLR=0x0001;    // mise a 0
    //  pinMode(LED1,OUTPUT);
    digitalWrite(Out_LED_A_Plus,LOW);
    digitalWrite(Out_LED_A_Moins,LOW);
    digitalWrite(Out_LED_T1,LOW);
    digitalWrite(Out_LED_T2,LOW);

    pinMode(Out_Control_TX,OUTPUT);
    pinMode(Out_HorlogeMoteur,OUTPUT);
    pinMode(Out_DIR,OUTPUT);

    pinMode(In_PEC1,INPUT);
    pinMode(In_RAQ_Moins,INPUT_PULLUP);
    pinMode(In_RAQ_Plus,INPUT_PULLUP);
    pinMode(In_ST4_Moins,INPUT);
    pinMode(In_ST4_Plus,INPUT);
    pinMode(In_VIT1,INPUT);
    pinMode(In_VIT2,INPUT);
    // pinMode(In_PB3,INPUT);
    /*        Initialise la COMM a 19200-8-N-1    */

    Serial.begin(115200);
    mU1IntDisable();    // on bloque l'interruption UART1-->port serie
    
    Delay_Target = 40;
    Park = read_eeprom(E_PARK_MODE);        // V?rifie le mode Park

    R_Microsteps=read_eeprom(E_MICROSTEPS); 
    if (R_Microsteps==0)        /* R_Microsteps : defaults to 128 */
        R_Microsteps=128;

    R_Steps=read_eeprom(E_STEPS);
    if (R_Steps==0)             /* R_Steps : defaults to 200 */
        R_Steps=200;

    R_Reducer_Ratio=read_eeprom(E_REDUCER_RATIO);
    if (R_Reducer_Ratio==0)           /* R_Reducer : defaults to 1 */
        R_Reducer_Ratio=1;

    R_Axis_Ratio=read_eeprom(E_AXIS_RATIO);
    if(R_Axis_Ratio==0)     /* R_Axis_Ratio (~= number of teeth) defaults to 200 */
        R_Axis_Ratio=200;
    
    //
    // Microsteps_per_rev stands for the number of microsteps involved
    // in a full revolution of the worm gear, so accounting for
    // microsteps/step, step/rev and reducer ratio.
    // It defaults to 128*200*1 if those 3 parameters are 
    // set to 0 in the EEPROM (libraries and upper level software
    // must take care of that during the initial setup of the system
    // 
    Microsteps_Per_Rev=R_Microsteps*R_Steps*R_Reducer_Ratio;

    //
    // When tuning : Press RA+ and RA- simultaneously on the paddle as
    // a way to reset E_VM_GUIDE to a manageable value in case of problems during tuning
    // when a careless value of E_VM_GUIDE has been set in EEPROM on RA,
    // putting the microcontroller in an allways busy state. 
    //
    if ((digitalRead(In_RAQ_Plus)==ZERO)  && (digitalRead(In_RAQ_Moins)==ZERO)){
        write_eeprom_long(E_VM_GUIDE,0x00000000);
        }

    Delay_M = read_eeprom_long(E_VM_GUIDE);
    //    Delay_M_Temp = Delay_M;
    Dir_M = read_eeprom(E_DIR_GUIDE);
    // Old_Dir_M = Dir_M;

    Accel_M = read_eeprom(E_VM_ACCE);
    Pec_Activated = read_eeprom(E_PEC_ENABLED);
    Raq_Type_Can = read_eeprom(E_RAQ_TYPE_CAN);
    Flip = read_eeprom(E_FLIP);
    Mode_Vitesse = SPD_SIDEREAL;

    digitalWrite(Out_LED_A_Plus,HIGH);
    digitalWrite(Out_LED_A_Moins,HIGH);
    /*    Initialise les variables relatifs a la comm */

    digitalWrite(Out_Control_TX,LOW);
    Buf_Serial[0] = '\0';
    Buf_Serial[1] = 0;
    Buf_Count = 0;
    Command_Rec = 0;

    /*    Initialise les variables relatifs au positionnement */

    Num_Posi = AXIS_POSITION_INIT;

    if (digitalRead(In_PEC1)==1)
        Pos_Pec1 = 1;
    else
        Pos_Pec1 = 0;

    Init_PEC = 1;
    Pointage = 0;
    Guide = 1;

    step_moteur();

    /*    Initialise l'adresse de la carte */

    Num_Carte = Carte;  //PORTE & 0x07;
    Num_Carte += 0xE0;
    if (Num_Carte > 0xE0) {
        Dec_Mode = 1;
    }
    if (Num_Carte == 0xE2) {
        Focus_Mode = 1;
    }
    delay( 500 );
    //
    /*    Initialise le TIMER 45 - PRESCALER CLOCK/1 et configure CCPs    */
    OpenTimer45( T45_ON | T45_PS_1_1 | T45_SOURCE_INT, 0x0001);
    ConfigIntTimer45((T45_INT_OFF | T45_INT_PRIOR_2));
    PR4=Delay_M & 0x0000FFFF;
    PR5=(Delay_M & 0xFFFF0000) >> 16;
    setIntVector(_TIMER_45_VECTOR,T45);
    setIntPriority(_TIMER_45_VECTOR,2,1);
    mT45IntEnable(1);

    /*    Initialise le TIMER 23 - PRESCALER CLOCK/1 et configure CCPs    */
    OpenTimer23( T23_ON | T23_PS_1_1 | T23_SOURCE_INT, 0x0100);
    ConfigIntTimer23((T23_INT_OFF | T23_INT_PRIOR_3));
    setIntVector(_TIMER_23_VECTOR,T23);
    setIntPriority(_TIMER_23_VECTOR,3,1);

    if (Delay_M!=0) {                // Timer23 is used to flash led and compensate 
                                     // sidereal steps during slewing
        PR2=(Delay_M & 0x0000FFFF);  // Valeur du delai transferee dans le registre comparateur
        PR3=(Delay_M & 0xFFFF0000) >> 16;
        Flash_Init=80000000/Delay_M;  // Is this a number of steps per second (?)
    }
    else {          // if guiding speed 0, init to a default value
        PR2=0x3500;
        PR3=0x000C;
        Flash_Init=50;
    }

    digitalWrite(Out_LED_A_Plus,LOW);
    digitalWrite(Out_LED_A_Moins,LOW);

    Clavier = (lowByte(PORTD) & 0xC0 )+(lowByte(PORTE) & 0x03);   /* ST4 et raquette */
    PingReturn=0x20;// pointage << 7 + guidage << 6 +tracking << 5 + rapide << 1 + dir
}

void loop() {
    //
    //
    //  Catching commands from paddle or ST4 port
    // 
    //
    //
    if(Clavier!=((lowByte(PORTD) & 0xC0 )+(lowByte(PORTE) & 0x03))){
        // Verifie si ST4 ou Raquette modifie.
        Temp = (lowByte(PORTD) & 0xC0 )+(lowByte(PORTE) & 0x03);
        delay(10);
        //
        // Reject key rebound
        //
        if(Temp == (lowByte(PORTD) & 0xC0 )+(lowByte(PORTE) & 0x03)) // Test si rebond.
        { 
            Clavier=Temp;
            //
            // process command
            //
            Raquette();
        }
    }

    //
    //  Catching serial input
    //
    if bitRead(U1STA,0){
        serial_receive();    // Traite octet recu
    }

    //
    //  processing serial input when needed
    //
    if(Command_Rec){                // Traite commande
        traite_serial();
    }

    //
    //  processing speed change requests
    //
    if(Change_Mode){                   // Ordre de changement de vitesse.
        Change_Mode_Vitesse();
    }

    if(Pointage){                   // Ordre de pointage.
        mT45IntEnable(0);           // Stops tracking
        mU1RXClearIntFlag();
        Pointage_Corr=0;
        //    mU1RXIntEnable(1);    // on autorise l'interruption UART1-->port s?rie
        MoveNumOfUSteps();
        //    mU1IntDisable();    // on bloque l'interruption UART1-->port s?rie
        Mode_Vitesse = 0;
        Change_Mode=1;
        Guide=1;
        Dir_M=read_eeprom(E_DIR_GUIDE);
        Pointage=0;        
    }

    if((Guide) && (!Park) && (Delay_M != 0)){                    // Mode suivi
        mT45IntEnable(1);           // re-enable tracking 
        if(OKstep) {
            OKstep = 0;
        }
    }
    else{
        mT45IntEnable(0);
    }
}
// 0x41: //A    Passage en controle azimutal
// 0x44: //D     Correction +
// 0x45: //E     Enregistrer PEC
// 0x46: //F      Lent -
// 0x47: //G      Lent +
// 0x4A: //J       Lecture octet dans EEPROM
// 0x4B: //K       Lecture des param?tres en batch
// 0x4C: //L  Ecriture EEPROM
// 0x4E: //N     No Park Mode
// 0x50: //P     Park Mode
// 0x51: //Q     Correction -
// 0x52: //R  Ecriture Valeur Delay_M et Dir_M
// 0x53: //S       Sid?rale - 25600 micro-pas
// 0x56: //V  Lecture Version
// 0x57: //W    Rapide -
// 0x58: //X      Rapide +
// 0x61: //a    Passage en controle equatorial
// 0x65: //e   Effacer MEM PEC
// 0x66: //f   Switch Auxilaire 1     ???????????????
// 0x67: //g   Switch Auxilaire 1    ?????????????????
// 0x6C: //l  Ecriture EEPROM d'un longword
// 0x72: //r  Lecture Valeur Delay_M, Delay_M_LSB, Pec_Base, Pec_Step, Pec_Comp, PEC_STATE et Park
// 0x70: //p   POSITIONNEMENT
// 0x78: //x   pour le debug, lecture d'une valeur word
// 0x79: //y   pour le debug, lecture d'une valeur long
