/* This file is part of libmcmtii
 * Licence: GPLv3
 * Project page: http://free-astro.vinvin.tf/index.php/MCMTII
 */

#ifndef MCMTII_H
#define MCMTII_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/******************* TELESCOPE CONFIGURATION (MANDATORY) *******************/

/* number of teeth on the right-ascension (sidereal rotation) gear */
#define NB_TEETH_RA	720
/* number of teeth on the declination gear */
#define NB_TEETH_DEC	370
/* number of micro-steps for one motor revolution, which is the number of steps
 * of the motor (normally 200 if regular motor are used) times the number of
 * micro-steps used by MCMTII (128). 200 * 128 = 25600 */
#define NB_MICROSTEP	25600

/* Latitude and longitude of the telescope, in degrees */
#define LATITUDE	43.619604	/* positive is north */
#define LONGITUDE	7.039153	/* positive is east */
/* = 43�37'11", 7�02'21" */

/***************************************************************************/

/* TODO
 * FLIP:
 *	set _flip_status where it should
 *	how to display the current position while flipping?
 *		= when to do the +pi on HA/RA and DEC values?
 *	
 *
 * set longitude (used in HA) and latitude (not used yet) from external source
 * backlash (DEC only when guiding, all when slewing)
 *
 * the cable problem on many turns, avoid flipping by the bad direction too
 *
 * what's the behavior of requesting slewing while already slewing?
 *
 * update park mode - from 'r' command (done: 'K') or better, 'J'
 *
 * goto_relative(angle, angle)
 *
 *
 * TOTEST
 * values of park mode and PEC enabled from EEPROM read where not correctly
 * displayed in first test
 *
 */

#define MCMTII_WRITE_TIMEOUT_SEC	1	/* seconds */
#define MCMTII_WRITE_TIMEOUT_USEC	500000	/* microseconds */
#define MCMTII_READ_TIMEOUT_SEC		2	/* seconds */
#define MCMTII_READ_TIMEOUT_USEC	0	/* microseconds */

#define MCMTII_RET_OK		0
#define MCMTII_RET_ERR		-1
#define MCMTII_RET_ARGERR	-2
#define MCMTII_RET_TIMEOUT	-3
#define MCMTII_RET_UNINIT	-4
#define MCMTII_RET_INCOMPLETE	-5
char *mcmtii_strerror(int errno);

/* The two axis of the mount, MCMTII has a controller for each */
typedef enum {
	MCMTII_AXIS_RA	= 0xe0,
	MCMTII_AXIS_DEC	= 0xe1
} mcmtii_axis;

struct mcmtii_eeprom;		/* too large, defined at the end of the file */


/* serial link management */

int mcmtii_serial_link_open(const char *port_device);
void mcmtii_serial_link_close(int serial_link_fd);
int mcmtii_serial_link_list(char **device_name, int max_devices);


/* low-level commands */

int mcmtii_device_ready(int fd, mcmtii_axis axis);
int mcmtii_get_version(int fd, mcmtii_axis axis, char *version);

int mcmtii_read_encoder(int fd, mcmtii_axis axis, unsigned int *encoder_value);
int mcmtii_move_number_of_steps(int fd, mcmtii_axis axis, int number_of_steps);

int mcmtii_park_mode(int fd, mcmtii_axis axis, int enable);
int mcmtii_stop_slewing(int fd, mcmtii_axis axis);

int mcmtii_read_eeprom(int fd, mcmtii_axis axis, struct mcmtii_eeprom *eeprom);


/* high-level commands */

typedef enum {
	MCMTII_SPEED_GUID,	/* guiding (aka sidereal) speed */
	MCMTII_SPEED_MIN,	/* correction speed */
	MCMTII_SPEED_MED,	/* slow slewing speed */
	MCMTII_SPEED_MAX	/* fast slewing speed */
} mcmtii_speed;

typedef enum {
	MCMTII_NORTH,
	MCMTII_SOUTH,
	MCMTII_EAST,
	MCMTII_WEST
} mcmtii_direction;

struct mcmtii_angle {
	unsigned char degrees;
	unsigned char minutes;
	unsigned char seconds;
	unsigned char centiseconds;
};

int mcmtii_move(int fd, mcmtii_direction dir, mcmtii_speed speed);
int mcmtii_guide(int fd, mcmtii_direction dir, mcmtii_speed speed);
int mcmtii_stop_sidereal_speed(int fd);
void mcmtii_stop_all_slewing(int fd);

int mcmtii_set_current_position(int fd, double ra, double dec);
int mcmtii_get_current_ha_position(int fd, double *ha, double *dec);
int mcmtii_get_current_position(int fd, double *ra, double *dec);

int mcmtii_slew_absolute(int fd, double ra, double dec, int allow_flip);
//int mcmtii_slew_absolute2(int fd, double ra, double dec);	// flip allowed
//int mcmtii_slew_relative(int fd, double ra, double dec);

typedef enum {
	MCMTII_FLIP_UNKNOWN,
	MCMTII_FLIP_NONE, MCMTII_FLIP_FLIPPING,
	MCMTII_FLIP_FLIPPED, MCMTII_FLIP_UNFLIPPING
} mcmtii_flip_status;

mcmtii_flip_status mcmtii_get_flip_status();
void mcmtii_set_flip_status(mcmtii_flip_status st);
int mcmtii_guess_flip_status(int fd);

void mcmtii_get_latitude_longitude(double *lat, double *lon);

int mcmtii_is_parked(int fd);
void mcmtii_print_eeprom_configuration(FILE *output, struct mcmtii_eeprom *eeprom);


/* angle/encoder conversion */

void mcmtii_conversion_set_reference(mcmtii_axis axis, unsigned int steps, double angle);
void mcmtii_conversion_reset_reference();
int mcmtii_conversion_is_init();

int mcmtii_convert_is_over_meridian(double ha, double dec);

extern double ra_degree_per_microstep;
extern double dec_degree_per_microstep;

/* data structure */

struct mcmtii_eeprom {
	/* #define VM_Guidage              0	vitesse moyenne de guidage */
	unsigned short	VM_Guidage;
	/* #define VM_Guidage_LSB          2	*/
	unsigned char	VM_Guidage_LSB;
	/* #define VM_Corec_Plus           3	vitesse moyenne de correction + */
	unsigned short	VM_Corect_Plus;
	/* #define VM_Corec_Moins          5	vitesse moyenne de correction - */
	unsigned short	VM_Corect_Moins;
	/* #define VM_Lent                 7	vitesse moyenne de pointage lent */
	unsigned short	VM_Corect_Lent;
	/* #define VM_Rapide               9	vitesse moyenne de pointage rapide */
	unsigned short	VM_Corect_Rapide;
	/* #define VM_Acce                 11	? */
	unsigned char	VM_Corect_Acce;
	/* #define Dir_Guidage             12	*/
	unsigned char	Dir_Guidage;
	/* #define Dir_Corec_Plus          13	*/
	unsigned char	Dir_Corec_Plus;
	/* #define Dir_Corec_Moins         14	*/
	unsigned char	Dir_Corec_Moins;
	/* #define Dir_Lent_Plus           15	*/
	unsigned char	Dir_Lent_Plus;
	/* #define Dir_Lent_Moins          16	*/
	unsigned char	Dir_Lent_Moins;
	/* #define Dir_Rapide_Plus         17	*/
	unsigned char	Dir_Rapide_Plus;
	/* #define Dir_Rapide_Moins        18	*/
	unsigned char	Dir_Rapide_Moins;
	/* #define Resol_Guidage           19	*/
	unsigned char	Resol_Guidage;
	/* #define Resol_Corec_Plus        20	*/
	unsigned char	Resol_Corec_Plus;
	/* #define Resol_Corec_Moins       21	*/
	unsigned char	Resol_Corec_Moins;
	/* #define Resol_Lent              22	*/
	unsigned char	Resol_Lent;
	/* #define Resol_Rapide            23	*/
	unsigned char	Resol_Rapide;
	/* #define Courant_Guidage         24	courant envoye aux moteurs pour le guidage */
	unsigned char	Courant_Guidage;
	/* #define Courant_Lent            25	courant envoye aux moteurs pour le pointage lent */
	unsigned char	Courant_Lent;
	/* #define Courant_Rapide          26	courant envoye aux moteurs pour le pointage rapide */
	unsigned char	Courant_Rapide;
	/* #define Sens_Raq_Led            27	NEVER USED */
	unsigned char	Sens_Raq_Led;
	/* #define PasCodeur               28	*/
	unsigned short	PasCodeur;
	/* #define ParkMode                30	premier bit: park mode activated */
	unsigned char	ParkMode;
	/* #define Raq_type_can_address    31	NEVER USED */
	unsigned char	Raq_type_can_address;
	unsigned char	padding[14];
	/* #define PEC_ENABLED             47	*/
	unsigned char	PEC_ENABLED;
};

#ifdef __cplusplus
}
#endif

#endif

