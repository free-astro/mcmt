#ifndef _MCMTII_LOW_LEVEL_H
#define _MCMTII_LOW_LEVEL_H

#include "mcmtii.h"	/* for mcmtii types */

#define MCMTII_ACK	0x06
#define MCMTII_NOACK	0x15

/* internal functions for the MCMTII library */

unsigned char _axis_to_byte(mcmtii_axis axis);

int _request_response_8byte_command(int fd, unsigned char *request,
		unsigned char *response, int expected_response_size);

int _request_response(int fd, unsigned char *request, int request_size,
		unsigned char *response, int expected_reponse_size);

int _is_ack_retval(unsigned char byte);


void _eeprom_to_little_endian(struct mcmtii_eeprom *eeprom);
char * _eeprom_current_to_string(unsigned char current);

int _read_byte_from_eeprom(int fd, mcmtii_axis axis,
		unsigned char address, unsigned char *data);

/* hand controller commands */

int _move_NW_fast(int fd, mcmtii_axis axis);
int _move_NW_medium(int fd, mcmtii_axis axis);
int _move_SE_fast(int fd, mcmtii_axis axis);
int _move_SE_medium(int fd, mcmtii_axis axis);


/* guiding commands */

int _move_NW_slow(int fd, mcmtii_axis axis);
int _move_SE_slow(int fd, mcmtii_axis axis);
int _resume_sidereal(int fd, mcmtii_axis axis);

int _update_current_speed(int fd, mcmtii_axis axis, int new_speed);


/* libmcmtii internal status data *
 * representing the physical status of the mount/MCMTII */

typedef enum {
	MCMTII_MODE_UNINIT, MCMTII_MODE_SLEWING,
	MCMTII_MODE_GUIDING, MCMTII_MODE_PARKED
} _mcmtii_mode;
extern _mcmtii_mode _current_mode_ra, _current_mode_dec;

extern mcmtii_flip_status _flip_status;


#ifndef INT_MAX
#define INT_MAX       2147483647
#endif

#endif
