#include <stdio.h>
#include "mcmtii.h"
#include "low_level_commands.h"
#include "conversion.h"

/* virtual hand controller for the slewing mode
 * use MCMTII_SPEED_GUID to stop slewing */
int mcmtii_move(int fd, mcmtii_direction dir, mcmtii_speed speed) {
	int retval = -1;
	mcmtii_axis axis;
	switch (dir) {
		case MCMTII_NORTH:
		case MCMTII_SOUTH:
			axis = MCMTII_AXIS_DEC;
			break;
		case MCMTII_EAST:
		case MCMTII_WEST:
			axis = MCMTII_AXIS_RA;
			break;
	}
	if (dir == MCMTII_NORTH || dir == MCMTII_WEST) {
		switch (speed) {
			case MCMTII_SPEED_GUID:
				mcmtii_stop_slewing(fd, axis);
				break;
			case MCMTII_SPEED_MIN:
				return MCMTII_RET_ARGERR;
			case MCMTII_SPEED_MED:
				retval = _move_NW_medium(fd, axis);
				break;
			case MCMTII_SPEED_MAX:
				retval = _move_NW_fast(fd, axis);
				break;
		}
	} else {
		switch (speed) {
			case MCMTII_SPEED_GUID:
				mcmtii_stop_slewing(fd, axis);
				break;
			case MCMTII_SPEED_MIN:
				return MCMTII_RET_ARGERR;
			case MCMTII_SPEED_MED:
				retval = _move_SE_medium(fd, axis);
				break;
			case MCMTII_SPEED_MAX:
				retval = _move_SE_fast(fd, axis);
				break;
		}
	}
	return retval;
}

/* virtual hand controller for the guiding mode */
int mcmtii_guide(int fd, mcmtii_direction dir, mcmtii_speed speed) {
	int retval = -1;
	mcmtii_axis axis;
	switch (dir) {
		case MCMTII_NORTH:
		case MCMTII_SOUTH:
			if (_current_mode_dec == MCMTII_MODE_SLEWING)
				return MCMTII_RET_ERR;
			axis = MCMTII_AXIS_DEC;
			break;
		case MCMTII_EAST:
		case MCMTII_WEST:
			if (_current_mode_ra == MCMTII_MODE_SLEWING)
				return MCMTII_RET_ERR;
			axis = MCMTII_AXIS_RA;
			break;
	}
	if (dir == MCMTII_NORTH || dir == MCMTII_WEST) {
		switch (speed) {
			case MCMTII_SPEED_GUID:
				retval = _resume_sidereal(fd, axis);
				break;
			case MCMTII_SPEED_MIN:
				retval = _move_NW_slow(fd, axis);
				break;
			case MCMTII_SPEED_MED:
			case MCMTII_SPEED_MAX:
				return MCMTII_RET_ARGERR;
		}
	} else {
		switch (speed) {
			case MCMTII_SPEED_GUID:
				retval = _resume_sidereal(fd, axis);
				break;
			case MCMTII_SPEED_MIN:
				retval = _move_SE_slow(fd, axis);
				break;
			case MCMTII_SPEED_MED:
			case MCMTII_SPEED_MAX:
				return MCMTII_RET_ARGERR;
		}
	}
	return retval;
}


/* when telescope is moving at sidereal speed, it can be stopped using this method.
 * The original sidereal speed can be reset using mcmtii_move(MCMTII_SPEED_GUID) */
int mcmtii_stop_sidereal_speed(int fd) {
	return _update_current_speed(fd, MCMTII_AXIS_RA, 0);
}

/* When telescope is slewing (using mcmtii_move_number_of_steps() or mcmtii_move()),
 * the move can be stopped by using this command */
void mcmtii_stop_all_slewing(int fd) {
	mcmtii_stop_slewing(fd, MCMTII_AXIS_RA);
	mcmtii_stop_slewing(fd, MCMTII_AXIS_DEC);
}

/* setting reference position for angle conversion and relative slewing. The
 * current position is taken as reference with the angles passed in argument.
 * If the flip status is unknown when calling this function, it is guessed from
 * the passed coordinates (assuming flipped above meridian). To force it to
 * another value, use mcmtii_set_flip_status() before calling this function. */
int mcmtii_set_current_position(int fd, double ra, double dec) {
	unsigned int enc_val;
	if (_flip_status == MCMTII_FLIP_UNKNOWN) {
		/* guess flip status from coordinates */
		double ha = ra_to_ha(ra);
		if (mcmtii_convert_is_over_meridian(ha, dec))
			_flip_status = MCMTII_FLIP_FLIPPED;
		else _flip_status = MCMTII_FLIP_NONE;
	}
	if (mcmtii_read_encoder(fd, MCMTII_AXIS_RA, &enc_val) == MCMTII_RET_OK)
		mcmtii_conversion_set_reference(MCMTII_AXIS_RA, enc_val, ra);
	else return MCMTII_RET_ERR;

	if (mcmtii_read_encoder(fd, MCMTII_AXIS_DEC, &enc_val) == MCMTII_RET_OK)
		mcmtii_conversion_set_reference(MCMTII_AXIS_DEC, enc_val, dec);
	else return MCMTII_RET_ERR;
	return MCMTII_RET_OK;
}

/* get the current position of the telescope and convert it to RA/DEC values */
int mcmtii_get_current_ha_position(int fd, double *ha, double *dec) {
	unsigned int enc_val;
	double _ha, _dec;	/* avoid writing nonsense on errors */
	if (mcmtii_read_encoder(fd, MCMTII_AXIS_RA, &enc_val) == MCMTII_RET_OK)
		_ha = _convert_microsteps_to_ha(enc_val);
	else return MCMTII_RET_ERR;

	if (mcmtii_read_encoder(fd, MCMTII_AXIS_DEC, &enc_val) == MCMTII_RET_OK)
		_dec = _convert_microsteps_to_dec(enc_val);
	else return MCMTII_RET_ERR;
	*ha = _ha; *dec = _dec;
	return MCMTII_RET_OK;
}

/* get the current position of the telescope and convert it to RA/DEC values */
int mcmtii_get_current_position(int fd, double *ra, double *dec) {
	double ha, _dec;
	int retval;
	if (!mcmtii_conversion_is_init())
		return MCMTII_RET_UNINIT;
	if ((retval = mcmtii_get_current_ha_position(fd,
					&ha, &_dec)) == MCMTII_RET_OK) {
		*ra = ha_to_ra(ha);
		*dec = _dec;
	}
	return retval;
}

/* The GOTO method
 * Moves the telescope to designated RA/DEC values. RA is in hours, DEC in degrees */
int mcmtii_slew_absolute(int fd, double ra, double dec, int allow_flip) {
	unsigned int current_enc_val_ra, current_enc_val_dec,
		     remote_enc_val_ra, remote_enc_val_dec, tmp;
	int nb_steps, is_flipping = 0;
	if (!mcmtii_conversion_is_init())
		return MCMTII_RET_UNINIT;
	/* while flipping, deny other moves and new targets */
	if (_flip_status == MCMTII_FLIP_FLIPPING ||
			_flip_status == MCMTII_FLIP_UNFLIPPING) {
		fprintf(stderr, "Please wait until flip is over to give a new target\n");
		/* TODO the flipping status is removed only when calling
		 * mcmtii_device_ready(), so clients should call it periodically.
		 * It also means that there will be some latency between the end
		 * of the flip and being allowed to slew again.
		 * Solutions would be to add these calls above OR here and check
		 * again flip status:
		 * mcmtii_device_ready(fd, MCMTII_AXIS_RA);
		 * mcmtii_device_ready(fd, MCMTII_AXIS_DEC)
		 */
		return MCMTII_RET_ERR;
	}
	/* TODO: while slewing, what to do? */

	if (allow_flip) {
		int is_over_meridian;
		double target_ha = ra_to_ha(ra);
		is_over_meridian = mcmtii_convert_is_over_meridian(target_ha, dec);
		if (_flip_status == MCMTII_FLIP_NONE && is_over_meridian) {
			/* needs to flip */
			ra += 12.0;
			dec += 180.0;
			if (ra >= 24.0) ra -= 24.0;
			if (dec >= 180.0) dec -= 360.0;
			fprintf(stderr, "-- FLIPPING, RA = %g, DEC = %g\n", ra, dec);
			_flip_status = MCMTII_FLIP_FLIPPING;
			is_flipping = 1;
		} else if (_flip_status == MCMTII_FLIP_FLIPPED && !is_over_meridian) {
			/* needs to unflip: nothing to do */
			/*ra -= 12.0;
			dec -= 180.0;
			if (ra < 0.0) ra += 24.0;
			if (dec < -180.0) dec += 360.0;*/
			fprintf(stderr, "-- UNFLIPPING, RA = %g, DEC = %g\n", ra, dec);
			_flip_status = MCMTII_FLIP_UNFLIPPING;
			is_flipping = 1;
		}
	}
	/* modify target when flipped */
	if (_flip_status == MCMTII_FLIP_FLIPPED) {
		ra += 12.0;
		dec += 180.0;
		if (ra >= 24.0) ra -= 24.0;
		/*if (dec >= 360.0) dec -= 360.0;*/
	}

	_convert_double_to_microsteps(MCMTII_AXIS_RA, ra, &remote_enc_val_ra, !is_flipping);
	_convert_double_to_microsteps(MCMTII_AXIS_DEC, dec, &remote_enc_val_dec, !is_flipping);
	fprintf(stderr, "MCMTII: slewing to %gh (%u steps), %g� (%u steps)\n",
			ra, remote_enc_val_ra, dec, remote_enc_val_dec);

	/* RA axis. Managing the unsigned int - unsigned int given to an int arg */
	if (mcmtii_read_encoder(fd, MCMTII_AXIS_RA, &current_enc_val_ra) == MCMTII_RET_OK) {
		if (remote_enc_val_ra > current_enc_val_ra)
			tmp = remote_enc_val_ra - current_enc_val_ra;
		else tmp = current_enc_val_ra - remote_enc_val_ra;
		if (tmp > INT_MAX) {
			fprintf(stderr, "MCMTII: we have an integer overflow, that "
					"should not happen often, but it did. If you "
					"are not trying to slew on the other side of "
					"the sky, you may need to restart the MCMTII.\n");
			return MCMTII_RET_ERR;
		}
		nb_steps = (int)tmp;
		if (remote_enc_val_ra < current_enc_val_ra)
			nb_steps = -nb_steps;
		if (nb_steps != 0) {
			fprintf(stderr, "\t-> RA moving %d microsteps\n", nb_steps);
			if (mcmtii_move_number_of_steps(fd, MCMTII_AXIS_RA, nb_steps)
					!= MCMTII_RET_OK)
				return MCMTII_RET_ERR;
		}
	} else return MCMTII_RET_ERR;

	/* DEC axis. Managing the unsigned int - unsigned int given to an int arg */
	if (mcmtii_read_encoder(fd, MCMTII_AXIS_DEC, &current_enc_val_dec) == MCMTII_RET_OK) {
		fprintf(stderr, "target steps: %d, current steps: %d (t-c=%d)\n",
				remote_enc_val_dec, current_enc_val_dec,
				remote_enc_val_dec-current_enc_val_dec);
		if (remote_enc_val_dec > current_enc_val_dec)
			tmp = remote_enc_val_dec - current_enc_val_dec;
		else tmp = current_enc_val_dec - remote_enc_val_dec;
		if (tmp > INT_MAX) {
			fprintf(stderr, "MCMTII: we have an integer overflow, that "
					"should not happen often, but it did. If you "
					"are not trying to slew on the other side of "
					"the sky, you may need to restart the MCMTII.\n");
			return MCMTII_RET_ERR;
		}
		nb_steps = (int)tmp;
		if (remote_enc_val_dec < current_enc_val_dec)
			nb_steps = -nb_steps;
		if (nb_steps != 0) {
			fprintf(stderr, "\t-> DEC moving %d microsteps\n", nb_steps);
			if (mcmtii_move_number_of_steps(fd, MCMTII_AXIS_DEC, nb_steps)
					!= MCMTII_RET_OK)
				return MCMTII_RET_ERR;
		}
	} else return MCMTII_RET_ERR;
	return MCMTII_RET_OK;
}

int mcmtii_slew_absolute2(int fd, double ra, double dec) {
	return mcmtii_slew_absolute(fd, ra, dec, 1);
}

int mcmtii_slew_relative(int fd, double ra, double dec) {
	return MCMTII_RET_ERR;
}


mcmtii_flip_status mcmtii_get_flip_status() {
	if (!mcmtii_conversion_is_init())
		return MCMTII_FLIP_UNKNOWN;
	return _flip_status;
}

/* may be used after a flip abort, or when resuming operations */
void mcmtii_set_flip_status(mcmtii_flip_status st) {
	_flip_status = st;
}

#if 0
/* guess the flip status from position (assuming flipped above meridian) */
int mcmtii_guess_flip_status(int fd) {
	double current_ha, current_dec;
	if (_flip_status == MCMTII_FLIP_UNKNOWN) {
		if (mcmtii_get_current_ha_position(fd, &current_ha,
					&current_dec) != MCMTII_RET_OK)
			return MCMTII_RET_ERR;
		if (mcmtii_convert_is_over_meridian(current_ha, current_dec))
			_flip_status = MCMTII_FLIP_FLIPPED;
		else _flip_status = MCMTII_FLIP_NONE;
		fprintf(stderr, "Initializing flip status from current location: "
				"STATUS IS %s\n", _flip_status == MCMTII_FLIP_FLIPPED ?
				"FLIPPED" : "NOT FLIPPED");
	}
	return MCMTII_RET_OK;
}
#endif

/* may be used to position cartography software more precisely */
void mcmtii_get_latitude_longitude(double *lat, double *lon) {
	*lat = LATITUDE;
	*lon = LONGITUDE;
}

/* asks the MCMTII if the axes are in park mode.
 * Returns:	MCMTII_RET_ERR if anything goes wrong,
 *		1 if park mode is activated on AT LEAST ONE axis,
 *		0 else.
 */
int mcmtii_is_parked(int fd) {
	unsigned char byte1, byte2;
	if (_read_byte_from_eeprom(fd, MCMTII_AXIS_RA, 0x1e, &byte1) != MCMTII_RET_OK)
		return MCMTII_RET_ERR;
	if (_read_byte_from_eeprom(fd, MCMTII_AXIS_DEC, 0x1e, &byte2) != MCMTII_RET_OK)
		return MCMTII_RET_ERR;
	byte1 &= 0x01; byte2 &= 0x01;
	if (byte1) _current_mode_ra = MCMTII_MODE_PARKED;
	if (byte2) _current_mode_dec = MCMTII_MODE_PARKED;
	if (byte1 != byte2)
		fprintf(stderr, "Park mode mismatch between RA and DEC axes\n");
	return (byte1 == 0x01 || byte2 == 0x01);
}

/* Printing EEPROM data information (decimal values) */
void mcmtii_print_eeprom_configuration(FILE *output, struct mcmtii_eeprom *eeprom) {
	if (eeprom == NULL) {
		fprintf(stderr, "EEPROM data structure has not been initialized (NULL)\n");
		return;
	}

	fprintf(output, "Guiding speed: 16-bit MSBs: %hu, 8-bit LSB: %hhu\n",
			eeprom->VM_Guidage, eeprom->VM_Guidage_LSB);
	fprintf(output, "\tSteps per second: %.3g, resolution: %hhu\n",
			eeprom->VM_Guidage == 0 ? 0.0 :
			625000.0 / ((double)eeprom->VM_Guidage_LSB / 10.0 + (double)eeprom->VM_Guidage),
			eeprom->Resol_Guidage);

	fprintf(output, "Correcting speed +: %hu, steps per second: %.2g, resolution: %hhu\n",
			eeprom->VM_Corect_Plus, 625000.0 / (double)eeprom->VM_Corect_Plus,
			eeprom->Resol_Corec_Plus);

	fprintf(output, "Correcting speed -: %hu, steps per second: %.2g, resolution: %hhu\n",
			eeprom->VM_Corect_Moins, 625000.0 / (double)eeprom->VM_Corect_Moins,
			eeprom->Resol_Corec_Moins);

	fprintf(output, "Slewing speed (low): %hu, steps per second: %.2g, resolution: %hhu\n",
			eeprom->VM_Corect_Lent, 625000.0 / (double)(eeprom->VM_Corect_Lent * 8),
			eeprom->Resol_Lent);

	fprintf(output, "Slewing speed (high): %hu, steps per second: %.2g, resolution: %hhu\n",
			eeprom->VM_Corect_Rapide, 625000.0 / (double)(eeprom->VM_Corect_Rapide * 32),
			eeprom->Resol_Rapide);

	fprintf(output, "Acceleration: %s\n", eeprom->VM_Corect_Acce & 1 ? "enabled" : "disabled");

	fprintf(output, "Rotation direction: %d (%s)\n", eeprom->Dir_Guidage & 1,
			eeprom->Dir_Guidage & 1 ? "positive" : "negative");

	fprintf(output, "Electrical current for guiding operations: %s\n",
			_eeprom_current_to_string(eeprom->Courant_Guidage));

	fprintf(output, "Electrical current for slewing (low speed) operations: %s\n",
			_eeprom_current_to_string(eeprom->Courant_Lent));
	
	fprintf(output, "Electrical current for slewing (high speed) operations: %s\n",
			_eeprom_current_to_string(eeprom->Courant_Rapide));

	/*fprintf(output, "Resolution: %d\n", 25600 * eeprom->PasCodeur);*/

	fprintf(output, "Park mode %s\n", eeprom->ParkMode & 1 ? "enabled" : "disabled");
	fprintf(output, "PEC is %s\n", eeprom->PEC_ENABLED & 1 ? "enabled" : "disabled");
}

