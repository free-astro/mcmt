/* This file is part of libmcmtii
 * Licence: GPLv3
 * Project page: http://free-astro.vinvin.tf/index.php/MCMTII
 *
 * ***********************************************************
 *
 * Serial port operations:
 * - opening a port			mcmtii_serial_link_open()
 * - closing an opened port		mcmtii_serial_link_close()
 * - listing available serial ports	mcmtii_serial_link_list()
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include "log.h"	/* open_log_file() */

/* opens the specified device and configures it for the MCMTII serial link.
 * Return value is the file descriptor, -1 on error.
 */
int mcmtii_serial_link_open(const char *port_device) {
	struct termios termattr;
	int fd;
	if (!port_device || port_device[0] == '\0') {
		fprintf(stderr, "libmcmtii, serial_link_open: argument not specified\n");
		return -1;
	}

	if ((fd = open(port_device, O_RDWR)) == -1) {
		perror("libmcmtii, serial_link_open, open");
		return -1;
	}

	/* termios stuff */
	if (tcgetattr(fd, &termattr) == -1) {
		perror("libmcmtii, serial_link_open, tcgetattr");
		close(fd);
		return -1;
	}
	/* MCMTII uses 19200 baud rate, 8 for byte size, no parity, no control flow, stop bit is 0 */
	cfsetospeed(&termattr, (speed_t) B19200);
	cfsetispeed(&termattr, (speed_t) B19200);
	termattr.c_cflag = (termattr.c_cflag & ~CSIZE) | CS8;
	termattr.c_iflag = IGNBRK | IGNPAR;
	termattr.c_lflag = 0;
	termattr.c_oflag = 0;
	termattr.c_cflag |= CLOCAL | CREAD;
	termattr.c_cc[VMIN] = 1;
	termattr.c_cc[VTIME] = 5;
	termattr.c_iflag &= ~(IXON | IXOFF | IXANY);
	termattr.c_cflag &= ~(PARENB | PARODD);
	if (tcsetattr(fd, TCSAFLUSH, &termattr) == -1) {
		perror("libmcmtii, serial_link_open, tcsetattr");
		close(fd);
		return -1;
	}

	/* initialize stuff */
	mcmtii_log_open_file();
	return fd;
}

void mcmtii_serial_link_close(int serial_link_fd) {
	mcmtii_log_close_file();
	if (serial_link_fd >= 0) {
		if (close(serial_link_fd) == -1)
			perror("libmcmtii, serial_link_close");
	} else {
		fprintf(stderr, "libmcmtii, serial_link_close: bad file descriptor.\n");
	}
}

/* returns the number of serial devices found, and fills the already-allocated
 * device_name parameter.
 * The allocation has to be on the first dimension only, strings are allocated
 * in this method. They should be freed in the application's code after use.
 * The max_devices argument is the maximum number of device names to set,
 * generally the allocated size of device_name.
 * Return value: -1 if list cannot be made, and number of filled devices names else.
 */
int mcmtii_serial_link_list(char **device_name, int max_devices) {
	/* http://stackoverflow.com/questions/2530096/how-to-find-all-serial-devices-ttys-ttyusb-on-linux-without-opening-them */
	/* in /sys/class/tty, list ttyS* and ttyUSB* */
	DIR *ttydir;
	struct dirent *ttydev;
	int nb_devices = 0;

	if (max_devices <= 0) {
		fprintf(stderr, "libmcmtii, serial_link_list: argument max_devices has to be >0\n");
		return -1;
	}
	if ((ttydir = opendir("/sys/class/tty")) == NULL) {
		perror("libmcmtii, serial_link_list");
		return -1;
	}

	while ((ttydev = readdir(ttydir)) != NULL) {
		if (!strncmp(ttydev->d_name, "ttyS", 4) || !strncmp(ttydev->d_name, "ttyUSB", 6) ) {
			if (nb_devices >= max_devices)
				break;
			device_name[nb_devices++] = strdup(ttydev->d_name);
		}
	}
	closedir(ttydir);
	return nb_devices;
}

