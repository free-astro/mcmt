#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include "mcmtii.h"

#define MODE_CONSOLE	0

char *devices[20];
int nb_devices = 0;
int mcmtii_fd = -1;
int run = 1;
int current_mode = MODE_CONSOLE;

void console_mode();
int calculate_ms_from_timeval(struct timeval *start, struct timeval *stop);

int main() {
	int i;
	nb_devices = mcmtii_serial_link_list(devices, 20);
	fprintf(stdout, "List of available serial devices:\n");
	for (i=0; i<nb_devices; i++) {
		fprintf(stdout, "%2d: %s\n", i, devices[i]);
	}

	do {
		if (current_mode == MODE_CONSOLE)
			console_mode();
	} while (run) ;

	if (mcmtii_fd != -1)
		mcmtii_serial_link_close(mcmtii_fd);
	return 0;
}

void display_help() {
	fprintf(stdout, "Available commands:\n"
			"\topen dev_num:     open serial device dev_num in the displayed list\n"
			"\thelp (or h):      display this help\n"
			"\tping (or ready):  get MCMTII's status\n"
			"\tversion:          get MCMTII's versions\n"
			"\tencoders (or e):  get current encoders values (mount position)\n"
			"\teeprom 0|1:       get and display EEPROM values (0 for RA, 1 for DEC)\n"
			"\tmoveRA nb_steps:  move signed nb_steps on the RA axis\n"
			"\tmoveDEC nb_steps: move signed nb_steps on the DEC axis\n"
			"\tstopall (or s):   stop moving before steps are reached\n");
	fprintf(stdout, "quit with 'q' or ^d\n");
}

void console_mode() {
	char command[200], *command_ptr;
	do {
		fprintf(stdout, "> ");
		command_ptr = fgets(command, 200, stdin);

		if (!command_ptr || !strcasecmp(command, "q\n")) {
			if (!command_ptr)
				fputc('\n', stdout);
			run = 0;
			return ;
		}
		if (command[0] == '\n') continue;

		/* OPEN THE SERIAL LINK */
		if (!strncasecmp(command, "open", 4)) {
			if (strlen(command) > 6 && command[4] == ' ' &&
					command[5] >= '0' && command[5] <= '9') {
				int dev_num = atoi(command+5);
				char device[50];
				if (dev_num >= nb_devices) {
					fprintf(stdout, "wrong device number\n");
				} else {
					sprintf(device, "/dev/%s", devices[dev_num]);
					fprintf(stdout, "opening serial device %s for MCMTII\n",
							devices[dev_num]);
					mcmtii_fd = mcmtii_serial_link_open(device);
					if (mcmtii_fd != -1)
						fprintf(stdout, "serial link successfully opened.\n");
				}
			} else {
				fprintf(stdout, "malformed command. use 'open number', with"
						" number being the index in the displayed list"
						" of serial devices\n");
			}
		}
		else if (!strcasecmp(command, "h\n") || !strcasecmp(command, "help\n")) {
			display_help();
		}
		else if (mcmtii_fd == -1) {
			fprintf(stdout, "connect to the MCMTII first, use open\n");
		}
		/* IS DEVICE READY (PING) */
		else if (!strcasecmp(command, "ping\n") || !strcasecmp(command, "ready\n")) {
			int retval;
			struct timeval start, stop;
			gettimeofday(&start, NULL);
			retval = mcmtii_device_ready(mcmtii_fd, MCMTII_AXIS_RA);
			if (retval >= 0) {
				int ms;
				gettimeofday(&stop, NULL);
				ms = calculate_ms_from_timeval(&start, &stop);
				fprintf(stdout, "right ascension axis is %s. Ping: %dms\n",
						retval ? "ready (guiding)" : "not ready (slewing)",
						ms);
			}
			else fprintf(stdout, "right ascension axis did not respond (%s)\n",
					mcmtii_strerror(retval));

			gettimeofday(&start, NULL);
			retval = mcmtii_device_ready(mcmtii_fd, MCMTII_AXIS_DEC);
			if (retval >= 0) {
				int ms;
				gettimeofday(&stop, NULL);
				ms = calculate_ms_from_timeval(&start, &stop);
				fprintf(stdout, "declination axis is %s. Ping: %dms\n",
					       retval ? "ready (guiding)" : "not ready (slewing)",
					       ms);
			}
			else fprintf(stdout, "declination axis did not respond (%s)\n",
					mcmtii_strerror(retval));
		}
		/* READ MCMTII VERSION */
		else if (!strcasecmp(command, "version\n")) {
			char ver[80];
			if (mcmtii_get_version(mcmtii_fd, MCMTII_AXIS_RA, ver) == MCMTII_RET_OK)
				fprintf(stdout, "RA version: %s\n", ver);
			else fprintf(stdout, "could not get RA version\n");
			if (mcmtii_get_version(mcmtii_fd, MCMTII_AXIS_DEC, ver) == MCMTII_RET_OK)
				fprintf(stdout, "DEC version: %s\n", ver);
			else fprintf(stdout, "could not get DEC version\n");
		}
		/* READ ENCODER VALUE */
		else if (!strcasecmp(command, "e\n") || !strcasecmp(command, "encoders\n")) {
			unsigned int encoder_val;
			if (mcmtii_read_encoder(mcmtii_fd, MCMTII_AXIS_RA, &encoder_val) ==
					MCMTII_RET_OK)
				fprintf(stdout, "RA encoder value: %u\n", encoder_val);
			else fprintf(stdout, "RA encoder reading failed\n");
			if (mcmtii_read_encoder(mcmtii_fd, MCMTII_AXIS_DEC, &encoder_val) ==
					MCMTII_RET_OK)
				fprintf(stdout, "DEC encoder value: %u\n", encoder_val);
			else fprintf(stdout, "DEC encoder reading failed\n");
		}
		/* READ EEPROM DATA AND DISPLAY IT */
		else if (!strncasecmp(command, "eeprom", 6)) {
			if (strlen(command) > 8 && command[6] == ' ' &&
					(command[7] == '0' || command[7] == '1')) {
				struct mcmtii_eeprom eeprom;
				if (mcmtii_read_eeprom(mcmtii_fd,
						command[7] == '0' ? MCMTII_AXIS_RA : MCMTII_AXIS_DEC,
						&eeprom) == MCMTII_RET_OK)
					mcmtii_print_eeprom_configuration(stdout, &eeprom);
				else fprintf(stdout, "Error getting the EEPROM data\n");
			} else {
				fprintf(stdout, "malformed command. use 'eeprom 0|1' (0 for RA, 1 for DEC)\n");
			}
		}

		/* GOTO: MOVE NUMBER OF STEPS */
		else if (!strncasecmp(command, "movera", 6)) {
			if (strlen(command) > 8 && command[6] == ' ' &&
					((command[7] >= '0' && command[7] <= '9') || command[7] == '-')) {
				int steps = atoi(command+7);
				if (mcmtii_move_number_of_steps(mcmtii_fd, MCMTII_AXIS_RA, steps) ==
						MCMTII_RET_OK)
					fprintf(stdout, "moving %d steps on RA. Use 's' to stop\n",
							steps);
				else fprintf(stdout, "error when trying to move RA.\n");
			} else {
				fprintf(stdout, "malformed command. use 'movera nb_steps'\n");
			}
		}
		else if (!strncasecmp(command, "movedec", 7)) {
			if (strlen(command) > 9 && command[7] == ' ' &&
					((command[8] >= '0' && command[8] <= '9') || command[8] == '-')) {
				int steps = atoi(command+8);
				if (mcmtii_move_number_of_steps(mcmtii_fd, MCMTII_AXIS_DEC, steps) ==
						MCMTII_RET_OK)
					fprintf(stdout, "moving %d steps on DEC. Use 's' to stop\n",
							steps);
				else fprintf(stdout, "error when trying to move DEC.\n");
			} else {
				fprintf(stdout, "malformed command. use 'movedec nb_steps'\n");
			}
		}
		else if (!strcasecmp(command, "s\n") ||
				!strcasecmp(command, "stopall\n")) {
			fprintf(stdout, "stopping all slewing\n");
			mcmtii_stop_all_slewing(mcmtii_fd);
		}
		else {
			fprintf(stdout, "command not found, use `help' for help, `q' to quit\n");
		}

	} while (1) ;
}

int calculate_ms_from_timeval(struct timeval *start, struct timeval *stop) {
	if (!start || !stop)
		return 9999999;
	time_t delta_seconds      = stop->tv_sec - start->tv_sec;
	time_t delta_milliseconds = (stop->tv_usec - start->tv_usec) / 1000;

	if (delta_milliseconds < 0) { /* manually carry a one from the seconds field */
		delta_milliseconds += 1000;
		-- delta_seconds;
	}
	return (delta_seconds * 1000) + delta_milliseconds;
}

