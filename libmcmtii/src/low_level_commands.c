#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <string.h>	/* for memcpy */
#include <byteswap.h>
#include "mcmtii.h"
#include "low_level_commands.h"
#include "log.h"

/*************** internal data ***************/

_mcmtii_mode _current_mode_ra = MCMTII_MODE_UNINIT;
_mcmtii_mode _current_mode_dec = MCMTII_MODE_UNINIT;

mcmtii_flip_status _flip_status;

/*************** internal functions ***************/

/* This method provides the proper interface to 2 bytes interrupting commands */
int _request_response_2byte_command(int fd, unsigned char *request,
		unsigned char *response, int expected_response_size) {
	if (request[1] <= 0x7f) {
		fprintf(stderr, "Wrongly made 2-byte command\n");
		return MCMTII_RET_ARGERR;
	}
	return _request_response(fd, request, 2,
			response, expected_response_size);
}

/* This method provides the proper interface to 8 bytes ASCII commands.
 * request should be 6 bytes: the axis, the command ASCII char, and 4 data bytes.
 * expected_response_size must not include the ACK, it's already managed here. 
 * Returns MCMTII_RET_OK when the response size is the expected response size
 * and the ACK has been received (removed from the response),
 * MCMTII_RET_INCOMPLETE if the response is smaller with ACK,
 * MCMTII_RET_ERR else and for any error.
 */
int _request_response_8byte_command(int fd, unsigned char *request,
		unsigned char *response, int expected_response_size) {
	unsigned char eightbc[8];	/* eight-byte command */
	unsigned char *response_buf;
	int i, retval, my_retval;
#ifdef TESTING
	return MCMTII_RET_OK;
#endif
	if (request[1] > 0x7f || expected_response_size < 0) {
		fprintf(stderr, "Wrongly made 8-byte command\n");
		return MCMTII_RET_ARGERR;
	}
	eightbc[0] = request[0];	/* axis	*/
	eightbc[1] = request[1];	/* command id (ASCII letter) */
	eightbc[6] = 0;			/* heavyweight bits */
	eightbc[7] = 0;			/* checksum */
	/* we need to remove the eighth bit to bytes [2;5] */
	for (i=2; i<=5; i++) {
		eightbc[i] = request[i] & 0x7f;
		if (request[i] & 0x80)
			eightbc[6] |= 1 << (3-(i-2));
	}
	/* checksum */
	for (i=0; i<=6; i++)
		eightbc[7] += eightbc[i];
	eightbc[7] = eightbc[7] & 0x7f;

	expected_response_size++;
	response_buf = malloc(expected_response_size);
	/* do the request */
	retval = _request_response(fd, eightbc, 8,
			response_buf, expected_response_size);
	/* handle return values and the ACK */
	if (retval < 0) my_retval = retval;	/* keep upstream errors */
	else my_retval = MCMTII_RET_ERR;
	if (retval >= 1 && _is_ack_retval(response_buf[0])) {
		/* copy anyway if not everything is here */
		for (i=1; i<retval; i++)
			response[i-1] = response_buf[i];
		if (retval == expected_response_size)
			my_retval = MCMTII_RET_OK;
		else my_retval = MCMTII_RET_INCOMPLETE;
	} else {
		/* not an ack? should not happen! */
	}
	free(response_buf);
	return my_retval;
}

/* The main input/output method.
 * Return value is the number of bytes read (>= 0) in response in case of success;
 * error return codes are MCMTII_RET_ERR for failure, MCMTII_RET_ARGERR for invalid
 * argument, and MCMTII_RET_TIMEOUT in case of read or write timeout */
int _request_response(int fd, unsigned char *request, int request_size,
		unsigned char *response, int expected_response_size) {
	fd_set fds;
	struct timeval tv;
	int retval, my_retval, read_bytes;
	if (request_size != 2 && request_size != 8) {
		/* case not handled: 16-byte commands for EEPROM update */
		fprintf(stderr, "Command type not handled (%d bytes)\n", request_size);
		return MCMTII_RET_ARGERR;
	}

	/* Sending the request */
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	tv.tv_sec = MCMTII_WRITE_TIMEOUT_SEC;
	tv.tv_usec = MCMTII_WRITE_TIMEOUT_USEC;
	retval = select(fd+1, NULL, &fds, NULL, &tv);
	if (retval == -1) {
		perror("mcmtii internal select write");
		return MCMTII_RET_ERR;
	}
	if (retval == 0 || !FD_ISSET(fd, &fds)) {
		fprintf(stderr, "MCMTII is busy, retry writing to it later\n");
		return MCMTII_RET_TIMEOUT;
	}

	mcmtii_log_binary("write", request, request_size);
	retval = write(fd, request, request_size);
	if (retval == -1) {
		perror("mcmtii internal write");
		return MCMTII_RET_ERR;
	}
	if (retval == 0) {	/* end of file */
		fprintf(stderr, "Write error: end of file. The connection was probably cut.\n");
		return MCMTII_RET_ERR;
	}
	if (retval != request_size) {
		fprintf(stderr, "mcmtii internal write: request has not been fully "
				"sent, leaving the link in an inconsistent state.\n");
		return MCMTII_RET_ERR;
	}

	/* Receiving the response */
	read_bytes = 0;
	my_retval = 0;
	do {	/* loop because it may not read it all the first time */
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		tv.tv_sec = MCMTII_READ_TIMEOUT_SEC;
		tv.tv_usec = MCMTII_READ_TIMEOUT_USEC;
		retval = select(fd+1, &fds, NULL, NULL, &tv);
		if (retval == -1) {
			perror("mcmtii internal read");
			my_retval = MCMTII_RET_ERR;
			break;
		}
		if (retval == 0 || !FD_ISSET(fd, &fds)) {
			fprintf(stderr, "MCMTII is busy after reading %d bytes, "
					"retry reading from it later\n", read_bytes);
			my_retval = MCMTII_RET_TIMEOUT;
			break;
		}

		retval = read(fd, response+read_bytes, expected_response_size-read_bytes);
		if (retval == -1) {
			perror("mcmtii internal read");
			my_retval = MCMTII_RET_ERR;
			break;
		}
		if (retval == 0) {	/* end of file */
			fprintf(stderr, "Read error: end of file. The connection was probably cut.\n");
			my_retval = MCMTII_RET_ERR;
			break;
		}
		read_bytes += retval;
	} while (read_bytes < expected_response_size) ;

	mcmtii_log_text("read", "%d bytes for %d expected response size:",
			read_bytes, expected_response_size);
	if (read_bytes > 0) {
		mcmtii_log_binary("data", response, read_bytes);
		if (read_bytes != expected_response_size) {
			fprintf(stderr, "mcmtii internal: short read (%d/%d).\n",
					read_bytes, expected_response_size);
		}
		/* override timeout error if something has been read */
		if (my_retval == MCMTII_RET_TIMEOUT || my_retval == 0)
			my_retval = read_bytes;
	}
	return my_retval;
}

unsigned char _axis_to_byte(mcmtii_axis axis) {
	return (unsigned char) axis;
}

int _is_ack_retval(unsigned char byte) {
	if (byte == MCMTII_ACK)
		return 1;
	if (byte == MCMTII_NOACK)
		return 0;
	return MCMTII_RET_ERR;
}

/********************************************************************************
 *                   PUBLIC FUNCTIONALITIES (listed in mcmtii.h)                *
 ********************************************************************************/

/* returns <0 on error, 1 if device is ready (guiding), 0 if busy (slewing) */
int mcmtii_device_ready(int fd, mcmtii_axis axis) {
	/* request:	axis_id, FF
	 * response:	01 */
	unsigned char resp[1];
	int retval;
#ifndef TESTING
	unsigned char req[2];
	req[0] = _axis_to_byte(axis);
	req[1] = 0xff;
	retval = _request_response_2byte_command(fd, req, resp, 1);
#else
	resp[0] = 0x01;
	retval = 1;
#endif
	if (retval < 1)
		return retval;
	if (resp[0] == 0x01) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_GUIDING;
		else 	_current_mode_dec = MCMTII_MODE_GUIDING;
		/* update flip status after the flip */
		if ((_flip_status == MCMTII_FLIP_FLIPPING ||
					_flip_status == MCMTII_FLIP_UNFLIPPING) &&
				_current_mode_ra == MCMTII_MODE_GUIDING &&
				_current_mode_dec == MCMTII_MODE_GUIDING) {
			if (_flip_status == MCMTII_FLIP_FLIPPING)
				_flip_status = MCMTII_FLIP_FLIPPED;
			else _flip_status = MCMTII_FLIP_NONE;
		}
	} else if (resp[0] == 0x00) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_SLEWING;
		else 	_current_mode_dec = MCMTII_MODE_SLEWING;
	}
	return (resp[0] == 0x01);
}

/* Gets the current value of the encoder and stores it in the pointer encoder_value.
 * Returns MCMTII_RET_OK on success, MCMTII_RET_ERR on error */
int mcmtii_read_encoder(int fd, mcmtii_axis axis, unsigned int *encoder_value) {
	/* request:	axis_id, F1 + byte number [0..3]
	 * (must start with F4, this copies the value in the 4 byte buffer for
	 * subsequent requests)
	 * response:	the requested byte from the int32 value
	 */
#ifdef TESTING
	*encoder_value = 1073741824;
	return MCMTII_RET_OK;
#endif
	unsigned char req[2];
	unsigned char resp[1];
	unsigned int value = 0;
	int i, retval;
	req[0] = _axis_to_byte(axis);
	for (i=3; i>=0; i--) {
		req[1] = 0xf1 + i;
		retval = _request_response_2byte_command(fd, req, resp, 1);
		if (retval != 1)
			return MCMTII_RET_ERR;
		value |= (resp[0] << (8*i));
	}
	*encoder_value = value;
	return MCMTII_RET_OK;
}

/* interrupts the current slewing operation (when speed is medium or max) */
int mcmtii_stop_slewing(int fd, mcmtii_axis axis) {
	/* request:	axis_id, F0
	 * response:	01 */
	unsigned char req[2];
	unsigned char resp[1];
	int retval;
	if ((axis == MCMTII_AXIS_RA && _current_mode_ra == MCMTII_MODE_UNINIT) ||
			(axis == MCMTII_AXIS_DEC && _current_mode_dec == MCMTII_MODE_UNINIT)) {
		retval = mcmtii_device_ready(fd, axis);
		if (retval < 0) return retval;
	}
	if ((axis == MCMTII_AXIS_RA && _current_mode_ra == MCMTII_MODE_GUIDING) ||
			(axis == MCMTII_AXIS_DEC && _current_mode_dec == MCMTII_MODE_GUIDING))
		return MCMTII_RET_OK;
	/* stop slewing does not reply if it's not slewing, so we need to be sure
	 * of the current state to avoid waiting for the timeout. */

	req[0] = _axis_to_byte(axis);
	req[1] = 0xf0;
	retval = _request_response_2byte_command(fd, req, resp, 1);
	if (retval == MCMTII_RET_ERR || retval == MCMTII_RET_ARGERR)
		return MCMTII_RET_ERR;
	if (retval == MCMTII_RET_TIMEOUT ||
			(retval == 1 && _is_ack_retval(resp[0]))) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_GUIDING;
		else _current_mode_dec = MCMTII_MODE_GUIDING;
		/* flip status is returned to previous value if flipping is
		 * interrupted. That can be fixed afterwards w/ mcmtii_set_flip_status() */
		if (_flip_status == MCMTII_FLIP_FLIPPING)
			_flip_status = MCMTII_FLIP_NONE;
		else if (_flip_status == MCMTII_FLIP_UNFLIPPING)
			_flip_status = MCMTII_FLIP_FLIPPED;
		return MCMTII_RET_OK;
	}
	fprintf(stderr, "libmcmtii stop slewing: WEIRD CASE HAPPENED! retval=%d\n", retval);
	return MCMTII_RET_ERR;	/* not an ACK?, should not happen */
}

/* move number of steps: slewing at max speed (the sign gives the direction) */
int mcmtii_move_number_of_steps(int fd, mcmtii_axis axis, int number_of_steps)
{
	unsigned char req[6];
	int retval;
	req[0] = _axis_to_byte(axis);
	req[1] = 'p';
	/* fix the reversed axis on the telescope */
	if (axis == MCMTII_AXIS_DEC && _flip_status == MCMTII_FLIP_NONE)
		number_of_steps = -number_of_steps;
	if (number_of_steps >= 0) {
		number_of_steps = __bswap_32(number_of_steps);
		memcpy(req+2, &number_of_steps, 4);
	} else {
		/* negative value is in fact transferred as a positive value
		 * but with the heavy bit set, that's not a regular swap */
		number_of_steps = __bswap_32(-number_of_steps);
		memcpy(req+2, &number_of_steps, 4);
		req[2] |= 0x80;
	}
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval != MCMTII_RET_ERR) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_UNINIT;
		else 	_current_mode_dec = MCMTII_MODE_UNINIT;
		/* mode is set to uninit ^ because we don't know if next use will
		 * be before or after the completion of the slewing */
	}
	return retval;
}

/********************************************************************************
 *                           VIRTUAL HAND CONTROLLER                            *
 ********************************************************************************/

int _move_NW_fast(int fd, mcmtii_axis axis) {
	/* request:	'X', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[6];
	int retval;
	req[0] = _axis_to_byte(axis);
	req[1] = 'X';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval == MCMTII_RET_OK) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_SLEWING;
		else	_current_mode_dec = MCMTII_MODE_SLEWING;
	}
	return retval;
}

int _move_NW_medium(int fd, mcmtii_axis axis) {
	/* request:	'G', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[6];
	int retval;
	req[0] = _axis_to_byte(axis);
	req[1] = 'G';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval == MCMTII_RET_OK) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_SLEWING;
		else	_current_mode_dec = MCMTII_MODE_SLEWING;
	}
	return retval;
}

/* guiding correction, WARNING: cannot be done in slewing mode */
int _move_NW_slow(int fd, mcmtii_axis axis) {
	/* request:	'D', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[6];
	int retval;
	if ((axis == MCMTII_AXIS_RA && _current_mode_ra == MCMTII_MODE_SLEWING) ||
			(axis == MCMTII_AXIS_DEC && _current_mode_dec == MCMTII_MODE_SLEWING))
		return MCMTII_RET_ERR;
	req[0] = _axis_to_byte(axis);
	req[1] = 'D';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval == MCMTII_RET_OK) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_GUIDING;
		else	_current_mode_dec = MCMTII_MODE_GUIDING;
	}
	return retval;
}

int _move_SE_fast(int fd, mcmtii_axis axis) {
	/* request:	'W', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[6];
	int retval;
	req[0] = _axis_to_byte(axis);
	req[1] = 'W';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval == MCMTII_RET_OK) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_SLEWING;
		else	_current_mode_dec = MCMTII_MODE_SLEWING;
	}
	return retval;
}

int _move_SE_medium(int fd, mcmtii_axis axis) {
	/* request:	'F', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[6];
	int retval;
	req[0] = _axis_to_byte(axis);
	req[1] = 'F';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval == MCMTII_RET_OK) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_SLEWING;
		else	_current_mode_dec = MCMTII_MODE_SLEWING;
	}
	return retval;
}

/* guiding correction, WARNING: cannot be done in slewing mode */
int _move_SE_slow(int fd, mcmtii_axis axis) {
	/* request:	'Q', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[6];
	int retval;
	if ((axis == MCMTII_AXIS_RA && _current_mode_ra == MCMTII_MODE_SLEWING) ||
			(axis == MCMTII_AXIS_DEC && _current_mode_dec == MCMTII_MODE_SLEWING))
		return MCMTII_RET_ERR;
	req[0] = _axis_to_byte(axis);
	req[1] = 'Q';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval == MCMTII_RET_OK) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_GUIDING;
		else	_current_mode_dec = MCMTII_MODE_GUIDING;
	}
	return retval;
}

/* stop corrections and resume sidereal speed, WARNING: cannot be done in slewing mode */
int _resume_sidereal(int fd, mcmtii_axis axis) {
	/* request:	'S', 0, 0, 0, 0
	 * response:	ack */
	unsigned char req[6];
	int retval;
	if ((axis == MCMTII_AXIS_RA && _current_mode_ra == MCMTII_MODE_SLEWING) ||
			(axis == MCMTII_AXIS_DEC && _current_mode_dec == MCMTII_MODE_SLEWING))
		return MCMTII_RET_ERR;
	req[0] = _axis_to_byte(axis);
	req[1] = 'S';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval == MCMTII_RET_OK) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_GUIDING;
		else	_current_mode_dec = MCMTII_MODE_GUIDING;
	}
	return retval;
}

/* Update guiding speed, can only be set while device is already in sidereal speed.
 * The new speed setting is not saved in the EEPROM, but restored when changing speed,
 * for example using the 'S' command (return to sidereal) or {XGDWFQ} commands.
 * The new speed setting is returned by the 'r' command if it hasn't been reset yet.
 * Updating the guiding speed is generally used periodically to compensate
 * errors like PEC and KING.
 * WARNING: in version <=2.7.5 (current), the MCMTII acknowledges the command
 * even if it was not in sidereal speed and the new speed was not set.
 *
 * The format of new_speed is:
 * bytes 0 and 1 for DELAY_M
 * byte 2 for DELAY_M_LSB
 * byte 3, low bit, for direction of movement (0 for reverse)
 *
 * this ^ conversion is due to the fact that argument is speed in steps per seconds
 * but the MCMTII uses a value corresponding to the delay between (micro?)steps
 */
int _update_current_speed(int fd, mcmtii_axis axis, int new_speed) {
	/* request:	'R', DELAY_M{1,2}, DELAY_M_LSB, DIR_M
	 * response:	ack */
	unsigned char req[6];
	if ((axis == MCMTII_AXIS_RA && _current_mode_ra == MCMTII_MODE_SLEWING) ||
			(axis == MCMTII_AXIS_DEC && _current_mode_dec == MCMTII_MODE_SLEWING))
		return MCMTII_RET_ERR;
	req[0] = _axis_to_byte(axis);
	req[1] = 'R';
	if (new_speed == 0) {
		/* 0, 0, 0, 0 */
		memcpy(req+2, &new_speed, 4);
	} else {
		new_speed = 625000 / new_speed;
		req[2] = (new_speed >> 8) & 0xff;
		req[3] = new_speed & 0xff;
		req[4] = (10 * new_speed) & 0xff;
		req[5] = (new_speed > 0);
	}
	return _request_response_8byte_command(fd, req, NULL, 0);
}

/* enable or disable park mode */
int mcmtii_park_mode(int fd, mcmtii_axis axis, int enable) {
	/* request:	'P', 0, 0, 0, 0		park mode
	 * or request:	'N', 0, 0, 0, 0		nopark mode
	 * response:	ack */
	unsigned char req[6];
	int retval;
	if ((axis == MCMTII_AXIS_RA && _current_mode_ra == MCMTII_MODE_PARKED) ||
			(axis == MCMTII_AXIS_DEC && _current_mode_dec == MCMTII_MODE_PARKED))
		return MCMTII_RET_OK;
	req[0] = _axis_to_byte(axis);
	req[1] = enable ? 'P' : 'N';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, NULL, 0);
	if (retval == MCMTII_RET_OK) {
		if (axis == MCMTII_AXIS_RA)
			_current_mode_ra = MCMTII_MODE_PARKED;
		else _current_mode_dec = MCMTII_MODE_PARKED;
	}
	return retval;
}

/********************************************************************************
 *                           SETUP-RELATED COMMANDS                             *
 ********************************************************************************/

/* request version number for the microcontroller's code.
 * It will be stored in version, which must be at least 80 characters wide.
 * Return value is 0 (MCMTII_RET_OK) on success, -1 (MCMTII_RET_ERR) on error.
 */
int mcmtii_get_version(int fd, mcmtii_axis axis, char *version) {
	/* request:	'V', 0, 0, 0, 0
	 * response:	a string allocated to 80 characters */
	unsigned char req[6];
	int retval;
	if (version == NULL) return MCMTII_RET_ERR;
	memset(version, 0, 80);		/* fix the short read nul byte issue */
	req[0] = _axis_to_byte(axis);
	req[1] = 'V';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req,
			(unsigned char*)version, 80);
	version[79] = '\0';
	if (retval != MCMTII_RET_OK && retval != MCMTII_RET_INCOMPLETE)
		return MCMTII_RET_ERR;
	/* fprintf(stdout, "MCMTII version: %s\n", resp); */
	return MCMTII_RET_OK;
}

/* Return value is MCMTII_RET_OK on success, MCMTII_RET_ERR on error. */
int mcmtii_read_eeprom(int fd, mcmtii_axis axis, struct mcmtii_eeprom *eeprom) {
	/* request:	'K', 0, 0, 0, 0
	 * response:	48 bytes from the EEPROM
	 */
	unsigned char req[6];
	int retval;
	if (eeprom == NULL) return MCMTII_RET_ERR;
	req[0] = _axis_to_byte(axis);
	req[1] = 'K';
	req[2] = 0; req[3] = 0; req[4] = 0; req[5] = 0;
	retval = _request_response_8byte_command(fd, req, (unsigned char*)eeprom, 48);
	if (retval != MCMTII_RET_OK)
		return MCMTII_RET_ERR;
	_eeprom_to_little_endian(eeprom);
	if ((axis == MCMTII_AXIS_RA &&
				_current_mode_ra != MCMTII_MODE_UNINIT &&
				((eeprom->ParkMode == 0x01) !=
				 (_current_mode_ra == MCMTII_MODE_PARKED))) ||
			(axis == MCMTII_AXIS_DEC &&
			 _current_mode_dec != MCMTII_MODE_UNINIT &&
			 ((eeprom->ParkMode == 0x01) !=
			  (_current_mode_dec == MCMTII_MODE_PARKED))))
		fprintf(stderr, "Park mode mismatch between known value and EEPROM's\n");
	/* if the EEPROM says the telescope is in park mode, we should write it as such,
	 * but if it is in fact moving, it may lead to an uncontrollable situation.
	 if (axis == MCMTII_AXIS_RA)
		_current_mode_ra = MCMTII_MODE_PARKED;
	else _current_mode_dec = MCMTII_MODE_PARKED; */
	/* initialize conversion data */
	//ra_per_step = 
	return MCMTII_RET_OK;
}

#define SWAP_SHORT_BYTES(n) ((n & 255) << 8) + ((n >> 8) & 255)
void _eeprom_to_little_endian(struct mcmtii_eeprom *eeprom) {
	eeprom->VM_Guidage = SWAP_SHORT_BYTES(eeprom->VM_Guidage);
	eeprom->VM_Corect_Plus = SWAP_SHORT_BYTES(eeprom->VM_Corect_Plus);
	eeprom->VM_Corect_Moins = SWAP_SHORT_BYTES(eeprom->VM_Corect_Moins);
	eeprom->VM_Corect_Lent = SWAP_SHORT_BYTES(eeprom->VM_Corect_Lent);
	eeprom->VM_Corect_Rapide = SWAP_SHORT_BYTES(eeprom->VM_Corect_Rapide);
	eeprom->PasCodeur = SWAP_SHORT_BYTES(eeprom->PasCodeur);
}

char *_eeprom_current_to_string(unsigned char current) {
	switch (current) {
		case 0:
			return "20.0%";
		case 1:
			return "46.7%";
		case 2:
			return "73.3%";
		case 3:
		default:
			return "100%";
	}
}

/* alternative to the read_eeprom, where only one byte is read from EEPROM.
 * data is the address of a single byte, not an array. */
int _read_byte_from_eeprom(int fd, mcmtii_axis axis,
		unsigned char address, unsigned char *data) {
	/* request:	'J', address, 0, 0, 0
	 * response:	the byte from EEPROM at requested address
	 */
	unsigned char req[6];
	if (address > 0x2f || data == NULL) return MCMTII_RET_ARGERR;
	req[0] = _axis_to_byte(axis);
	req[1] = 'J';
	req[2] = address;
	req[3] = 0; req[4] = 0; req[5] = 0;
	return _request_response_8byte_command(fd, req, data, 1);
}

char *mcmtii_strerror(int errno) {
	switch (errno) {
		case MCMTII_RET_OK:
			return "no error";
		case MCMTII_RET_ERR:
			return "general error";
		case MCMTII_RET_ARGERR:
			return "bad argument";
		case MCMTII_RET_TIMEOUT:
			return "timeout";
		case MCMTII_RET_UNINIT:
			return "operation cannot be done because it relies on some uninitialized date";
		case MCMTII_RET_INCOMPLETE:
			return "response is incomplete but successful otherwise";
		default:
			return "unknown error code";
	}
}

