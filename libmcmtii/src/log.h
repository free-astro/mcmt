#ifndef _MCMTII_LOG_H
#define _MCMTII_LOG_H

#define LOGFILE "libmcmtii.log"

void mcmtii_log_open_file();
void mcmtii_log_close_file();
void mcmtii_log_text(char *title, const char *format, ...);
void mcmtii_log_binary(char *title, unsigned char *buf, int buflen);

#endif
