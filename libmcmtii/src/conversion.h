#ifndef _CONVERSION_H_
#define _CONVERSION_H_

#include "mcmtii.h"

double _convert_microsteps_to_ha(unsigned int encoder_value);
double _convert_microsteps_to_dec(unsigned int encoder_value);

int _convert_double_to_microsteps(mcmtii_axis axis,
		double value, unsigned int *steps, int allow_short_path);

/* all code listed below, except step 4, is from XEphem 3.7.5 */

/* step 1, Julian day calculation. */
#define MJD0  2415020.0
#define J2000 (2451545.0 - MJD0)      /* yes, 2000 January 1 at 12h */

/* Given a date in months, mn, days, dy, years, yr,
 * return the modified Julian date (number of days elapsed since 1900 jan 0.5),
 * *mjd.
 */
void cal_mjd (int mn, double dy, int yr, double *mjp);

/* given an mjd, truncate it to the beginning of the whole day */
double mjd_day(double mj);

/* given an mjd, return the number of hours past midnight of the whole day */
double mjd_hr(double mj);

/* step 2, greenwich mean sidereal time */
#define SIDRATE         .9972695677

/* insure 0 <= *v < r. */
void range (double *v, double r);

/* gmst0() - return Greenwich Mean Sidereal Time at 0h UT; stern */
double gmst0 (
double mj);      /* date at 0h UT in Julian days since MJD0 */

/* given a modified Julian date, mj, and a universally coordinated time, utc,
 * return greenwich mean siderial time, *gst.
 * N.B. mj must be at the beginning of the day.
 */
void utc_gst (double mj, double utc, double *gst);

/* step 3, local sidereal time, in hours */

#define degrad(x)       ((x)*M_PI/180.)
#define raddeg(x)       ((x)*180./M_PI)
#define deghr(x)        ((x)/15.)
#define radhr(x)        deghr(raddeg(x))

/* given the modified Julian date, mj, find the mean obliquity of the
 * ecliptic, *eps, in radians.
 *
 * IAU expression (see e.g. Astron. Almanac 1984); stern
 */
void obliquity (double mj, double *eps);

void now_lst(double mjd, double *lstp);


/* nutation.c */
void nutation (double mj,
		double *deps,	/* on input:  precision parameter in arc seconds */
		double *dpsi);


/* sphcart.c */ 
void sphcart (	double l, double b, double r,		/* source: spherical coordinates */
		double *x, double *y, double *z);	/* result: rectangular coordinates */

void cartsph (	double x, double y, double z,		/* source: rectangular coordinates */
		double *l, double *b, double *r);	/* result: spherical coordinates */


/* step 4, get the hour angle from right ascension - not from XEphem */
double ra_to_ha(double ra);
double ha_to_ra(double ha);
double ha_dec_to_az(double ha, double dec);

#endif
