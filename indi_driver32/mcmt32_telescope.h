/* This is the INDI Telescope driver for the MCMT32 motor unit for telescopes.
 * Developped by Vincent from free-astro.vinvin.tf
 * Documentation on the MCMT32, libmcmt32 and this driver is here:
 * http://free-astro.vinvin.tf/index.php/MCMT32
 */

#ifndef _MCMT32_SCOPE_H 
#define _MCMT32_SCOPE_H 

#include "inditelescope.h"
//#include "indiguiderinterface.h"

class mcmt32_Telescope: public INDI::Telescope//, public INDI::GuiderInterface
{
	public:
		mcmt32_Telescope();

		// General device functions
		bool Connect();
		bool Disconnect();
		const char *getDefaultName();
		bool initProperties();
		void ISGetProperties (const char *dev);
		bool updateProperties();
		bool ISNewSwitch (const char *dev, const char *name, ISState *states, char *names[], int n);

	protected:
		// Telescope specific functions
		bool ReadScopeStatus();
		bool Goto(double, double);
		bool Abort();
		bool canPark();
		bool MoveNS(INDI_DIR_NS dir, TelescopeMotionCommand command);
		bool MoveWE(INDI_DIR_WE dir, TelescopeMotionCommand command);
		bool canSync();
		bool Sync(double, double);

	private:
		int fd;
		double currentRA, currentDEC;
		double target_RA, target_DEC;
		double parkRA, parkDEC;
		
		ISwitch SwAllowFlip[1];
		ISwitchVectorProperty SwVectorAllow;
		ISwitch SwIsFlipped[1];
		ISwitchVectorProperty SwVectorStatus;
		/* Firmware */
		IText   FirmwareT[5];
		ITextVectorProperty FirmwareTP;
		

};

#endif
