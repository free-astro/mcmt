/* This is the INDI Telescope driver for the mcmt32 motor unit for telescopes.
 * Developped by Vincent from free-astro.vinvin.tf
 * Documentation on the mcmt32, libmcmt32 and this driver is here:
 * http://free-astro.vinvin.tf/index.php/mcmt32
 */

#include <sys/time.h>
#include <math.h>
#include <memory>

#define POLLMS 1000
#ifdef TESTING
#define DEFAULT_NAME "mcmt32 Telescope TEST"
#else
#define DEFAULT_NAME "mcmt32 Telescope"
#endif
#define UNINIT_RA_VALUE -1.0
#define UNINIT_DEC_VALUE -91.0

#include "mcmt32_telescope.h"
#include "indicom.h"
#include "mcmt32.h"

//const float SIDRATE	= 0.004178;	/* sidereal rate, degrees/s */
//const int   SLEW_RATE	= 1;  		/* slew rate, degrees/s */

std::unique_ptr<mcmt32_Telescope> mcmt32(new mcmt32_Telescope());

/**************************************************************************************
 ** Initilize mcmt32_Telescope object
 ***************************************************************************************/
void ISInit()
{
    static int isInit=0;
    if (isInit)
	return;
    if (mcmt32.get() == 0)
    {
	isInit = 1;
	mcmt32.reset(new mcmt32_Telescope());
    }
}

/**************************************************************************************
 ** Return properties of device.
 ***************************************************************************************/
void ISGetProperties (const char *dev)
{
    ISInit();
    mcmt32->ISGetProperties(dev);
}

/**************************************************************************************
 ** Process new switch from client
 ***************************************************************************************/
void ISNewSwitch (const char *dev, const char *name, ISState *states, char *names[], int n)
{
    ISInit();
    mcmt32->ISNewSwitch(dev, name, states, names, n);
}

/**************************************************************************************
 ** Process new text from client
 ***************************************************************************************/
void ISNewText (const char *dev, const char *name, char *texts[], char *names[], int n)
{
    ISInit();
    mcmt32->ISNewText(dev, name, texts, names, n);
}

/**************************************************************************************
 ** Process new number from client
 ***************************************************************************************/
void ISNewNumber (const char *dev, const char *name, double values[], char *names[], int n)
{
    ISInit();
    mcmt32->ISNewNumber(dev, name, values, names, n);
}

/**************************************************************************************
 ** Process new blob from client
 ***************************************************************************************/
void ISNewBLOB (const char *dev, const char *name, int sizes[], int blobsizes[],
	char *blobs[], char *formats[], char *names[], int n)
{
    ISInit();
    mcmt32->ISNewBLOB(dev, name, sizes, blobsizes, blobs, formats, names, n);
}

/**************************************************************************************
 ** Process snooped property from another driver
 ***************************************************************************************/
void ISSnoopDevice (XMLEle *root)
{
    mcmt32->ISSnoopDevice(root);
}

mcmt32_Telescope::mcmt32_Telescope()
{
    fd = -1;
    SetTelescopeCapability (TELESCOPE_CAN_PARK | TELESCOPE_CAN_SYNC | TELESCOPE_CAN_ABORT | TELESCOPE_HAS_LOCATION,3);
    target_RA = UNINIT_RA_VALUE;
    target_DEC = UNINIT_DEC_VALUE;
}

bool mcmt32_Telescope::initProperties()
{
    INDI::Telescope::initProperties();
    
    IUFillSwitch(SwAllowFlip, "ALLOW", "Allow flip", ISS_OFF);
    IUFillSwitch(SwIsFlipped, "STATUS", "Flip status", ISS_OFF);
    IUFillSwitchVector(&SwVectorAllow, SwAllowFlip, 1, getDeviceName(),
	    "ALLOW_FLIP", "Allow flip", MOTION_TAB, IP_RW,
	    ISR_NOFMANY, 2, IPS_OK);
    IUFillSwitchVector(&SwVectorStatus, SwIsFlipped, 1, getDeviceName(),
	    "FLIP_STATUS", "Flip status", MOTION_TAB, IP_RW,
	    ISR_NOFMANY, 2, IPS_IDLE);
    
    IUFillNumber(&LocationN[LOCATION_LATITUDE],"LAT","Lat (dd:mm:ss)","%010.6m",-90,90,0,0.0);
    IUFillNumber(&LocationN[LOCATION_LONGITUDE],"LONG","Lon (dd:mm:ss)","%010.6m",0,360,0,0.0 );
    IUFillNumber(&LocationN[LOCATION_ELEVATION],"ELEV","Elevation (m)","%g",-200,10000,0,0 );
    IUFillNumberVector(&LocationNP,LocationN,3,getDeviceName(),"GEOGRAPHIC_COORD","Scope Location",SITE_TAB,IP_RW,60,IPS_OK);
    
    return true;
}

void mcmt32_Telescope::ISGetProperties (const char *dev)
{
    //IDLog("ISGetProperties()\n");
    INDI::Telescope::ISGetProperties(dev);
    
    if(isConnected()) {
	defineSwitch(&SwVectorAllow);
	defineSwitch(&SwVectorStatus);
    }
}

bool mcmt32_Telescope::updateProperties()
{
    //IDLog("updateProperties()\n");
    INDI::Telescope::updateProperties();
    
    if (isConnected()) {
	defineSwitch(&SwVectorAllow);
	defineSwitch(&SwVectorStatus);
    } else {
	deleteProperty(SwVectorAllow.name);
	deleteProperty(SwVectorStatus.name);
    }
    return true;
}

bool mcmt32_Telescope::ISNewSwitch (const char *dev, const char *name, ISState *states, char *names[], int n)
{
    //IDLog("Enter IsNewSwitch for %s\n", name);
    //for (int x=0; x<n; x++)
    //	IDLog("Switch %s %d\n", names[x], states[x]);
    
    if (!strcmp(dev, getDeviceName())) {
	if (!strcmp(name,"ALLOW_FLIP")) {
	    IUUpdateSwitch(&SwVectorAllow, states, names, n);
	    SwVectorAllow.s = IPS_OK;
	    //IUResetSwitch(&SwVectorAllow);
	    IDSetSwitch(&SwVectorAllow, NULL);
	    return true;
	}
	else {
	    if (!strcmp(name,"FLIP_STATUS")) { 
		IUUpdateSwitch(&SwVectorStatus, states, names, n);
		if (SwIsFlipped[0].s == ISS_ON){
			mcmt32_set_flip_status(MCMT32_FLIP_FLIPPED);
			}
		else{
			mcmt32_set_flip_status(MCMT32_FLIP_NONE);
		}
	    SwVectorStatus.s = IPS_OK;
	    IDSetSwitch(&SwVectorStatus, NULL);
	    }
	}
    }
    
    return INDI::Telescope::ISNewSwitch(dev,name,states,names,n);
}
/**************************************************************************************
 ** Client is asking us to establish connection to the device
 ***************************************************************************************/
bool mcmt32_Telescope::Connect()
{
    IDLog("Connect() tou %s\n", PortT[0].text);
#ifdef TESTING
    TrackState = SCOPE_IDLE;
    fd = 1;
    SetTimer(POLLMS);
    return true;
#endif
    fd = mcmt32_serial_link_open(PortT[0].text);
    if (fd == -1) {
	IDMessage(getDeviceName(), "mcmt32 Telescope failed to connect.");
	IDLog("open failed fd=-1");
	fprintf(stderr, "open failed fd=-1");
	return false;
    }
    else {
	IDLog("open ok fd=%d",fd);
    }
    if (this->ReadScopeStatus() == false) {
	IDLog("ReadScopeStatus Failed\n");
	IDMessage(getDeviceName(), "mcmt32 Telescope failed to communicate, disconnecting (readstatus).");
	mcmt32_serial_link_close(fd);
	fd = -1;
	return false;
    }
    
    if (TrackState == SCOPE_SLEWING ){
	mcmt32_stop_all_slewing(fd);
    }
    
    if (this->ReadScopeStatus() == false) {
	IDMessage(getDeviceName(), "mcmt32 Telescope failed to communicate, disconnecting (readstatus).");
	IDLog("ReadScopeStatus v2 Failed\n");
	mcmt32_serial_link_close(fd);
	fd = -1;
	return false;
    }
    
    if (TrackState == SCOPE_IDLE ){
	int retval = mcmt32_is_parked(fd);
	if (retval == MCMT32_RET_ERR) {
	    IDMessage(getDeviceName(), "mcmt32 Telescope failed to get parked status, disconnecting (is_parked).");
	    mcmt32_serial_link_close(fd);
	    fd = -1;
	    return false;
	}
	if (retval)
	    TrackState = SCOPE_PARKED;
    }
    mcmt32_set_flip_status(MCMT32_FLIP_NONE);
    //mcmt32_set_flip_status(MCMT32_FLIP_FLIPPED);
    IDMessage(getDeviceName(), "mcmt32 Telescope connected successfully!");
    IDLog("mcmt32 Telescope connected successfully \n");
    
    //	mcmt32_get_latitude_longitude(fd, &lat, &lon);
    int retval=mcmt32_init_ratios(fd);
    
    if (retval == MCMT32_RET_ERR) {
	IDMessage(getDeviceName(), "mcmt32 Telescope failed to communicate (init_ratios), disconnecting.");
	mcmt32_serial_link_close(fd);
	fd = -1;
	return false;
    }
    
    //    retval=mcmt32_init_pos(fd);
    double lat, lon, alt; 
    
    mcmt32_init_pos(fd, &lat, &lon, &alt);
    LocationN[0].value=lat;
    LocationN[1].value=lon; 
    LocationN[2].value=alt; 
    LocationNP.p = IP_RO;
    
    
    if (retval == MCMT32_RET_ERR) {
	IDMessage(getDeviceName(), "mcmt32 Telescope failed to communicate,(init_pos) disconnecting.");
	mcmt32_serial_link_close(fd);
	fd = -1;
	return false;
    }
    // This timer calls Telescope::TimerHit(), which calls ReadScopeStatus and
    // rearms the timer to 1000 ms, so if TimerHit is not redefined here, our
    // POLLMS will be used only once (inditelescope.cpp)
    SetTimer(POLLMS);
    return true;
}

/**************************************************************************************
 ** Client is asking us to terminate connection to the device
 ***************************************************************************************/
bool mcmt32_Telescope::Disconnect()
{
    IDLog("Disconnect()\n");
    if (fd != -1) {
#ifndef TESTING
	mcmt32_stop_slewing(fd, MCMT32_AXIS_RA);
	mcmt32_stop_slewing(fd, MCMT32_AXIS_DEC);
	mcmt32_serial_link_close(fd);
#endif
	fd = -1;
	IDMessage(getDeviceName(), "mcmt32 Telescope disconnected successfully.");
	return true;
    } else {
	IDMessage(getDeviceName(), "mcmt32 Telescope was not connected.");
	return false;
    }
}

/**************************************************************************************
 ** INDI is asking us for our default device name
 ***************************************************************************************/
const char * mcmt32_Telescope::getDefaultName()
{
    return DEFAULT_NAME;
}

/**************************************************************************************
 ** Client is asking us to slew to a new position
 ***************************************************************************************/
bool mcmt32_Telescope::Goto(double ra, double dec)
{
    int retval;
    IDLog("Goto() RA=%g, DEC=%g\n", ra, dec);
    if (fd == -1) {
	IDMessage(getDeviceName(), "mcmt32 Telescope is not connected.");
	return false;
    }
    // TrackState is of type enum TelescopeStatus (from inditelescope.h)
    // { SCOPE_IDLE, SCOPE_SLEWING, SCOPE_TRACKING, SCOPE_PARKING, SCOPE_PARKED };
    if (TrackState == SCOPE_SLEWING) {
	// I have no idea yet if it supports new values while slewing
	// so we'll say it does not for now.
	IDMessage(getDeviceName(), "Slewing already in progress, please wait");
	return false;
    }
    if (TrackState != SCOPE_IDLE && TrackState != SCOPE_TRACKING) {
	// is parking or parke d
	IDMessage(getDeviceName(), "Slewing is not a possible operation right know");
	return false;
    }
    
    retval = mcmt32_slew_absolute(fd, ra, dec, LocationN[LOCATION_LONGITUDE].value, (SwAllowFlip->s == ISS_OFF));
    if (retval == MCMT32_RET_OK) {
	IDMessage(getDeviceName(), "Slewing to RA: %g - DEC: %g", ra, dec);
	target_RA = ra;
	target_DEC = dec;
#ifdef TESTING
	pTrackState = SCOPE_IDLE;	// instantaneous slew
#else
	TrackState = SCOPE_SLEWING;
#endif
    } else if (retval == MCMT32_RET_UNINIT) {
	// if sync is not supported by the client, sync should be done here
	// but if it is, then it should have been done before the Goto and
	// this UNINIT case won't happen
	return this->Sync(ra, dec);
    } else if (retval == MCMT32_RET_ERR) {
	IDMessage(getDeviceName(), "ERROR: an unknown error occurred while "
		"trying to slew");
	return false;
    }
    return true;
}

/**************************************************************************************
 ** Client is asking us to abort our motion
 ***************************************************************************************/
bool mcmt32_Telescope::Abort()
{
    IDLog("Abort()\n");
    mcmt32_stop_slewing(fd, MCMT32_AXIS_RA);
    mcmt32_stop_slewing(fd, MCMT32_AXIS_DEC);
    target_RA = UNINIT_RA_VALUE; target_DEC = UNINIT_DEC_VALUE;
    TrackState = SCOPE_IDLE;
    IDMessage(getDeviceName(), "mcmt32 Telescope slewing stopped.");
    // reseting properties and switches
    IUResetSwitch(&MovementWESP);
    MovementWESP.s = IPS_IDLE;
    IDSetSwitch(&MovementWESP, NULL);
    IUResetSwitch(&MovementNSSP);
    MovementNSSP.s = IPS_IDLE;
    IDSetSwitch(&MovementNSSP, NULL);
    return true;
}

/**************************************************************************************
 ** Client is asking us to report telescope status, and called by the timer set in Connect()
 ***************************************************************************************/
bool mcmt32_Telescope::ReadScopeStatus()
{
    int retval1, retval2;
    mcmt32_flip_status flip_status;
    if (fd == -1) {
	IDMessage(getDeviceName(), "mcmt32 Telescope is not connected.");
	return false;
    }
    //IDLog("ReadScopeStatus()\n");
    
    // check status of the telescope first
    retval1 = mcmt32_device_ready(fd, MCMT32_AXIS_RA);
    retval2 = mcmt32_device_ready(fd, MCMT32_AXIS_DEC);
    if (retval1 == -1 || retval2 == -1) {
	IDMessage(getDeviceName(), "mcmt32 Telescope has some communication problems (ping).");
	return false;
    }
    if (retval1 && retval2)	{	// telescope is ready
	if (target_RA != UNINIT_RA_VALUE || target_DEC != UNINIT_DEC_VALUE) {
	    IDMessage(getDeviceName(), "Telescope slew is complete.");
	    target_RA = UNINIT_RA_VALUE; target_DEC = UNINIT_DEC_VALUE;
	    // end of slew is also end of flip
	    flip_status = mcmt32_get_flip_status();
	    if (flip_status == MCMT32_FLIP_FLIPPED)
		SwIsFlipped[0].s = ISS_ON;
	    else if (flip_status == MCMT32_FLIP_NONE)
		SwIsFlipped[0].s = ISS_OFF;
	}
	TrackState = SCOPE_IDLE;
    } else {
	TrackState = SCOPE_SLEWING;
	// if (target_RA == UNINIT_RA_VALUE && target_DEC == UNINIT_DEC_VALUE)
	// this is external slewing (hand controller)
    }
    
    // then get its current position
    if (mcmt32_conversion_is_init()) {
	double ra, dec;
	if (mcmt32_get_current_position(fd, &ra, &dec, LocationN[LOCATION_LONGITUDE].value) == MCMT32_RET_OK) {
	    IDLog("Current RA: %g - DEC: %g\n", ra, dec);
	    NewRaDec(ra, dec);
	} else {
	    IDMessage(getDeviceName(), "mcmt32 Telescope has some problems, "
		    "unable to get the current position.");
	    return false;
	}
    }
    return true;
}

bool mcmt32_Telescope::MoveNS(INDI_DIR_NS dir, TelescopeMotionCommand command) {
    int retval;
    IDLog("MoveNS() dir=%s\n", dir == DIRECTION_NORTH ? "north" : "south");
    if (fd == -1) {
	IDMessage(getDeviceName(), "mcmt32 Telescope is not connected.");
	return false;
    }
    
    if(command==MOTION_START){
	if (dir == DIRECTION_NORTH) {
	    retval = mcmt32_move(fd, MCMT32_NORTH, MCMT32_SPEED_MED);
	} else {	// MOTION_SOUTH
	    retval = mcmt32_move(fd, MCMT32_SOUTH, MCMT32_SPEED_MED);
	}
	if (retval == MCMT32_RET_OK) {
	    TrackState = SCOPE_SLEWING;
	    IDMessage(getDeviceName(), "Moving %s at medium speed",
		    dir == DIRECTION_NORTH ? "north" : "south");
	    return true;
	}	
    }	
    
    if(command==MOTION_STOP){
	retval = mcmt32_stop_slewing(fd, MCMT32_AXIS_DEC);
	if (retval == MCMT32_RET_OK) {
	    if (mcmt32_device_ready(fd, MCMT32_AXIS_RA))
		TrackState = SCOPE_IDLE;
	    IDMessage(getDeviceName(), "Stopped %s motion",
		    dir == DIRECTION_NORTH ? "north" : "south");
	    return true;
	}		
    }
    
    
    /* code from inditelescope.cpp:MoveNS() */
    IUResetSwitch(&MovementNSSP);
    MovementNSSP.s = IPS_IDLE;
    IDSetSwitch(&MovementNSSP, NULL);
    return false;
}

bool mcmt32_Telescope::MoveWE(INDI_DIR_WE dir, TelescopeMotionCommand command) {
    int retval;
    IDLog("MoveWE() dir=%s\n", dir == DIRECTION_WEST ? "west" : "east");
    if (fd == -1) {
	IDMessage(getDeviceName(), "mcmt32 Telescope is not connected.");
	return false;
    }
    if(command==MOTION_START){
	if (dir == DIRECTION_WEST) {
	    retval = mcmt32_move(fd, MCMT32_WEST, MCMT32_SPEED_MED);
	} else {	// MOTION_EAST
	    retval = mcmt32_move(fd, MCMT32_EAST, MCMT32_SPEED_MED);
	}
	if (retval == MCMT32_RET_OK) {
	    TrackState = SCOPE_SLEWING;
	    IDMessage(getDeviceName(), "Moving %s at medium speed",
		    dir == DIRECTION_WEST ? "west" : "east");
	    return true;
	}	
    }	
    
    if(command==MOTION_STOP){
	retval = mcmt32_stop_slewing(fd, MCMT32_AXIS_RA);
	if (retval == MCMT32_RET_OK) {
	    if (mcmt32_device_ready(fd, MCMT32_AXIS_DEC))
		TrackState = SCOPE_IDLE;
	    IDMessage(getDeviceName(), "Stopped %s motion",
		    dir == DIRECTION_WEST ? "west" : "east");
	    return true;
	}		
    }
    
    /* code from inditelescope.cpp:MoveWE() */
    IUResetSwitch(&MovementWESP);
    MovementWESP.s = IPS_IDLE;
    IDSetSwitch(&MovementWESP, NULL);
    return false;
}


bool mcmt32_Telescope::canPark() {
    // parking is not implemented yet	(this is default behaviour too)
    // we could still set the ParkMode flag to true in the mcmt32 to make it not move.
    return false;
}

/* Sync is the method used to inform the telescope about its position (if I
 * understand right the following documentation of the Sync method:
 * "Set the telescope current RA and DEC coordinates to the supplied RA and DEC
 * coordinates."
 * http://www.indilib.org/api/classINDI_1_1Telescope.html
 * By default in inditelescope.cpp canSync returns false, we need to redefine
 * canSync and Sync below in order to use the Sync functionality */

bool mcmt32_Telescope::canSync() {
    return true;
}

bool mcmt32_Telescope::Sync(double ra, double dec) {
    IDLog("Sync() RA=%g, DEC=%g\n", ra, dec);
    if (mcmt32_set_current_position(fd, ra, dec, LocationN[LOCATION_LONGITUDE].value) == MCMT32_RET_OK) {
	IDMessage(getDeviceName(), "Sync complete. This destination has been "
		"taken as current absolute position for future "
		"relative GoTo requests");
	NewRaDec(ra, dec);	// inform about position
	return true;
    } else {
	IDMessage(getDeviceName(), "ERROR while syncing. "
		"Relative GoTo is not initialized");
	return false;
    }
}

