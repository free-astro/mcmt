#define XTAL_FREQ	20000000
#define PIC_CLK 	XTAL_FREQ

////////////////////////////////////////////////////////////////////////////
////                               MCMTII                               ////
////                                                                    ////
////                                                                    ////
////             Prog:   Patrick Dufour                                 ////
////             Ouranos Instrument                                     ////
////             Ver:    2.7            R: 6-beta                       ////
////             D�but:  3 septembre 2001                               ////
////             Compilateur: HT-PICC                                   ////
////                                                                    ////
////                  Copyright 2002-2006                               ////
//// This source code may only be used for AIM development purpose      ////
//// No other use, reproduction  or distribution is permitted without   ////
//// written permission.                                                ////
////                                                                    ////
////////////////////////////////////////////////////////////////////////////

#include	<pic.h>
#include	<stdio.h>
#include	<math.h>
#include	"always.h"
#include	"delay.h"

#include	"delay.c"

/****************************************************************
*               Definition de la version                        *
****************************************************************/

#define version		"MCMTII V2.7.6-beta\r"

/****************************************************************
*                  Definition des Output                        *
****************************************************************/

/*
 *      Bits de control des moteurs *      RA0..3 pour la phase A
 *      RD0..3 pour la phase B
 *
 */

static bit	PHASE @ (unsigned)&PORTA*8+0;			// bit0 in port A - Selection de la phase
static bit	WR_N @ (unsigned)&PORTA*8+1;			// bit1 in port A - MAX505
static bit	LDAC_N @ (unsigned)&PORTA*8+2;			// bit2 in port A - MAX505
static bit	DIR_PHASEA @ (unsigned)&PORTA*8+3;		// bit3 in port A - Direction du courant dans la phase
static bit	Control_TX @ (unsigned)&PORTA*8+4;		// Controle de la ligne TX pour MCMT Laurent B.
static bit	DIR_PHASEB @ (unsigned)&PORTA*8+5;		// bit5 in port A

static bit	COURANT_M3 @ (unsigned)&PORTC*8+2;		// bit2 in port C - Amplitude du courant dans le moteur
static bit	COURANT_M4 @ (unsigned)&PORTC*8+5;		// bit5 in port C

/*
 *      Bits de control des LED
 *
 */

static bit	LED_A_Moins @ (unsigned)&PORTC*8+0;
static bit	LED_A_Plus @ (unsigned)&PORTC*8+1;

/*
 *      Bits de control pour output auxiliaire
 *
 */

static bit	AUX_1 @ (unsigned)&PORTB*8+6;
static bit	AUX_2 @ (unsigned)&PORTB*8+7;

/****************************************************************
*                  Definition des Input                         *
****************************************************************/

/*
 *      Bits des signaux d'entrees
 *
 */

static bit	ST4_Plus @ (unsigned)&PORTB*8+0;
static bit	ST4_Moins @ (unsigned)&PORTB*8+1;
static bit	RAQ_Plus @ (unsigned)&PORTB*8+2;
static bit	RAQ_Moins @ (unsigned)&PORTB*8+3;
static bit	VIT1 @ (unsigned)&PORTB*8+4;
static bit	VIT2 @ (unsigned)&PORTB*8+5;
static bit	PEC1 @ (unsigned)&PORTB*8+6;
static bit	PEC2 @ (unsigned)&PORTB*8+7;

/****************************************************************
*     Adresses des data dans la memoire EEPROM                  *
****************************************************************/
#define VM_Guidage		0	/* vitesse moyenne de guidage */
#define VM_Guidage_LSB		2	/* voir commandes 'r' et 'R' */
#define VM_Corec_Plus		3	/* vitesse moyenne de correction + */
#define VM_Corec_Moins		5	/* vitesse moyenne de correction - */
#define VM_Lent			7	/* vitesse moyenne de pointage lent */
#define VM_Rapide		9	/* vitesse moyenne de pointage rapide */
#define VM_Acce			11	/* ? */
#define Dir_Guidage		12	/* sens de rotation du moteur pour guidage, voir 'R' */
#define Dir_Corec_Plus		13
#define Dir_Corec_Moins		14
#define Dir_Lent_Plus		15
#define Dir_Lent_Moins		16
#define Dir_Rapide_Plus		17
#define Dir_Rapide_Moins	18
#define Resol_Guidage		19
#define Resol_Corec_Plus	20
#define Resol_Corec_Moins	21
#define Resol_Lent		22
#define Resol_Rapide		23
#define Courant_Guidage		24	/* courant envoye aux moteurs pour le guidage */
#define Courant_Lent		25	/* courant envoye aux moteurs pour le pointage lent */
#define Courant_Rapide		26	/* courant envoye aux moteurs pour le pointage rapide */
#define Sens_Raq_Led		27	/* NEVER USED */

#define PasCodeur		28
#define ParkMode		30	/* premier bit: park mode activated */
#define Raq_type_can_address	31	/* NEVER USED */

#define PEC_ENABLED		47


/****************************************************************
*                  D�finition de constantes                     *
****************************************************************/

#define		NOACK	0x15
#define		ACK	0x06


/*
 * Vecteur determinant la sequence des micropas
 * pour une resolution de 128 micropas par pas
 * selon une courbe sinusoidale
 *
 */

unsigned char const	T_STEP[256] =	{

0,3,6,9,13,16,19,22,25,28,31,34,37,41,44,47,50,53,56,59,62,
65,68,71,74,77,80,83,86,89,92,95,98,100,103,106,109,112,115,
117,120,123,126,128,131,134,136,139,142,144,147,149,152,154,
157,159,162,164,167,169,171,174,176,178,180,183,185,187,189,
191,193,195,197,199,201,203,205,207,208,210,212,214,215,217,
219,220,222,223,225,226,228,229,231,232,233,234,236,237,238,
239,240,241,242,243,244,245,246,247,247,248,249,249,250,251,
251,252,252,253,253,253,254,254,254,255,255,255,255,255,255,
255,255,255,255,255,254,254,254,253,253,253,252,252,251,251,
250,249,249,248,247,247,246,245,244,243,242,241,240,239,238,
237,236,234,233,232,231,229,228,226,225,223,222,220,219,217,
215,214,212,210,208,207,205,203,201,199,197,195,193,191,189,
187,185,183,180,178,176,174,171,169,167,164,162,159,157,154,
152,149,147,144,142,139,136,134,131,128,126,123,120,117,115,
112,109,106,103,100,98,95,92,89,86,83,80,77,74,71,68,65,62,
59,56,53,50,47,44,41,37,34,31,28,25,22,19,16,13,9,6,3

};

#include	"table.c"


/***************************************************************************
 ******         Definition des variables        ****************************
 ***************************************************************************/

/*
 * Variables globales
 *
 */

// Drapeau pour le positionnement en pas

static bank1 bit 		Flag_DIR_PHASEA, Flag_DIR_PHASEB, Old_Dir_M, Dir_M;
static bank1 bit 		Command_Rec, Inverse_Sens;
static bank1 bit 		Init_PEC, Pos_Pec1, Stop, Pointage, Act_Corr_pointage, Guide, Change_Mode, ACCU_ADD;

static bank1 unsigned char 	DIR_PHASE, DELAY_M_LSB_Compteur, DELAY_M_LSB, Niveau_Courant_M;

static bank1 unsigned char 	Buf_Count, Num_Carte, Clavier, resol, Mode_Vitesse, Accel_M;

static bank1 unsigned char  	Buf_Serial[8];

static bank1 unsigned int  	NUM_STEP_M_A, NUM_STEP_M_B, DELAY_M, DELAY_M_Guide, DELAY_TARGET, PEC_STEP, PEC_COMP, TEMP_PEC_STEP;

static bank1 unsigned long	TEMP_NUM_POSI, NUM_POSI, Num_Pointage;
static bank2 unsigned char	Old_Mode_Vitesse, PEC_BASE, PEC_COUNT, Envoyer_Info;
static bank2 unsigned int	Compt_target, Compt_Correc, Compt_Flash;
static bank2 bit		PEC_ACTIVATED, PEC_WRITE, PEC_START, DECMODE, FOCUSMODE, Park, Raq_type_can, OKstep;


/*************************************************************************************/
/********************************      Fonctions      ********************************/
/*************************************************************************************/

#define bit_set(var,bitno) ((var) |= 1 << (bitno))
#define bit_clr(var,bitno) ((var) &= ~(1 << (bitno)))

#define testbit_on(data,bitno) ((data>>bitno)&0x01)

/****************************************************************
*       Function Name:  WRITE_EEPROM                        *
*       Return Value:   none                                    *
*       Parametres:     adresse de la variable, data            *
*       Description:    ecrit un octet ds eeprom                *
****************************************************************/

void
WRITE_EEPROM(unsigned char data_address, unsigned char datavalue)
{
	//data_address &= 0x7F;

	EEADR = data_address;   			// establish address to write to
	EEDATA = datavalue;     			// get data to write
	EEPGD = 0;              			// set for access to data memory
	WREN = 1;               			// set eeprom write enable bit

	while(GIE)
		GIE = 0;

	EECON2 = 0x55;          			// required
	EECON2 = 0xAA;          			//  write
	WR = 1;                 			//   sequence
	GIE = 1;                			// ok to re-enable interrupts
	do {                    			// wait for write to complete
		; // nothing
	} while(WR);
	WREN = 0;               			// clear eeprom write enable bit
	EEIF = 0;               			// clear write complete interrupt flag
}

/****************************************************************
*       Function Name:  WRITE_EEPROM_INT                        *
*       Return Value:   void                                    *
*       Parametres:     unsigned int                            *
*       Description:    ecriture 16 bits ds eeprom              *
****************************************************************/

	void
WRITE_EEPROM_INT(unsigned char address, unsigned int datalongvalue)
{
	unsigned char * var_pointeur;

	var_pointeur = (unsigned char *)&datalongvalue;
	WRITE_EEPROM( address, var_pointeur[0] );
	WRITE_EEPROM( address+1, var_pointeur[1] );
}

/****************************************************************
*       Function Name:  READ_EEPROM                             *
*       Return Value:   byte                                    *
*       Parametres:     adresse de la variable                  *
*       Description:    Lit un octet ds eeprom                  *
****************************************************************/
	unsigned char
READ_EEPROM(unsigned char data_address)
{
	//data_address &= 0x7F;

	EEADR = data_address;
	EEPGD = 0;
	RD = 1;
	return(EEDATA);

}

/****************************************************************
*       Function Name:  READ_EEPROM_INT                         *
*       Return Value:   int                                     *
*       Parametres:     adresse de la variable                  *
*       Description:    lecture 16 bits ds eeprom               *
****************************************************************/

	unsigned int
READ_EEPROM_INT(unsigned char address)
{
	unsigned char * var_pointeur;
	unsigned int RESULT;

	var_pointeur = (unsigned char *)&RESULT;
	var_pointeur[0] = READ_EEPROM( address );
	var_pointeur[1] = READ_EEPROM( address+1 );

	return(RESULT);
}

/****************************************************************
*       MACRO Name:  clear_usart_errors_inline			*
*       Return Value:   None					*
*       Parametres:     None					*
*       Description:    Traitement des erreurs USART		*
****************************************************************/

unsigned char dummy;

#define clear_usart_errors_inline	\
if (OERR)				\
{					\
	TXEN=0;				\
	TXEN=1;				\
	CREN=0;				\
	CREN=1;				\
}					\
if (FERR)				\
{					\
	dummy=RCREG;			\
	TXEN=0;				\
	TXEN=1;				\
}

/****************************************************************
*       Function Name:  putch					*
*       Return Value:   None					*
*       Parametres:     None					*
*       Description:    writes a character to the serial port	*
****************************************************************/

void
putch(unsigned char c)
{
	Control_TX = 1;
	while(!TXIF)					//Registre de transfert pr�t?
	{
		clear_usart_errors_inline;
		CLRWDT();
	}
	TXREG=c;
	while(!TRMT);
	Control_TX = 0;
}

/****************************************************************
*       Function Name:  	getch				*
*       Return Value:   None					*
*       Parametres:     None					*
*       Description:    gets a character from the serial port	*
****************************************************************/

unsigned char
getch(void)
{
	while(!RCIF)					//Registre de r�ception pr�t?
	{
		CLRWDT();
		clear_usart_errors_inline;
	}
	return RCREG;
}

/****************************************************************
*       Function Name:  reglage_Courant                         *
*       Return Value:   void                                    *
*       Parametres:                                             *
*       Description:                                            *
****************************************************************/
void
Reglage_Courant(void)
{
	if (Niveau_Courant_M == 1)
	{
		COURANT_M3 = 1;// 46.7%
		COURANT_M4 = 0;
	}

	else if (Niveau_Courant_M == 2)
	{
		COURANT_M3 = 0; // 73.3%
		COURANT_M4 = 1;
	}
	else if (Niveau_Courant_M == 3)
	{
		COURANT_M3 = 1;// 100%
		COURANT_M4 = 1;
	}
	else
	{
		COURANT_M3 = 0; // 20%  Au cas ou la valeur serait differente de 0, 1, 2 ou 3.
		COURANT_M4 = 0;
	}
}

/****************************************************************
*       Function Name:  coupe_Courant                           *
*       Return Value:   void                                    *
*       Parametres:                                             *
*       Description:                				*
****************************************************************/
/* XXX cette methode n'est jamais appelee */
void
Coupe_Courant(void)
{
	PHASE = 0;		// ecriture pour phase A
	PORTD = 0;
	WR_N = 0;
	WR_N = 1;

	PHASE = 1;		// ecriture pour phase B
	PORTD = 0;
	WR_N = 0;
	WR_N = 1;

	LDAC_N = 0;		// Validation du DAC
	LDAC_N = 1;
}


/****************************************************************
*       Function Name:  step_moteur                             *
*       Return Value:   void                                    *
*       Parametres:     Numero selon la table des pas et moteur *
*       Description:    Generation des pas d'un moteur          *
****************************************************************/

void
step_moteur(void)
{
	if(Dir_M)
	{
		NUM_STEP_M_A += resol;
		NUM_STEP_M_B += resol;
		NUM_POSI += resol;
		PEC_STEP += resol;
		if(PEC_STEP > 25599)
		{
			PEC_STEP -= 25600;
		}
	}
	else
	{
		NUM_STEP_M_A -= resol;
		NUM_STEP_M_B -= resol;
		NUM_POSI -= resol;
		if(PEC_STEP < resol)
		{
			PEC_STEP += 25599;
		}
		PEC_STEP -= resol;
	}

	PHASE = 0;    				// ecriture pour phase A
	PORTD = T_STEP[lobyte(NUM_STEP_M_A)];
	WR_N = 0;
	WR_N = 1;

	PHASE = 1;                      	// ecriture pour phase B
	PORTD = T_STEP[lobyte(NUM_STEP_M_B)];
	WR_N = 0;
	WR_N = 1;

	LDAC_N = 0;   				// Validation du DAC
	LDAC_N = 1;

	if((hibyte(NUM_STEP_M_A) & 0x01) == 0x01) 	// positionne ligne DirA.
		DIR_PHASEA = 0;
	else
		DIR_PHASEA = 1;
	if((hibyte(NUM_STEP_M_B) & 0x01) == 0x01) 	// positionne ligne DirB.
		DIR_PHASEB = 0;
	else
		DIR_PHASEB = 1;

}

/****************************************************************
*       Function Name:  MoveNumOfUSteps				*
*       Return Value:   none					*
*       Parameters:     none					*
*       Description:    Move MOTOR X STEPS IN DIR		*
****************************************************************/

void
MoveNumOfUSteps(void)
{
	unsigned int DELAY,DELAY_INIT,temp,temp2,m,y0,index;
	unsigned long i;
	unsigned char en_arret,decel;
	signed char c;

	Clavier = PORTB & 0x0C;

	DELAY_INIT = READ_EEPROM_INT(VM_Corec_Plus);

	if(DELAY_INIT == 0)
		DELAY_INIT = 20000;

	DELAY = DELAY_INIT;

	if ((Mode_Vitesse == 2) || (Mode_Vitesse == 5))
	{
		//	1/16 pas
		resol = 8;
	}
	else
	{
		//	1/4 pas
		resol = 32;
	}

	y0 = DELAY_M_Guide * (int)resol;

	if(y0 < DELAY_M_Guide)
		y0 = 65535;


	/*	Correction en temps pour le pointage - utilise TMR2 comme r�f�rence	*/

	if (DELAY_M_Guide > 0)
	{
		Compt_target = DELAY_M_Guide / 32;
		Compt_Correc = 0;
	}
	else
		Act_Corr_pointage = 0;

	TMR2IF = 0;
	TMR2IE = 1;

	//printf("Nombre de pas a faire = %lu\n\r",Num_Pointage);
	//printf("D�but NUM_POSI = %lu\n\r",NUM_POSI);

	/*	On cherche le nombre de pas � faire pour se rendre au 1/2 pas le plus proche    */

	temp = 0;
	if(NUM_STEP_M_A != 0)
	{
		temp = NUM_STEP_M_A;
		temp /= resol;
		temp *= resol;
		temp2 = NUM_STEP_M_A - temp;
		if(Dir_M)
			temp = resol - temp2;
		else
			temp = temp2;
	}

	CCPR1L = lobyte(DELAY_INIT);			// Valeur du d�lai transf�r�e dans le registre comparateur
	CCPR1H = hibyte(DELAY_INIT);

	temp2 = resol;
	resol = 1;

	//printf("#uPas Actuel = %u\n\r",NUM_STEP_M_A);
	//printf("#uPas correction = %u\n\r",temp);

	while((temp != 0) && (Num_Pointage > 0))
	{
		CLRWDT();
		Num_Pointage -= 1;
		temp -= 1;
		step_moteur();

		TMR1H = 0;				// Remise � z�ro du TIMER1
		TMR1L = 0;

		CCP1IF = 0;				// Remise � z�ro du drapeau de comparaison

		while(!CCP1IF);				// En attente de la fin du d�lai
	}

	resol = temp2;

	//printf("NUM_POSI avant Rampe = %lu\n\r",NUM_POSI);

	i = 0;
	c = 1;
	index = 0;
	Stop = 0;
	en_arret = 0;
	decel = 0;
	m = 5637;

	//y0 = 65535;

	//printf("GO \n\r");

	/*	D�clanche le mouvement rapide avec une r�solution 1/2 pas    */

	while(Num_Pointage > resol)			// Boucle de positionnement
	{
		DELAY = y0 - c*m;			// Delai2 �valu� selon une droite par rapport
		// � la courbe d'acc�l�ration - approximation de
		// A[i] = (i^0.5 - (i - 1)^0.5)A[0]

		CCPR1L = lobyte(DELAY);			// Valeur du d�lai transf�r�e dans le registre comparateur
		CCPR1H = hibyte(DELAY);

		TMR1H = 0;				// Remise � z�ro du TIMER1
		TMR1L = 0;

		CCP1IF = 0;				// Remise � z�ro du drapeau de comparaison

		CLRWDT();

		if(Stop)
		{
			Act_Corr_pointage = 0;
			Num_Pointage = i;
			Num_Pointage += 1;
			Stop = 0;
			en_arret = 1;
			//printf("OK");
		}

		//temp3 = resol;


		//if(Num_Pointage > resol)
		Num_Pointage -= resol;

		step_moteur();

		if((i < Num_Pointage) && (DELAY > DELAY_TARGET))
		{
			if(decel)
				Act_Corr_pointage = 0;
			i += resol;			// Conditions pour changer le d�lai de g�n�ration des pas
			c += 1;
			c += Accel_M;
			if(c>10)
			{
				index += 1;
				c -= 10;
			}
		}
		else
			if(i > Num_Pointage)
			{
				decel = 1;
				i-= resol;
				c -= 1;
				c -= Accel_M;
				if(c<1)
				{
					index -= 1;
					c += 10;
				}
			}

		if(index < 1999)
		{
			m = TABLE_ACC[index] - TABLE_ACC[index+1];// Delai �valu� selon une droite par rapport
			m >>= 5;                          	// � la courbe d'acc�l�ration - approximation de
			y0 = TABLE_ACC[index] + m;		// A[i] = (i^0.5 - (i - 1)^0.5)A[0]
		}

		while(!CCP1IF)				// En attente de la fin du d�lai
		{
			if(Clavier!=(PORTB & 0x0C))
			{
				Stop = 1;		// Ordre d'arr�ter le mouvement
			}
			else
			{
				if( (Clavier!=0x0C) && (en_arret) )
				{
					en_arret = 0;
					decel = 0;
					Num_Pointage = 0xFF000000;
				}
			}
		}

	}

	TMR2IE = 0;
	Act_Corr_pointage = 0;

	CCPR1L = lobyte(DELAY_INIT);			// Valeur du d�lai transf�r�e dans le registre comparateur
	CCPR1H = hibyte(DELAY_INIT);

	//if(Num_Pointage <= tempresol)
	if((Num_Pointage > 0) && (!en_arret))
	{
		resol = 1;

		while(Num_Pointage > 0)
		{
			CLRWDT();
			Num_Pointage -= 1;
			step_moteur();

			TMR1H = 0;				// Remise � z�ro du TIMER1
			TMR1L = 0;

			CCP1IF = 0;				// Remise � z�ro du drapeau de comparaison

			while(!CCP1IF);				// En attente de la fin du d�lai
		}
	}

	Num_Pointage = 0;

	//printf("FIN NUM_POSI = %lu\n\r",NUM_POSI);
}

/****************************************************************
*       Function Name:  Change_Mode_Vitesse                     *
*       Return Value:   void                                    *
*       Parametres:                                             *
*       Description:	Changement de la vitesse		*
****************************************************************/

void
Change_Mode_Vitesse(void)
{
	Change_Mode = 0;

	Dir_M = READ_EEPROM(Dir_Guidage);

	LED_A_Plus = 0;
	LED_A_Moins = 0;

	switch(Mode_Vitesse)
	{
		case	0:	DELAY_M_LSB=   READ_EEPROM(VM_Guidage_LSB);
				DELAY_M =    READ_EEPROM_INT(VM_Guidage);
				DELAY_M_Guide = DELAY_M;
				Niveau_Courant_M =  READ_EEPROM(Courant_Guidage);
				break;
		case	1:	DELAY_M =    READ_EEPROM_INT(VM_Corec_Plus);
				Niveau_Courant_M =  READ_EEPROM(Courant_Guidage);
				LED_A_Plus = 1;
				break;
		case	2:	DELAY_TARGET =    READ_EEPROM_INT(VM_Lent);
				Niveau_Courant_M =  READ_EEPROM(Courant_Lent);
				LED_A_Plus = 1;
				Pointage = 1;
				break;
		case	3:	DELAY_TARGET =    READ_EEPROM_INT(VM_Rapide);
				Niveau_Courant_M =  READ_EEPROM(Courant_Rapide);
				LED_A_Plus = 1;
				Pointage = 1;
				break;
		case	4:	DELAY_M =    READ_EEPROM_INT(VM_Corec_Moins);
				Niveau_Courant_M =  READ_EEPROM(Courant_Guidage);
				LED_A_Moins = 1;
				break;
		case	5:	DELAY_TARGET =    READ_EEPROM_INT(VM_Lent);
				Niveau_Courant_M =  READ_EEPROM(Courant_Lent);
				LED_A_Moins = 1;
				Pointage = 1;
				break;
		case	6:	DELAY_TARGET =    READ_EEPROM_INT(VM_Rapide);
				Niveau_Courant_M =  READ_EEPROM(Courant_Rapide);
				LED_A_Moins = 1;
				Pointage = 1;
				break;
		default:
				break;
	}

	if(Mode_Vitesse > 3)
	{
		ACCU_ADD = 0;

		if(DECMODE || (Mode_Vitesse > 4))
		{
			if(Dir_M)
				Dir_M = 0;
			else
				Dir_M = 1;
		}
	}
	else
		ACCU_ADD = 1;

	Reglage_Courant();
	if(Pointage)
	{
		Guide = 0;
		if(Num_Pointage	== 0)
			Num_Pointage = 0xFF000000;
	}
	else
		Guide = 1;
	if(Park && Mode_Vitesse > 0)
	{
		Park = 1;
		WRITE_EEPROM( ParkMode, 1 );
	}
}

/****************************************************************
*       Function Name:  traite_serial                           *
*       Return Value:   void                                    *
*       Parametres:                                             *
*       Description:    Traitement des commandes du port serie  *
****************************************************************/

void
traite_serial(void)
{
	unsigned char achar,respond,address;

	respond = ACK;

	//printf("%c",Buf_Serial[1]);

	// Attention modification de la structure des valeurs selon que le bit de poids fort
	// de chacun des octets Buf_Serial[2] a Buf_Serial[5] se retrouve dans l'octet
	// Buf_Serial[6] de facon a ce que chaque octet ne puisse depasser en valeur 0x7F

	if (testbit_on(Buf_Serial[6],0)) bit_set(Buf_Serial[5],7);
	if (testbit_on(Buf_Serial[6],1)) bit_set(Buf_Serial[4],7);
	if (testbit_on(Buf_Serial[6],2)) bit_set(Buf_Serial[3],7);
	if (testbit_on(Buf_Serial[6],3)) bit_set(Buf_Serial[2],7);

	switch (Buf_Serial[1])
	{

		case	0x44:	//D 	Correction +
				putch(respond);
				Mode_Vitesse = 1;
				Change_Mode = 1;
				break;

		case	0x45:	//E 	Enregistrer PEC
				putch(respond);
				PEC_START = 1;
				break;

		case 	0x46:  	//F  	Lent -
				putch(respond);
				Mode_Vitesse = 5;
				Change_Mode = 1;
				break;

		case	0x47:   //G  	Lent +
				putch(respond);
				Mode_Vitesse = 2;
				Change_Mode = 1;
				break;

		case	0x4A: 	//J   	Lecture octet dans EEPROM
				putch(respond);
				address = Buf_Serial[2];
				achar=READ_EEPROM(address);
				putch(achar);
				break;

		case	0x4B: 	//K   	Lecture des param�tres en batch
				putch(respond);
				for(address = 0; address < 48; address++)
				{
					achar=READ_EEPROM(address);
					putch(achar);
				}
				break;

        	case	0x4C: 	//L  Ecriture EEPROM
				putch(respond);
				address = VM_Guidage+Buf_Serial[2];
				WRITE_EEPROM( address, Buf_Serial[3] );
				if((address < 11 && address != 2) || (address > 47))
					WRITE_EEPROM( address+1, Buf_Serial[4] );
				switch (address)
				{
					case VM_Acce:		Accel_M=READ_EEPROM(VM_Acce);
								break;
					case Dir_Guidage:	Dir_M=READ_EEPROM(Dir_Guidage);
								break;
					case Sens_Raq_Led:	Inverse_Sens=READ_EEPROM(Sens_Raq_Led);
								break;
					case PEC_ENABLED:	PEC_ACTIVATED=READ_EEPROM(PEC_ENABLED);
								break;
					case Raq_type_can_address:	Raq_type_can=READ_EEPROM(Raq_type_can_address);
									break;
				}
				Change_Mode = 1;
				break;

		case 	0x4D:	//M  MISE A JOUR DU FIRMWARE
				putch(respond);
				while(GIE)
					GIE = 0;
				LED_A_Plus = 1;
				LED_A_Moins = 1;

				#asm
				 ljmp 0x0000
				#endasm
				break;

		case	0x4E:  	//N 	No Park Mode
				putch(respond);
				Park = 0;
				WRITE_EEPROM( ParkMode, 0 );
				break;

		case	0x50:  	//P 	Park Mode
				putch(respond);
				Park = 1;
				WRITE_EEPROM( ParkMode, 1 );
				break;

		case	0x51:  	//Q 	Correction -
				putch(respond);
				Mode_Vitesse = 4;
				Change_Mode = 1;
				break;

		case 	0x52: 	//R  Ecriture Valeur DELAY_M, DELAY_M_LSB et Dir_M
				if(Mode_Vitesse==0)
				{
					putch(respond);
					byte1_atbank1(DELAY_M) = Buf_Serial[2];
					byte0_atbank1(DELAY_M) = Buf_Serial[3];
					DELAY_M_LSB = Buf_Serial[4];

					Buf_Serial[5] = Buf_Serial[5] & 0x01;
					if (Buf_Serial[5] == 1)
						Dir_M=READ_EEPROM(Dir_Guidage);
					else
						Dir_M=!READ_EEPROM(Dir_Guidage);
				}
				else putch(NOACK);
				break;

		case	0x53:  	//S   	Sid�rale - 25600 micro-pas
				putch(respond);
				Mode_Vitesse = 0;
				Change_Mode = 1;
				break;

		case 	0x56: 	//V  Lecture Version
				putch(respond);
				printf(version);
				break;

		case 	0x57:  	//W	Rapide -
				putch(respond);
				Mode_Vitesse = 6;
				Change_Mode = 1;
				break;

		case 	0x58: 	//X  	Rapide +
				putch(respond);
				Mode_Vitesse = 3;
				Change_Mode = 1;
				break;

		case 	0x65: 	//e   Effacer MEM PEC
				putch(respond);
				for(address = 0; address < 200; address++)
					WRITE_EEPROM_INT(PEC_ENABLED + 2 + address, 0);
				break;

		case 	0x66: 	//f   Switch Auxilaire 1
				putch(respond);
				if(AUX_1 == 0)
					AUX_1 = 1;
				else
					AUX_1 = 0;
				break;

		case	0x67: 	//g   Switch Auxilaire 1
				putch(respond);
				if(AUX_2 == 0)
					AUX_2 = 1;
				else
					AUX_2 = 0;
				break;

		case 	0x72: 	//r  Lecture Valeur DELAY_M, DELAY_M_LSB, PEC_BASE, PEC_STEP, PEC_COMP, PEC_STATE et Park
				putch(respond);
				TEMP_PEC_STEP = PEC_STEP;
				putch(byte1_atbank1(DELAY_M));
				putch(byte0_atbank1(DELAY_M));
				putch(DELAY_M_LSB);
				putch(PEC_BASE);
				putch(byte1_atbank1(TEMP_PEC_STEP));
				putch(byte0_atbank1(TEMP_PEC_STEP));
				putch(byte1_atbank1(PEC_COMP));
				putch(byte0_atbank1(PEC_COMP));
				achar = 0;
				if(PEC_START)
					achar = 2;
				else
					if(PEC_WRITE)
						achar = 3;
					else
						if(PEC_ACTIVATED)
							achar = 1;
				putch(achar);
				putch(Park);
				break;

		case	0x70:	//p   POSITIONNEMENT
				putch(respond);
				byte3_atbank1(Num_Pointage) = Buf_Serial[2] & 0x7F;
				byte2_atbank1(Num_Pointage) = Buf_Serial[3];
				byte1_atbank1(Num_Pointage) = Buf_Serial[4];
				byte0_atbank1(Num_Pointage) = Buf_Serial[5];
				if((Buf_Serial[2] & 0x80) == 0x80)
					Mode_Vitesse = 6;
				else
					Mode_Vitesse = 3;
				if(Num_Pointage != 0)
				{
					Change_Mode = 1;
					Act_Corr_pointage = 1;
				}
				break;

		default	:	//respond = NOACK;		// Commande non conforme
				//putch(respond);
				break;
	}

	Buf_Serial[0] = '\0';
	Buf_Serial[1] = 0;
	Command_Rec =0;

}

/****************************************************************
*       Function Name:  serial_receive      	                *
*       Return Value:   void                                    *
*       Parametres:                                             *
*       Description:    Reception du port serie  		*
****************************************************************/

void
serial_receive(void)
{
	unsigned char compteur,cheksum,temp;
	unsigned int timeout_int;

	CLRWDT();

	if(Buf_Serial[0] == Num_Carte)
	{

		Buf_Serial[1]=RCREG;

		switch(Buf_Serial[1])
		{
			case	0xff:	putch(1);	// MCMT ready?
					break;

			case	0xf4:	GIE=0;
					byte0_atbank1(TEMP_NUM_POSI) = byte0_atbank1(NUM_POSI);
					byte1_atbank1(TEMP_NUM_POSI) = byte1_atbank1(NUM_POSI);
					byte2_atbank1(TEMP_NUM_POSI) = byte2_atbank1(NUM_POSI);
					byte3_atbank1(TEMP_NUM_POSI) = byte3_atbank1(NUM_POSI);
					putch(byte3_atbank1(TEMP_NUM_POSI));
					GIE=1;
					break;

			case	0xf3:	putch(byte2_atbank1(TEMP_NUM_POSI));
					break;

			case	0xf2:	putch(byte1_atbank1(TEMP_NUM_POSI));
					break;

			case	0xf1:	putch(byte0_atbank1(TEMP_NUM_POSI));
					break;

			default:	if(!Command_Rec)
					{
						compteur = 6;
						Buf_Count = 1;
						while(compteur>0)
						{
							compteur--;
							Command_Rec = 0;
							timeout_int = 40960;
							while ((hibyte(timeout_int)!=0) && (!Command_Rec)) //only check the msb of the int for being 0, it saves space, see always.h for macro
							{
								CLRWDT();
								timeout_int--;
								if (RCIF)
								{
									Buf_Serial[++Buf_Count] = RCREG;
									Command_Rec = 1;
								}
							}
							if(!Command_Rec)
							{
								compteur = 0;
							}
						}
						if(Command_Rec)
						{
							cheksum = 0;
							for(compteur = 0; compteur < 7; compteur++)
								cheksum += Buf_Serial[compteur];
							bit_clr(cheksum,7);	//cheksum &= 0x7F;	// 128 bits
							if(cheksum != Buf_Serial[7])
							{
								Command_Rec = 0;
							}
						}
					}
					break;
		}

		if(!Command_Rec)
		{
			Buf_Serial[0] = '\0';
			Buf_Serial[1] = 0;
		}
	}
	else
		Buf_Serial[0]=RCREG;

	clear_usart_errors_inline;
}

#include	"raquette.c"

/*************************************************************************************/
/******************************** Programme principal ********************************/
/*************************************************************************************/

main()
{
	unsigned char temp,temp1,temp2;
	unsigned int temp_pec;

	//BRGH = 1;
	//SPBRG = 129;

	ADCON1 = 0x06;                          	// PORTA en mode DIGITAL

	TRISA = 0x00;           			// Pins du PORTA en output
	TRISB = 0xFF;           			// Pins du PORTB en input
	TRISC = 0xD0;           			// PORTC.7 (RX) en input, autres Pins du PORTC en output
	TRISD = 0x00;           			// Pins du PORTD en output
	TRISE = 0x07;           			// PORTE en input
	T0IF = 0;

	/*	Initialise le TIMER 0 PRESCALER CLOCK/1 */

	OPTION = 0b00001111;


	/*	Initialise les param�tres de contr�le des moteurs */

	DELAY_TARGET = 40;
	Park = READ_EEPROM(ParkMode);		// V�rifie le mode Park
	DELAY_M_LSB =  READ_EEPROM(VM_Guidage_LSB);
	DELAY_M =   READ_EEPROM_INT(VM_Guidage);
	DELAY_M_Guide = DELAY_M;
	Dir_M=   READ_EEPROM(Dir_Guidage);
	Old_Dir_M = Dir_M;
	Niveau_Courant_M =  READ_EEPROM(Courant_Guidage);
	Accel_M =   READ_EEPROM(VM_Acce);
	Inverse_Sens =    READ_EEPROM(Sens_Raq_Led);
	PEC_ACTIVATED=READ_EEPROM(PEC_ENABLED);
	Raq_type_can=READ_EEPROM(Raq_type_can_address);

	Mode_Vitesse = 0;

	/*	Initialise les param�tres de contr�le des moteurs */

	PHASE = 0;  						// initialise le MAX505
	WR_N = 1;
	LDAC_N = 1;
	LED_A_Plus = 1;
	LED_A_Moins = 1;

	NUM_STEP_M_A = 0;
	NUM_STEP_M_B = 128;
	DIR_PHASEA = 1;
	Flag_DIR_PHASEA = 1;
	DIR_PHASEB = 1;
	Flag_DIR_PHASEB = 1;
	DIR_PHASE = 0;

	Reglage_Courant();
	step_moteur();

	/*	Initialise les variables relatifs � la comm */

	Control_TX = 0;
	Buf_Serial[0] = '\0';
	Buf_Serial[1] = 0;
	Buf_Count = 0;
	Command_Rec = 0;

	/*	Initialise les variables relatifs au positionnement */

	NUM_POSI = 1073741824;
	if (PEC1)
		Pos_Pec1 = 1;
	else
		Pos_Pec1 = 0;

	Init_PEC = 1;
	Pointage = 0;
	Guide = 1;

	/*	Initialise l'adresse de la carte */

	Num_Carte = PORTE & 0x07;
	Num_Carte += 0xE0;
	if (Num_Carte > 0xE0)
	{
		DECMODE = 1;
	}

	if (Num_Carte == 0xE2)
	{
		FOCUSMODE = 1;
	}

	DelayBigMs( 500 );

	Clavier = PORTB;

	/*		Initialise la COMM � 19200-8-N-1	*/

	BRGH = 1;
	SPBRG = 64;
	SPEN = 1;
	CREN = 1;
	TXEN = 1;

	/*	Initialise le TIMER 2 - PRESCALER CLOCK = 1 et POSCALE OVERFLOW = 1	*/

	T2CON = 0b00000100;

	/*	Initialise le TIMER 1 - PRESCALER CLOCK/8 et configure CCPs	*/

	CCP1CON = 0b00001010;
	CCP2CON = 0b00001010;
	T1CON = 0b00110101;

	//CCP1IE = 1;						// enables CCP1 interrupts
	//RCIE = 1;						// enables USART receive interrupts
	PEIE = 1;						// enables Peripheral interrupts
	GIE = 1;						// enables global un-masked interrupts

	LED_A_Plus = 0;
	LED_A_Moins = 0;

	Clavier = PORTB & 0x0F;

	//printf("REBOOT %u",Num_Carte);

	for(;;)
	{
		CLRWDT();

		if(Clavier!=(PORTB & 0x0F)) 			// Verifie si ST4 ou Raquette modifie.
		{
			temp = PORTB & 0x0F;
			DelayMs(10);
			if(temp == (PORTB & 0x0F)) 		// Test si rebond.
			{
				Clavier=temp;
				Raquette();
			}
		}

		if(RCIF)					// Traite octet recu
			serial_receive();

		if(Command_Rec)					// Traite commande
			traite_serial();

		if(Change_Mode)					// Ordre de changement de vitesse.
			Change_Mode_Vitesse();

		if(Pointage)					// Ordre de pointage.
		{
			CCP1IE = 0;
			RCIE = 1;
			MoveNumOfUSteps();
			RCIE = 0;
			Mode_Vitesse = 0;
			Change_Mode = 1;
			Guide = 1;

			Dir_M = READ_EEPROM(Dir_Guidage);

			if(PEC_ACTIVATED)
			{
				PEC_BASE = 0;
				PEC_COMP = READ_EEPROM_INT(PEC_ENABLED + 2);
				PEC_COMP &= 0x3FFF;

				if(Dir_M)	// Replace le pointeur de correction p�riodique apr�s un pointage
				{
					while( ((PEC_STEP >> 1) > PEC_COMP) && (PEC_BASE < 100) )
					{
						PEC_BASE += 2;
						PEC_COMP = READ_EEPROM_INT(PEC_ENABLED + 2 + (PEC_BASE << 1));
						PEC_COMP &= 0x3FFF;
					}
				}
				else
				{
					while( ((PEC_STEP >> 1) < PEC_COMP) && (PEC_BASE < 100) )
					{
						PEC_BASE += 2;
						PEC_COMP = READ_EEPROM_INT(PEC_ENABLED + 2 + (PEC_BASE << 1));
						PEC_COMP &= 0x3FFF;
					}
				}
				if(PEC_BASE >= 100)
					PEC_ACTIVATED = 0;
			}

			Pointage = 0;
		}

		if((Guide) && (!Park) && (DELAY_M != 0))					// Mode suivi
		{
			CCP1IE = 1;
			if(OKstep)
			{
				if(Init_PEC)				// une seule fois au d�marrage on lit le capteur jusqu'au passage
				{
					if (!Pos_Pec1)			// d�tection d'un front montant sur entree PEC1 pour synchronisation
					{
						if (PEC1)
						{
							Pos_Pec1 = 1;
							PEC_COUNT = 0;
							PEC_STEP = 0;
							Init_PEC = 0;
						}
					}
					else
					{
						if (!PEC1)
							Pos_Pec1=0;
					}
				}

				if(PEC_STEP == 0)
				{
					PEC_BASE = 0;
					PEC_COMP = READ_EEPROM_INT(PEC_ENABLED + 2);
					PEC_COMP &= 0x3FFF;
					if(PEC_WRITE)
					{
						PEC_WRITE = 0;
						PEC_ACTIVATED = 1;
					}
					if(PEC_START)
					{
						PEC_ACTIVATED = 0;
						PEC_START = 0;
						PEC_WRITE = 1;
						Old_Mode_Vitesse = 0;
					}
				}


				if (PEC_ACTIVATED)
				{
					if ((PEC_STEP >> 1) == PEC_COMP)
					{
						PEC_COMP = READ_EEPROM_INT(PEC_ENABLED + 2 + (PEC_BASE << 1));
						if((PORTB & 0x0F) == 0x0F)		//V�rifie que la commande ne vient pas de la raquette ou st4
						{
							if((PEC_COMP & 0x8000) == 0x8000)
								Mode_Vitesse = 1;
							else
								if((PEC_COMP & 0x4000) == 0x4000)
									Mode_Vitesse = 4;
								else
									Mode_Vitesse = 0;
							Change_Mode = 1;
						}
						++PEC_BASE;
						if (PEC_BASE >= 100)
							PEC_BASE = 0;
						PEC_COMP = READ_EEPROM_INT(PEC_ENABLED + 2 + (PEC_BASE << 1));
						PEC_COMP &= 0x3FFF;
					}
				}
				else
				{
					if(PEC_WRITE)
					{
						if(Old_Mode_Vitesse != Mode_Vitesse)
						{
							Old_Mode_Vitesse = Mode_Vitesse;
							temp_pec = PEC_STEP >> 1;
							if(Mode_Vitesse == 1)
								temp_pec |= 0x8000;
							else
								if(Mode_Vitesse == 4)
									temp_pec |= 0x4000;
							WRITE_EEPROM_INT(PEC_ENABLED + 2 + (PEC_BASE << 1), temp_pec);
							++PEC_BASE;
							if (PEC_BASE >= 100)
								PEC_BASE = 0;
						}
						if(LED_A_Plus)
						{
							LED_A_Plus = 0;
							LED_A_Moins = 1;
						}
						else
						{
							LED_A_Plus = 1;
							LED_A_Moins = 0;
						}
					}
				}
				OKstep = 0;
			}
		}
		else
			CCP1IE = 0;
	}
}

/****************************************************************
*       Function Name:  interrupt isr				*
*       Return Value:   none					*
*       Parametres:     none					*
*       Description:    Routine d'interruption 			*
****************************************************************/

static void interrupt
isr(void)
{
	unsigned int temp;

	if(CCP1IF && CCP1IE)				// Mode Guidage
	{
		//dly400n;
		TMR1H = 0;				// Remise � z�ro du TIMER1
		TMR1L = 0;
		temp = DELAY_M-4;
		if(++DELAY_M_LSB_Compteur <= DELAY_M_LSB)
			++temp;

		if(DELAY_M_LSB_Compteur == 10)
			DELAY_M_LSB_Compteur = 0;

		CCPR1L = lobyte(temp);
		CCPR1H = hibyte(temp);
		resol = 1;
		step_moteur();
		CCP1IF = 0;
		OKstep = 1;
	}
	else
	if(RCIF && RCIE) 				// Reception mode pointage
	{
                CLRWDT();

                if(Buf_Serial[0] == Num_Carte)
                {

			Buf_Serial[1]=RCREG;

			switch(Buf_Serial[1])
			{
				case	0xff:	Control_TX = 1; // MCMT ready?
						while(!TXIF)	// Registre de transfert pr�t?
						{
							//Wait
						}
						TXREG = 0;// Envoi Occup�
						TMR0 = 0;
						T0IE = 1;
						break;

				case	0xf4:	Control_TX = 1;
						while(!TXIF)	// Registre de transfert pr�t?
						{
							//Wait
						}
						TEMP_NUM_POSI = NUM_POSI;
						TXREG = byte3_atbank1(TEMP_NUM_POSI);
						TMR0 = 0;
						T0IE = 1;
						// Envoi Num_Posi
						break;

				case	0xf3:	Control_TX = 1;
						while(!TXIF)	// Registre de transfert pr�t?
						{
							//Wait
						}
						TXREG = byte2_atbank1(TEMP_NUM_POSI);
						TMR0 = 0;
						T0IE = 1;
						break;

				case	0xf2:	Control_TX = 1;
						while(!TXIF)	// Registre de transfert pr�t?
						{
							//Wait
						}
						TXREG = byte1_atbank1(TEMP_NUM_POSI);
						TMR0 = 0;
						T0IE = 1;
						break;

				case	0xf1:	Control_TX = 1;
						while(!TXIF)	// Registre de transfert pr�t?
						{
							//Wait
						}
						TXREG = byte0_atbank1(TEMP_NUM_POSI);
						TMR0 = 0;
						T0IE = 1;
						break;

				case	0xf0:	Stop = 1;	// Stop slewing
						Control_TX = 1;
						while(!TXIF)	// Registre de transfert pr�t?
						{
							//Wait
						}
						TXREG = ACK;	// Acknowledge
						TMR0 = 0;
						T0IE = 1;
						break;
			}

			Buf_Serial[0] = '\0';
			Buf_Serial[1] = 0;
		}
		else
			Buf_Serial[0]=RCREG;

		clear_usart_errors_inline;
	}
	else
	if(T0IF)
	{
		if(TRMT)
		{
			Control_TX = 0;
			T0IE = 0;
		}
		T0IF = 0;
	}
	else
	if(TMR2IF && TMR2IE)		// Pour correction du pointage en fonction du temps
	{
		if(++Compt_Flash == 3500)
		{
			Compt_Flash = 0;
			if(!ACCU_ADD)
			{
				if(LED_A_Moins)
					LED_A_Moins = 0;
				else
					LED_A_Moins = 1;
			}
			else
			{
				if(LED_A_Plus)
					LED_A_Plus = 0;
				else
					LED_A_Plus = 1;
			}
		}

		if (Act_Corr_pointage)
		{
			Compt_Correc += 1;

			if(Compt_target == Compt_Correc)
			{
				Compt_Correc = 0;
				if(!ACCU_ADD)
				{
					if(Num_Pointage > resol)
						Num_Pointage -= 1;
				}
				else
				{
					Num_Pointage += 1;

				}
			}
		}

		TMR2IF = 0;
	}
}

